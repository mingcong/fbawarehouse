var Script = function () {

	jQuery('.sidebar .item.vertical > a').click(function () {
        var ver = jQuery(this).next();
        if (ver.is(":visible")) {
            jQuery(this).parent().removeClass("open");
            ver.slideUp(200);
        } else {
            jQuery(this).parent().addClass("open");
            ver.slideDown(200);
        }
    });

	$(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('.main').addClass('sidebar-close');
                $('.sidebar .sidebar-menu').hide();
            }

            if (wSize > 768) {
                $('.main').removeClass('sidebar-close');
                $('.sidebar .sidebar-menu').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.sidebar-toggle').click(function () {
        if ($('.sidebar .sidebar-menu').is(":visible") === true) {
            $('.main-content').css({
                'margin-left': '0px'
            });
            $('.sidebar').css({
                'margin-left': '-220px'
            });
            $('.sidebar .sidebar-menu').hide();
            $(".main").addClass("sidebar-closed");
        } else {
            $('.main-content').css({
                'margin-left': '220px'
            });
            $('.sidebar .sidebar-menu').show();
            $('.sidebar').css({
                'margin-left': '0'
            });
            $(".main").removeClass("sidebar-closed");
        }
    });

}();


//////////////////////////全选按钮
$(function() {
    $("#checkAll").click(
        function() {
            if (this.checked) {
                $("input[name='subBox']").prop('checked', true);
            } else {
                $("input[name='subBox']").prop('checked', false);
            }
        }
    );
});



// 选择时间和日期
/* $(".form-datetime").datetimepicker(
    {
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        format: "yyyy-mm-dd hh:ii"
    });

// 仅选择日期
$(".form-date").datetimepicker(
    {
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        format: "yyyy-mm-dd"
    });

// 选择时间
$(".form-time").datetimepicker({
    language:  "zh-CN",
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    format: 'hh:ii'
});
*/
// 选择时间

$('.form-date').datetimepicker({
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y-m-d',

});
$(".form-datetime").datetimepicker(
    {
        lang:'ch',
        timepicker:true,
        format: "Y-m-d H:i:s",
        formatDate: "Y-m-d H:i:s"
    });
// 选择今天之前的日期灰色不能选
$('.dateafter').datetimepicker({
   lang:"ch",
    timepicker:false,
    minDate:'-1970/01/1',
    //maxDate:'+1970/01/2',
    format:'Y-m-d',
    formatDate:'Y-m-d',
});

// 选择今天之后的日期灰色不能选
$('.datebefore').datetimepicker({
    onGenerate:function( ct ){
        $(this).find('.xdsoft_date')
            .toggleClass('xdsoft_disabled');
    },
    lang:"ch",
    timepicker:false,
    minDate:'-1970/01/1',
    //maxDate:'+1970/01/2',
    format:'Y-m-d',
    formatDate:'Y-m-d',
});

/*
$('#s').datetimepicker({
    lang:"ch",           //语言选择中文
    format:"Y-m-d",      //格式化日期
    timepicker:false,    //关闭时间选项
    yearStart：2000,     //设置最小年份
    yearEnd:2050,        //设置最大年份
    todayButton:false    //关闭选择今天按钮
});

 $.datetimepicker.setLocale('ch');//设置中文

    */