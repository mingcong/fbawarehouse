/**
 * 自封装用库
 * 林洁颖
 * 2014-12-21
 */
console.log('当浮云遮掩了阳光，你必须坚强地走出黑暗！当群星隐没在夜空，你要发现自己的斑斓?');
console.log('我要成为一名伟大的前端设计师！ --林洁颖');
/**
 类的定义
*/
function JYChart(id)
{
	this.jqEffectTime = 300;
	
	this.setObjByIds = function(id)
	{
		this.obj = document.getElementById(id);
		this.ok = true;
		
		if(typeof(this.obj) == 'undefined') {
			console.error('林洁颖说：LJYChatShow初始化错误，不存在id为：'+id+'的元素！');
			this.ok = false;
		}
		
		return this.ok;
	}
	
	//设置模板
	this.setTpl = function(tplString)
	{
		this.tpl = tplString;
	}

	this.setTplById = function(id)
	{
		var obj = document.getElementById(id);

		if(obj && obj.innerHTML) {
			this.tpl = obj.innerHTML;
			if(obj == this.obj) {
				obj.innerHTML = '';
			}
			
		} else {
			this.tpl = '';
		}
	}

	this.replaceString = function(ostr, find, replace)
	{
		var pos		= ostr.indexOf(find);
		var findlen = find.length;
		replace = new String(replace);
		
		while(pos != -1) {
			ostr = ostr.substr(0, pos) + replace + ostr.substr(pos+findlen);
			pos  = ostr.indexOf(find, pos+findlen+replace.length);
		}

		return ostr;
	}

	this.setData = function(data)
	{
		if(typeof(data.length) == 'undefined') {
			console.error('林洁颖说：调用setData方法传入的data数据格式错误,必须为数组！');
			return;
		}
		
		if(0 == data.length) {
			console.warn('林洁颖说：调用setData方法传入的data数据长度为0');
			return;
		}
		
		this.data = data;
	}

	this.setKey = function(key)
	{
		this.key = key;
	}
	
	this.setSortKey = function(sortkey) 
	{
		this.sortkey = sortkey;
	}

	this.setSort = function(sort) 
	{
		if(sort != 'XtoD' && sort != 'DtoX'){
			return;
		}
		
		this.sort = sort;
	}

	this.setEffectTime = function(time)
	{
		if(!time) {
			time = 300;
		}
		
		this.jqEffectTime = time;
	}
	
	this.updateData = function(data)
	{
		if(typeof(data.length) == 'undefined' 
				&& (typeof(data) != 'object'
				&& typeof(data) != 'array')) {
			console.error('林洁颖说：调用updateData方法传入的data数据格式错误为 必须为数组！');
			return;
		}
		
		if(0 == data.length) {
			console.warn('林洁颖说：调用updateData方法传入的data数据长度为0');
			return;
		}
		
		this.data = data;
		this.show();
	}

	this.show = function()
	{
		if(!this.ok) {
			console.error('林洁颖说：未初始化调用show！！');
			return;
		}
		
		var tpl;
		var htmString = '';
		var mainkey = '';
		
		//默认为第为个字为
		if(this.data.length>0){
			for(var key in this.data[0]){
				mainkey = key;
				break;
			}
		}
		
		mainkey = ''==this.key?mainkey:this.key;
		var sortkey = ''==this.sortkey?mainkey:this.sortkey;
		var sons = $(this.obj).find('> div div[JYChart-key='+mainkey+']');
		var htmString = '';
		
		for(var j=0; j<sons.length; j++) {
			sons.eq(j).attr('JYChart-flag-delete', 1);
		}
		
		for(var i=0; i<this.data.length; i++){
			for(var j=i+1; j<this.data.length; j++) {
				var pd;
				//alert(this.sorttype);
				if('XtoD' == this.sort) {
					pd = this.data[i][sortkey] > this.data[j][sortkey];
				} else {
					pd = this.data[i][sortkey] < this.data[j][sortkey];
				}
				
				if(pd) {
					var tmp = this.data[i];
					this.data[i] = this.data[j];
					this.data[j] = tmp;
				}
			}
			
			var exist = false;
			
			for(var j=0; j<sons.length; j++) {
				//alert("mkey="+mainkey+"; sons=" + sons.eq(j).html()+"; data=" + this.data[i][mainkey]);
				
				if(sons.eq(j).html() == this.data[i][mainkey]) {
					exist = true;
					sons.eq(j).attr('JYChart-flag-delete', 0);
					
					for(var tmpkey in this.data[i]) {
						$(sons.eq(j)[0].parentNode).find('div[JYChart-key='+tmpkey+']').html(this.data[i][tmpkey]);
					}
					
					break;
				}
			}
			
			var tpl = '';
			if(!exist) {
				tpl = this.tpl;
				
				for(var tmpkey in this.data[i]) {
					tpl = this.replaceString(tpl,'['+tmpkey+']', this.data[i][tmpkey]);
				}	
			}
			
			htmString += tpl;
		}//遍历

		for(var i=0; i<sons.length; i++) {
			if(sons.eq(i).attr('JYChart-flag-delete') == 1) {
				sons.eq(i)[0].parentNode.remove();
			}
		}
		
		this.obj.innerHTML += htmString;
		var sons = $(this.obj).find('> div div[JYChart-key='+mainkey+']');
		var setbg = false;
		
		for(var i=0; i<this.data.length; i++) {
			for(var j=0; j<sons.length; j++) {
				if(sons.eq(j).html() == this.data[i][mainkey]) {
					sons.eq(j)[0].parentNode.style.position = "relative";
					var height = sons.eq(j)[0].offsetHeight;
					var calc = (i-j)*height;

					var parent = $(sons.eq(j)[0].parentNode);
					parent.find('div[JYChart-AutoInc=true]').html(i+1);
					parent.stop();
					
					/*
					if(0 == i && null !== this.firstBgColor) {
						if(parent.attr('JYChart-isfirst') == 0){
							parent[0].style.backgroundColor = this.firstBgColorMove;
						}
						
						parent.attr('JYChart-isfirst', 1);
						
					} else {
						parent.attr('JYChart-isfirst', 0);
					}
					
					*/	
					var top = parseInt(parent[0].style.top);
					top = isNaN(top)?false:top;

					if(false!==top && calc<top && !setbg) {
						parent[0].style.zIndex = 21;
						//parent.animate({backgroundColor:this.firstBgColorMove});
						parent[0].style.backgroundColor = this.firstBgColorMove;
						setbg = true;
						
					} else {
						parent[0].style.zIndex = 20;
					}
					
					var lastclass = parent.attr('JYChart-lastclass');
					
					if(typeof(this.data[i]['_class']) != 'undefined' && lastclass != this.data[i]['_class'] ) {
						if(lastclass != '') {
							parent.removeClass(parent.attr('JYChart-lastclass'));
							parent.addClass(this.data[i]['_class']);
							parent.attr('JYChart-lastclass', this.data[i]['_class']);
						}
					}
					
					parent.animate({top:calc+"px"}, this.jqEffectTime, (function(elm, colorOver){
						return function(){
							elm.style.backgroundColor = colorOver;
						}
					})(parent[0], this.firstBgColorOver));
				}
			}
		}
		
	} //show

	this.updateFromURL = function(url, callBackFn)
	{
		$.ajax({
			url:url,
			type:'get',
			dataType:'text',
			error:function(){
				console.error('林洁颖说：连接到['+url+']时出错！');
				//alert(callBackFn)
				if(typeof(callBackFn) != 'undefined') {
					callBackFn();
				}
			},
			success:function(data){
				var updateData;
				
				try{
					updateData = eval('('+data+')')
				
				} catch(e) {
					console.error('林洁颖说：['+url+']返回的数据格式错误（应返回JSON）！');
				}
				
				this.updateData(data);
				this.show();
				
				if(typeof(callBackFn) != 'undefined') {
					callBackFn();
				}
			}
		});
	}

	this.autoUpdateFromURL = function(url, time)
	{
		setTimeout((function(ob, url, time){
			return function(){
				ob.updateFromURL(url, function(){
					if(ob.autoUpdate) {
						ob.autoUpdateFromURL(url, time);
					}
				});
			}
		})(this, url, time), time);
	}
	
	this.setFirstBgColor = function(colorMove, colorOver)
	{
		this.firstBgColorMove = colorMove;
		this.firstBgColorOver = colorOver;
	}
	
	//数据初始为
	this.obj = document.getElementById(id);
	this.data = [];

	if(typeof($) == 'undefined' || typeof(jQuery) == 'undefined') { 
		console.error('林洁颖说：LJYChatShow 为 为要依赖jQuery库！');
	} 
	
	this.ok = true;
	
	if(typeof(this.obj) == 'undefined') {
		console.error('林洁颖说：LJYChatShow初始化错误，不存在id为：'+id+'的元素！');
		this.ok = false;
	}
	
	this.sortkey  = '';
	this.sort = 'XtoD';
	this.key = '';
	this.setTplById(id);
	this.firstBgColorMove = null;
	this.firstBgColorOver = null;
	
	if(this.obj.innerHTML != "") {
		this.obj.innerHTML = '';
	}
	
	return this;
	
} //JYChat