<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>
    .enleter {
        width: 300px;
        word-wrap: break-word;
        word-break: break-all;
    }
</style>
<div class="container-fluid">
    <div class="wrapper">
        <div class="panel">
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Inbound/SingleShipmentidGrab/grabShipmentid" method="post">
                    <b style="color: red">应关务组（高玲玲、郑燚）和物流组（陈彦宇）要求，出口退税和非出口退税货物不能一起创建Shipmentid</b>
                    <div class="form-inline">
                        <b>帐号：</b>
                        <select class="chosen-select form-control" tabindex="-1" name="accountId"
                                data-name-group="common">
                            <option value="" class="empty-opt">-- 请选择 --</option>
                            <?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$acc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($acc["id"]); ?>"  <?php if($postData["accountId"] == $acc['id']): ?>selected="selected"<?php endif; ?>><?php echo ($acc["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                        <b>Shipmentid：</b>
                        <input placeholder="输入Shipmentid" name="shipmentid" type="text" value="<?php echo ($postData["shipmentid"]); ?>" class="form-control">
                        <button class="btn btn-primary" type="submit">获取</button>
                    </div>
                </form>
                <hr>
                <?php if(!empty($inboundShipments)): ?><dl class="col-md-1">
                    <dt>Shipment name/ID</dt>
                    <dd><?php echo ($inboundShipments["Name"]); ?></dd>
                    <dd><?php echo ($inboundShipments["ShipmentName"]); ?></dd>
                </dl>
                <dl class="col-md-2">
                    <dt>Ship from</dt>
                    <dd><?php echo ($inboundShipments["ShipFromAddress"]["Name"]); ?></dd>
                    <dd><?php echo ($inboundShipments["ShipFromAddress"]["AddressLine1"]); ?></dd>
                    <?php if($inboundShipments.ShipFromAddress.AddressLine2 != ''): ?><dd><?php echo ($inboundShipments["ShipFromAddress"]["AddressLine2"]); ?></dd><?php endif; ?>
                    <dd><?php echo ($inboundShipments["ShipFromAddress"]["City"]); ?>,<?php echo ($inboundShipments["ShipFromAddress"]["StateOrProvinceCode"]); ?>&nbsp;<?php echo ($inboundShipments["ShipFromAddress"]["PostalCode"]); ?></dd>
                    <dd><?php echo ($inboundShipments["ShipFromAddress"]["CountryCode"]); ?></dd>
                </dl>
                <dl class="col-md-2">
                    <dt>Ship to</dt>
                    <?php if(empty($shipToAddress)): ?>DestinationCenterId：<?php echo ($inboundShipments["DestinationFulfillmentCenterId"]); ?>
                        <textarea id="shipToAddressDec"></textarea><?php endif; ?>
                    <?php if(!empty($shipToAddress)): ?><dd><?php echo ($shipToAddress["Name"]); ?></dd>
                        <dd><?php echo ($shipToAddress["AddressLine1"]); ?></dd>
                        <?php if($shipToAddress["AddressLine2"] != ''): ?><dd><?php echo ($shipToAddress["AddressLine2"]); ?></dd><?php endif; ?>
                        <dd><?php echo ($shipToAddress["City"]); ?>,<?php echo ($shipToAddress["StateOrProvinceCode"]); ?>&nbsp;<?php echo ($shipToAddress["PostalCode"]); ?></dd>
                        <dd><?php echo ($shipToAddress["CountryCode"]); ?>(<?php echo ($shipToAddress["CenterId"]); ?>)</dd><?php endif; ?>
                </dl>

                    <?php if(empty($shipToAddress)): ?><dl class="col-md-3">
                        <img src="/fbawarehouse/Public/images/shipToAddressCopyShow.png" />
                    </dl><?php endif; ?>

                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="140">sellerSku</th>
                        <th>title</th>
                        <th width="130">fnsku</th>
                        <th width="130">asin</th>
                        <th width="40">QuantityShipped</th>
                        <th width="90">sku</th>
                        <th>中文名称</th>
                        <th>是否单独发货</th>
                        <th>需求申请对应</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($shipmentItems)): $i = 0; $__LIST__ = $shipmentItems;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($item["seller_sku"]); ?></td>
                        <td class="enleter"><?php echo ($item["title"]); ?></td>
                        <td><?php echo ($item["fnsku"]); ?></td>
                        <td><?php echo ($item["asin"]); ?></td>
                        <td><?php echo ($item["quantity"]); ?></td>
                        <td><?php echo ($item["sku"]); ?></td>
                        <?php if($item["exception"] != 1): ?><td><?php echo ($item["sku_name"]); ?></td>
                            <td><?php if($item["single_ship"] == 1): ?>是<?php else: ?>否<?php endif; ?></td>
                            <td>
                                <?php if(is_array($item['radios'])): $i = 0; $__LIST__ = $item['radios'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$radio): $mod = ($i % 2 );++$i;?><div class="radio">
                                        <label>
                                            <input type="radio" name="<?php echo ($item["seller_sku"]); ?>" class="needsDetailId"  value="<?php echo ($radio['value']); ?>" <?php if($item["needsCount"] == 1): ?>checked<?php endif; ?> />
                                            <?php echo ($radio['valueStr']); ?>
                                        </label>
                                    </div><?php endforeach; endif; else: echo "" ;endif; ?>
                            </td>
                            <?php else: ?>
                            <td colspan="3" <?php if($item["exception"] == 1): ?>class="text-danger"<?php endif; ?>><b>注意：以下原因导致本次获取失败：<br/><?php echo ($item["exceptionMsg"]); ?></b></td><?php endif; ?>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </table>
                <?php if($shipmentOk == 1): ?><div class="fixed-table-toolbar col-md-offset-6">
                    <input type="text" id="remark" class="form-control" placeholder="输入Shipmentid的运输方式"><br/>
                    <button type="button" class="btn btn-info grabSubmit">提交结果</button>
                    <button type="button" class="btn btn-info grabSubmit pushWarehouseFlag">提交并推送仓库</button>
                </div><?php endif; endif; ?>
                <div class="clearfix clear"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".grabSubmit").click(function() {
        var shipToAddressDec = '';
        if($("#shipToAddressDec").length > 0) {
            shipToAddressDec = $("#shipToAddressDec").val();
            if(shipToAddressDec == '') {
                alert("请输入FBA目的仓地址，参考地址输入框右侧图片");
                return false;
            }
        }

        var eachTrRadioChecked = true;
        var needsDetailIdStr = Array();
        $(".needsDetailId").each(function(){
            var trRadioName = $(this).attr('name');
            var trRadioChecked = $('input:radio[name="'+ trRadioName +'"]:checked').val();
            if(trRadioChecked == null){
                eachTrRadioChecked = false;
                alert("请选择SKU：" + trRadioName + "对应的需求!");
                return false;
            }
            if ($(this).attr("checked")) {
                needsDetailIdStr.push($(this).val());
            }
        });

        if(!eachTrRadioChecked) {
            return;
        }

        if(!$.trim($("#remark").val())) {
            alert("请输入Shipmentid的运输方式，例如：空运或海运，这将会告诉仓库同事是否能拼箱子");
            return false;
        }

        $(this).attr("disabled", true);

        var pushWarehouseFlag = 0;
        if($(this).hasClass("pushWarehouseFlag")) {
            pushWarehouseFlag = 1;
        }

        $.ajax({
            url: "<?php echo U('SingleShipmentidGrab/grabSubmit');?>",
            type: 'post',
            async: true,
            data: {
                'accountId' : $("select[name='accountId']").val(),
                'shipmentid' : $("input[name='shipmentid']").val(),
                'needsDetailIdStr' : needsDetailIdStr,
                'shipToAddressDec' : shipToAddressDec,
                'pushWarehouseFlag' : pushWarehouseFlag,
                'remark' : $("#remark").val()
            },
            dataType: 'json',
            success: function (data) {
                layer.closeAll('loading');
                if(data.status==0) {
                    layer.msg(data.info);
                    return;
                }
                layer.msg(data.msg);
                window.location.href= '/fbawarehouse/index.php/Inbound/SingleShipmentidGrab/index';
            }
        });
    });
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>