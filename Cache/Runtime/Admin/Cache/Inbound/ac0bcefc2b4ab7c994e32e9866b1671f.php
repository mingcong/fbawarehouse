<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>.chosen-container-single{ width: 100%;}</style>
<div class="container-fluid">
    <div class="wrapper">
             <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">平台SKU列表</h3></div>
            <div class="panel-body">
                <form action="" id="search-form" class="search-form">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input class="form-control " placeholder="公司内部sku,SellerSku或FNsku" name="sku"
                                       data-name-group="" type="text" value="<?php echo ($_GET['sku']); ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">ASIN</span>
                                <input class="form-control " placeholder="ASIN" name="asin"
                                       data-name-group="" type="text" value="<?php echo ($_GET['asin']); ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">账号</span>
                                <select class="chosen-select form-control" tabindex="-1" name="accountId"
                                        data-name-group="common">
                                    <option value="" class="empty-opt">-- 请选择 --</option>
                                    <?php if(is_array($accounts)): $k = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($k % 2 );++$k;?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">站点</span>
                                <select class="chosen-select form-control" tabindex="-1" name="siteId"
                                        data-name-group="common">
                                    <option value="" class="empty-opt">-- 请选择 --</option>
                                    <?php if(is_array($site)): $i = 0; $__LIST__ = $site;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>" ><?php echo ($v); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>

                                </select>
                            </div>
                        </div>


                        <div class="col-sm-12 col-md-6  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">新增时间</span>
                                <input class="form-control form-datetime" name="beforetime" type="text" value="<?php echo ($_GET['beforetime']); ?>">
                                <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                <input class="form-control form-datetime" name="aftertime" type="text" value="<?php echo ($_GET['aftertime']); ?>">
                            </div>
                        </div>


                        <div class="col-md-2  checkbox">
                            <label>
                                <input type="checkbox" name="title" value="1">筛选空白TITLE
                            </label>
                        </div>
                            <div class="col-md-1 add-space">
                                <input class="btn btn-primary jsSearchBtn" type="submit" value="搜索">
                            </div>


                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <div class="" style="width: 40%;float: left">

                    <button type="button" class="btn" data-toggle="modal" data-target="#btn_import">批量导入新SKU
                    </button>
                    <?php if($is_up == 200): ?><button type="button" class="btn" data-toggle="modal" data-target="#btn_import_status">批量导入SKU销售状态</button><?php endif; ?>
                    <button type="button" class="btn" data-toggle="modal" data-target="#btn_add">添加新的SKU
                    </button>
                    <button class="btn btn-danger" onclick="delSku()">废除</button>
                    <button class="btn btn-success" onclick="getTitle()">拉取帐号数据</button>
                    <button class="btn btn-warning" onclick="download()">导出需要更新的sku</button>
                    <button class="btn btn-primary" onclick="downloadAllSku()">下载所有SKU</button>

                </div>
                <div class="b10" style="color: red;width: 60%;float: right">
                    <b>注意1：</b>每天开发的新的Listing需要先到亚马逊后台做一下FBA备货(WORKING状态,无需上传装箱数据),然后取消；<br>
                    <b>注意2：</b>每天开发的新的Listing务必到当前系统该页面录入(添加新的SKU)<br>
                    <b>注意3：</b>一定要按照标题来导入数据，发现有同事列弄错顺序导致需求创建失败<br>
                    <b>注意4：</b>表格的列中不能含有公式，只能是文本的内容，前车之鉴<br>
                    <b>注意5：</b>鼠标选中表格最后一行后的空白行，按住Ctrl + Shift + 向下箭头，然后删除选中的空白行<br>
                    <b>注意6：</b>鼠标选中表格最后一列（title）后的空白列，按住Ctrl + Shift + 向右箭头，然后删除选中的空白列；<br>
                </div>
                <div class="clearfix"></div>
                <table id="example" class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20"><input id="checkAll" type="checkbox"></th>
                        <th width="30">序号</th>
                        <th>账号</th>
                        <th>站点</th>
                        <th>SellerSKU</th>
                        <th>FNsku</th>
                        <th>公司内部SKU</th>
                        <th>ASIN</th>
                        <th>title</th>
                        <th>首次FBA入库时间</th>
                        <th>是否可用</th>
                        <th>销售状态</th>
                        <th width="180">日志</th>
                        <th width="80">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($data)): $k = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($k % 2 );++$k;?><tr>
                            <?php $use = $v.is_used ?>
                            <td class="text-center"><input name="subBox" type="checkbox" value="<?php echo ($v["id"]); ?>"></td>
                            <td><?php echo ($k); ?></td>
                            <td><?php echo ($v["account_id"]); ?></td>
                            <td><?php echo ($v["site_id"]); ?></td>
                            <td><?php echo ($v["seller_sku"]); ?></td>
                            <td><?php echo ($v["fnsku"]); ?></td>
                            <td><?php echo ($v["private_sku"]); ?></td>
                            <td><?php echo ($v["asin"]); ?></td>
                            <td><?php echo ($v["title"]); ?></td>
                            <td><?php echo ($v["first_received_date"]); ?></td>
                            <td><?php if($v["is_used"] != 1): ?>否<?php else: ?>是<?php endif; ?></td>
                            <td><?php echo ($v["statusName"]); ?></td>
                            <td>增加人 : <?php echo ($v["add_user_id"]); ?><br>
                                创建 : <?php echo ($v["add_time"]); ?><br>修改 : <?php echo ($v["update_time"]); ?></td>


                            <td>
                                <div class="dropdown">
                                    <button class="btn" type="button" data-toggle="modal" value="<?php echo ($v["id"]); ?>" data-target="#btn_edit">修改</button>
                                </div>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    </tbody>
                </table>


                <div class="clearfix clear"></div>
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <label>每页 20 条记录
                            显示 1 到 20 项，共 <?php echo ($count); ?>项</label>
                    </div>
                    <div class="col-md-6">
                        <div class="page pull-right" style="padding: 0;margin: 0">
                            <?php echo ($page); ?>
                        </div>
                    </div>
                </div>
                <!--<div class="yema">页码：<?php echo ($page); ?>/<?php echo ($pageSize); ?>，总记录：<span><?php echo ($total); ?></span>-->
                    <!--<div class="page pull-right"><?php echo ($Pager); ?></div>-->
                <!--</div>-->
            </div>
        </div>


    </div><!---wrapper end--->

</div>


<!---model 弹出页面----->


<!--导入文件 -->
<div class="modal fade" id="btn_import">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件<span style="color: red">（导入请使用xlsx类型的）</span></h4>
            </div>
            <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/upload_sku" method="post" enctype="multipart/form-data" id="upSku">
            <div class="modal-body form-horizontal">
                <div class="alert alert-info top20" role="alert">
                    <p>模板下载: <a href="javascript:void(0)" id="download" >模板</a></p></div>

                <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                    <div class="col-md-4"><input name="file" class="form-control" data-val="true"
                                                 data-val-required="请选择[上传文件]!" id="File" type="file" value="">
                    </div>
                </div>

                <div class="form-group">
                    <button id="submit-button" type="submit" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                    </button>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>

<!--导入SKU销售状态文件 -->
<div class="modal fade" id="btn_import_status">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件<span style="color: red">（导入请使用xlsx类型的）</span></h4>
            </div>
            <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/upload_sku?upStatus=1" method="post" enctype="multipart/form-data" id="upSkuStatus">
                <div class="modal-body form-horizontal">
                    <div class="alert alert-info top20" role="alert">
                        <p>模板下载: <a href="javascript:void(0)" id="status_download" >模板</a></p></div>

                    <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                        <div class="col-md-4"><input name="file" class="form-control" data-val="true"
                                                     data-val-required="请选择[上传文件]!" id="status_file" type="file" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <button id="status_submit-button" type="submit" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>


<!--新增sku -->

<div class="modal fade" id="btn_add">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/addSku" method="post" id="addSku" role="form">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增</h4>
            </div>
            <div class="modal-body">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">公司内部SKU <span style="color: red">*</span></label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="privateSku" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">SellerSKU <span style="color: red">*</span></label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="sellerSku" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">FNsku <span style="color: red">*</span></label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="FNsku" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">ASIN <span style="color: red">*</span></label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="asin" value="" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">账号 <span style="color: red">*</span></label>

                                    <div class="col-md-8">
                                        <select class="chosen-select form-control" tabindex="-1" name="accountId" id="select">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <?php if(is_array($accounts)): $k = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($k % 2 );++$k;?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">站点 <span style="color: red">*</span></label>

                                    <div class="col-md-8">
                                        <select class="form-control chosen-select" name="siteId"  tabindex="-1"  id="addSite">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <?php if(is_array($site)): foreach($site as $key=>$v): ?><option value="<?php echo ($key); ?>"><?php echo ($v); ?></option><?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">title <span style="color: red">*</span></label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="title" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><small>首次FBA入库时间</small></label>

                                    <div class="col-md-8">
                                        <input class="form-control form-datetime" type="text"  name="firsttime" value="" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">是否可用</label>

                                    <div class="col-md-8">
                                        <input name="is_used" value="1" type="radio" checked>
                                        可用
                                        <input name="is_used" value="0" type="radio">
                                        不可用
                                    </div>
                                </div>

                            </div>


                        </div>


                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary add-btn">保存</button>
            </div>

        </div>
        </form>
    </div>
</div>

<!--修改-->
<div class="modal fade" id="btn_edit">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/updateSku" method="post" role="form" id="editSku">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only">关闭</span></button>
                    <h4 class="modal-title">修改</h4>
                </div>
                <div class="modal-body">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">公司内部SKU</label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="privateSku" id="privateSku" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">SellerSKU</label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="sellerSku" id="sellerSku" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">FNsku</label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="FNsku" id="FNsku" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">ASIN</label>

                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="asin" id="asin" value="" placeholder="">
                                    </div>
                                </div>
                                <?php if($is_up == 200): ?><div class="form-group">
                                        <label class="col-md-5 control-label">销售状态</label>

                                        <div class="col-md-7">
                                            <select class="form-control" id="sale_status_id" name="sale_status_id">
                                                <option value="" class="empty-opt">-- 请选择 --</option>
                                                <?php if(is_array($saleStatus)): $k = 0; $__LIST__ = $saleStatus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($k % 2 );++$k;?><option value="<?php echo ($v["number"]); ?>" ><?php echo ($v["value"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>

                                            </select>
                                        </div>
                                    </div><?php endif; ?>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">账号</label>

                                    <div class="col-md-8">
                                        <select class="form-control" id="accountId" name="accountId">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <?php if(is_array($accounts)): $k = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($k % 2 );++$k;?><option value="<?php echo ($v["id"]); ?>" ><?php echo ($v["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">站点</label>

                                    <div class="col-md-8">
                                        <select class="form-control" id="siteId" name="siteId">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <?php if(is_array($site)): $i = 0; $__LIST__ = $site;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($v); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">title</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="title" id="title" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><small>首次FBA入库时间</small></label>

                                    <div class="col-md-8">
                                        <input class="form-control form-datetime" type="text"  name="firsttime" id="first" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="col-md-4 control-label">是否可用</label>

                                    <div class="col-md-8">
                                        <input name="is_used" value="1" type="radio" checked>
                                        可用
                                        <input name="is_used" value="0" type="radio">
                                        不可用
                                    </div>
                                </div>

                            </div>


                        </div>


                    </div>


                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id" value="">;
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="edit-btn">保存</button>
                </div>

            </div>
        </form>
    </div>
</div>

<!-------------------------当前页面js引用处--------------------------------->
<?php include 'common/bottom.html' ?>
<script type="text/javascript" src="/fbawarehouse/Public/javascripts/admin/layer/layer.js"></script>
<script>
    $(function () {
        $("select[name='accountId']").val("<?php echo $_GET['accountId'];?>");
        $("select[name='siteId']").val("<?php echo $_GET['siteId'];?>");
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });
        $('#select_chosen').css("width",'150px');
        $('#addSite_chosen').css("width",'150px');
        //修改sku
        $('#edit-btn').click(function(){
            $.ajax({

                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/updateSku",
                type: 'post',
                async: true, //default: true
                data: $('#editSku').serialize(),
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data, textStatus, jqXHR) {
                    if(data==1){
                        layer.msg('修改成功', {icon: 6});
                        window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/show_sku_info';
                    }else{
                        layer.msg(data, {icon: 5});
                    }
                }
            })
        });
        //新增sku
        $('.add-btn').click(function(){
            $.ajax({
                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/addSku",
                type: 'post',
                async: true, //default: true
                data: $('#addSku').serialize(),
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data, textStatus, jqXHR) {
                    if(data==1){
                        layer.msg('新增成功', {icon: 6});
                        window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/show_sku_info';
                    }else{
                        layer.msg(data, {icon: 5});
                    }
                }
            })
        });
        $('.dropdown').click(function(){
            var arr = [];
            $(this).parent('td').parent('tr').find('td').each(function (i) {
                arr[i] = $.trim($(this).text());
            });
          //  var a = "option[name='" + arr[2] + "']";
//            $("#accountId option").text().attr("selected","selected");
            $("#accountId option").each(function(){
                if($(this).text()==arr[2]){
                    $(this).attr("selected","selected")
                }
            })
            $("#siteId option").each(function(){
                if($(this).text().substring(0,2)==arr[3]){
                    $(this).attr("selected","selected")
                }
            })
            $("#sale_status_id option").each(function(){
                if($(this).text() == arr[11]) {
                    $(this).attr("selected","selected")
                }
            });
            var id = $(this).children().val();
            $('#sellerSku').val(arr[4]);
            $('#FNsku').val(arr[5]);
            $('#privateSku').val(arr[6]);
            $('#asin').val(arr[7]);
            $('#title').val(arr[8]);
            $('#first').val(arr[9]);
            $('#id').val(id);
        })
    });
    //废除
    function delSku() {
        var id = [];

        $('input[name="subBox"]:checked').each(function(){
            id.push($(this).val());
        });
        if(id==''){
            layer.msg('请选择要废除的数据', {icon: 5});
            return false;
        }
        if(confirm('你确定要废除吗？')==true) {
            $.ajax({
                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/delSku",
                type: 'post',
                async: true, //default: true
                data: {'id':id},
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data, textStatus, jqXHR) {
                    //alert(data)
                    if (data == true) {
                        layer.msg('成功', {icon: 6});
                        window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/show_sku_info';
                    } else {
                        layer.msg('失败', {icon: 5});
                    }
                }
            });
        }
    }
    $("#download").click(function () {
        var fileName="sku导入模板";
        var list=new Array(["帐号名称"],["SellerSku"],["FNSKU"],["公司内部SKU"],["ASIN"],["title"]);
        var list1= new Array(["Bpro-ES"],["AF_AM28284"],["X000FNDOCL"],["AM28284"],["B017APM04Q"],["title"]);
        window.location.href ="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/skuDownload?"+"&fileName="+fileName+"&list="+list+"&list1="+list1;
    });
    $("#status_download").click(function () {
        var fileName="sku状态导入模板";
        var list=new Array(["帐号名称"],["SellerSku"],["FNSKU"],["公司内部SKU"],["ASIN"],["销售状态"]);
        var list1= new Array(["Bpro-ES"],["AF_AM28284"],["X000FNDOCL"],["AM28284"],["B017APM04Q"],["正常在售"]);
        window.location.href ="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/skuDownload?"+"&fileName="+fileName+"&list="+list+"&list1="+list1;
    });
    function getTitle(){
        var accountId = $('select[name="accountId"]').val();
        if(accountId ==''){
            layer.msg('请选择帐号', {icon: 5});
        }else{
            //window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/getTitle?accountId='+accountId;
            layer.load();
            $.ajax({
                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/getTitle",
                type: 'post',
                async: true, //default: true
                data: {'accountId':accountId},
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data, textStatus, jqXHR) {
                    layer.closeAll('loading');
                    alert(data);
                    //layer.msg(data, {icon: 5});
                    parent.location.reload();
                }
            });
            //window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/getTitle?accountId='+accountId;
        }
    }
    function download(){
        var accountId = $('select[name="accountId"]').val();
        if(accountId ==''){
            layer.msg('请选择帐号', {icon: 5});
        }else{
            window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/skuDownload?accountId='+accountId;
        }
    }

    function downloadAllSku() {
        window.location.href='/fbawarehouse/index.php/Inbound/Inboundshipmentplan/downloadAllSku';
    }
</script>

            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>