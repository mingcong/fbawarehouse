<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">物流计划单列表</h3></div>
            <div class="panel-body">
                <form action="" method="GET">
                    <div class="container-fluid">

                        <div class="col-sm-12 col-md-4  add-space">
                            <div class="input-group input-group-md">
                                <select name="codetype" class="form-control" style="width:40%" >
                                    <option value="transport_plan_code"
                                    <?php if(isset($_GET['codetype'])&&$_GET['codetype']=='transport_plan_code'){ echo ' selected';}?>
                                    >物流计划单号</option>
                                    <option value="contract_no"
                                    <?php if(isset($_GET['codetype'])&&$_GET['codetype']=='contract_no'){ echo ' selected';}?>
                                    >合同号</option>
                                    <option value="invoice_code"
                                    <?php if(isset($_GET['codetype'])&&$_GET['codetype']=='invoice_code'){ echo ' selected';}?>
                                    >发票号</option>
                                </select>
                                <input type="text" name="transcode" class="form-control" style="width:60%"
                                       value="<?php if(isset($_GET['transcode'])){ echo $_GET['transcode'];}?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">承运商服务</span>
                                <select class="chosen-select form-control" tabindex="-1"
                                        data-name-group="common" name='carrier_service' id='carrier_service'>
                                    <option value=""
                                    <?php if(isset($_GET['carrier_service'])&&$_GET['carrier_service']==''){ echo ' selected';}?>>-- 请选择 --</option>
                                    <?php if(is_array($carrier_service)): $i = 0; $__LIST__ = $carrier_service;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($i); ?>"
                                        <?php if(isset($_GET['carrier_service'])&&$_GET['carrier_service']==$i){ echo ' selected';}?>
                                        ><?php echo ($vo); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">状态</span>
                                <select name='status' id='statuslist' class="form-control">
                                    <option value="all"
                                    <?php if(isset($_GET['status'])&&$_GET['status']=='all'){ echo ' selected';}?>>--请选择--</option>
                                    <option value="50"
                                    <?php if(isset($_GET['status'])&&$_GET['status']=='50'){ echo ' selected';}?>>待发货</option>
                                    <option value="60"
                                    <?php if(isset($_GET['status'])&&$_GET['status']=='60'){ echo ' selected';}?>>已发货</option>
                                    <option value="70"
                                    <?php if(isset($_GET['status'])&&$_GET['status']=='70'){ echo ' selected';}?>>已到货</option>
                                    <option value="100"
                                    <?php if(isset($_GET['status'])&&$_GET['status']=='100'){ echo ' selected';}?>>已取消</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">创建人</span>
                                <input type="text" name="creater" class="form-control"
                                       value="<?php if(isset($_GET['creater'])){ echo $_GET['creater'];}?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-10  add-space">
                            <div class="input-group">
                                <span class="input-group-addon">日期选择</span>
                                <select name="datetype" class="form-control">
                                    <!-- <option value="" class="empty-opt">--请选择--</option> -->
                                    <option value="expected_pickup_time"
                                    <?php if(isset($_GET['datetype'])&&$_GET['datetype']=='expected_pickup_time'){ echo ' selected';}?>
                                    >预计提货时间</option>
                                    <option value="pickup_op_time"
                                    <?php if(isset($_GET['datetype'])&&$_GET['datetype']=='pickup_op_time'){ echo ' selected';}?>
                                    >发货时间</option>
                                    <option value="create_time"
                                    <?php if(isset($_GET['datetype'])&&$_GET['datetype']=='create_time'){ echo ' selected';}?>
                                    >物流计划创建时间</option>
                                </select>
                                <span class="input-group-addon fix-border fix-padding"></span>
                                <input class="form-control form-date" type="text" name="start_time"
                                       value="<?php if(isset($_GET['start_time'])){ echo $_GET['start_time'];}?>"/>
                                <span class="input-group-addon fix-border fix-padding"><span
                                        class="icon-calendar"></span></span>
                                <input class="form-control form-date" type="text" name="end_time"
                                       value="<?php if(isset($_GET['end_time'])){ echo $_GET['end_time'];}?>"/>
                            </div>
                        </div>
                        <div class="col-md-3 add-space">
                            <input type="submit" value="查询" class="btn btn-primary">
                            <input type="submit" name="exportall" id="exportall" value="导出全部" class="btn btn-info">
                            <input type="submit" name="downall" id="downall" value="下载FBA物流计划单" class="btn btn-info">
                        </div>

                    </div>

                </form>
                <div class="clearfix"></div>
                <hr>
                <form action="" method="post" name="trans_list" id="trans_list">

                    <div class="fixed-table-toolbar b10">

                        <div class="fixed-table-toolbar b10">

                            <button type="button" name="addTransPlan" id="addTransPlan" class="btn">添加物流计划单</button>
                            <input type="hidden" name="transplans" id="transplans" value="">
                            <input type="submit" name="checked" id="export" value="导出选中" class="btn">
                            <!-- <input type="submit" name="checked" id="markship" value="标记发货" class="btn"> -->

                        </div>
                    </div>
                    <div style="min-width:1000px;max-width:100%;overflow-x:scroll">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>
                                <input id="checkAll" type="checkbox"/>
                                <!-- <button onclick="chkall(document.trans_list,'all');">全选</button>
                                /
                                <button href="#bottom" onclick="chkall(document.trans_list,'none');">取消</button> -->
                            </th>
                            <th>序号</th>
                            <th>状态</th>
                            <th>物流计划单号</th>
                            <th>合同号</th>
                            <th>发票号</th>
                            <th>入仓号/柜号</th>
                            <th>承运商服务</th>
                            <th>创建人</th>
                            <th><a style="cursor:pointer;">创建时间</a></th>
                            <th>司机姓名（电话）</th>
                            <th>车牌号</th>
                            <th>提单号/快递单号</th>
                            <th>封条号</th>
                            <th>预计提货时间</th>
                            <th>发货人</th>
                            <th><a style="cursor:pointer;">发货时间</a></th>
                            <th>到港时间</th>
                            <th>起飞时间</th>
                            <th>到目的地时间</th>
                            <th>清关时间</th>
                            <th>派送时间</th>
                            <th>包裹信息</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="pageData">
                        <?php if(is_array($transresult)): $i = 0; $__LIST__ = $transresult;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                                <td><input type="checkbox" name="check" value="<?php echo ($vo["id"]); ?>"></td>
                                <td><?php echo ($i); ?></td>
                                <td><?php echo ($vo["status"]); ?></td>
                                <td><?php echo ($vo["transport_plan_code"]); ?></td>
                                <td><?php echo ($vo["contract_no"]); ?></td>
                                <td><?php echo ($vo["invoice_code"]); ?></td>
                                <td><?php echo ($vo["certificate_code"]); ?></td>
                                <td><?php echo ($vo["carrier_service_id"]); ?></td>
                                <td><?php echo ($vo["create_user_id"]); ?></td>
                                <td><?php echo ($vo["create_time"]); ?></td>
                                <td><?php echo ($vo["ori_driver_name"]); ?><br>(<?php echo ($vo["ori_driver_tel"]); ?>)</td>
                                <td><?php echo ($vo["ori_license_plate_num"]); ?></td>
                                <td><?php echo ($vo["ori_tracking_num"]); ?></td>
                                <td><?php echo ($vo["certificate_code"]); ?></td>
                                <td><?php echo ($vo["expected_pickup_time"]); ?></td>
                                <td><?php echo ($vo["pickup_op_user_id"]); ?></td>
                                <td><?php echo ($vo["pickup_op_time"]); ?></td>
                                <td><?php echo ($vo["arrive_port_date"]); ?></td>
                                <td><?php echo ($vo["take_off_date"]); ?></td>
                                <td><?php echo ($vo["arrive_time"]); ?></td>
                                <td><?php echo ($vo["customs_clearance_date"]); ?></td>
                                <td><?php echo ($vo["delivery_date"]); ?></td>
                                <td onclick="chk_tr_display('chk_tr_<?php echo ($i); ?>','act_<?php echo ($i); ?>')" id="pkg_<?php echo ($i); ?>" title="点击展开"
                                width="6%">
                                    包裹数:<?php echo ($vo["sumboxs"]); ?><br>
                                    总重量:<?php echo ($vo["sumweight"]); ?><br>
                                    总体积:<?php echo ($vo["sumvol"]); ?>
                                </td>
                                <td>
                                    <button style="width:60px;" onclick="chk_tr_display('chk_tr_<?php echo ($i); ?>','act_<?php echo ($i); ?>')" 
                                    id="act_<?php echo ($i); ?>" type="button"
                                            class="btn btn-xs btn-info" value="<?php echo ($vo["id"]); ?>">展开
                                    </button><br>
                                    <?php if($vo["status"] == '待发货'): ?><button style="width:60px;" name="markship" type="button"
                                                class="btn btn-xs btn-info" value="<?php echo ($vo["id"]); ?>">标记发货
                                        </button><br>
                                    <?php else: endif; ?>
                                    <button style="width:60px; margin-bottom:4px;" name="editTransPlan" class="btn 
                                    btn-xs btn-info"
                                            type="button" value="<?php echo ($vo["id"]); ?>">物流追踪
                                    </button><br>
                                    <button style="width:60px; margin-bottom:4px;" name="cancelTransPlan" class="btn 
                                    btn-xs btn-info"
                                            type="button" value="<?php echo ($vo["id"]); ?>">取消
                                    </button>
                                </td>
                            </tr>
                            <tr style="display:none;" id="chk_tr_<?php echo ($i); ?>">
                                <td></td>
                                <td colspan="16">
                                    <table>
                                        <tr>
                                            <td>序号</td>
                                            <td>Shipmentid</td>
                                            <!-- <td>SKU</td>
                                            <td>虚拟SKU</td>
                                            <td>产品名称</td>
                                            <td>储位</td>
                                            <td>数量</td>
                                            <td>是否退税</td>
                                            <td>销售员</td>
                                            <td>采购员</td> -->
                                            <th>状态</th>
                                            <th>所属需求批次</th>
                                            <th>账号名</th>
                                            <th>站点</th>
                                            <!--<th>承运商服务</th>-->
                                            <th>中转仓库名</th>
                                            <th>亚马逊目的仓地址</th>
                                            <th>要求到货时间</th>
                                            <th>包裹信息</th>
                                            <td>操作</td>
                                        </tr>
                                        <?php if(is_array($vo['detail'])&&count($vo['detail'])>0){ ?>
                                        <?php if(is_array($vo["detail"])): $k = 0; $__LIST__ = $vo["detail"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub): $mod = ($k % 2 );++$k;?><tr>
                                                <td><?php echo ($k); ?></td>
                                                <td>
                                                    <a href="<?php echo U('Inbound/Inboundshipmentplan/index');?>?shipmentid=<?php echo ($sub["shipmentid"]); ?>"><?php echo ($sub["shipmentid"]); ?></a>
                                                </td>
                                                <td><?php echo ($sub["value"]); ?></td>
                                                <td><?php echo ($sub["batch_code"]); ?></td>
                                                <td><?php echo ($sub["account_name"]); ?></td>
                                                <td><?php echo ($sub["shorthand_code"]); ?></td>
                                                <td><?php echo ($sub["name"]); ?></td>
                                                <td><?php if(isset($shiptoaddresses[$sub["ship_to_address_id"]])){ echo ($shiptoaddresses[$sub["ship_to_address_id"]]); } ?></td>
                                                <td><?php echo ($sub["claim_arrive_time"]); ?></td>
                                                <td>
                                                    包裹数:<?php echo ($sub["total_package"]); ?><br>
                                                    重量:<?php echo ($sub["total_weight"]); ?><br>
                                                    体积:<?php echo ($sub["total_capacity"]); ?>
                                                </td>
                                                <td>
                                                    <?php if($vo["status"] == '待发货'): ?><button style="width:60px;" name="deleteplat" type="button"
                                                                class="btn btn-xs" value="<?php echo ($sub["shipmentid"]); ?>">移除
                                                        </button>
                                                        <?php else: endif; ?>
                                                </td>
                                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                                        <?php } ?>
                                    </table>
                                </td>
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                        </tbody>


                    </table>
                </div>
                </form>

                <div class="yema">页码：<?php echo ($page); ?>/<?php echo ($pageSize); ?>，总记录：<span><?php echo ($total); ?></span><div class="page 
                pull-right"><?php echo ($Pager); ?></div></div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/fbawarehouse/Public/javascripts/admin/layer/layer.js"></script>
<script>
    $(document).ready(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").attr("checked") == "checked") {

                $("input[name='check']").attr("checked", "checked");
            } else {

                $("input[name='check']").removeAttr("checked", "checked");
            }

        });
        var checked = [];
        $("input[name='checked']").click(function () {
            $("input[name='check']:checked").each(function () {
                checked.push($(this).val());
            });
            $("#transplans").val(checked);
            //alert(checked);
            //return false;
        });
        /**
         * 添加
         */
        $('button[name="addTransPlan"]').on('click', function () {
            var url = "<?php echo U('Transportplan/createTransPlan');?>";
            /*layer.open({
             type: 2,
             title: '添加渠道优先级',
             shadeClose: false,
             shade: 0.8,
             area: ['800px', '72%'],
             content: url
             })*/
            window.location.href = url;
        })
        /**
         * 修改
         */
        $('button[name="editTransPlan"]').on('click', function () {
            var id = $(this).val();
            var url = "<?php echo U('Transportplan/editTransPlan');?>?id=" + id
            layer.open({
                type: 2,
                title: '物流追踪',
                shadeClose: false,
                shade: 0.8,
                area: ['50%', '70%'],
                content: url
            });
        })
        /**
         * 标记发货
         */
        $('button[name="markship"]').on('click', function () {
            var id = $(this).val();
            var url = "<?php echo U('Transportplan/markship');?>?id=" + id
            layer.open({
                type: 2,
                title: '标记发货',
                shadeClose: false,
                shade: 0.8,
                area: ['50%', '55%'],
                content: url
            });
        })
        /**
         * 取消物流计划单
         */
        $('button[name="cancelTransPlan"]').on('click', function () {
            var id = $(this).val();
            layer.confirm("确定要取消物流计划单?",
                    {btn: ['确定', '取消']},
                    function () {
                        $.ajax({
                            url: "<?php echo U('Transportplan/cancelTransPlan');?>",
                            type: 'post',
                            async: true, //default: true
                            data: {'id': id},
                            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                            success: function (data, textStatus, jqXHR) {
                                if (data == "取消成功") {
                                    alert("物流计划单(" + id + ")已取消.");
                                    location.reload()
                                }
                            }
                        })  //ajax end
                    }
            )   //layer end
        })
        /**
         * 从物流计划单中移除单个平台计划单
         */
        $('button[name="deleteplat"]').on('click', function () {
            var id = $(this).val();
            layer.confirm("确定要移除平台计划单(" + id + ")?",
                    {btn: ['确定', '取消']},
                    function () {
                        $.ajax({
                            url: "<?php echo U('Transportplan/delplat');?>",
                            type: 'post',
                            async: true, //default: true
                            data: {'id': id},
                            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                            success: function (data, textStatus, jqXHR) {
                                if (data == "移除成功") {
                                    alert("平台计划单(" + id + ")已移除.");
                                    location.reload()
                                }
                            }
                        })  //ajax end
                    }
            )   //layer end
        })
    })
    function chk_tr_display(tr, a) {
        if (document.getElementById(tr).style.display == 'none') {
            document.getElementById(tr).style.display = '';
            document.getElementById(a).innerHTML = '收起';
        } else {
            document.getElementById(tr).style.display = 'none';
            document.getElementById(a).innerHTML = '展开';
        }
    }
    function chkall(f, t) {
        els = f.elements;
        if (t == 'all') {
            chked = true;
        }
        if (t == 'none') {
            chked = false;
        }
        for (i = 0; i < els.length; i++) {
            if (els[i].type == 'checkbox') {
                els[i].checked = chked;
            }
        }
    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>