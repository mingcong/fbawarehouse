<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">Shipmentid 不良申请</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/shipmentIdRejects" method="get" onsubmit="return checkSave()">
                    <div class="container-fluid">
                        <!--<div class="col-sm-12 col-md-3 add-space">-->
                            <!--<div class="input-group input-group-md">-->
                                <!--<span class="input-group-addon">账号</span>-->
                                <!--<select  class="form-control chosen-select" tabindex="-1" name="account_id"-->
                                         <!--data-name-group="common">-->
                                    <!--<option value="" class="empty-opt">&#45;&#45; 请选择 &#45;&#45;</option>-->
                                    <!--<?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$acc): $mod = ($i % 2 );++$i;?>-->
                                        <!--<option value="<?php echo ($acc["id"]); ?>"><?php echo ($acc["name"]); ?></option>-->
                                    <!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
                                <!--</select>-->
                            <!--</div>-->
                        <!--</div>-->

                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">Shipmentid</span>
                                <input class="form-control" type="text" name="shipmentid" value="">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-1 add-space">
                            <button type="submit" class="btn btn-info">查询</button>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <div style="color: red;margin-left: 10px;">
                    <b>
                        Shipmentid不良申请使用条件:<br>
                        1.不良申请时要确定亚马逊平台上的数据跟公司系统的一致，不良品返仓亚马逊平台上就要把对应SKU数量改正，良品返仓的话就要在亚马逊平台上对应sku数量改为0<br>
                        2.Shipmentid状态为待录入箱唛数据才能进行不良申请；<br>
                        3.由于sku中有某个产品不良了可以申请不良；
                    </b>
                </div>
                <hr>
                <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/shipmentIdRejectsSave" method="post">
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>帐号</th>
                        <th>SKU</th>
                        <th>中文名称</th>
                        <th>总需求数</th>
                        <th>不良数量</th>
                        <th>是否退税</th>
                        <th>公司主体</th>
                        <!--<th>不良回库单ID</th>-->
                        <th>备注</th>
                        <!--<th>操作</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$da): $mod = ($i % 2 );++$i; if(is_array($da["detail"])): $num = 0; $__LIST__ = $da["detail"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($num % 2 );++$num;?><tr>
                            <td><input type="hidden" name="account_id[]" value="<?php echo ($da["account_id"]); ?>"><?php echo ($da["account_name"]); ?></td>
                            <td><input type="hidden" name="sku[]" value="<?php echo ($vo["sku"]); ?>"><?php echo ($vo["sku"]); ?></td>
                            <td><input type="hidden" name="sku_name[]" value="<?php echo ($vo["sku_name"]); ?>"><?php echo ($vo["sku_name"]); ?></td>
                            <td><input type="hidden" name="old_quantity[]" value="<?php echo ($vo["quantity"]); ?>"><?php echo ($vo["quantity"]); ?></td>
                            <td><input type="hidden" name="detail_id[]" value="<?php echo ($vo["id"]); ?>"><input class="form-control w50" type="text" name="quantity[]" value=""/></td>
                            <td><input type="hidden" name="export_tax_rebate[]" value="<?php echo ($vo["export_tax_rebate"]); ?>"><?php echo ($vo["export_tax_rebate"]); ?></td>
                            <td><input type="hidden" name="enterprise_dominant[]" value="<?php echo ($vo["enterprise_dominant"]); ?>"><?php echo ($vo["enterprise_dominant"]); ?></td>
                            <td><input class="form-control" type="text" name="remark[]" value=""/></td>
                            <!--<td><button type="button" class="btn btn-info btn-sm add-space">废除行</button></td>-->
                        </tr><?php endforeach; endif; else: echo "" ;endif; endforeach; endif; else: echo "" ;endif; ?>
                </table>
                <div class="clearfix clear"></div>
                <div class="col-md-offset-5 col-md-6 add-space">
                    <input type="hidden" value="<?php echo ($data["0"]["shipmentid"]); ?>" name="shipmentid">
                    <input type="hidden" value="<?php echo ($data["0"]["seller_id"]); ?>" name="seller_id">
                    <input type="hidden" value="<?php echo ($data["0"]["site_id"]); ?>" name="site_id">
                    <input class="btn btn-primary" type="submit" value="申请">
                    <!--<input class="btn btn-default" type="button" onclick="save('<?php echo ($data["0"]["shipmentid"]); ?>')" value="Shipmentid 确认修改数量">-->
                </div>
                </form>
                <div class="clearfix"></div>
                <!--<form action="" method="get">-->
                    <!--<div class="container-fluid">-->
                        <!--<div class="col-sm-12 col-md-3 add-space">-->
                            <!--<div class="input-group input-group-md">-->
                                <!--<span class="input-group-addon">Shipmentid</span>-->
                                <!--<input class="form-control" type="text" value="">-->
                            <!--</div>-->
                        <!--</div>-->

                        <!--<div class="col-sm-12 col-md-1 add-space">-->
                            <!--<button type="button" class="btn btn-info">查询</button>-->
                        <!--</div>-->
                    <!--</div>-->
                <!--</form>-->
                <!--<div class="clearfix"></div>-->
                <!--<hr>-->
                <!--<table class="table table-striped  table-hover" cellspacing="0" width="100%">-->
                    <!--<thead>-->
                    <!--<tr>-->
                        <!--<th>SKU</th>-->
                        <!--<th>中文名称</th>-->
                        <!--<th>数量</th>-->
                        <!--<th>是否退税</th>-->
                        <!--<th>公司主体</th>-->
                        <!--<th>不良回库单ID</th>-->
                        <!--<th>备注</th>-->
                        <!--<th>操作</th>-->
                    <!--</tr>-->
                    <!--</thead>-->
                    <!--<tbody>-->
                    <!--<tr>-->
                        <!--&lt;!&ndash;<td class="text-center"><input name="subBox" type="checkbox" value="103"></td> &ndash;&gt;-->
                        <!--<td>Q124饿</td>-->
                        <!--<td>dafadadas</td>-->
                        <!--<td><input class="form-control w50" type="text" /></td>-->
                        <!--<td>是</td>-->
                        <!--<td>有棵树</td>-->
                        <!--<td><input class="form-control w80" type="text" /></td>-->
                        <!--<td><input class="form-control" type="text" /></td>-->
                        <!--<td><button type="button" class="btn btn-info btn-sm add-space">废除行</button></td>-->
                    <!--</tr>-->
                <!--</table>-->
                <!--<div class="clearfix clear"></div>-->
                <!--<div class="col-md-offset-4 col-md-6 add-space">-->
                    <!--<input class="btn btn-primary" type="submit" value="确认生成新的已拣货确认平台计划单">-->
                <!--</div>-->
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("input[name='shipmentid']").val("<?php  echo $_GET['shipmentid'];?>");
        $("select[name='account_id']").val("<?php  echo $_GET['account_id'];?>");
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });

    });
    function checkSave() {
        if($("input[name='shipmentid']").val()==''){
            layer.msg('请输入shipmentid', {icon: 5});
            return false;
        }else{
            return true;
        }
    }
    function save(shipmentid) {
        $.ajax({
            url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/checkPosition",
            type: 'post',
            async: false, //default: true
            data: {'shipmentid':shipmentid},
            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
            success: function (data) {
                if ($.trim(data.status) == 200) {
                    flag = 'ok';
                }else{
                    layer.msg(data.msg, {icon: 5});
                    flag = 'no';
                }
            }
        });
    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>