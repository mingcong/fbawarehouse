<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <link rel="stylesheet" href="/fbawarehouse/Public/yksui/css/multi-select.css" rel="stylesheet"/>
<style>
    .w80{width: 80px;}

</style>
<div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">亚马逊物流库存事件详情报告</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Inbound/Report/index" method="get" id="form">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">账号</span>
                                <select class="chosen-select form-control" tabindex="-1" name="account_id"
                                        data-name-group="common">
                                    <option value="" class="empty-opt">-- 请选择 --</option>
                                    <?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$acc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($acc["id"]); ?>"><?php echo ($acc["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">FBA(sku)</span>
                                <input type="text" class="form-control" value="<?php echo ($_GET['sku']); ?>" name="sku">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                            <span class="input-group-addon">日期</span>
                            <select class="form-control w80" tabindex="-1" name="date">
                                <?php if(is_array($date)): $i = 0; $__LIST__ = $date;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$acc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($acc); ?>"><?php echo ($acc); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                        </div>

                    </div>
                    <!--<div class="col-sm-12 col-md-3 add-space">-->
                        <!--<div class="input-group input-group-md">-->
                            <!--<span class="input-group-addon">销售员</span>-->
                            <!--<select class="chosen-select form-control" tabindex="-1" name="seller_ids"-->
                                    <!--data-name-group="common">-->
                                <!--<option value="" class="empty-opt">&#45;&#45; 请选择 &#45;&#45;</option>-->
                                <!--<?php if(is_array($remark)): $i = 0; $__LIST__ = $remark;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sel): $mod = ($i % 2 );++$i;?>-->
                                    <!--<option value="<?php echo (L("$key")); ?>"><?php echo ($sel); ?></option>-->
                                <!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
                            <!--</select>-->
                        <!--</div>-->
                    <!--</div>-->
                    <div class="col-md-4 add-space">
                        <?php if($_GET['account_id'] != null): if($pass == 20): ?><!--<input class="btn btn-danger" type="button" disabled value="已审核,不通过">-->
                                <a href="javascript:void(0)" data-toggle="modal" data-target=".btn_yy" >审核不通过,点击查看原因</a>
                                <?php elseif($pass == 10): ?>
                                    <input class="btn btn-success" type="button" disabled value="审核通过">
                                <?php else: ?>
                                <input class="btn btn-success" type="button" data-toggle="modal" data-target=".btn_ck" value="审核">
                                <!--<input class="btn btn-danger" type="button" data-toggle="modal" data-target=".btn_ck" value="审核不通过">--><?php endif; endif; ?>
                    </div>
                    <div class="col-md-6 add-space">
                        <input class="btn btn-primary" type="submit" value="搜索">
                        <input class="btn btn-default" type="button" onclick="download()" value="导出">
                        <input class="btn btn-warning" type="button" onclick="apiGet()" value="拉取报表">
                        <input class="btn btn-success" type="button" onclick="agreeDown()" value="导出审核列表">
                        <input class="btn btn-info" type="button" onclick="unDown()" value="导出账号拉取列表">
                    </div>
                </form>
            </div>
            <hr>
            <table class="table table-striped  table-hover " cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th style="border-bottom: none;"></th>
                    <th style="border-bottom: none;"></th>
                    <th style="border-bottom: none;"></th>
                    <th style="border-bottom: none;"></th>
                    <th colspan="6" class="text-center">本期入库</th>
                    <th colspan="5" class="text-center">本期出库</th>
                    <th style="border-bottom: none;"></th>
                    <th style="border-bottom: none;"></th>
                    <th style="border-bottom: none;"></th>
                </tr>
                <tr>
                    <!--<th width="40">序号</th>-->
                    <th>账号</th>
                    <!--<th>站点</th>-->
                    <th>FBA(sku)</th>
                    <th>公司(sku)</th>
                    <th>期初库存</th>
                    <th>入库</th>
                    <th>退货入库</th>
                    <th>转仓入库</th>
                    <th>盘盈入库</th>
                    <th>其它入库</th>
                    <th>入库合计</th>
                    <th>销售出库</th>
                    <th>转仓出库</th>
                    <th>盘亏出库</th>
                    <th>其它出库</th>
                    <th>出库合计</th>
                    <th>实际期末库存</th>
                    <th>理论期末库存</th>
                    <th>实际比理论相差</th>
                </tr>
                </thead>
                <tbody>
                <?php if(is_array($data)): $num = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($num % 2 );++$num;?><tr>
                        <!--<td><?php echo ($num); ?></td>-->
                        <td><?php echo ($vo["account_name"]); ?></td>
                        <!--<td><?php echo ($vo["site"]); ?></td>-->
                        <td><?php echo ($vo["sku"]); ?></td>
                        <td><?php echo ($vo["piv_sku"]); ?></td>
                        <td><?php echo ($vo["qckc"]); ?></td>
                        <td><?php echo ($vo["bhrk"]); ?></td>
                        <td><?php echo ($vo["thrk"]); ?></td>
                        <td><?php echo ($vo["zcrk"]); ?></td>
                        <td><?php echo ($vo["pyrk"]); ?></td>
                        <td><?php echo ($vo["qtrk"]); ?></td>
                        <td><?php echo ($vo["rkhj"]); ?></td>
                        <td><?php echo ($vo["xsck"]); ?></td>
                        <td><?php echo ($vo["zcck"]); ?></td>
                        <td><?php echo ($vo["pkck"]); ?></td>
                        <td><?php echo ($vo["qtck"]); ?></td>
                        <td><?php echo ($vo["ckhj"]); ?></td>
                        <td><?php echo ($vo["qmkc"]); ?></td>
                        <td><?php echo ($vo["llqmkc"]); ?></td>
                        <td><?php echo ($vo["diff"]); ?></td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
            <div class="clearfix clear"></div>
            <div class="row">
                <div class="col-md-6 pull-left">
                    <label>每页 20 条记录
                        显示 1 到 20 项，共 <?php echo ($count); ?>项</label>
                </div>
                <div class="col-md-6">
                    <div class="page pull-right" style="padding: 0;margin: 0">
                        <?php echo ($page); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade btn_ck">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">请核对好数据审核</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" name="remark" placeholder="备注（选填，不通过必填）"></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal" onclick="checkOk()">通过</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="checkNot()">不通过</button>
        </div>
    </div>
</div>
<div class="modal fade btn_yy">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">审核不通过原因</h4>
            </div>
            <div class="modal-body">
                <p class="text-primary"><?php echo ($remark); ?></p></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">关闭</button>
        </div>
    </div>
</div>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/javascripts/admin/layer/layer.js"></script>
<script type="text/javascript">
    $(function () {
        $("select[name='account_id']").val("<?php  echo $_GET['account_id'];?>");
        $("select[name='date']").val("<?php  echo $_GET['date'];?>");
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });
    });

</script>
<script type="text/javascript">
    function download() {
        var data = $('#form').serialize();
        window.location.href ='/fbawarehouse/index.php/Inbound/Report/index?down=ok&'+data;
    }
    function checkOk() {
        var data = $('#form').serialize();
        var remark = $.trim($('input[name="remark"]').val());
        window.location.href ='/fbawarehouse/index.php/Inbound/Report/index?pass=10&remark='+remark+'&'+data;
    }
    function checkNot() {
        var data = $('#form').serialize();
        var remark = $.trim($('input[name="remark"]').val());
        if(remark==''){
            layer.msg('请填写不通过备注', {icon: 5});
            return false;
        }else{
            window.location.href ='/fbawarehouse/index.php/Inbound/Report/index?pass=20&remark='+remark+'&'+data;
        }

    }
    function apiGet() {
        var data = $('#form').serialize();
        if(data){
            window.location.href ='/fbawarehouse/index.php/Inbound/Report/index?api=ok&'+data;
        }
    }
    //已审核列表
    function agreeDown() {
        var data = $('#form').serialize();
        if(data){
            window.location.href ='/fbawarehouse/index.php/Inbound/Report/index?agree=ok&'+data;
        }
    }
    //账号拉取列表
    function unDown() {
        var data = $('#form').serialize();
        if(data){
            window.location.href ='/fbawarehouse/index.php/Inbound/Report/index?unDown=ok&'+data;
        }
    }


</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>