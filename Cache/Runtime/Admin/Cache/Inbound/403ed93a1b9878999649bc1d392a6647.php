<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">返仓入库表</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/shipmentIdBackList" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">shipmentid</span>
                                <input class="form-control" type="text" name="shipmentid" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">账号</span>
                                <select class="chosen-select form-control" tabindex="-1" name="account_id"
                                        data-name-group="common">
                                    <option value="">请选择</option>
                                    <?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$acc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($acc["id"]); ?>"><?php echo ($acc["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">销售员</span>
                                <select class="chosen-select form-control" tabindex="-1" name="seller_id"
                                        data-name-group="common">
                                    <option value="">请选择</option>
                                    <?php if(is_array($sellers)): $i = 0; $__LIST__ = $sellers;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sl): $mod = ($i % 2 );++$i;?><option value="<?php echo (L("$key")); ?>"><?php echo ($sl); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">状态</span>
                                <select class="chosen-select form-control" tabindex="-1" name="back_status"
                                        data-name-group="common">
                                    <option value="">请选择</option>
                                    <?php if(is_array($back_status)): $i = 0; $__LIST__ = $back_status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$bs): $mod = ($i % 2 );++$i;?><option value="<?php echo (L("$key")); ?>"><?php echo ($bs); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input class="form-control" type="text" name="sku" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">返仓时间</span>
                                <input class="form-control form-datetime" type="text" name="actual_back_time_from"
                                       style="cursor:pointer;" readonly>
                                <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                <input class="form-control form-datetime" type="text" name="actual_back_time_to"
                                       style="cursor:pointer;" readonly>
                            </div>
                        </div>
                        <div class="col-md-2 add-space">
                            <input class="btn btn-primary" type="submit" value="搜索">
                            <input class="btn btn-success" type="submit" value="导出">
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20"><input id="checkAll" type="checkbox"></th>
                        <th>序号</th>
                        <th>状态</th>
                        <th>shipmentid</th>
                        <th>账号</th>
                        <th>SKU</th>
                        <th width="200">产品名称</th>
                        <th>数量</th>
                        <th>分配储位</th>
                        <th>是否退税</th>
                        <th>公司主体</th>
                        <th>销售员</th>
                        <th>申请人</th>
                        <th>申请时间</th>
                        <th>审核时间</th>
                        <th>发货仓库</th>
                        <th>返仓时间</th>
                        <th>备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($data)): $num = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($num % 2 );++$num;?><tr>
                            <td class="text-center"><input name="subBox" type="checkbox" value="<?php echo ($vo["id"]); ?>"></td>
                            <td><?php echo ($num); ?></td>
                            <td class="back_status"><input type="hidden" name="status" value="<?php echo ($vo["back_status"]); ?>"><?php echo ($vo["back_status_name"]); ?></td>
                            <td class="back_shipmentId"><input type="hidden" name="backShipment" value="<?php echo ($vo["shipmentid"]); ?>"><?php echo ($vo["shipmentid"]); ?></td>
                            <td><?php echo ($vo["account_name"]); ?></td>
                            <td><?php echo ($vo["sku"]); ?></td>
                            <td><?php echo ($vo["sku_name"]); ?></td>
                            <td class="need_qty"><?php echo ($vo["back_quantity"]); ?></td>
                            <td>
                                <?php if(($vo["back_status"] == 20) AND ($success == 1)): ?><a href="javascript:void(0);" class="addPosition" data-toggle="modal" data-target=".addRelation">分配储位</a>
                                    <?php else: ?>
                                    分配储位<?php endif; ?>
                            </td>
                            <td><?php if($vo["export_tax_rebate"] == 1): ?>是<?php else: ?>否<?php endif; ?></td>
                            <td><?php echo ($vo["enterprise_dominant_name"]); ?></td>
                            <td><?php echo ($vo["seller_name"]); ?></td>
                            <td><?php echo ($vo["back_apply_user_name"]); ?></td>
                            <td><?php echo ($vo["back_apply_time"]); ?></td>
                            <td><?php echo ($vo["confirm_time"]); ?></td>
                            <td><?php if($vo["tranfer_hopper_id"] == 1): ?>东莞高埗仓<?php else: ?>其他<?php endif; ?></td>
                            <td><?php echo ($vo["actual_back_time"]); ?></td>
                            <td><?php echo ($vo["reason"]); ?></td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    </tbody>
                </table>
                <div class="clearfix clear">
                </div>
                <!--<div class="page page-wrapper">
                    <?php echo ($page); ?>
                </div>--><div class="row">
                <div class="col-md-6 pull-left">
                    <label>每页 20 条记录
                        显示 1 到 20 项，共 <?php echo ($count); ?>项</label>
                </div>
                <div class="col-md-6">
                    <div class="page pull-right" style="padding: 0;margin: 0">
                        <?php echo ($page); ?>
                    </div>
                </div>
                <div class="clearfix clear"></div>
                <div class="col-md-offset-5 col-md-2 add-space">
                    <?php if(($role_id == 1) OR ($role_id == 3)): ?><input class="btn btn-success" type="button" onclick="examine()" value="审核">
                        <!--<input class="btn btn-success" type="button" onclick="alert('没有权限')" value="审核">--><?php endif; ?>

                    <!--<input class="btn btn-primary" type="button" onclick="saveBackShipment()" value="确认返仓">-->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="modal fade addRelation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/saveBackShipment" id="save" method="post" onsubmit="return checkPosition()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">分配储位</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>储位</th>
                            <th>数量</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="addHtml">
                        <tr>
                            <td><input type="text" name="position[]" class="form-control" /></td>
                            <td><input type="text" name="quantity[]" class="form-control" /></td>
                            <td><a href="javascript:add();">增加</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="" name="need_qty">
                    <input type="hidden" value="" name="backId">
                    <button type="submit" class="btn btn-primary save">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("input[name='shipmentid']").val("<?php  echo $_GET['shipmentid'];?>");
        $("input[name='sku']").val("<?php  echo $_GET['sku'];?>");
        $("select[name='back_status']").val("<?php echo $_GET['back_status'];?>");
        $("select[name='account_id']").val("<?php echo $_GET['account_id'];?>");
        $("select[name='seller_id']").val("<?php echo $_GET['seller_id'];?>");
        $("input[name='actual_back_time_from']").val("<?php  echo $_GET['actual_back_time_from'];?>");
        $("input[name='actual_back_time_to']").val("<?php  echo $_GET['actual_back_time_to'];?>");
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });


        $(document).on("click", ".storageQtyDel", function(){
            $(this).parent().parent().remove();
        });
    });
    $('.addPosition').click(function () {
        var id = parseInt($(this).parent('td').siblings('.text-center').find('input').val());
        var qty = parseInt($(this).parent('td').siblings('.need_qty').text());
        $("input[name='backId']").val(id);
        $("input[name='need_qty']").val(qty);
    });
    function add() {
        $("#addHtml").append('<tr><td><input type="text" name="position[]" class="form-control" /></td><td><input type="text" name="quantity[]" class="form-control" /></td><td><a href="javascript:add();">增加</a>  <a class="storageQtyDel" style="cursor: pointer">删除</a></td></tr>');
    }
    //审核
    function examine() {
        var id = [];
        var msg = '';
        var shipmentId = '';
        $('input[name="subBox"]:checked').each(function(){
            var status = $(this).parent('td').siblings('.back_status').find('input').val();
            shipmentId = $(this).parent('td').siblings('.back_shipmentId').find('input').val();
            if($.trim(status)!=10){
                msg += shipmentId+'的状态不能审核</br>';
            }
            id.push($(this).val());
        });
        if(msg!=''){
            layer.msg(msg, {icon: 5});
            return false;
        }
        if(id==''){
            layer.msg('请选择要审核的数据', {icon: 5});
            return false;
        }
        if(confirm('你确定要审核吗？')==true) {
            $.ajax({
                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/examineBackShipment",
                type: 'post',
                async: true, //default: true
                data: {'id':id},
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data, textStatus, jqXHR) {
                    //alert(data)
                    if ($.trim(data.status) == 200) {
                        layer.msg(data.msg);
                        window.location.reload();
                    }
                    else{
                        layer.msg(data.msg, {icon: 5});
                    }
                }
            });
        }
    }
    //分配储位检查
    function checkPosition() {
        var msg = '';
        var flag = '';
        var qty = 0;
        var position = [];
        var need_qty = $("input[name='need_qty']").val();
        $('input[name="position[]"]').each(function(){
            if($(this).val()==''){
                msg +='储位不能为空<br>';
            }else{
                position.push($(this).val());
            }
        });
        $('input[name="quantity[]"]').each(function(){
            if($(this).val()==''){
                msg +='数量不能为空<br>';
            }else if(isNaN($(this).val())){
                msg +='数量必须填写数字<br>';
            }
            else{
                qty += parseInt($(this).val());
            }
        });
        if(msg!=''){
            layer.msg(msg, {icon: 5});
            return false;
        }
        if(qty>need_qty){
            layer.msg('分配数量大于实际数量', {icon: 5});
            return false;
        }
        if(qty<need_qty){
            layer.msg('分配数量小于实际数量', {icon: 5});
            return false;
        }
        $.ajax({
            url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/checkPosition",
            type: 'post',
            async: false, //default:
            data: {'position':position},
            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
            success: function (data) {
                if ($.trim(data.status) == 200) {
                    flag = 'ok';
                }else{
                    layer.msg(data.msg, {icon: 5});
                    flag = 'no';
                }
            }
        });
        if(flag == 'no'){
            return false;
        }
        return true;
    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>