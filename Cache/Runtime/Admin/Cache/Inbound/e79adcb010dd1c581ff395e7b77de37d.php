<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>
    .font14{
        font-size : 14px;
        font-weight : 500;
        margin-right : 40px;
    }
    a:hover {
        cursor:pointer;
    }

</style>

<div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading">
                <h3 class="panel-title" id="ccc">备货需求申请</h3>
            </div>
            <div class="panel-body">
                <div>
                    <p>
                        <span style="color: #91bef0"><b>Tip:</b></span><br>
                        1.创建需求的时候如果提示SKU不存在，则需要找销售同事到平台SKU列表录入平台SKU与公司SKU对应关系;<br>
                        2.选项除了备注可为空之外，其他<span style="color: red">必填！</span><br>
                        3.可用库存为0并且所有站点可用库存为0不能下单，产生的原因有：实际库存为0、库存被其他帐号占用、还没有入库;<br>
                        4.可用库存为0但所有站点可用库存不为0可以进行<span style="color: red">库存调拨</span>,备货量不能超过总可用库存。<br>
                        5.不能选择帐号请到&nbsp;<span style="color: red">备货需求管理 > 销售员帐号对应关系表</span>，新增对应关系
                    </p>
                </div>
                <form class="form-inline" action="Inbound/StockingApply/index" id="option-form">
                    <div class="form-group">
                        <lable class="label-control font14">
                            申请人：<span><b><?php echo ($_SESSION['current_account']['remark']); ?></b></span>
                            <input type="hidden" id="applyMan" value="<?php echo ($_SESSION['current_account']['role_id']); ?>">
                        </lable>
                     </div>
                    <div class="form-group font14" id="siteName">
                        <label>店铺帐号：</label>
                        <select class="chosen-select form-control w200" tabindex="-1" name="accountId" 
                        data-name-group="common">
                            <option value="" class="empty-opt">-- 请选择 --</option>
                            <?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i; if ($accountId) { ?>
                                <option value="<?php echo ($item["id"]); ?>"
                                <?php if($item["id"] === $accountId): ?>selected="selected"<?php endif; ?>><?php echo ($item["name"]); ?>
                                </option>
                                <?php
 } else { ?>
                                <option value="<?php echo ($item["id"]); ?>"><?php echo ($item["name"]); ?></option>
                                <?php
 } endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>

                    <div class="form-group font14" id="div">
                        <label>销售员：</label>
                        <?php
 if (is_array($salesman)){ ?>
                        <select class="chosen-select form-control w120" id="saleMan" tabindex="-1" 
                        data-name-group="common">
                            <option value="" class="empty-opt">-- 请选择 --</option>
                            <?php
 if($accountId) { ?>
                            <?php if(is_array($salesman)): $i = 0; $__LIST__ = $salesman;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($item["id"]); ?>"><?php echo ($item["remark"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            <?php } else { ?>
                            <?php if(is_array($salesman)): $i = 0; $__LIST__ = $salesman;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>" ><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            <?php } ?>
                        </select>
                        <?php
 } else { ?>
                        <input type="hidden" value="<?php echo ($_SESSION['current_account']['id']); ?>" id="saleMan" >
                        <span><b><?php echo ($_SESSION['current_account']['remark']); ?></b></span>
                        <?php
 } ?>
                    </div>
                    <div class="form-group font14ert">
                            <label>PC单号：</label>
                            <input class="form-control" aria-describedby="inputSuccess2Status" name="pc_id" value="" onkeyup="value=value.replace(/[^A-z0-9]/g,'').toUpperCase(value)" placeholder="PC单号" required>


                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <div style="width:100%;overflow-x:scroll">
                    <table class="table table-striped  table-hover" cellspacing="0" width="2800px">
                    <thead>
                    <tr>
                        <!--<th>编号</th>-->
                        <th>SKU</th>
                        <th>SellerSKU</th>
                        <th>是否退税</th>
                        <th>公司主体</th>
                        <th>发货仓库</th>
                        <th>产品名称</th>
                        <th>实际库存</th>
                        <th>实际可用库存</th>
                        <th>实际占用库存</th>
                        <th>所有站点可用库存</th>
                        <th>需求数量</th>
                        <th>要求到货时间</th>
                        <th>备注</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="html">
                    <tr>
                        <!--<td></td>-->
                        <td class="td_sku">
                            <input class="form-control w80 sku" type="text" autocomplete="off"
                                   onkeyup="doAutoComplete(this)" onblur="skuInfoCheck(this)" />
                        </td>
                        <td class="td_sellersku">
                            <select type="text" name="sellersku" class="form-control option">
                                <option value="">请选择</option>
                            </select>
                        </td>
                        <td class="td_tax">
                            <select type="text" name="tax" class="form-control option">
                                <option value="">请选择</option>
                                <option value="0">否</option>
                                <option value="1">是</option>
                            </select>
                        </td>
                        <td class="td_enterprise">
                            <select type="text" name="enterprise" class="form-control option">
                                <option value="">请选择</option>
                                <?php if(is_array($enterprise_dominant)): $i = 0; $__LIST__ = $enterprise_dominant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </td>
                        <td class="address">
                            <select type="text" name="creater" class="form-control option">
                                <option>请选择</option>
                                <?php if(is_array($address)): $i = 0; $__LIST__ = $address;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($item["id"]); ?>"><?php echo ($item["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </td>
                        <td class="title"></td>
                        <td class="quantity"></td>
                        <td class="actualQuantity"></td>
                        <td class="occupyQuantity"><a></a></td>
                        <td class="totalActualQuantity"></td>
                        <td class="needQuantity"><input class="form-control w50 option input_quantity" type="text" /></td>
                        <td class="claimArriveTime"><input class="form-control Wdate w120" type="text" onClick="WdatePicker()"></td>
                        <td class="remark"><input class="form-control w80" type="text" /></td>
                        <td><button type="button" class="btn btn-info btn-sm add-space del">删除</button></td>
                    </tr>
                </table>
                </div>
                <div class="fixed-table-toolbar">
                    <h3>
                        <button type="button" class="btn btn-success" id="addRow">+ 新增</button>
                    </h3>
                </div>
                <div class="col-md-offset-5 col-md-2 add-space">
                    <button class="btn btn-primary" id="submit" >提交申请</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var tr_html = $("#html").html();
        var td_html = $("#html").children('tr').eq(0).html();
        $("#html").children('tr').eq(0).attr('name', 'firstRow');

        $('select.chosen-select').chosen({
            no_results_text : '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold : 2,  // 个以下的选择项则不显示检索框
            search_contains : true         // 从任意位置开始检索
        });

        //增加列
        $('#addRow').click(function () {
            $('tbody').append(tr_html);
        });

        //删除列，第一列清空
        $(document).on('click', '.del', function () {
            if ($(this).parent().parent().attr('name') == 'firstRow') {
                $(this).parent().parent().html(td_html);
                return;
            }
            $(this).parent().parent().remove();
        });

        $(document).on('blur', '.input_quantity', function () {
           if ($(this).val() != '') {
               var needQuantity = Number($(this).val());
               if (isNaN(needQuantity)) {
                   layer.msg("请输入数字！", {icon : 5});
                   return false;
               }
               if (needQuantity > $(this).parent().prevAll('.totalActualQuantity').text()) {
                   layer.msg("备货数字不能大于所有站点总可用量！", {icon : 5});
                   $(this).val($(this).parent().prevAll('.totalActualQuantity').text());
                   return false;
               }
               /*if (needQuantity >= $(this).parent().prevAll('.actualQuantity').text()) {
                   var accountId = $('select[name="accountId"]').val();
                   var tax = $(this).parent().prevAll('.td_tax').children().val();
                   var sku = $(this).parent().prevAll('.td_sku').children('.sku').val();
                   var enterprise_dominant = $(this).parent().prevAll('.td_enterprise').children().val();

                   layer.confirm(
                       '您是否要调拨库存？',
                       {
                           title : '调拨库存'
                       },
                       function (index) {
                           $.ajax({
                               type: "POST",
                               url: "/fbawarehouse/index.php/Inbound/StockingApply/moveInventory?sku=" + sku + '&export_tax_rebate=' + tax
                               + '&enterprise_dominant=' + enterprise_dominant +'&account_id=' + accountId
                               + '&needQuantity=' + needQuantity,
                               dataType: "json",
                               success: function (data) {
                                   if (data.flag == true) {
                                       layer.msg("库存调拨成功！", {icon : 1});
                                   } else {
                                       layer.msg("库存调拨错误！", {icon : 5});
                                   }
                               },
                               error: function () {
                                   layer.msg("请求失败！", {icon: 5});
                               }
                           });
                           layer.close(index);
                       }
                   )

               }*/
           }
        });

        //选择框光标离开触发事件
        $(document).on('blur', '.option',  function () {
           if ($(this).val() == ''){
               layer.msg("请选择或者输入值！", {icon : 5});
               return false;
           }

           var td = $(this);
           var accountId = $('select[name="accountId"]').val();
            var checkflag = true;

           if ($(this).attr('name') == 'enterprise') {
               var tax = $(this).parent().prevAll('.td_tax').children().val();
               var sku = $(this).parent().prevAll('.td_sku').children('.sku').val();
               if (!sku) {
                   layer.msg('请先填写SKU！', {icon : 5});
                   clearRow(td, td_html);
                   return;
               }
               if (!tax) {
                   layer.msg("请先选择是否退税！", {icon : 5});
                   clearRow(td, td_html);
                   return;
               }

               if ((tax == 0 && $(this).val() != 1) || (tax == 1 && $(this).val() == 1)) {
                   layer.msg("主体选择错误，请重试！", {icon : 5});
                   clearRow(td, td_html);
                   return;
               }

               if ($(this).val() != 1) {
                   $.ajax({
                       type : "POST",
                       async : false,
                       url : "/fbawarehouse/index.php/Inbound/StockingApply/checkWarehouseOrder?sku=" + sku + "&enterprise_dominant=" + $(this).val(),
                       dataType : "json",
                       success : function (data) {
                           if (!data.status) {
                               layer.msg(data.msg, {icon : 5});
                               clearRow(td, td_html);
                               checkflag = false;
                               return false;
                           }
                       }
                   });

                   if (checkflag == false) return false;

                   $.ajax({
                       type : "POST",
                       async : false,
                       url : "/fbawarehouse/index.php/Inbound/StockingApply/checkPurchaseOrder?sku=" + sku + "&enterprise_dominant=" + $(this).val(),
                       dataType : "json",
                       success : function (data) {
                            if (data.allow) {
                                layer.msg(data.allow, {icon : 1})
                            } else {
                                if (data.deny) {
                                    layer.msg(data.deny, {icon : 5});
                                    clearRow(td, td_html);
                                    checkflag = false;
                                    return false;
                                }
                            }
                       }

                   });
               }

               if (checkflag == false) return false;
               $.ajax({
                   type: "POST",
                   async : false,
                   url: "/fbawarehouse/index.php/Inbound/StockingApply/skuInventoryGet?sku=" + sku + '&export_tax_rebate=' + tax
                        + '&enterprise_dominant=' + $(this).val() +'&account_id=' + accountId,
                   dataType: "json",
                   success: function (data) {
                       if (data) {
                           td.parent().nextAll('.quantity').text(data.quantity);
                           td.parent().nextAll('.actualQuantity').text(data.actualQuantity);
                           td.parent().nextAll('.occupyQuantity').children().text(data.occupyQuantity);
                           td.parent().nextAll('.totalActualQuantity').text(data.totalActualQuantity);
                           if (data.actualQuantity <= 0 && data.totalActualQuantity <= 0) {
                               layer.msg("可用库存为0或已被占用，不能申请备货！", {icon : 5});
                               clearRow(td, td_html);
                           }
                       } else {
                           layer.msg("获取库存数据失败，请联系IT！", {icon : 5});
                       }
                   }
               })
           }

        });

        $(".occupyQuantity").click(function () {
            var sku = $(this).prevAll('.td_sku').children('.sku').val();
            var export_tax_rebate = $(this).prevAll('.td_tax').children().val();
            var enterprise_dominant = $(this).prevAll('.td_enterprise').children().val();
            window.open("/fbawarehouse/index.php/Inbound/StockingApply/occupiedInventory?sku=" + sku
                    + "&export_tax_rebate=" + export_tax_rebate
                    + "&enterprise_dominant=" + enterprise_dominant);
        });

        //选择帐号之后联动查出销售员
        $('select[name="accountId"]').change(function () {
            if ($("#applyMan").val() !=4) {
                var accountId = $(this).val();
                if (accountId != '') {
                    window.location.href = "/fbawarehouse/index.php/Inbound/StockingApply/index?accountId=" + accountId;
                }
            }
        });

        //提交备货申请
        $('#submit').click(function () {
            if ($('#saleMan').val() == '') {
                layer.msg("请选择销售帐号！", {icon : 5});
                return false;
            }
            if ($("input[name='pc_id']").val() == '') {
                layer.msg("请输入PC单号！", {icon : 5});
                $("input[name='pc_id']").focus();
                return false;
            }
            var confirm = window.confirm("您确定要提交吗？");
            if (!confirm) {
                return false;
            }
            var flag = true;
            $('#html').children('tr').each(function () {
                $(this).children('td').children('.option').each(function () {
                    if ($(this).val() == '') {
                        flag = false;
                        layer.msg("[" +
                                $(this).parent().prevAll('.td_sku').children('.sku').val()
                                + "]:信息未完整！");
                        return false;
                    }
                });
                if (flag == false){
                    return false;
                }
            });

            if (flag == false){
                return false;
            }

            stockingApplySubmit($('#html').children('tr'));
        });
    });

    function clearRow(td, td_html) {
        if (td.parent().parent().attr('name') == 'firstRow') {
            td.parent().parent().html(td_html);
            return;
        }
        td.parent().parent().remove();
        return false;
    }

    function doAutoComplete(skuInputObj) {
        var accountId = $('select[name="accountId"]').val();

        if(!accountId) {
            layer.msg('请选择帐号!', {icon: 5});
            return;
        }

        if (!$(skuInputObj).attr('readonly')) {
            $(skuInputObj).autocomplete({
                autoFocus: true,
                minLength: 0,
                autoFill: true,
                source: "/fbawarehouse/index.php/Inbound/StockingApply/skuAutoComplete?accountId=" + accountId + '&skuKeywords=' + $(skuInputObj).val(),
                select: function (event, ui) {
                    $.ajax({
                        type: "POST",
                        url: "/fbawarehouse/index.php/Inbound/StockingApply/skuInfoGet?sku=" + ui.item.sku,
                        dataType: "json",
                        success: function (data) {
                            $(skuInputObj).parent().nextAll('.title').text(data.name);
                        },
                        error: function () {
                            layer.msg("SKU标题不存在!", {icon: 5});
                        }
                    })
                }
            });
        }
    }

    /**
     * 描述：检查SKU是否存在
     */
    function skuInfoCheck(skuInputObj) {
        var accountId = $('select[name="accountId"]').val();

        if (!$(skuInputObj).attr('readonly') && $(skuInputObj).val().trim() != '') {
            $.ajax({
                type : "POST",
                url : "/fbawarehouse/index.php/Inbound/StockingApply/skuCheck?accountId=" + accountId + '&skuKeywords=' + $(skuInputObj).val(),
                dataType: 'json',
                success : function (data) {
                    if (data && data.sku && data.sellerSku.length > 0) {
                        $(skuInputObj).val(data.sku);
                        $(skuInputObj).parent().nextAll('.title').text(data.name);
                        $(skuInputObj).attr("readonly", "readonly");

                        var strSellerSku = '';
                        for (var i = 0; i < data.sellerSku.length; i++) {
                            strSellerSku += '<option value="' + data.sellerSku[i] +'">'
                                    + data.sellerSku[i] + '</option>';
                        }
                        $(skuInputObj).parent().nextAll('.td_sellersku').children().html(strSellerSku);
                    } else {
                        layer.msg('该SKU不存在，请去平台SKU列表下录入！', {icon : 5});
                        return false;
                    }
                }
            })
        }
    }

    /**
     * @param stockingApply
     * 描述：提交备货申请
     */
    function stockingApplySubmit(stockingApply) {
        var stockingApplyArray = [];
        stockingApply.each(function () {
            var stockingApplyObj = {};
            stockingApplyObj.account_id = $('select[name="accountId"]').val();
            stockingApplyObj.seller_id = $('#saleMan').val();
            stockingApplyObj.sku = $(this).children('.td_sku').children('input').val();
            stockingApplyObj.seller_sku = $(this).children('.td_sellersku').children().val();
            stockingApplyObj.export_tax_rebate = $(this).children('.td_tax').children().val();
            stockingApplyObj.enterprise_dominant = $(this).children('.td_enterprise').children().val();
            stockingApplyObj.transfer_hopper_id = $(this).children('.address').children().val();
            stockingApplyObj.sku_name = $(this).children('.title').text();
            stockingApplyObj.needs_quantity = $(this).children('.needQuantity').children().val();
            stockingApplyObj.claim_arrive_time = $(this).children('.claimArriveTime').children().val();
            stockingApplyObj.remark = $(this).children('.remark').children().val();
            stockingApplyObj.pc_id = $("input[name='pc_id']").val();
            stockingApplyArray.push(stockingApplyObj)
        });

        $.ajax({
            type : "POST",
            url : "/fbawarehouse/index.php/Inbound/StockingApply/stockingApplyInsert",
            data : {data : JSON.stringify(stockingApplyArray)},
            dataType : "JSON",
            success : function (data) {
                if (data['status'] == 1) {
                    layer.msg(data['message'], {icon : 1});
                } else {
                    layer.alert(data['message'], {icon : 5});
                }
                //window.location.reload();
            },
            error : function () {
                layer.msg("提交失败！", {icon : 5});
                //window.location.reload()
            }
        })
    }
//    function checkPcId(pc_id) {
//        if(pc_id!=''){
//                $.ajax({
//                    type: "post",
//                    async: true,
//                    url: "/fbawarehouse/index.php/Inbound/StockingApply/checkPcId",
//                    data: {'pc_id':pc_id},
//                    dataType: "json",
//                    success: function(data){
//                        if(data.status==200){
//                            layer.msg(data.msg,{icon:5});
//                            $('#submit').attr("disabled", true);
//                        } else {
//                            $('#submit').attr("disabled", false);
//                        }
//                    }
//                });
//        }else{
//            layer.msg('PC单号必填',{icon:5});
//            $('#submit').attr("disabled", true);
//        }
//    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>