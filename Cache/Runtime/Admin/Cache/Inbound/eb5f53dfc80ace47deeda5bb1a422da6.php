<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">库存占用报表</h3>
    </div>
    <div class="panel-body">
        <div class="container-fluid">
            <form id="search-form" class="search-form">
                <div class="col-sm-12 col-md-4  add-space">
                    <div class="input-group input-group-md">
                        <span class="input-group-addon">账号</span>
                        <select class="chosen-select form-control" tabindex="-1" name="account_id" data-name-group="common">
                            <option value="" class="empty-opt">-- 请选择 --</option>
                            <?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($item["id"]); ?>" ><?php echo ($item["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                        <input name="page" id="page" type="hidden" />
                    </div>
                </div>
                <div class="col-sm-12 col-md-4  add-space">
                    <div class="input-group input-group-md">
                        <span class="input-group-addon">是否退税</span>
                        <select class="form-control" tabindex="-1" name="export_tax_rebate"
                                data-name-group="common">
                            <option value="" class="empty-opt">-- 请选择 --</option>
                            <option value="1"
                            <?php if($info['export_tax_rebate'] === 1): ?>selected="selected"<?php endif; ?>>是
                            </option>
                            <option value="0"
                            <?php if($info['export_tax_rebate'] === 0): ?>selected="selected"<?php endif; ?>>否
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4  add-space">
                    <div class="input-group input-group-md">
                        <span class="input-group-addon">公司主体</span>
                        <select type="text" tabindex="-1" name="enterprise_dominant" class="form-control option">
                            <option value="">-- 请选择 --</option>
                            <?php if(is_array($enterprise_dominant)): $i = 0; $__LIST__ = $enterprise_dominant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"
                                <?php if($info['enterprise_dominant'] == $key): ?>selected="selected"<?php endif; ?>><?php echo ($item); ?>
                                </option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3  add-space">
                    <div class="input-group input-group-md">
                        <span class="input-group-addon">SKU</span>
                        <input class="form-control" type="text" name="sku" value="<?php echo ($info['sku']); ?>">
                    </div>
                </div>
                <div class="col-sm-12 col-md-3  add-space">
                    <div class="input-group input-group-md">
                        <span class="input-group-addon">ShipmentId</span>
                        <input class="form-control" type="text" name="shipmentid" value="">
                    </div>
                </div>
                <div class="col-md-2 add-space">
                    <input class="btn btn-primary jsSearchBtn" type="button" value="搜索">
                </div>
            </form>
        </div>
        <div class="clearfix"></div>
        <hr>
        <table class="table table-striped  table-hover" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td>SKU</td>
                <td>中文名称</td>
                <td>数量</td>
                <td>是否出口退税</td>
                <td>公司主体</td>
                <td>状态</td>
                <td>帐号</td>
                <td>销售员</td>
                <td>ShipmentId</td>
            </tr>
            </thead>
            <tbody id="info"></tbody>
        </table>
    </div>
    <div class="clearfix clear"></div>
    <div id="pagecount"></div>
</div>

<script type="text/javascript">
    $(document).ready(function (){
        $('select.chosen-select').chosen({
            no_results_text : '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold : 2,  // 个以下的选择项则不显示检索框
            search_contains : true         // 从任意位置开始检索
        });

        $('#pagecount').on('click','span a',function(){
            var rel = $(this).attr("rel");
            $('#page').val(rel);
            getData();
        });

        $(".jsSearchBtn").click(function(){
            $('#page').val(1);
            getData();
        });

    });

    /**
     * 获取当页数据
     */
    function getData(){

        $.ajax({
            type: 'POST',
            url: '/fbawarehouse/index.php/Inbound/StockingApply/occupiedInventoryGet',
            data: $('#search-form').serialize(),
            dataType:'json',
            success:function(arr){
                total = arr.total; //总记录数
                pageSize = arr.pageSize; //每页显示条数
                curPage = arr.page; //当前页
                totalPage = arr.totalPage; //总页数
                var list = arr.result;

                var content = '';
                if(typeof(list) == "undefined"){
                    content = '';
                    layer.msg("查询结果为空！", {icon : 5});
                } else{
                    $.each(list,function(index,array){ //遍历json数据列
                        content += "<tr>";
                        content += "<td>" + array['sku'] + "</td>";
                        content += "<td>" + array['sku_name'] + "</td>";
                        content += "<td>" + array['quantity'] + "</td>";
                        content += "<td>" + array['export_tax_rebate'] + "</td>";
                        content += "<td>" + array['enterprise_dominant'] + "</td>";
                        content += "<td>" + array['status'] + "</td>";
                        content += "<td>" + array['account_name'] + "</td>";
                        content += "<td>" + array['seller'] + "</td>";
                        content += "<td>" + array['shipmentid'] + "</td>";
                        content += "</tr>";

                    });
                }
                $('#info').html(content);
                getPage();

            },
            error:function(){
                layer.msg("数据出错，请联系IT！", {icon : 5});
            }
        });
    }

    /**
     * 分页展示
     */
    function getPage(){

        if(curPage>totalPage)curPage=totalPage;
        if(curPage<1)curPage=1;
        var pageStr="<span class='span'>共"+total+"条</span><span class='span'>"+curPage+"/"+totalPage+"</span>"
        if(curPage==1){
            pageStr += "<span class='span'>首页</span><span class='span'>上一页</span>";
        }else{
            pageStr += "<span><a class='a' href='javascript:void(0)' rel='1'>首页</a></span>" +
                    "<span><a class='a' href='javascript:void(0)' rel='"+(curPage-1)+"'>上一页</a></span>"
        }
        if(curPage>=totalPage){
            pageStr += "<span class='span'>下一页</span><span class='span'>尾页</span>";
        }else{
            pageStr += "<span><a class='a' href='javascript:void(0)' rel='"+(parseInt(curPage)+1)+"'>下一页</a></span>" +
                    "<span><a class='a' href='javascript:void(0)' rel='"+totalPage+"'>尾页</a></span>";
        }
        $("#pagecount").html(pageStr);
    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>