<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Inbound" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>
    .aadl{width: 210px;}
    .aadl dt {width:85px; float: left;}
</style>

<div class="container-fluid ">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">Shipmentid 不良申请处理</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Inbound/Inboundshipmentplan/shipmentIdRejectsList" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">账号</span>
                                <select type="text" name="account_id" class="form-control chosen-select" tabindex="-1" data-name-group="common">
                                    <option value="">请选择</option>
                                    <?php if(is_array($accounts)): $i = 0; $__LIST__ = $accounts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$acc): $mod = ($i % 2 );++$i;?><option value="<?php echo ($acc["id"]); ?>"><?php echo ($acc["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">Shipmentid</span>
                                <input class="form-control" type="text" value="" name="shipmentid">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input class="form-control" type="text" value="" name="sku">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">销售员</span>
                                <input class="form-control" type="text" value="" name="seller">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">状态</span>
                                <select type="text" name="status" class="form-control chosen-select" value="">
                                    <option value="">请选择</option>
                                    <?php if(is_array($status)): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$bs): $mod = ($i % 2 );++$i;?><option value="<?php echo (L("$key")); ?>"><?php echo ($bs); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-1 add-space">
                            <button type="submit" class="btn btn-info">查询</button>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <table class="table table-striped  table-hover" cellspacing="0" width="auto">
                    <thead>
                    <tr>
                        <th width="20">序号</th>
                        <th>Shipmentid</th>
                        <th>帐号</th>
                        <th>状态</th>
                        <th>SKU</th>
                        <th>SKU中文名</th>
                        <th>总需<br/>求数</th>
                        <th>不良<br/>数量</th>
                        <th>是否<br/>退税</th>
                        <th>主体</th>
                        <th>销售员</th>
                        <th>销售选择</th>
                        <th>日志</th>
                        <th>进良品仓id</th>
                        <th>进不良品仓id</th>
                        <th>备注</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($data)): $num = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($num % 2 );++$num;?><tr>
                            <td class="text-center"><input type="hidden" name="num_id" value="<?php echo ($vo["id"]); ?>"><?php echo ($num); ?></td>
                            <td><?php echo ($vo["shipmentid"]); ?></td>
                            <td><?php echo ($vo["account_name"]); ?></td>
                            <td><?php echo ($vo["rejects_status_name"]); ?></td>
                            <td><?php echo ($vo["sku"]); ?></td>
                            <td><?php echo ($vo["sku_name"]); ?></td>
                            <td class="ori_qty"><?php echo ($vo["ori_qty"]); ?></td>
                            <td class="rej_qty"><?php echo ($vo["rej_qty"]); ?></td>
                            <td><?php if($vo["export_tax_rebate"] == 1): ?>是<?php else: ?>否<?php endif; ?></td>
                            <td><?php echo ($vo["enterprise_dominant_name"]); ?></td>
                            <td><?php echo ($vo["seller_name"]); ?></td>
                            <td class="rejects_type">
                                <?php if($vo["rejects_status"] == 10): ?><select name="rejects_type" type="text" class="form-control w80">
                                        <option value="">请选择</option>
                                        <?php if(is_array($type)): $i = 0; $__LIST__ = $type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ty): $mod = ($i % 2 );++$i;?><option value="<?php echo (L("$key")); ?>"><?php echo ($ty); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                <?php else: ?>
                                    <?php echo ($vo["rejects_type_name"]); endif; ?>
                            </td>
                            <td><dl class="aadl">
                                <dt>申请人</dt><dd><?php echo ($vo["rejects_apply_user_name"]); ?></dd>
                                <dt>申请时间</dt><dd><?php echo ($vo["rejects_apply_time"]); ?></dd>
                                <dt>处理人</dt><dd><?php echo ($vo["decision_user_name"]); ?></dd>
                                <dt>处理时间</dt><dd><?php echo ($vo["decision_time"]); ?></dd>
                                <dt>实物处理人</dt><dd><?php echo ($vo["actual_do_user_name"]); ?></dd>
                                <dt>实物处理时间</dt><dd><?php echo ($vo["actual_do_time"]); ?></dd>
                            </dl></td>
                            <td class="warehouse">
                                <?php if(($vo["rejects_status"] == 20) and ($vo["rejects_type"] == 20) and ($backSaveOk == 1) and ($vo["warehouseorders_id"] == null)): ?><a class="addPosition" data-toggle="modal" data-target=".addRelation">分配储位</a>
                                    <input type="hidden" name="warehouse_id" value="">
                                <?php elseif(($vo["rejects_status"] == 20) and ($vo["rejects_type"] == 10)): ?>

                                <?php else: echo ($vo["warehouseorders_id"]); endif; ?>
                            </td>
                            <td class="unqualified">
                                <?php if(($vo["rejects_status"] == 20) and ($vo["unqualified_deal_invoices_id"] == null) and ($backSaveOk == 1)): ?><a class="badDo" data-toggle="modal" data-target="#badDo" onclick="badDo('<?php echo ($vo["id"]); ?>')">不良处理</a>
                                    <!--<input type="text" name="unqualified_deal_invoices_id" class="w80 form-control" value="">-->
                                    <?php else: echo ($vo["unqualified_deal_invoices_id"]); endif; ?>
                            </td>
                            <td><?php echo ($vo["remark"]); ?></td>
                            <td>
                                <input type="hidden" name="site_id" value="<?php echo ($vo["site_id"]); ?>">
                                <?php if(($vo["rejects_status"] == 10) and ($sellerSaveOk == 1)): ?><button type="button" class="btn btn-info btn-sm add-space" onclick="save('<?php echo ($vo["id"]); ?>',$(this))">选择确定</button>
                                <?php elseif(($vo["rejects_status"] == 20) and ($backSaveOk == 1) and ($vo["warehouseorders_id"] == null) and ($vo["rejects_type"] == 20)): ?>
                                    待分配储位
                                <?php elseif(($vo["rejects_status"] == 10)): ?>
                                    待销售操作
                                <?php elseif(($vo["rejects_status"] == 20) and ($backSaveOk == 1) and ($vo["unqualified_deal_invoices_id"] == null)): ?>
                                    待不良处理
                                <?php else: ?>
                                    无操作<?php endif; ?>

                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                </table>

                <div class="clearfix clear"></div>
                <div class="col-md-6 pull-left">
                    <label>每页 20 条记录
                        显示 1 到 20 项，共 <?php echo ($count); ?> 项</label>
                </div>
                <div class="col-md-6">
                    <div class="page pull-right" style="padding: 0;margin: 0">
                        <?php echo ($page); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--良品返仓处理-->
<div class="modal fade addRelation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">分配储位</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>储位</th>
                            <th>数量</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="addHtml">
                        <tr class="">
                            <td><input type="text" name="position[]" class="form-control" /></td>
                            <td><input type="text" name="quantity[]" class="form-control" /></td>
                            <td><a href="javascript:add();">增加</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="" name="ori_qty">
                    <input type="hidden" value="" name="rej_qty">
                    <input type="hidden" value="" name="id">
                    <button type="button" class="btn btn-primary save-position">确定</button>
                    <button type="button" class="btn btn-default " data-dismiss="modal" id="mo-close">关闭</button>
                </div>

        </div>
    </div>
</div>
<!--不良返仓处理-->
<div class="modal fade" id="badDo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >不良处理</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group form-group-lg">
                        <label class="col-sm-4 control-label text-right">储位</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="bad_position" placeholder="储位(选填">
                        </div>
                    </div>
                    <!--储位<input type="text" class="form-control" name="position" placeholder="储位">-->
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="" name="rejects_id">
                    <button type="button" class="btn btn-primary bad-position">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>

        </div>
    </div>
</div>
<script>
    $(function () {
        $("input[name='shipmentid']").val("<?php  echo $_GET['shipmentid'];?>");
        $("input[name='sku']").val("<?php  echo $_GET['sku'];?>");
        $("select[name='status']").val("<?php echo $_GET['status'];?>");
        $("select[name='account_id']").val("<?php echo $_GET['account_id'];?>");
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });
    });
    //销售选择
    function save(id,isthis) {
        var type = isthis.parent().siblings('.rejects_type').find('select').val();
        var site = isthis.parent().find('input[name="site_id"]').val();
        if(type==''){
            layer.msg('销售选择为空',{icon: 5});
            return ;
        }
        layer.load();
        $.ajax({
            url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/rejectsSellerSave",
            type: 'post',
            async: false, //default:
            data: {'id':id,'reject_type':type,'site_id':site},
            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
            success: function (data) {
                if ($.trim(data.status) == 200) {
                    layer.msg(data.msg, {icon: 6});
                    parent.location.reload();
                }else{
                    layer.msg(data.msg, {icon: 5});
                }
            }
        });

    }
    //实物返仓
    function back(id,type,isthis) {
        var warehouse_id = '';
        var unqualified_deal_invoices_id = isthis.parent().siblings('.unqualified').find('input').val();
        if(type==20){
            warehouse_id = isthis.parent().siblings('.warehouse').find('input').val();
            var ck = isthis.parent().siblings('.warehouse').children('a').text();
            if($.trim(ck)=='分配储位'){
                layer.msg('请先分配储位',{icon: 5});
                return ;
            }
            if(warehouse_id==''){
                layer.msg('请输入进良品仓id',{icon: 5});
                return ;
            }
        }
        if(unqualified_deal_invoices_id==''){
            layer.msg('请输入进不良品仓id',{icon: 5});
            return ;
        }
        layer.load();
        $.ajax({
            url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/rejectsBackSave",
            type: 'post',
            async: false, //default:
            data: {'id':id,'reject_type':type,'warehouseorders_id':warehouse_id,'unqualified_deal_invoices_id':unqualified_deal_invoices_id},
            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
            success: function (data) {
                if ($.trim(data.status) == 200) {
                    layer.msg(data.msg, {icon: 6});
                    parent.location.reload();
                }else{
                    layer.msg(data.msg, {icon: 5});
                }
            }
        });
    }
    $('.addPosition').click(function () {
        var ori_qty = parseInt($(this).parent('td').siblings('.ori_qty').text());
        var rej_qty = parseInt($(this).parent('td').siblings('.rej_qty').text());
        var id = parseInt($(this).parent('td').siblings('.text-center').find('input').val());
        $("input[name='ori_qty']").val(ori_qty);
        $("input[name='rej_qty']").val(rej_qty);
        $("input[name='id']").val(id);
    });
    //储位增加
    function add() {
        $("#addHtml").append('<tr><td><input type="text" name="position[]" class="form-control" /></td><td><input type="text" name="quantity[]" class="form-control" /></td><td><a href="javascript:add();">增加</a>  <a class="storageQtyDel" style="cursor: pointer">删除</a></td></tr>');
    }
    //储位删除
    $(document).on("click", ".storageQtyDel", function(){
        $(this).parent().parent().remove();
    });
    //良品处理
    $(document).on("click", ".save-position", function(){
        var msg = '',
            flag = '',
            qty = 0,
            position = [],
            quantity = [],
            ori_qty = $("input[name='ori_qty']").val(),
            rej_qty = $("input[name='rej_qty']").val(),
            id = $("input[name='id']").val(),
            need_qty = parseInt(ori_qty)-parseInt(rej_qty);
        $("input[name='position[]']").each(function () {
            if($(this).val()==''){
                msg +='储位不能为空<br>';
            }else{
                position.push($(this).val());
            }
        });
        $("input[name='quantity[]']").each(function () {
            if($(this).val()==''){
                msg +='数量不能为空<br>';
            }else if(isNaN($(this).val())){
                msg +='数量必须填写数字<br>';
            }
            else{
                qty += parseInt($(this).val());
            }
            quantity.push($(this).val());
        });
        if(msg!=''){
            layer.msg(msg, {icon: 5});
            return false;
        }
        if(qty>need_qty){
            layer.msg('分配数量大于实际数量', {icon: 5});
            return false;
        }
        if(qty<need_qty){
            layer.msg('分配数量小于实际数量', {icon: 5});
            return false;
        }
        $.ajax({
            url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/checkPosition",
            type: 'post',
            async: false, //default:
            data: {'position':position},
            dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
            success: function (data) {
                if ($.trim(data.status) == 200) {
                    flag = 'ok';
                }else{
                    layer.msg(data.msg, {icon: 5});
                    flag = 'no';
                }
            }
        });
        if(flag == 'no'){
            return false;
        }
        if(msg==''){
            $.ajax({
                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/rejectsBackSave",
                type: 'post',
                async: false, //default:
                data: {'id':id,'position':position,'quantity':quantity},
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data) {
                    if ($.trim(data.status) == 200) {
                        layer.msg(data.msg, {icon: 6});
                        parent.location.reload();
                    }else{
                        layer.msg(data.msg, {icon: 5});
                    }
                }
            });
        }else{
            layer.msg(msg,{icon:5});
        }
    });
    //不良赋值
    function badDo(id) {
        $("input[name='rejects_id']").val(id);
    }
    //不良处理
    $('.bad-position').click(function () {
        var id = $("input[name='rejects_id']").val();
        var position = $("input[name='bad_position']").val();
        if($.trim(id)!=''){
            $.ajax({
                url: "/fbawarehouse/index.php/Inbound/Inboundshipmentplan/badProductsSave",
                type: 'post',
                async: false, //default:
                data: {'id':id,'position':position},
                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
                success: function (data) {
                    if ($.trim(data.status) == 200) {
                        layer.msg(data.msg, {icon: 6});
                        parent.location.reload();
                    }else{
                        layer.msg(data.msg, {icon: 5});
                    }
                }
            });
        }else{
            layer.msg('参数错误', {icon: 5});
        }
    });
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>