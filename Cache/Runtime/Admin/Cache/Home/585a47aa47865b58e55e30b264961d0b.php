<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <title>登录 - <?php echo C('SITE_TITLE');?></title>
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/login/css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, 
    false); -->
    <!--function hideURLbar(){ window.scrollTo(0,1); } </script>-->
</head>
<body>
<!--     <h1>&nbsp;</h1> -->
<div class="container w3">
    <h2>亚马逊国内转运仓库(LDAP登录)</h2>
    <form>
        <div class="username">
            <span class="username">员工帐号:</span>
            <input type="text" class="name" placeholder="" required="" name="admin[account]" id="account">
            <div class="clear"></div>
        </div>
        <div class="password-agileits">
            <span class="username">员工密码:</span>
            <input type="password" name="admin[password]" id="pwd" class="password" placeholder="" required="">
            <div class="clear"></div>
        </div>
        <div class="login-w3">
            <input type="button" class="login submit" value="登录">
        </div>
        <!--  <div class="rem-for-agile">
             <input type="button" class="login findPwd" value="找回密码">
         </div> -->
        <div class="clear"></div>
        <h3><a href="http://userinfo.youkeshu.com/Auser/forgot/password/" target="_blank">忘记密码</a>|<a
                href="http://userinfo.youkeshu.com/Auser/change/password/" target="_blank">修改密码</a> | <a
                href="http://userinfo.youkeshu.com" target="_blank">修改信息</a></h3>
    </form>
</div>
<script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

<script type="text/javascript">
    //回车时默认登陆 hm
    $("body").keydown(function(event) {
        if (event.keyCode == "13") {
            $(".submit").click();
        }
    });
    $(function(){
        // 登录
        $(".submit").click(function(){
            if($("#account").val() == ''
                    || $("#pwd").val() == ''
            ){
                popup.alert("请填写帐号名和密码！");
                return false;
            }
            commonAjaxSubmit("<?php echo U('Public/dsubmit');?>");
        });

        // 找回密码
        $(".findPwd").click(function() {
            if($("#account").val() == ''){
                popup.alert("请填需要找回密码的帐号！");
                return false;
            }
            commonAjaxSubmit("<?php echo U('Public/sendFindPwdMail');?>");
        });
    });
</script>
</body>
</html>