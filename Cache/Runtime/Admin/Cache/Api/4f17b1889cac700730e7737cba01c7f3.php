<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Api" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>
    /* th, td {
         white-space: nowrap;
     }

     div.dataTables_wrapper {
         width: 100%;
         margin: 0 auto;
     }*/

    .tabliv {
        display: none;
    }
    .list-inline{width: 100%;}
    .list-inline li{float:left;width:33%; padding: 4px 20px;}

    .tabliv > table th, .tabliv > table td {
        padding: 8px;
        border-bottom: 1px solid #555;
    }
</style>
<div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">FBA库存销量表(FBA备货参数)</h3></div>
            <div class="panel-body">
                <div class="btn-group" style="float: right">
                    <button type="button" class="btn btn-default single_download" data-name="inventory">实时库存</button>
                    <button type="button" class="btn btn-default single_download" data-name="unsuppressedInventory">实时国际在途库存</button>
                    <button type="button" class="btn btn-default single_download" data-name="sale">30天销量明细</button>
                </div>
                <div>
                    <p>
                        <span style="color: #91bef0"><b>Tip:</b></span><br>
                        1.输入的SKU不能超<span style="color: red"><b>5个</b></span>,超过5个请上传文件;<br>
                        2.SKU分割的逗号要用<span style="color: red">英文  <b>','</b>  </span>符号;<br>
                        3.周销量为0的sellerSKU查不出数据;<br>
                        4.右上角三个按钮能分别导出所有账号的不同类型的数据<br>
                    </p>
                </div>

                <form id="form" action="/fbawarehouse/index.php/Api/Amazon/FbaStockingArgs/exportReport" method="POST">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="col-sm-12 col-md-5  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">SKU</span>
                                        <input class="form-control " placeholder="A301,A302,A303" name="sku"
                                               data-name-group="" type="text" value="" >
                                    </div>
                                </div>
                                <div class="col-md-4 add-space">
                                    <input id="select" class="btn btn-primary " type="button" value="搜索">
                                    <button id="up_download" class="btn btn-primary btn-success" type="button"
                                            data-toggle="modal" data-target="#btn_import">上传下载</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <hr>
                <div id="onWayStock"></div>
                <div style="min-width:1000px;max-width:100%;overflow-x:scroll">
                    <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>销售员</th>
                        <th>站点</th>
                        <th>店铺帐号</th>
                        <th>SellerSKU</th>
                        <th>ASIN</th>
                        <th>日均销量</th>
                        <th>公司SKU</th>
                        <th>品名</th>
                        <th>FBA库存</th>
                        <th>国际在途</th>
                        <th>空运在途</th>
                        <th>海运在途</th>
                        <th title='销售确认至待发货间的"国际在途"'>待发货数</th>
                        <th>销售状态</th>
                        <th>有棵树电子商务有限公司(可用库存)</th>
                        <th>有棵树电子商务有限公司(在途库存)</th>
                        <th>杭州有棵树科技有限公司(可用库存)</th>
                        <th>杭州有棵树科技有限公司(在途库存)</th>
                        <th>深圳市有棵树科技股份有限公司(可用库存)</th>
                        <th>深圳市有棵树科技股份有限公司(在途库存)</th>
                        <th>深圳市有棵树电子商务有限公司(可用库存)</th>
                        <th>深圳市有棵树电子商务有限公司(在途库存)</th>
                        <th>总可用库存</th>
                        <th>总在途库存</th>
                    </tr>
                    </thead>
                    <tbody id="pageData">
                    </tbody>
                </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!--导入文件 -->
<div class="modal fade" id="btn_import">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件<span style="color: red">（导入请使用xlsx类型的）</span></h4>
            </div>
            <form action="/fbawarehouse/index.php/Api/Amazon/FbaStockingArgs/upload_sku" method="post" enctype="multipart/form-data" id="upSku">
                <div class="modal-body form-horizontal">
                    <div class="alert alert-info top20" role="alert">
                        <p>模板下载: <a href="javascript:void(0)" id="download" >模板</a></p></div>

                    <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                        <div class="col-md-4"><input name="file" class="form-control" data-val="true"
                                                     data-val-required="请选择[上传文件]!" id="File" type="file" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <button id="submit-button" type="submit" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('select.chosen-select').chosen({
            no_results_text:'没有找到',   // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 2个以下的选择项则不显示检索框
            search_contains: true        // 从任意位置开始检索
        });
    });
</script>
<script type="text/javascript" charset="utf-8">
    $(function () {
        $("#select").click(function(){
            $('#page').val(1);
            getData();
        });

        $("#download").click(function () {
            var fileName="sku导入模板";
            var list= new Array(["公司SKU"]);
            var list1= new Array(["A301"]);
            window.location.href ="/fbawarehouse/index.php/Api/Amazon/FbaStockingArgs/skuDownload?"+"&fileName="+fileName+"&list="+list+"&list1="+list1;
        });

        $(".single_download").click(function () {
            var downloadType = $(this).data("name");
            window.location.href = "/fbawarehouse/index.php/Api/Amazon/FbaStockingArgs/singleDownload?" + "&downloadType=" + downloadType;
        })
    });

    /**
     * 获取当页数据
     */
    function getData(){

        $.ajax({
            type: 'POST',
            url: '/fbawarehouse/index.php/Api/Amazon/FbaStockingArgs/getReport',
            data: $('#form').serialize(),
            dataType:'json',
            success:function(arr){
                if (arr.count) {
                    layer.msg(arr.message, {icon : 5});
                    window.location.reload();
                    return;
                }
                var list = arr;
                var content = '';
                if(typeof(list) == "undefined"){
                    content = '';
                }
                else{
                    i=0;
                    $.each(list,function(index,array){ //遍历json数据列
                        if(!array['private_sku']) array['private_sku'] = '';
                        if(!array['item']) array['item'] = '';
                        content += "<tr>";
                        content += "<td>"+array['sellerName']+"</td>";
                        content += "<td>"+array['shorthand_code']+"</td>";
                        content += "<td>"+array['accountName']+"</td>";
                        content += "<td>"+array['sku']+"</td>";
                        content += "<td>"+array['asin']+"</td>";
                        content += "<td>"+array['daysale']+"</td>";
                        content += "<td>"+array['private_sku']+"</td>";
                        content += "<td>"+array['item']+"</td>";
                        content += "<td>"+array['inventory']+"</td>";
                        content += "<td>"+array['unsuppressedInventory']+"</td>";
                        content += "<td>"+array['air']+"</td>";
                        content += "<td>"+array['ocean']+"</td>";
                        content += "<td>"+array['homeNum']+"</td>";
                        content += "<td>"+array['saleStatus']+"</td>";
                        content += "<td>"+array['1:actual']+"</td>";
                        content += "<td>"+array['1:onway']+"</td>";
                        content += "<td>"+array['2:actual']+"</td>";
                        content += "<td>"+array['2:onway']+"</td>";
                        content += "<td>"+array['3:actual']+"</td>";
                        content += "<td>"+array['3:onway']+"</td>";
                        content += "<td>"+array['4:actual']+"</td>";
                        content += "<td>"+array['4:onway']+"</td>";
                        content += "<td>"+array['totalActual']+"</td>";
                        content += "<td>"+array['totalOnway']+"</td>";
                        content += "</tr>";
                    });
                    /*if (onWayList.length != 0) {
                        $('#onWayStock').html('');
                        onWayContent = '<table class="table table-striped  table-hover">' +
                                '<thead><tr><th width="20%">主体</th><th>实际库存</th><th>在途库存</th></tr></thead>';
                        onWayContent += '<tbody>';

                        for (var sku in onWayList)
                            for (var num in enterprise) {
                                onWayContent += "<tr>";
                                onWayContent += "<td>" + enterprise[num] + "</td>";
                                onWayContent += "<td>" + onWayList[sku]['actualStock'][num] + "</td>";
                                onWayContent += "<td>" + onWayList[sku]['onWayStock'][num] + "</td>";
                                onWayContent += "</tr>";
                            }

                        }*/

                        //onWayContent += "</tbody></<table>";

                        //$('#onWayStock').html(onWayContent);
                    }
                $('#pageData').html(content);

            },
            error:function(){
                layer.msg("数据未找到,请检查公司SKU输入是否正确!", {icon : 5});
            }
        });
    }
</script>

            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>