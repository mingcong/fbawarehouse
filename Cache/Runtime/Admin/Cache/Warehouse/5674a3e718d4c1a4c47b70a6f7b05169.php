<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">储位列表</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Warehouse/StockIn/storage_lists" method="get" class="form">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">储位编码</span>
                                <input type="text" name="storage_position" class="form-control" value="<?php echo ($_GET['storage_position']); ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">绑定sku</span>
                                <input type="text" name="bind_sku" class="form-control" value="<?php echo ($_GET['bind_sku']); ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">是否退税</span>
                                <select name="export_tax_rebate" class="form-control">
                                    <option value="">全部</option>
                                    <?php if(is_array($export_tax_rebate)): $i = 0; $__LIST__ = $export_tax_rebate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>" <?php if($_GET['export_tax_rebate']!= '' && $_GET['export_tax_rebate']== $key): ?>selected<?php endif; ?>><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">状态</span>
                                <select name="status" class="form-control">
                                    <option value="">全部</option>
                                    <option value="1" <?php if($_GET['status']!= '' && $_GET['status']== 1): ?>selected<?php endif; ?>>可用</option>
                                    <option value="0" <?php if($_GET['status']!= '' && $_GET['status']== 0): ?>selected<?php endif; ?>>禁用</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 add-space">
                            <input class="btn btn-primary" type="submit" value="查询">
                            <input class="btn btn-primary" type="reset" value="重置">
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <div class="fixed-table-toolbar">
                </div>

                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center">编号</th>
                        <th class="text-center">储位编码</th>
                        <th class="text-center">储位位置</th>
                        <th class="text-center">绑定sku</th>
                        <th class="text-center">主体</th>
                        <th class="text-center">是否退税</th>
                        <th class="text-center">状态</th>
                        <th class="text-center">备注</th>
                        <th class="text-center">创建时间</th>
                        <th class="text-center">操作</th>
                    </tr>
                    </thead>
                    <tbody id="StockIn">
                        <?php if(is_array($result)): foreach($result as $key=>$vo): ?><tr>
                            <td class="text-center"><?php echo ($vo["id"]); ?></td>
                            <td class="text-center"><?php echo ($vo["storage_position"]); ?></td>
                            <td class="text-center"><?php echo ($vo["position"]); ?></td>
                            <td class="text-center" style="word-break:break-all;"><?php echo ($vo["bind_sku"]); ?></td>
                            <td class="text-center">
                                <?php if($vo.enterprise_dominant): echo ($enterprise_dominant[$vo['enterprise_dominant']]); endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if($vo["export_tax_rebate"] == 1): ?>出口退税
                                <?php else: ?>
                                    非出口退税<?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php if($vo['status'] == 1): ?>可用
                                <?php else: ?>
                                    禁用<?php endif; ?>
                            </td>
                            <td class="text-center"><?php echo ($vo["remark"]); ?></td>
                            <td class="text-center"><?php echo ($vo["create_time"]); ?></td>
                            <td>
                                <a class="btn btn-xs btn-info" href="<?php echo U('Warehouse/StockIn/edit_storage', array('id' => $vo['id']));?>">编辑</a>
                                <button type="button" class="btn btn-xs btn-primary bind-sku" data-toggle="modal" data-sku="<?php echo ($vo["bind_sku"]); ?>" data-storage-position="<?php echo ($vo["storage_position"]); ?>" data-export-tax-rebate="<?php echo ($vo["export_tax_rebate"]); ?>" data-target=".bind-modal">绑定sku</button>
                                
                                <?php if($vo['status'] == 1): ?><a class="btn btn-xs btn-danger btn-disabled" data-id="<?php echo ($vo["id"]); ?>" data-status="0" data-storage="<?php echo ($vo["storage_position"]); ?>">禁用</a>
                                <?php else: ?>
                                    <a class="btn btn-xs btn-danger btn-disabled" data-id="<?php echo ($vo["id"]); ?>" data-status="1" data-storage="<?php echo ($vo["storage_position"]); ?>">取消禁用</a><?php endif; ?>
                            </td>
                        </tr><?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="clearfix clear"></div>
            <div class="row">
                <div class="col-md-6 pull-left">
                    <label>每页 20 条记录
                        显示 1 到 20 项，共 <?php echo ($count); ?>项</label>
                </div>
                <div class="col-md-6">
                    <div class="page pull-right" style="padding: 0;margin: 0">
                        <?php echo ($page); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 绑定sku -->
<div class="modal fade bind-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">绑定sku</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="" method="post" role="form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">储位编码：</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static p-storage-position"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">SKU：</label>
                                    <div class="col-md-9">
                                        <input type="text" name="sku" class="form-control" style="width:70%">
                                        <p class="form-control-static">注：多个sku以英文逗号分隔</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="sku_old" />
                <input type="hidden" name="export_tax_rebate" />
                <button type="button" class="btn btn-primary sure-bind" data-storage-position="">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        //禁用（取消禁用）
        $('.btn-disabled').click(function() {
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            var storage_position = $(this).attr('data-storage');
            $.ajax({
                type: "post",
                url: 'Warehouse/StockIn/set_storage_status',
                data: {
                    id: id,
                    status: status,
                    storage_position: storage_position,
                },
                dataType: "json",
                success: function (result) {
                    if (result.status) {
                        layer.msg(result.info, {icon: 6});
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    } else {
                        layer.msg(result.info, {icon: 5});
                    }
                }
            });
        });

        //绑定sku
        $('.bind-sku').click(function() {
            $('.p-storage-position').html($(this).attr('data-storage-position'));
            $('input[name="sku"]').val($(this).attr('data-sku'));
            $('input[name="sku_old"]').val($(this).attr('data-sku'));
            $('input[name="export_tax_rebate"]').val($(this).attr('data-export-tax-rebate'));
            $('.sure-bind').attr('data-storage-position',$(this).attr('data-storage-position'));
        });

        //确认绑定sku
        $('.sure-bind').click(function() {
            var storage_position = $(this).attr('data-storage-position');
            var _self = $(this);
            _self.attr('disabled', 'disabled').html('保存中...');
            $.ajax({
                type: "post",
                url: 'Warehouse/StockIn/storage_bind_sku',
                data: {
                    storage_position: storage_position, 
                    sku: $.trim($('input[name="sku"]').val()),
                    sku_old: $.trim($('input[name="sku_old"]').val()),
                    export_tax_rebate: $.trim($('input[name="export_tax_rebate"]').val())
                },
                dataType: "json",
                success: function (result) {
                    if (result.status) {
                        layer.msg(result.info, {icon: 6});
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    } else {
                        layer.msg(result.info, {icon: 5});
                    }
                    _self.removeAttr('disabled').html('确定');
                }
            });
        })
    });
</script>

            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>