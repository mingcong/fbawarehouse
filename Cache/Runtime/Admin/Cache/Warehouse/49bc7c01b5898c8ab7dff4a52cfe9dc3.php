<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">正常质检入库</h3></div>
            <div class="panel-body">

                <form action="/fbawarehouse/index.php/Warehouse/StockIn/Qc_StockIn" method="get" class="form">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-2  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">质检单</span>
                                <input type="hidden" name="qualified_quantity" value="stockin_num">
                                <input type="text" name="id" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input type="text" name="sku" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-2 add-space">
                            <input class="btn btn-primary" type="submit" value="查询">
                            <input class="btn btn-primary" type="reset" value="重置">
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <div class="fixed-table-toolbar">
                </div>

                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20"></th>
                        <th>质检单号</th>
                        <th>批次号</th>
                        <th>收获明细单号</th>
                        <th>采购单号</th>
                        <th>转运仓</th>
                        <th>运输方式</th>
                        <th>性质</th>
                        <th>主体</th>
                        <th>SKU</th>
                        <th>中文名称</th>
                        <th>供应商</th>
                        <th>合格量</th>
                        <th>不合格量</th>
                        <th>质检量</th>
                        <th>已入库量</th>
                        <th>需入库量</th>
                        <th>储位编号</th>
                        <th>质检时间</th>
                        <th>质检员</th>
                        <th>状态</th>
                        <th>目的仓</th>
                        <th>站点</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="StockIn">
                    <?php foreach($data as $_data):?>
                    <tr>
                        <td class="text-center"></td>
                        <td><?php echo $_data['id']?></td>
                        <td><?php echo $_data['batch_code']?></td>
                        <td><?php echo $_data['recieve_detail_id']?></td>
                        <td><?php echo $_data['purchaseorder_id']?></td>
                        <td><?php echo $_data['transfer_hopper_id']?></td>
                        <td><?php echo $_data['transfer_type']?></td>
                        <td><?php echo $_data['export_tax_rebate']?></td>
                        <td><?php echo $_data['enterprise_dominant']?></td>
                        <td><?php echo $_data['sku']?> </td>
                        <td><?php echo $_data['sku_name']?></td>
                        <td><?php echo $_data['supplier_id']?></td>
                        <td><?php echo $_data['qualified_quantity']?></td>
                        <td><?php echo $_data['unquality_count']?></td>
                        <td><?php echo $_data['check_quantity']?></td>
                        <td><?php echo $_data['stockin_num']?></td>
                        <td id="num" width="80"><input type="text" name="num" onblur="check_num(this)"
                                                       value="<?php echo $_data['qualified_quantity']-$_data['stockin_num']?>"
                                                       class="form-control w50 add-space"></td>
                        <td id="storage_position" width="80">
                            <!--<input type="text" name="storage_position" class="form-control w80 add-space" onkeydown="judge_is_skucname(this)">-->
                            <select name="storage_position" class="form-control w100 add-space">
                                <option value="">--请选择--</option>
                                <?php if(is_array($storage_position)): $i = 0; $__LIST__ = $storage_position;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($item); ?>"><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </td>
                        <td><?php echo $_data['invoice_date']?></td>
                        <td><?php echo $_data['check_man']?></td>
                        <td><?php echo $_data['print_status']?></td>
                        <td><?php if($_data['store']='11'){ echo '亚马逊中转仓';}?>
                            <input type="hidden" value="<?php echo $_data['store'];?>"/>
                        </td>
                        <td><?php echo $_data['site_id']?></td>
                        <td>
                            <button type="button" onclick="add_num(this)" class="btn  btn-success">新增</button>
                            <button type="button" onclick="delate_remove(this)" class="btn  btn-success add-space">删除
                            </button>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </table>
                <div class="clearfix clear"></div>

                <div class="fixed-table-toolbar">
                    <h3>
                        <button type="button" id="submit" class="btn  btn-success">确认</button>
                    </h3>
                </div>
                <div class="clearfix clear"></div>
                <div class="show" style="display: none">
                    <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>入库单</th>
                            <th>储位号</th>
                            <th>主体</th>
                            <th>出口退税</th>
                            <th>中转仓</th>
                            <th>采购单</th>
                            <th>质检单</th>
                            <th>入库日期</th>
                            <th>sku</th>
                            <th>sku中文名称</th>
                            <th>供应商</th>
                            <th>数量</th>
                            <th>价格</th>
                            <th>金额</th>
                            <th>运费</th>
                            <th>成本</th>
                            <th>入库类型</th>
                            <th>目的仓</th>
                            <th>运输方式</th>
                            <th>备注</th>
                            <th>入库人</th>
                            <th>站点</th>
                        </tr>
                        </thead>
                        <tbody class="show_detail">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("input[name=id]").val("<?php echo $_GET['id']?>");
        $("input[name=sku]").val("<?php echo $_GET['sku']?>");
    })

    var Checkqualitydetails = {

        createNew: function () {

            var checkqualitydetails = {};

            checkqualitydetails.flag = true;

            checkqualitydetails.page_data = new Array();

            checkqualitydetails.storage_position = new Array();

            checkqualitydetails.get_page_data = function () {

                $("#storage_position select").each(function (index, obj) {
                    checkqualitydetails.storage_position[index] = $.trim($(obj).val());
                });

                var num = 0;

                $("#num input").each(function (index, obj) {
                    if ($.trim($(obj).val()) == '' || $.trim($(obj).val()) <= '0') {
                        checkqualitydetails.flag = false;
                        return;
                    }
                    var a = $(obj).parent().parent();//$(element)代表每行tr，后面的children代表tr下面的td，a即这一行所有td的集合
                    checkqualitydetails.page_data[index] = {
                        'check_quality_detail_id': $.trim(a.find("td").eq(1).text()),
                        'batch_code': $.trim(a.find("td").eq(2).text()),
                        'purchaseorders_id': $.trim(a.find("td").eq(4).text()),
                        'transfer_hopper_id': $.trim(a.find("td").eq(5).text()),
                        'transfer_type': $.trim(a.find("td").eq(6).text()),
                        'export_tax_rebate': $.trim(a.find("td").eq(7).text()),
                        'enterprise_dominant': $.trim(a.find("td").eq(8).text()),
                        'sku': $.trim(a.find("td").eq(9).text()),
                        'sku_name': $.trim(a.find("td").eq(10).text()),
                        'supplier_id': $.trim(a.find("td").eq(11).text()),
                        'stockin_num': $.trim(a.find("td").eq(15).text()),
                        'quantity': $.trim($(obj).val()),
                        'storage_position': checkqualitydetails.storage_position[index],
                        'store': $.trim(a.find("td").eq(21).find("input").val()),
                        'site_id': $.trim(a.find("td").eq(22).text()),
                        'type': '10'
                    };
                    var b = $.trim($(obj).val());
                    num = parseInt(b) + parseInt(num);
                });


                //验证入库数量
                var data = {
                    'num': num,
                    'qualified_quantity': $("#num input").parent().parent().find("td").eq(12).text(),
                    'stockin_num': $("#num input").parent().parent().find("td").eq(15).text()
                };

                if (parseInt($.trim(data.num)) > (parseInt($.trim(data.qualified_quantity)) - parseInt($.trim(data.stockin_num)))) {
                    checkqualitydetails.flag = false;
                    return;
                }
                if (parseInt($.trim(data.num)) != (parseInt($.trim(data.qualified_quantity)) - parseInt($.trim(data.stockin_num)))) {
                    checkqualitydetails.flag = false;
                    return;
                }

            }


            checkqualitydetails.batch_create = function () {
                $.ajax({
                    type: "POST",
                    url: 'Warehouse/StockIn/batch_create',
                    data: {'page_data': checkqualitydetails.page_data},

                    dataType: "json",

                    success: function (data) {

                        if (data.data) {
                            checkqualitydetails.success_call_back(data.data);
                        } else {
                            checkqualitydetails.error_call_back();
                        }
                    }
                });
            }

            checkqualitydetails.success_call_back = function (data) {
                $(".show").show();
                var html = '';
                $.each(data, function (n, result) {
                    html += '<tr>' + '<td>' + data[n].id + '</td>' + '<td>' + data[n].storage_position + '</td>' + '<td>' + data[n].enterprise_dominant + '</td>' + '<td>' + data[n].transfer_hopper_id + '</td>' +
                            '<td>' + data[n].export_tax_rebate + '</td>' + '<td>' + data[n].purchaseorders_id + '</td>' +
                            '<td>' + data[n].check_quality_detail_id + '</td>' + '<td>' + data[n].op_time + '</td>' + '<td>' + data[n].sku + '</td>' + '<td>' + data[n].sku_name + '</td>' +
                            '<td>' + data[n].supplier_id + '</td>' + '<td>' + data[n].warehouse_quantity + '</td>' + '<td>' + data[n].single_price + '</td>' +
                            '<td>' + data[n].money + '</td>' + '<td>' + data[n].transportation_expense + '</td>' + '<td>' + data[n].cost + '</td>' +
                            '<td>' + data[n].type + '</td>' + '<td>' + data[n].store + '</td>' + '<td>' + data[n].transfer_type + '</td>' +
                            '<td>' + data[n].remark + '</td>' + '<td>' + data[n].warehouse_man + '</td>' + '<td>' + data[n].site_id + '</td>'
                            '</tr>';
                });
                $(".show_detail").html(html);
                setTimeout(function () {
                    layer.msg("入库成功");
                }, 5000);
                top.window.location.href = 'Warehouse/StockIn/Qc_StockIn?qualified_quantity=stockin_num&id=' + $("input[name=id]").val();
                return false;
            }

            checkqualitydetails.error_call_back = function () {
                popup.error("新建失败");
                setTimeout(function () {
                    popup.close("asyncbox_error");
                }, 2000);
            }
            return checkqualitydetails;
        }

    };

    var StockIn = {

        createNew: function () {

            var StockIn = Checkqualitydetails.createNew();

            StockIn.submit_page_data = function () {
                StockIn.get_page_data();

                if (!StockIn.flag) {
                    popup.error("入库量不等于合格量减已入库量的总和");
                    return false;
                }
                $.ajax({
                    type: "post",
                    url: 'Warehouse/StockIn/Judge_is_sku_cname',
                    data: {
                        'check_quality_detail_id': $.trim($("#StockIn tr").parent().parent().find("td").eq(1).text()),
                        'sku': $.trim($("#StockIn tr").parent().parent().find("td").eq(9).text()),
                        'storage_position': StockIn.storage_position
                    },
                    dataType: "json",
                    success: function (result) {
                        if (result.flag == false) {
                            popup.error("该sku没有绑定此储位或是该质检单已入库");
                            setTimeout(function () {
                                popup.close("asyncbox_error");
                            }, 2000);
                            return false;
                        } else {
                            StockIn.batch_create();
                        }
                    }
                });
            }


            return StockIn;
        }
    };

    $("#submit").click(function () {
        $('#submit').attr('disabled',"true");
        StockIn.createNew().submit_page_data();
    });

    function judge_is_skucname(obj) {
        if ($(obj).val() && $(obj).parent().parent().find("td").eq(9).text()) {
            $.ajax({
                type: "post",
                url: 'Warehouse/StockIn/Judge_is_skucname',
                data: {
                    'sku': $.trim($(obj).parent().parent().find("td").eq(9).text()),
                    'storage_position': $.trim($(obj).val())
                },
                dataType: "json",
                success: function (result) {
                    if (result.flag == false) {
                        popup.error("该sku没有绑定此储位");
                        setTimeout(function () {
                            popup.close("asyncbox_error");
                        }, 2000);
                        return false;
                    }
                }
            });
        } else {
            layer.msg('储位为空', {offset: 250});
            return false;
        }
    }
    ;
    //验证入库数是否大于合格量
    function check_num(obj) {

        if ($.trim($(obj).val()) == '' || $.trim($(obj).val()) <= '0') {
            popup.error("入库数量有问题");
            return false;
        }

        var num = 0;

        $("#num input").each(function (index, obj) {
            var a = $.trim($(obj).val());
            num = parseInt(a) + parseInt(num);
        });

        var data = {
            'num': num,
            'qualified_quantity': $(obj).parent().parent().find("td").eq(12).text(),
            'stockin_num': $(obj).parent().parent().find("td").eq(15).text()
        };

        if (parseInt($.trim(data.num)) > (parseInt($.trim(data.qualified_quantity)) - parseInt($.trim(data.stockin_num)))) {
            popup.error("入库数量大于合格量减已入库量的总和")
            return false;
        }
    }
    //添加
    function add_num() {
        $("#num").append('<input type="text"  name="num" id="add_num" onblur="check_num(this)" class="form-control w50 add-space">');
        //$("#storage_position").append('<input type="text"  name="storage_position" id="add_storage_position" class="form-control w80 add-space" onkeydown="judge_is_skucname(this)">');
        $("#storage_position").append('<select id="add_storage_position" name="storage_position" class="form-control w100 add-space"> <option value="">--请选择--</option> <?php if(is_array($storage_position)): $i = 0; $__LIST__ = $storage_position;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($item); ?>"><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?> </select>');

    }
    //删除
    function delate_remove() {
        $("#add_num").remove();
        $("#add_storage_position").remove();
    }


</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>