<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">FBA库存数据监控</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Warehouse/Inventory/fbaInventory" method="get" id="form" onsubmit="return check()">
                    <div class="container-fluid">

                        <div class="col-sm-12 col-md-6  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">日期</span>
                                <input class="form-control form-date_1" type="text" name="claim_arrive_time_from" value="<?php echo ($_GET['claim_arrive_time_from']); ?>" style="cursor:pointer;" readonly>
                                <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                <input class="form-control form-date_1" type="text" name="claim_arrive_time_to" value="<?php echo ($_GET['claim_arrive_time_to']); ?>" style="cursor:pointer;" readonly>
                                <span class="input-group-addon fix-border fix-padding">&nbsp;<span class="text-danger">0</span>天</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 add-space">
                        <input class="btn btn-primary" type="submit" value="搜索">
                        <input class="btn btn-default" type="reset" value="重置">
                        <input class="btn btn-warning" type="button" onclick="downloadFba()" value="导出明细">
                    </div>
                </form>
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr style="align-content: center">
                        <th></th>
                        <th colspan="3" class="text-center">采购在途</th>
                        <th colspan="3" class="text-center">中转仓</th>
                        <th colspan="3" class="text-center">国际运输在途</th>
                        <th colspan="3" class="text-center">各站点库存汇总</th>
                        <th colspan="2" class="text-center">合计</th>
                    </tr>
                    <tr>
                        <th>日期</th>
                        <th>sku款数</th>
                        <th>PCS</th>
                        <th>金额(元)</th>
                        <th>sku款数</th>
                        <th>PCS</th>
                        <th>金额(元)</th>
                        <th>sku款数</th>
                        <th>PCS</th>
                        <th>金额(元)</th>
                        <th>sku款数</th>
                        <th>PCS</th>
                        <th>金额(元)</th>
                        <th>总PCS</th>
                        <th>总金额(元)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><tr>
                            <td><?php echo ($val["date"]); ?></td>
                            <td><?php echo ($val["cg_sku"]); ?></td>
                            <td><?php echo ($val["cg_pcs"]); ?></td>
                            <td><?php echo ($val["cg_money"]); ?></td>
                            <td><?php echo ($val["zzc_sku"]); ?></td>
                            <td><?php echo ($val["zzc_pcs"]); ?></td>
                            <td><?php echo ($val["zzc_money"]); ?></td>
                            <td><?php echo ($val["gjzt_sku"]); ?></td>
                            <td><?php echo ($val["gjzt_pcs"]); ?></td>
                            <td><?php echo ($val["gjzt_money"]); ?></td>
                            <td><?php echo ($val["hz_sku"]); ?></td>
                            <td><?php echo ($val["hz_pcs"]); ?></td>
                            <td><?php echo ($val["hz_money"]); ?></td>
                            <td><?php echo ($val["all_pcs"]); ?></td>
                            <td><?php echo ($val["all_money"]); ?></td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                </table>
                <div class="row">
                    <div class="col-md-6" id="container1" style="border:1px solid #ccc; margin-bottom:20px;"></div>
                    <div class="col-md-6" id="container2" style="border:1px solid #ccc; margin-bottom:20px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/fbawarehouse/Public/yksui/js/chart/highcharts.js"></script>
<script>
    $('.form-date_1').datetimepicker({
        lang:'ch',
        timepicker:false,
        format:'Y-m-d',
        formatDate:'Y-m-d'
    });
    function check() {
        var start_time = Date.parse($("input[name=claim_arrive_time_from]").val());
        var end_time   = Date.parse($("input[name=claim_arrive_time_to]").val());
        var diff       = end_time-start_time;                         //时间差的毫秒数
        var days=Math.floor(diff/(24*3600*1000));
        if(days >=0){
            return true;
        }else if($("input[name=claim_arrive_time_from]").val()=='' && $("input[name=claim_arrive_time_to]").val()==''){
            return true;
        }else{
            layer.msg('日期填写错误', {icon: 5});
            return false;
        }

    }
    function downloadFba() {
        var data = $('#form').serialize();
        window.location.href="/fbawarehouse/index.php/Warehouse/Inventory/fbaInventory?down=ok&"+data;
    }
    $(function () {
        var url = $('#form').serialize();
        $.get("/fbawarehouse/index.php/Warehouse/Inventory/fbaInventory?api=ok&"+url,
            function(data){
                allChart = eval("("+data+")");
                all_day           = allChart.dateDay;
                report_money      = allChart.all_money;
                report_pcs        = allChart.all_quantity;
                highChart();
            });
    });
    function highChart() {
        $('#container1').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: '总金额曲线图(元)'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: all_day
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '￥{value}',
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: '#89A54E'
                    }
                }
            }],
            tooltip: {
                shared: true
            },
            series: [ {
                name: '总金额',
                color: '#89A54E',
                type: 'spline',
                data: report_money,
                tooltip: {
                    valueSuffix: '元'
                },
                dataLabels: {
                    enabled: false,
                    rotation: -45,
                    color: '#000000'
                }
            }]
        });
        $('#container2').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: '总PCS曲线图'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: all_day
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: '#89A54E'
                    }
                }
            }],
            tooltip: {
                shared: true
            },
            series: [ {
                name: '总PCS',
                color: '#89A54E',
                type: 'spline',
                data: report_pcs,
                tooltip: {
                    valueSuffix: ''
                },
                dataLabels: {
                    enabled: false,
                    rotation: -45,
                    color: '#000000'
                }
            }]
        });
    }
</script>






            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>