<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading">
                <h3 class="panel-title">单个sku库存查询</h3>
            </div>
            <div class="panel-body">

                <form action="/fbawarehouse/index.php/Warehouse/Inventory/index" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">请输入SKU号</span>
                                <input class="form-control" type="text" name="sku" onkeyup="this.value=trim(this.value)"
                                       value="<?php echo ((isset($_GET['sku']) && ($_GET['sku'] !== ""))?($_GET['sku']):''); ?>">
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">所属公司主体</span>
                                <select name="enterprise_dominant" id="enterprise_dominant" class="form-control">
                                    <option value="">--请选择--</option>
                                    <?php if(is_array($enterprise_dominant)): $num = 0; $__LIST__ = $enterprise_dominant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($num % 2 );++$num;?><option value="<?php echo ($num); ?>"><?php echo ($val); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">站点</span>
                                <select name="site_id" id="site_id" class="form-control">
                                    <option value="">--请选择--</option>
                                    <?php if(is_array($sites)): $i = 0; $__LIST__ = $sites;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($value); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <input class="btn btn-primary" type="button" id="serach" value="查询">
                            <input class="btn btn-primary" type="button" onclick="reset()" value="重置">
                        </div>

                    </div>
                </form>
                <div class="clearfix"></div>
                <div id="pageContent" class="well">

                    <div id="content">
                        <h4>SKU状态</h4>
                        <hr/>
                        <div class="col-md-3">
                            <span>在售状态：</span>
                            <span class="xia"><?php echo ((isset($skuStatusAndPlace['status']) && ($skuStatusAndPlace['status'] !== ""))?($skuStatusAndPlace['status']):''); ?></span>
                        </div>
                        <div>
                            <span>sku主仓库：</span>
                            <span class="xia"><?php echo ((isset($skuStatusAndPlace['location']) && ($skuStatusAndPlace['location'] !== ""))?($skuStatusAndPlace['location']):''); ?></span>
                        </div>

                        <hr>
                        <h4>储位库存信息</h4>
                        <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <th>sku</th>
                                <th>中文名称</th>
                                <th>是否出口退税</th>
                                <th>储位</th>
                                <th>可用库存</th>
                            </tr>
                            <tbody>
                            <?php foreach($skuStorageInventories as $_skuStorageInventories) { ?>
                            <tr>
                                <td><?php echo ((isset($_skuStorageInventories["sku"]) && ($_skuStorageInventories["sku"] !== ""))?($_skuStorageInventories["sku"]):''); ?></td>
                                <td><?php echo ((isset($_skuStorageInventories["sku_name"]) && ($_skuStorageInventories["sku_name"] !== ""))?($_skuStorageInventories["sku_name"]):''); ?></td>
                                <td><?php  switch($_skuStorageInventories['export_tax_rebate']) { case '0': echo '非出口退税'; break; case '1': echo '1出口退税'; break; } ?>
                                </td>
                                <td><?php echo ((isset($_skuStorageInventories["storage_position"]) && ($_skuStorageInventories["storage_position"] !== ""))?($_skuStorageInventories["storage_position"]):''); ?></td>
                                <td><?php echo ((isset($_skuStorageInventories["all_available_quantity"]) && ($_skuStorageInventories["all_available_quantity"] !== ""))?($_skuStorageInventories["all_available_quantity"]):0); ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                        <hr>
                        <h4>非出口退税库存明细</h4>
                        <hr/>
                        <div class="col-md-3">
                            <span>期初库存数量：</span>
                            <span class="xia"><?php echo ((isset($stocks["open_stock"]) && ($stocks["open_stock"] !== ""))?($stocks["open_stock"]):0); ?></span>
                        </div>
                        <div class="col-md-3">
                            <span>本月入库数量：</span>
                            <span class="xia"><?php echo ((isset($stocks["wo_num"]) && ($stocks["wo_num"] !== ""))?($stocks["wo_num"]):0); ?></span>
                        </div>
                        <div class="col-md-3">
                            <span>本月出库数量：</span>
                            <span class="xia"><?php echo ((isset($stocks["do_num"]) && ($stocks["do_num"] !== ""))?($stocks["do_num"]):0); ?></span>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>
                        <div>
                            <span>实际库存数量(期初库存数量+本月入库数量-本月出库数量)：</span>
                            <span class="xia"><?php echo ((isset($stocks["stock"]) && ($stocks["stock"] !== ""))?($stocks["stock"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>总在途数量（已采购未入库的数量,未完结的采购量-入库量）：</span>
                            <span class="xia"><?php echo ((isset($stocks["onway_num"]) && ($stocks["onway_num"] !== ""))?($stocks["onway_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>总未发数量（订单物品新录入、已通过、已打印、缺货减去未发货已扫描出库的数量）：</span>
                            <span class="xia"><?php echo ((isset($stocks["order_occupy_num"]) && ($stocks["order_occupy_num"] !== ""))?($stocks["order_occupy_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>库存可用数量（实际库存数量-总未发量）：</span>
                            <span class="xia"><?php echo ((isset($stocks["actual_available_num"]) && ($stocks["actual_available_num"] !== ""))?($stocks["actual_available_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>总可用数量（库存可用数量+总在途数量）：</span>
                            <span class="xia"><?php echo ((isset($stocks["all_available_num"]) && ($stocks["all_available_num"] !== ""))?($stocks["all_available_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <h4>当月的库存数量及成本明细</h4>
                        <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <th rowspan="2">sku</th>
                                <th rowspan="2">中文名称</th>
                                <th rowspan="2">规格</th>
                                <th colspan="2">期初</th>
                                <th colspan="2">本期入</th>
                                <th colspan="2">本期出</th>
                                <th colspan="2">期末结存</th>
                            </tr>
                            <tr>
                                <th>数量</th>
                                <th>成本</th>
                                <th>数量</th>
                                <th>成本</th>
                                <th>数量</th>
                                <th>成本</th>
                                <th>数量</th>
                                <th>成本</th>
                            </tr>
                            <tr>
                                <td><?php echo ((isset($stocks["sku"]) && ($stocks["sku"] !== ""))?($stocks["sku"]):'SKU'); ?></td>
                                <td><?php echo ((isset($stocks["sku_name"]) && ($stocks["sku_name"] !== ""))?($stocks["sku_name"]):'SKU中文名'); ?></td>
                                <td><?php echo ((isset($stocks["sku_standard"]) && ($stocks["sku_standard"] !== ""))?($stocks["sku_standard"]):''); ?></td>
                                <td><?php echo ((isset($stocks["open_stock"]) && ($stocks["open_stock"] !== ""))?($stocks["open_stock"]):0); ?></td>
                                <td><?php echo ((isset($stocks["open_cost"]) && ($stocks["open_cost"] !== ""))?($stocks["open_cost"]):0); ?></td>
                                <td><?php echo ((isset($stocks["wo_num"]) && ($stocks["wo_num"] !== ""))?($stocks["wo_num"]):0); ?></td>
                                <td><?php echo ((isset($stocks["wo_cost"]) && ($stocks["wo_cost"] !== ""))?($stocks["wo_cost"]):0); ?></td>
                                <td><?php echo ((isset($stocks["do_num"]) && ($stocks["do_num"] !== ""))?($stocks["do_num"]):0); ?></td>
                                <td><?php echo ($stocks["do_cost"]); ?></td>
                                <td><?php echo ($stocks['open_stock']+$stocks['wo_num']-$stocks['do_num']); ?></td>
                                <td><?php echo ($stocks['open_cost']+$stocks['wo_cost']-$stocks['do_cost']); ?></td>
                            </tr>
                            </tbody>
                        </table>

                        <br>
                        <br>
                        <hr>
                        <h4>出口退税库存明细</h4>
                        <hr/>
                        <div class="col-md-3">
                            <span>期初库存数量：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["open_stock"]) && ($taxstocks["open_stock"] !== ""))?($taxstocks["open_stock"]):0); ?></span>
                        </div>
                        <div class="col-md-3">
                            <span>本月入库数量：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["wo_num"]) && ($taxstocks["wo_num"] !== ""))?($taxstocks["wo_num"]):0); ?></span>
                        </div>
                        <div class="col-md-3">
                            <span>本月出库数量：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["do_num"]) && ($taxstocks["do_num"] !== ""))?($taxstocks["do_num"]):0); ?></span>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>
                        <div>
                            <span>实际库存数量(期初库存数量+本月入库数量-本月出库数量)：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["stock"]) && ($taxstocks["stock"] !== ""))?($taxstocks["stock"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>总在途数量（已采购未入库的数量,未完结的采购量-入库量）：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["onway_num"]) && ($taxstocks["onway_num"] !== ""))?($taxstocks["onway_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>总未发数量（订单物品新录入、已通过、已打印、缺货减去未发货已扫描出库的数量）：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["order_occupy_num"]) && ($taxstocks["order_occupy_num"] !== ""))?($taxstocks["order_occupy_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>库存可用数量（实际库存数量-总未发量）：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["actual_available_num"]) && ($taxstocks["actual_available_num"] !== ""))?($taxstocks["actual_available_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <div>
                            <span>总可用数量（库存可用数量+总在途数量）：</span>
                            <span class="xia"><?php echo ((isset($taxstocks["all_available_num"]) && ($taxstocks["all_available_num"] !== ""))?($taxstocks["all_available_num"]):0); ?></span>
                        </div>
                        <hr/>
                        <h4>当月的库存数量及成本明细</h4>
                        <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <th rowspan="2">sku</th>
                                <th rowspan="2">中文名称</th>
                                <th rowspan="2">规格</th>
                                <th colspan="2">期初</th>
                                <th colspan="2">本期入</th>
                                <th colspan="2">本期出</th>
                                <th colspan="2">期末结存</th>
                            </tr>
                            <tr>
                                <th>数量</th>
                                <th>成本</th>
                                <th>数量</th>
                                <th>成本</th>
                                <th>数量</th>
                                <th>成本</th>
                                <th>数量</th>
                                <th>成本</th>
                            </tr>
                            <tr>
                                <td><?php echo ((isset($taxstocks["sku"]) && ($taxstocks["sku"] !== ""))?($taxstocks["sku"]):'SKU'); ?></td>
                                <td><?php echo ((isset($taxstocks["sku_name"]) && ($taxstocks["sku_name"] !== ""))?($taxstocks["sku_name"]):'SKU中文名'); ?></td>
                                <td><?php echo ((isset($taxstocks["sku_standard"]) && ($taxstocks["sku_standard"] !== ""))?($taxstocks["sku_standard"]):''); ?></td>
                                <td><?php echo ((isset($taxstocks["open_stock"]) && ($taxstocks["open_stock"] !== ""))?($taxstocks["open_stock"]):0); ?></td>
                                <td><?php echo ((isset($taxstocks["open_cost"]) && ($taxstocks["open_cost"] !== ""))?($taxstocks["open_cost"]):0); ?></td>
                                <td><?php echo ((isset($taxstocks["wo_num"]) && ($taxstocks["wo_num"] !== ""))?($taxstocks["wo_num"]):0); ?></td>
                                <td><?php echo ((isset($taxstocks["wo_cost"]) && ($taxstocks["wo_cost"] !== ""))?($taxstocks["wo_cost"]):0); ?></td>
                                <td><?php echo ((isset($taxstocks["do_num"]) && ($taxstocks["do_num"] !== ""))?($taxstocks["do_num"]):0); ?></td>
                                <td><?php echo ((isset($taxstocks["do_cost"]) && ($taxstocks["do_cost"] !== ""))?($taxstocks["do_cost"]):0); ?></td>
                                <td><?php echo ($taxstocks['open_stock']+$taxstocks['wo_num']-$taxstocks['do_num']); ?></td>
                                <td><?php echo ($taxstocks['open_cost']+$taxstocks['wo_cost']-$taxstocks['do_cost']); ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    /**
     * 去掉字符串左右两侧的空格
     * @param str
     * @returns {void|string|XML}
     */
    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }

    $(function () {
        $("select[name='enterprise_dominant']").val("<?php  echo $_GET['enterprise_dominant'];?>");
        $("select[name='site_id']").val("<?php  echo $_GET['site_id'];?>");

    });

    /**
     * 下载
     */
    $("#serach").click(function () {
        var param = '';
        var sku = $("input[name='sku']").val();
        var enterprise_dominant = $("select[name='enterprise_dominant']").val();
        var site_id = $("select[name='site_id']").val();
        if ($.trim(sku) != '') {
            param += "&sku=" + sku;
        }
        if ($.trim(enterprise_dominant) != '') {
            param += "&enterprise_dominant=" + $.trim(enterprise_dominant);
        }
        if ($.trim(site_id) != '') {
            param += "&site_id=" + $.trim(site_id);
        }
        if(sku==''){
            layer.msg('请输入sku',{'icon':'5'});return;
        }
        if(enterprise_dominant==''){
            layer.msg('请选择主体',{'icon':'5'});return;
        }
        if(site_id==''){
            layer.msg('请选择站点',{'icon':'5'});return;
        }
        if (param != '') {
            param = '?' + param.substr(1);
        }
        window.location.href = '/fbawarehouse/index.php/Warehouse/Inventory/index' + param;
    });

    /**
     * 重置
     * */
    function reset() {
        $('#form_search').find('input').val('');
        $('#form_search').find('select').val('');
    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>