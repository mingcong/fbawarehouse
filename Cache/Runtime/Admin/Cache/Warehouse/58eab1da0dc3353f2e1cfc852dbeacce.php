<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">SKU质检标准确认</h3></div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="col-sm-12 col-md-3  add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">批次号</span>
                            <input class="form-control" type="text" name="batchCode" id="batchCode"
                                   onkeyup="this.value=this.value.replace(/\s/g,'')">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3  add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">确认状态</span>
                            <select type="text" name="skuCheckSign" class="form-control" id="skuCheckSign">
                                <option value="">请选择</option>
                                <option value="10">待确认</option>
                                <option value="-1">无需确认</option>
                                <option value="20">已确认</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-3 add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">收货明细号</span>
                            <input type="text" class="form-control" name="recieveDetailsId" id="recieveDetailsId"
                                   onkeyup="this.value=this.value.replace(/\D/g,'')"/>
                        </div>
                    </div>
                    <div class="col-md-2 add-space">
                        <input class="btn btn-primary" type="button" value="查询" onclick="getInfo()">
                    </div>


                </div>
                <div class="clearfix"></div>
                <hr>
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20"><input id="checkAll" type="checkbox"></th>
                        <th>站点</th>
                        <th>批次号</th>
                        <th>收货明细单号</th>
                        <th>收货人</th>
                        <th>采购单号</th>
                        <th>退税类型</th>
                        <th>采购员</th>
                        <th>SKU</th>
                        <th>采购名称</th>
                        <th>质检标准</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="txtHint">
                    </tbody>
                </table>
                <div class="clearfix clear"></div>
                <div class="col-md-6 pull-left">
                    <button class="btn btn-info btn-sm" id="patchcheck" onclick="batchProcessing()">批量确认</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    /**
     * 根据收货明细单号获取详情
     */
    function getInfo() {
        var recieveDetailsId = $("#recieveDetailsId").val();
        var batchCode = $("#batchCode").val();
        var skuCheckSign = $("#skuCheckSign option:selected").val();

        if ('' == recieveDetailsId && '' == batchCode) {
            layer.msg('请填入收货明细单号或者批次号',{'icon':'5'});
            return;
        }

        $.ajax({
            url: "/fbawarehouse/index.php/Warehouse/CheckOrders/check_standard_index",
            data: {'recieveDetailsId': recieveDetailsId, 'skuCheckSign': skuCheckSign, 'batchCode': batchCode},
            type: 'get',
            dataType: 'json',
            async: true,

            success: function (data) {
                if (false == data) {
                    layer.msg('该收货明细单不存在!',{'icon':'5'});
                    $("#txtHint").html('<tr></tr>');
                    return;
                }

                var obj = (new Function('return ' + data + ';'))();
                var strTem = '';
                var button = '';

                for (key in obj['result']) {
                    if ('0' == obj['result'][key]['skuchecksign']) {
                        button = '<span>无需确认</span>';
                    } else if (10 == obj['result'][key]['skuchecksign']) {
                        button = '<button type="button" class="btn btn-info btn-sm" id="check' + obj['result'][key]['id'] + '" ' +
                                'onclick="checkSingleSkuStandard(' + obj['result'][key]['id'] + ')">质标确认</button>';
                    } else if (20 == obj['result'][key]['skuchecksign']) {
                        button = '<button type="button" class="btn btn-access btn-sm">已确认</button>';
                    }
                    if (null == obj['result'][key]['standard']) obj['result'][key]['standard'] = '';
                    strTem += '<tr>' +
                            '<td class="text-center"><input name="subBox" type="checkbox" value="' + obj['result'][key]['id'] + '"></td>' +
                            '<td>' + obj['result'][key]['site_id'] + '</td>' +
                            '<td>' + obj['result'][key]['batch_code'] + '</td>' +
                            '<td>' + obj['result'][key]['id'] + '</td>' +
                            '<td>' + obj['users'][obj['result'][key]['recieve_man']] + '</td>' +
                            '<td>' + obj['result'][key]['purchaseorder_id'] + '</td>' +
                            '<td>' + obj['rebate'][obj['result'][key]['export_tax_rebate']] + '</td>' +
                            '<td>' + obj['users'][obj['result'][key]['purchase_id']] + '</td>' +
                            '<td>' + obj['result'][key]['sku'] + '</td>' +
                            '<td>' + obj['result'][key]['sku_name'] + '</td>' +
                            '<td>' + obj['result'][key]['standard'] + '</td>' +
                            '<td>' + button + '</td>' +
                            '</tr>';
                }
                $("#txtHint").html(strTem); //结果展示到id为txtHint的标签中

            },

            error: function () {
                layer.msg('查询失败,请检查输入,无法解决请找IT',{'icon':'5'});
            }
        });

    }

    /**
     * sku质量标准确认
     */
    function checkSingleSkuStandard(recieveDetailId) {
        $.ajax({
            url: '/fbawarehouse/index.php/Warehouse/CheckOrders/checkSkuStandard',
            data: {'recieveDetailId': recieveDetailId},
            type: 'post',
            dataType: 'json',
            async: true,

            beforeSend:function(){
                //请求前的处理
                $('#check'+recieveDetailId).text('正在确认...');
                $('#check'+recieveDetailId).attr('disabled','true');
            },

            success: function (data) {
                layer.msg(data,{'icon':'6'});
                getInfo();
            },

            error: function () {
                layer.msg('审核失败,请重试,无法解决请找IT',{'icon':'5'});
                getInfo();
            }
        });
    }

    /**
     * 批量确认质检标准
     */
    function batchProcessing() {
        var allChecked = new Array;
        $('input[name="subBox"]:checked').each(function (i) {
            allChecked[i] = $(this).val();
        });

        var ids = allChecked.join('-');
        if ('' == ids) {
            layer.msg('请先选择!',{'icon':'5'});
            return;
        }

        $.ajax({
            url: '/fbawarehouse/index.php/Warehouse/CheckOrders/checkSkuStandard',
            data: {'recieveDetailId': ids},
            type: 'post',
            dataType: 'json',
            async: true,

            beforeSend:function(){
                //请求前的处理
                $('#patchcheck').text('正在确认...');
                $('#patchcheck').attr('disabled','true');
            },

            success: function (data) {
                layer.msg(data,{'icon':'6'});
                getInfo();
                $('#patchcheck').text('批量确认');
                $('#patchcheck').removeAttr('disabled');
            },

            error: function () {
                layer.msg('审核失败,请重试,无法解决请找IT',{'icon':'5'});
                getInfo();
                $('#patchcheck').text('批量确认');
                $('#patchcheck').removeAttr('disabled');
            }
        })
    }

    /**
     * 去掉字符串左右两侧的空格
     * @param str
     * @returns {void|string|XML}
     */
    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>