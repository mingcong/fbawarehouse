<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">质检明细报表</h3></div>
            <div class="panel-body">
                <form id="form_search" action="/fbawarehouse/index.php/Warehouse/CheckOrders/check_orders_details_report" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-6  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">质检日期</span>
                                <input class="form-control form-datetime" type="text" name="create_time_from"
                                       value="<?php echo isset($_GET['create_time_from'])?$_GET['create_time_from']:''; ?>"
                                       style="cursor:pointer;" id="create_time_from" readonly>
                                <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                <input class="form-control form-datetime" type="text" name="create_time_to"
                                       value="<?php echo isset($_GET['create_time_to'])?$_GET['create_time_to']:''; ?>"
                                       style="cursor:pointer;" id="create_time_to" readonly>

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">是否是出口退税</span>
                                <select name="export_tax_rebate" id="export_tax_rebate" class="form-control">
                                    <option value="">全部</option>
                                    <option value="-1" <?php if(isset($_GET['export_tax_rebate']) and '-1' == $_GET['export_tax_rebate']) echo 'selected'; ?>
                                    >非出口退税
                                    </option>
                                    <option value="1" <?php if(isset($_GET['export_tax_rebate']) and '1' == $_GET['export_tax_rebate']) echo 'selected'; ?>
                                    >出口退税
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">采购单号</span>
                                <input type="text" name="purchaseorder_id" class="form-control" onkeyup="this.value=trim(this.value)"
                                       value="<?php echo ($_GET['purchaseorder_id']); ?>"
                                       id="purchaseorder_id"
                                >

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input type="text" name="sku" class="form-control" onkeyup="this.value=trim(this.value)"
                                       value="<?php echo isset($_GET['sku'])?$_GET['sku']:'';?>"
                                       id="sku"
                                >

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">所属公司主体</span>
                                <select name="enterprise_dominant" id="enterprise_dominant" class="form-control">
                                    <option value="">全部</option>
                                    <option value="1" <?php if(isset($_GET['enterprise_dominant']) and '1' == $_GET['enterprise_dominant']) echo 'selected'; ?>
                                    >有棵树电子商务有限公司</option>
                                    <option value="2" <?php if(isset($_GET['enterprise_dominant']) and '2' == $_GET['enterprise_dominant']) echo 'selected'; ?>
                                    >杭州有棵树科技有限公司</option>
                                    <option value="3" <?php if(isset($_GET['enterprise_dominant']) and '3' == $_GET['enterprise_dominant']) echo 'selected'; ?>
                                    >深圳市有棵树科技股份有限公司</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">供应商</span>
                                <input type="text" name="supplier_id" class="form-control" oninput="searchSupplier(this.value)"
                                       value="<?php echo isset($_GET['supplier_id'])?$_GET['supplier_id']:''; ?>"
                                       id="supplier_id"
                                >
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">批次号</span>
                                <input type="text" name="batch_code" class="form-control" value="">
                            </div>
                        </div>

                        <div class="col-md-3 add-space">
                            <input class="btn btn-primary" type="submit" value="查询">
                            <input class="btn btn-primary" type="button" onclick="reset()" value="重置">
                            <input class="btn btn-success" type="button" onclick="down_csv()" value="导出查询结果">
                        </div>

                    </div>
                </form>
                <input type="hidden" name="" value="" id="supper_check">
                <div class="clearfix"></div>
                <hr>

                <table class="table table-bordered" id="table_content" style="border-collapse:collapse;">
                    <thead>
                    <tr>

                        
                        <th >序号</th>
                        <th >质检日期</th>
                        <th>采购单号</th>
                        <th>站点</th>
                        <th>公司主体</th>
                        <th>是否出口退税</th>
                        <th>收货明细单号</th>
                        <th>SKU</th>
                        <th >采购名称</th>
                        <th>报关品名</th>
                        <th>储位</th>
                        <th>规格</th>
                        <th>供应商</th>
                        <th>采购数量</th>
                        <th>质检数量</th>
                        <th>合格数量</th>
                        <th>不合格数量</th>
                        <th>良品率</th>
                        <th width="3%">所属仓库</th>
                        <th>质检标准</th>
                        <th>质检员</th>
                    </tr>
                    </thead>
                    <tbody id="div_content">
                    <?php
 $i=0; foreach($data as $key=>$_data) { ++$i; ?>
                    <tr>
                        <td><?php echo ($i); ?></td>
                        <td><?php echo ($_data["invoice_date"]); ?></td>
                        <td><?php echo ($_data["purchaseorder_id"]); ?></td>
                        <td><?php echo ($_data["site_id"]); ?></td>
                        <td><?php echo ($dominant[$_data['enterprise_dominant']]); ?></td>
                        <td><?php echo ($rebate[$_data['export_tax_rebate']]); ?></td>
                        <td><?php echo ($_data["recieve_detail_id"]); ?></td>
                        <td><?php echo ($_data["sku"]); ?></td>
                        <td><?php echo ($_data["sku_name"]); ?></td>
                        <td><?php echo ($_data["item"]); ?></td>
                        <td><?php echo ($_data["storage_position"]); ?></td>
                        <td><?php echo ($_data["sku_standard"]); ?></td>
                        <td><?php echo ($suppliers[$_data['supplier_id']]); ?></td>
                        <td><?php echo ($_data["quantity"]); ?></td>
                        <td><?php echo ($_data["check_quantity"]); ?></td>
                        <td><?php echo ($_data["qualified_quantity"]); ?></td>
                        <td><?php echo ($_data["unquality_count"]); ?></td>
                        <td><?php echo (round($_data['qualified_quantity']/$_data['check_quantity']*100 ,2)); ?>%</td>
                        <td><?php echo ($_data['transfer_hopper_name']); ?></td>
                        <td title="<?php echo ($_data['check_type']); ?>"><?php echo mb_substr($_data['check_type'],0,12,'UTF-8'); ?>...</td>
                        <td><?php echo ($_data['check_man']); ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <td>合计</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><span id="sum_check_quantity"><?php echo ($summary["totalcheck"]); ?></span></td>
                    <td><span id="sum_qualified_quantity"><?php echo ($summary["totalquality"]); ?></span></td>
                    <td><span id="sum_unquality_count"><?php echo ($summary["totalunquality"]); ?></span></td>
                    <td><span id="qualified_rate"><?php echo (round($summary['totalquality']/$summary['totalcheck']*100 ,2)); ?></span>%</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tfoot>
                </table>
                <div class="clearfix clear"></div>
                <div class="col-md-6 pull-left">
                    <label>每页 20 条记录
                        显示 <?php echo isset($_GET['p'])?($_GET['p']-1)*20+1:1 ?> 到 <?php echo isset($_GET['p'])?($_GET['p'])*20:20 ?> 项，共 <?php echo ($count); ?> 项</label>
                </div>
                <div class="col-md-6">
                    <div class="page pull-right" style="padding: 0;margin: 0">
                        <?php echo ($page); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $("input[name='batch_code']").val("<?php  echo $_GET['batch_code'];?>");

    });

    /**
     * 重置
     * */
    function reset() {
        $('#form_search').find('input').val('');
        $('#form_search').find('select').val('');
    }

    /**
     * 下载数据
     */
    function down_csv()
    {
        var create_time_from = $('#create_time_from').val();
        var create_time_to = $('#create_time_to').val();
        var sku = $('#sku').val();
        var enterprise_dominant = $('#enterprise_dominant').val();
        var export_tax_rebate = $('#export_tax_rebate').val();
        var supplier_id = $('#supplier_id').val();
        var batch_code = $("input[name='batch_code']").val();
        var purchaseorder_id = $("input[name='purchaseorder_id']").val();

        var uri = 'batch_code='+batch_code+'&create_time_from='+create_time_from+'&create_time_to='+create_time_to+'&sku='+sku+
                '&enterprise_dominant='+enterprise_dominant+'&export_tax_rebate='+export_tax_rebate+
                '&supplier_id='+supplier_id+'&downcsv=down'+'&purchaseorder_id='+purchaseorder_id;
        window.location.href = '/fbawarehouse/index.php/Warehouse/CheckOrders/check_orders_details_report?'+uri;
    }

    /**
     * 去掉字符串左右两侧的空格
     * @param str
     * @returns {void|string|XML}
     */
    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }
//    function searchSupplier(val) {
//        if(val == ''){
//            return false;
//        }
//        else{
//            $.ajax({
//                url: "/fbawarehouse/index.php/Warehouse/CheckOrders/checkSupplier",
//                type: 'post',
//                async: false, //default:
//                data: {'name':val},
//                dataType: 'json', // default: Intelligent Guess (xml, json, script, or html)
//                success: function (data) {
//                    if(data){
//                        for (var i=0; i<data.length;i++){
//                            $('#supplier_id').parent().nextAll('#supplier_id').text(data[i].name);
//                        }
//                    }
//                }
//            });
//
//        }
//
//    }
</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>