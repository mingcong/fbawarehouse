<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style type="text/css">
    .ui-datepicker-calendar {display: none;}
</style>
<div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading">
                <h3 class="panel-title">财务月度库存成本报表</h3>
            </div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Warehouse/InventoryCost/index" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-2  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input class="form-control" type="text" name="sku" onkeyup="this.value=trim(this.value)"
                                       value="<?php echo ((isset($_GET['sku']) && ($_GET['sku'] !== ""))?($_GET['sku']):''); ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">公司主体</span>
                                <select name="enterprise_dominant" id="enterprise_dominant" class="form-control">
                                    <option value="">--请选择--</option>
                                    <?php if(is_array($enterprise_dominant)): $num = 0; $__LIST__ = $enterprise_dominant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($num % 2 );++$num;?><option value="<?php echo ($num); ?>"><?php echo ($val); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">期初月份</span>
                                <input name="warehouse_date" id="warehouse_date" class="form-control" readonly="readonly"/>

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-5  add-space">
                            <input class="btn btn-primary" type="submit" id="serach" value="查询">
                            <button class="btn btn-primary" type="button" id="cost_download">下载</button>
                            <button class="btn btn-primary btn-info" type="button" data-toggle="modal"
                                    data-target="#btn_import">修正上个月份成本为0入库明细</button>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <hr/>
                <div id="pageContent" class="well" style="min-width:600px;max-width:100%;overflow-x:scroll">
                    <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th rowspan="2">sku</th>
                            <th rowspan="2">中文名称</th>
                            <th rowspan="2">主体</th>
                            <th colspan="2">期初</th>
                            <th colspan="<?php echo ($type_length['in']*2+2); ?>">本期入</th>
                            <th colspan="<?php echo ($type_length['out']*2+2); ?>">本期出</th>
                            <th colspan="2">期末</th>
                        </tr>
                        <tr>
                            <th>期初数量</th>
                            <th>期初成本</th>
                            <?php if(is_array($stock_in_type)): foreach($stock_in_type as $key=>$vo): ?><th><?php echo ($vo); ?>数量</th>
                                <th><?php echo ($vo); ?>成本</th><?php endforeach; endif; ?>
                            <th>入库数量合计</th>
                            <th>入库成本合计</th>
                            <?php if(is_array($stock_out_type)): foreach($stock_out_type as $key=>$vo): ?><th><?php echo ($vo); ?>数量</th>
                                <th><?php echo ($vo); ?>成本</th><?php endforeach; endif; ?>
                            <th>出库数量合计</th>
                            <th>出库成本合计</th>
                            <th>期末数量</th>
                            <th>期末成本</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($inventoryCostDetail)) { foreach ($inventoryCostDetail as $data) {?>
                                    <tr>
                                    <td><?php echo $data['sku'] ?></td>
                                    <td><?php echo $data['sku_name'] ?></td>
                                    <td><?php echo $data['enterprise_dominant'] ?></td>
                                    <td><?php echo $data['beginning_quantity'] ?></td>
                                    <td><?php echo $data['beginning_cost'] ?></td>
                                    <?php foreach($stock_in_type as $type_in => $in) {?>
                                        <td><?php echo $data['in_' . $type_in . '_quantity'] ?></td>
                                        <td><?php echo $data['in_' . $type_in . '_cost'] ?></td>
                                    <?php } ?>
                                    <td><?php echo $data['inTotalQuantity'] ?></td>
                                    <td><?php echo $data['inTotalCost'] ?></td>
                                    <?php foreach($stock_out_type as $type_out => $out) {?>
                                        <td><?php echo $data['out_' . $type_out . '_quantity'] ?></td>
                                        <td><?php echo $data['out_' . $type_out . '_cost'] ?></td>

                                    <?php } ?>
                                    <td><?php echo $data['outTotalQuantity'] ?></td>
                                    <td><?php echo $data['outTotalQuantity'] ?></td>
                                    <td><?php echo $data['end_quantity'] ?></td>
                                    <td><?php echo $data['end_cost'] ?></td>
                                    </tr>
                        <?php  } } ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix clear"></div>
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <label>每页 50 条记录
                            显示 1 到 50 项，共 <?php echo ($count); ?>项</label>
                    </div>
                    <div class="col-md-6">
                        <div class="page pull-right" style="padding: 0;margin: 0">
                            <?php echo ($page); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--导入文件 -->
<div class="modal fade" id="btn_import">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件<span style="color: red">（导入请使用xlsx类型的）</span></h4>
            </div>
            <form action="/fbawarehouse/index.php/Warehouse/InventoryCost/upload_cost" method="post" enctype="multipart/form-data" id="upSku">
                <div class="modal-body form-horizontal">
                    <div class="alert alert-info top20" role="alert">
                        <p>入库明细: <a href="javascript:void(0)" id="download" >成本为0入库明细</a></p></div>

                    <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                        <div class="col-md-4"><input name="file" class="form-control" data-val="true"
                                                     data-val-required="请选择[上传文件]!" id="File" type="file" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <button id="submit-button" type="submit" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#warehouse_date').datetimepicker({
            format: 'Y-m',
            todayBtn: true,
            startView: 'year',
            minView:'year',
            maxView:'decade',
            language:'zh-CN'
        });

        $("#download").click(function () {
            var warehouse_date = $("#warehouse_date").val();
            if (!warehouse_date) {
                layer.msg("请先输入月份！",{'icon':'5'});
                return false;
            }

            var fileName="入库成本导入模板";
            window.location.href ="/fbawarehouse/index.php/Warehouse/InventoryCost/warehouseCostDownload?"+"&fileName="+fileName+"&warehouse_date="+warehouse_date;
        });

        $("#cost_download").click(function () {
            var sku = $("input[name='sku']").val();
            var enterprise_dominant = $("select[name='enterprise_dominant']").val();
            var warehouse_date = $("#warehouse_date").val();

            window.location.href ="/fbawarehouse/index.php/Warehouse/InventoryCost/index?download=1&sku="+sku+"&enterprise_dominant="+enterprise_dominant+"&warehouse_date="+warehouse_date;
        })
    });


    function trim(str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }

</script>
            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>