<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">查询不良品信息</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Warehouse/BadProducts/index" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">采购单号</span>
                                <input class="form-control" type="text" name="purchaseorder_id">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">供应商</span>
                                <input type="text" name="supplier" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">SKU</span>
                                <input class="form-control" type="text" name="sku">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">站点</span>
                                <select class="chosen-select form-control" tabindex="-1" name="site_id" data-name-group="common">
                                    <option value="" class="empty-opt">-- 请选择 --</option>
                                    <?php if(is_array($sites)): $i = 0; $__LIST__ = $sites;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">采购员</span>
                                <input type="text" name="purchase_man" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">质检时间</span>
                                <input class="form-control form-date" type="text" name="check_time_from"
                                       style="cursor:pointer;" readonly>
                                <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                <input class="form-control form-date" type="text" name="check_time_to"
                                       style="cursor:pointer;" readonly>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">状态</span>
                                <select class="chosen-select form-control" tabindex="-1" name="status"
                                        data-name-group="common">
                                    <option value="" class="empty-opt">-- 请选择 --</option>
                                    <?php if(is_array($status)): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>" ><?php echo ($item); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 add-space">
                            <input class="btn btn-primary" type="submit" value="查询">
                            <input class="btn btn-primary" type="button" onclick="reset()" value="重置">
                            <input class="btn btn-primary" type="submit" name="downcsv" value="下载">
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>采购单号</th>
                        <th>质检单号</th>
                        <th>SKU</th>
                        <th>中文名称</th>
                        <th>退税类型</th>
                        <th>站点</th>
                        <th>供应商</th>
                        <th>不合格量</th>
                        <th>质检时间</th>
                        <th>采购员</th>
                        <th>质检不合格原因</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($result)): $i = 0; $__LIST__ = $result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><tr>
                            <td><?php echo ($val["purchaseorder_id"]); ?></td>
                            <td><?php echo ($val["check_detail_id"]); ?></td>
                            <td><?php echo ($val["sku"]); ?></td>
                            <td><?php echo ($val["sku_name"]); ?></td>
                            <td><?php echo ($val["export_tax_rebate"]); ?></td>
                            <td><?php echo ($val["site_id"]); ?></td>
                            <td><?php echo ($val["supplier"]); ?></td>
                            <td><?php echo ($val["quantity"]); ?></td>
                            <td><?php echo ($val["check_time"]); ?></td>
                            <td><?php echo ($val["purchase_man"]); ?></td>
                            <td><?php echo ($val["quality_remark"]); ?></td>
                            <td><?php echo ($val["status"]); ?></td>
                            <td>
                                <?php if($val['status'] == '未处理'): ?><button type="button" class="btn btn-xs btn-info b10 edit_detail"  data-toggle="modal" data-target=".btn_edit" data-id="<?php echo ($val["id"]); ?>">编辑
                                </button><?php endif; ?>
                            </td>
                        </tr><?php endforeach; endif; else: echo "" ;endif; ?>

                </table>
                <div class="clearfix clear"></div>
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <label>每页 20 条记录
                            显示 1 到 20 项，共 <?php echo ($count); ?>项</label>
                    </div>
                    <div class="col-md-6">
                        <div class="page pull-right" style="padding: 0;margin: 0">
                            <?php echo ($page); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--编辑---->

<div class="modal fade btn_edit">
    <div class="modal-dialog modal-mg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" action="" method="post" role="form">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">SKU：</label>

                                    <div class="col-md-7">
                                        <p class="form-control-static sku"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">中文名称：</label>

                                    <div class="col-md-7">
                                        <p class="form-control-static sku_name"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">数量：</label>

                                    <div class="col-md-7">
                                        <p class="form-control-static quantity"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">质检不合格原因：</label>

                                    <div class="col-md-7">
                                        <input type="text" name="quality_remark" class="form-control quality_remark">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id_edit">
                <button type="button" class="btn btn-primary" onclick="edit()">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("input[name='purchaseorder_id']").val("<?php  echo $_GET['purchaseorder_id'];?>");
        $("input[name='supplier']").val("<?php echo $_GET['supplier'];?>");
        $("input[name='sku']").val("<?php echo $_GET['sku'];?>");
        $("input[name='purchase_man']").val("<?php  echo $_GET['purchase_man'];?>");
        $("input[name='check_time_from']").val("<?php  echo $_GET['check_time_from'];?>");
        $("input[name='check_time_to']").val("<?php echo $_GET['check_time_to'];?>");
        $("select[name='site_id']").val("<?php echo $_GET['site_id'];?>");
        $("select[name='status']").val("<?php echo $_GET['status'];?>");
    });

    /**
     * 重置
     * */
    function reset() {
        $('#form_search').find('input').val('');
        $('#form_search').find('select').val('');
    }
    /**
     * 填充编辑数据
     * khq 2017.3.2
     */
    $(".edit_detail").click(function(){
        var id = $(this).attr('data-id');
        if (id) {
            $.get("Warehouse/BadProducts/ajax_select_detail",
                    {id: id},
                    function (data) {
                        var obj = jQuery.parseJSON(data);
                        if (obj.status == 'Y') {
                            $(".sku").html(obj.message.sku);
                            $("input[name=id_edit]").val(id);
                            $(".sku_name").html(obj.message.sku_name);
                            $(".quantity").html(obj.message.quantity);
                            $(".quality_remark").val(obj.message.quality_remark);
                        } else {
                            layer.msg(obj.message, {icon: 5});
                        }
                    });
        }
    });
    /**
     * 编辑明细
     * khq 2017.1.13
     */
    function edit() {
        var id                 = $("input[name=id_edit]").val();
        var quality_remark     = $("input[name=quality_remark]").val();
        $.get("Warehouse/BadProducts//ajax_edit",
                {
                    id: id,
                    quality_remark:quality_remark
                },
                function (data) {
                    if(data.status==0) layer.msg(data.info);
                    var obj = jQuery.parseJSON(data);
                    if (obj.status == 'Y') {
                        layer.msg(obj.message, {icon: 6});
                        window.location.reload();
                    } else {
                        layer.msg(obj.message, {icon: 5});
                    }
                });
    }

    /**
     * @returns {string}
     * 获取搜索条件
     * khq 2017.3.2
     */
    function search_params(){
        var param = '';
        var purchaseorder_id = $("input[name='purchaseorder_id']").val();
        var supplier = $("input[name='supplier']").val();
        var sku = $("input[name='sku']").val();
        var purchase_man = $("input[name='purchase_man']").val();
        var check_time_from = $("input[name='check_time_from']").val();
        var check_time_to = $("input[name='check_time_to']").val();
        var site_id  = $("select[name='site_id']").val();
        var status = $("select[name='status']").val();

        if($.trim(purchaseorder_id) != ''){
            param += "&purchaseorder_id="+$.trim(purchaseorder_id);
        }
        if($.trim(supplier) != ''){
            param += "&supplier="+$.trim(supplier);
        }
        if($.trim(sku) != ''){
            param += "&sku="+$.trim(sku);
        }
        if($.trim(purchase_man) != ''){
            param += "&purchase_man="+$.trim(purchase_man);
        }
        if($.trim(status) != ''){
            param += "&status="+$.trim(status);
        }
        if($.trim(site_id) != ''){
            param += "&site_id="+$.trim(site_id);
        }
        if($.trim(check_time_from) != '' && $.trim(check_time_to) != ''){
            param += "&check_time_from="+$.trim(check_time_from);
            param += "&check_time_to="+$.trim(check_time_to);
        }
        return param;
    }
    function download_csv() {
        var param = search_params();
        window.location.href = '/fbawarehouse/index.php/Warehouse/BadProducts/index?downcsv=1' + param;
    }

</script>

            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>