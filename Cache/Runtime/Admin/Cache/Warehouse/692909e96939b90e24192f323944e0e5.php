<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">扫描收货</h3></div>
            <div class="panel-body">
                <div class="col-md-1 add-space" style="margin-right: 10px;">
                    <input class="btn  btn-success" type="button" value="生成批次号" id="create_batch_code">
                </div>


                <form action="" method="get">
                    <div class="container-fluid">
                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">卡板批次</span>
                                <input class="form-control" type="text" name="batch_code" onkeyup="checkval()"
                                       onblur="checkval()">
                            </div>
                            <span id="tip" style="margin-left: 100px;"></span>
                        </div>


                        <div class="col-sm-12 col-md-3  add-space">
                            <div class="input-group input-group-md">
                                <span class="input-group-addon">采购单号</span>
                                <input type="text" name="purchase_id" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-2 add-space">
                            <input class="btn btn-primary" type="button" onclick="select_purchaseinfo()" value="查询">
                        </div>


                    </div>
                </form>
                <div class="clearfix"></div>
                <hr>
                <div class="detail_content">
                    <!---->
                    <div class="fixed-table-toolbar">
                        <h3>采购单操作
                            <button onclick="check()" style="margin-left: 50px;" type="button" class="btn  btn-success">
                                收货确认
                            </button>
                        </h3>
                    </div>
                    <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>站点</th>
                            <th>退税类型</th>
                            <th>采购单号</th>
                            <th>采购明细单号</th>
                            <th>采购员</th>
                            <th>SKU</th>
                            <th>采购名称</th>
                            <th>规格</th>
                            <th>所属仓库</th>
                            <th>备注</th>
                            <th>已收货</th>
                            <th>未入库量</th>
                            <th>采购数量</th>
                            <th>现到货数量</th>
                            <th> 操作</th>
                        </tr>
                        </thead>
                        <tbody id="purchase">

                        </tbody>
                    </table>

                </div>
                <div class="clearfix clear"></div>
                <div class="fixed-table-toolbar">
                    <h3>现采购单已收货的明细
                        <!--<button style="margin-left: 50px;" type="button" class="btn  btn-success"-->
                                <!--onclick="update_batch_code()">批量添加到当前批次-->
                        <!--</button>-->
                    </h3>

                </div>

                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <!--<th width="20"><input id="checkAll" type="checkbox"></th>-->
                        <th>批次号</th>
                        <th>采购单号</th>
                        <th>收货单号</th>
                        <th>收货明细单号</th>
                        <th>SKU</th>
                        <th>采购名称</th>
                        <th>所属仓库</th>
                        <th>采购数量</th>
                        <th>到货数量</th>
                        <th>收货员</th>
                        <th>收货时间</th>
                        <th>站点</th>
                    </tr>
                    </thead>
                    <tbody id="recieve">

                    </tbody>

                </table>
                <div class="clearfix clear"></div>
            </div>
        </div>
    </div>
</div>

<!-- 修改对话框 -->
<div class="modal fade btn_ck">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">未质检收获明细单数量修改</h4>
            </div>
            <div class="modal-body">
                <!--<p class="text-primary">你确定要将已收货数量置为0？</p>-->
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <!--<th width="20"><input id="checkAll" type="checkbox"></th>-->
                    <th width="7%">批次号</th>
                    <th width="7%">采购单号</th>
                    <th width="7%">收货单号</th>
                    <th width="7%">收货明细单号</th>
                    <th width="9%">SKU</th>
                    <th width="12%">采购名称</th>
                    <th width="10%">所属仓库</th>
                    <th width="5%">采购数量</th>
                    <th width="13%">收获数量</th>
                    <th width="8%">收货员</th>
                    <th width="10%">收货时间</th>
                    <th width="5%">站点</th>
                </tr>
                </thead>
                <tbody id="edit_num">

                </tbody>

                </table>

            </div>

        </div>
        <div class="modal-footer">
            <input type="hidden" class="detail_id" value="" >
            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="change()">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("input[name='purchaseid']").val("<?php echo $_GET['purchaseid'];?>");
    });
    /**
     * 检查输入的批次号是否正确
     */
    function checkval() {
        $.ajax({
            type: "get",
            async: true,
            data: {'batch_code': $("input[name='batch_code']").val()},
            url: "<?php echo U('Purchaseorders/check_batch_code');?>",
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.info);
                    return;
                } else if (data.flag == 1) {
                    $('#tip').text('批次号正确').css('color', 'green');
                    $("input[name='batch_code']").attr('readonly', true);
                } else if (data.flag == -1) {
                    $('#tip').text('该批次号不存在').css('color', 'red');
                }
            }
        });
    }
    /**
     * 生成批次号
     */
    $('#create_batch_code').click(function () {
        $.ajax({
            type: "get",
            async: true,
            data: {},
            url: "<?php echo U('Purchaseorders/create_batch_code');?>",
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.info);
                    return;
                } else if (data.status == 1) {
                    $("input[name='batch_code']").val(data.batch_code).attr('readonly', true);
                    $('#tip').text('批次号正确').css('color', 'green');
                    $("input[name='batch_code']").attr('readonly', true);
                }
            }
        });
    })

    /**
     * 根据采购单号获取采购单信息
     */
    function select_purchaseinfo() {
        var purchase_id = $("input[name='purchase_id']").val();
        if (purchase_id == '') {
            layer.msg('采购单号为空哦！');
            return;
        } else {
            $.ajax({
                type: "get",
                async: true,
                data: {'purchase_id': purchase_id},//采购单表id
                url: "<?php echo U('Purchaseorders/select_purchaseinfo');?>",
                dataType: "json",
                success: function (data) {
                    if (data.status == 0) {
                        layer.msg(data.info);
                        return;
                    } else if (data.flag == -1) {
                        layer.msg('采购单未打印/采购单不存在');
                        return;
                    } else if (data.flag == 1) {
                        var html = '';
                        $.each(data.info, function (n, data) {
                            if (data.purchase_id == '' || data.purchase_id == null) {
                                data.purchase_id  = '';
                            }
                            if (data.sku_standard == null) {
                                data.sku_standard = '';
                            }
                            var unware_quantity = data.quantity-data.ware_quantity;
                            if(unware_quantity!=0){
                                var sku = "'"+data.sku+"'";
                                var site_id = "'"+data.site_id+"'";
                                sku = $.trim(sku);
                                site_id = $.trim(site_id);
                                html += "<tr>" +
                                    '<td>' + data.site_id + '</td>' +
                                    '<td>' + data.export_tax_rebate + '</td>' +
                                    '<td class="mainid">' + data.id + '</td>' +
                                    '<td class="detailsid">' + data.detailsid + '</td>' +
                                    '<td>' + data.purchase_id + '<input type="hidden" class="cids" value="'+data.cid+'"></td>' +
                                    '<td>' + data.sku + '</td>' +
                                    '<td>' + data.sku_name + '</td>' +
                                    '<td>' + data.sku_standard + '</td>' +
                                    '<td>' + data.transfer_hopper_id + '</td>' +
                                    '<td>' + data.remark + '</td>' +
                                    '<td class="recieve_quantity">' + data.recieve_quantity + '</td>' +
                                    '<td class="unware_quantity">' + unware_quantity + '</td>' +
                                    '<td class="quantity">' + data.quantity + '</td>' +
                                    '<td class="arrival_quantity"><input type="text" class="form-control w120" ></td>' +
                                    '<td><button type="button" class="btn btn-info btn-sm " data-toggle="modal" data-target=".btn_ck" onclick="edit('+sku+','+site_id+')" detail-id = "'+data.detailsid+'" >修改</button></td>' +
                                    '</tr>';
                            }
                        });
                        $("#purchase").html(html);
                        $('.recieve_quantity').each(function (i) {
                            if($(this).text()>0){
                                //$(this).parent().parent().css({'background-color':'#015532','color':'white'});
                                $(this).css({'background-color':'green','color':'white'});
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * 收获确认
     */
    function check() {
        var batch_code = $("input[name='batch_code']").val();
        if (batch_code == '') {
            layer.msg('卡板批次为空哦！');
            return;
        }
        var quantity = "";
        var arrival_quantity = "";
        var detailsid = "";
        var cids = "";
        var mainid = $("#purchase .mainid").eq(0).text();
        $("#purchase .detailsid").each(function () {
            detailsid += $(this).text() + ',';
        });
        $("#purchase .cids").each(function () {
            cids += $(this).val() + ',';
        });
        $("#purchase .quantity").each(function () {
            quantity += $(this).text() + ',';
        });
        var flag = true;
        var log = '';
        var con_firm = '';
        $("#purchase .arrival_quantity").each(function (i) {
            var j = i + 1;
            var val = parseInt($(this).find('input').val());
            var a   = val + parseInt($("#purchase .recieve_quantity").eq(i).text());//现收货量 + 已收货量
            if (val <= 0) {
                log+='第' + j + '行数量小于0'+"<br>";
            }else if (val > parseInt($("#purchase .quantity").eq(i).text())) {
                log+='第' + j + '行数量大于采购数量'+"<br>";
            }else if (val > parseInt($("#purchase .unware_quantity").eq(i).text())) {
                log+='第' + j + '行数量大于未入库数量'+"<br>";
            }else if (a > parseInt($("#purchase .quantity").eq(i).text())) {  //现收货量 + 已收货量 > 采购数量
                con_firm+="'第'"+j+"'行 现收货量 + 已收货量 > 采购数量'\n";
            }
            arrival_quantity += $(this).find('input').val() + ',';
        });
        if(log){
            layer.msg(log);
            flag = false;
        }
        if(con_firm){
           if(confirm(con_firm+"\n你确定要收货吗？"))
           {
               flag = true;
           }else{
               flag = false;
           }
        }
        if (flag==true) {
            $.ajax({
                type: "get",
                async: true,
                data: {
                    'batch_code':batch_code,'quantity': quantity, 'arrival_quantity': arrival_quantity,
                    'detailsid': detailsid, 'mainid': mainid,'cids':cids
                },
                url: "<?php echo U('Purchaseorders/check');?>",
                dataType: "json",
                success: function (data) {
                    if (data.status == 0) {
                        layer.msg(data.info);
                        return;
                    };
                    switch (data.flag) {
                        case 1:
                            layer.msg('扫描收货成功');
                            select_purchaseinfo();
                            bind_batch_code(mainid);
                            break;
                        case 2:
                            layer.msg('收货数量不能全部为空或0');
                            break;
                        case -1:
                            layer.msg('数据有误，请联系ＩＴ修正');
                            break;
                    };
                }
            });
        }

        /**
         * 添加收货明细数据
         */
        function bind_batch_code(mainid) {
            $.ajax({
                type: "get",
                async: true,
                data: {'purchaseorder_id': mainid},
                url: "<?php echo U('Purchaseorders/get_recieveinfo_by_id');?>",
                dataType: "json",
                success: function (data) {
                    if (data.status == 0) {
                        layer.msg(data.info);
                        return;
                    } else if (data.flag == 1) {
                        var html = '';
                        $.each(data.info, function (n, data) {
                            if(data.batch_code==='0'){
                                data.batch_code='';
                            }
                            html += "<tr>" +
                                //'<td class="text-center"><input name="subBox" type="checkbox" value="' + data.id + '"></td>' +
                                '<td class="batch_code">' + data.batch_code + '</td>' +
                                '<td>' + data.purchaseorder_id + '</td>' +
                                '<td>' + data.mainid + '</td>' +
                                '<td class="recieve_details_id">' + data.id + '</td>' +
                                '<td>' + data.sku + '</td>' +
                                '<td>' + data.sku_name + '</td>' +
                                '<td>' + data.transfer_hopper_id + '</td>' +
                                '<td>' + data.purchase_quantity + '</td>' +
                                '<td>' + data.arrival_quantity + '</td>' +
                                '<td>' + data.recieve_man + '</td>' +
                                '<td>' + data.recieve_time + '</td>' +
                                '<td>' + data.site_id + '</td>' +
                                '</tr>';
                        });
                        $("#recieve").html(html);
                        //$('#recieve .batch_code').text($("input[name='batch_code']").val());
                    } else {
                        layer.msg('哎呀，收货数据加载失败！')
                    }
                }
            });
        }
    }

    /**
     *
     * 修改
     */
    function edit(sku,site_id){
        $.ajax({
            type: "get",
            async: true,
            data: {'sku': sku,'purchase_id':$("input[name='purchase_id']").val(),'site_id':site_id},
            url: "<?php echo U('Purchaseorders/get_recieveinfo');?>",
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.info);
                    return;
                } else if (data.flag == 1) {
                    var html = '';
                    $.each(data.info, function (n, data) {
                        if(data.batch_code==='0'){
                            data.batch_code='';
                        }
                        html += "<tr>" +
                            //'<td class="text-center"><input name="subBox" type="checkbox" value="' + data.id + '"></td>' +
                            '<td class="batch_code">' + data.batch_code + '</td>' +
                            '<td class="purchaseorder_id">' + data.purchaseorder_id + '</td>' +
                            '<td>' + data.mainid + '</td>' +
                            '<td class="recieve_details_id">' + data.id + '</td>' +
                            '<td class="sku">' + data.sku + '</td>' +
                            '<td>' + data.sku_name + '</td>' +
                            '<td>' + data.transfer_hopper_id + '</td>' +
                            '<td>' + data.purchase_quantity + '</td>' +
                            '<td class="new_arrival_quantity"><input class="form-control" style="width:70px;" type="text" name="new_arrival_quantity" value="' + data.arrival_quantity + '"></td>' +
                            '<td>' + data.recieve_man + '</td>' +
                            '<td>' + data.recieve_time + '</td>' +
                            '<td class="site_id">' + data.site_id + '</td>' +
                            '</tr>';
                    });
                    $("#edit_num").html(html);
                } else {
                    layer.msg('该采购单sku生成的收获明细已经质检！');
                    $("#edit_num").html('');
                    return;
                }
            }
        });
    }
    /*
    * 修改*/
    function change() {
        var purchaseorder_id = $("#edit_num .purchaseorder_id").eq(0).text();
        var sku              = $("#edit_num .sku").eq(0).text();
        var site_id              = $("#edit_num .site_id").eq(0).text();
        var recieve_details_id   = "";
        var new_arrival_quantity = "";

        $("#edit_num .recieve_details_id").each(function () {
            recieve_details_id += $(this).text() + ',';
        });
        var flag = true;
        var log  = '' ;
        $("#edit_num .new_arrival_quantity").each(function (j) {
            if($(this).find('input').val()<0){
                flag = false;
                log+='第' + (j+1) + '行收获数量小于0'+"<br>";
            }else{
                new_arrival_quantity += $(this).find('input').val() + ',';
            }
        });
        if(flag){
            $.ajax({
                type: "get",
                async: true,
                data: {'purchaseorder_id':purchaseorder_id,'recieve_details_id':recieve_details_id,'sku':sku,
                    'new_arrival_quantity':new_arrival_quantity,'site_id':site_id},
                url: "<?php echo U('Purchaseorders/change_by_detailid');?>",
                dataType: "json",
                success: function (data){
                    if (data.status == 0) {
                        layer.msg(data.info);
                        return;
                    }
                    switch (data.flag) {
                        case 1:
                            layer.msg('数量修改成功');
                            select_purchaseinfo();
                            break;
                        case -1:
                            layer.msg('该采购明细已经生成质检单');
                            break;
                    };
                }
            });
        }else{
            layer.msg(log);
        }

    }

    /**
     * 获取选中的id
     */
   /* function chk() {
        var obj = document.getElementsByName('subBox'); //选择所有的对象，返回数组
        // 取到对象数组后，我们来循环检测它是不是被选中
        var s = '';
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) s += obj[i].value + ',';   //如果选中，将value添加到变量s中
        }
        if (s == '') {
            layer.msg('你还没有选择任何内容', {icon: 5});
        }
        return s;
    }*/

    /**
     * 绑定批次号
     */
    /*function update_batch_code() {
        var batch_code = $("input[name='batch_code']").val();
        if (batch_code == '') {
            layer.msg('卡板批次为空哦！');
            return;
        }
        var recieve_details_id = chk();
        $.ajax({
            type: "get",
            async: true,
            data: {'batch_code': batch_code, 'recieve_details_id': recieve_details_id},
            url: "<?php echo U('Purchaseorders/update_batch_code');?>",
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.info);
                    return;
                } else if (data.flag == 1) {
                    layer.msg('操作成功');
                    var obj = document.getElementsByName('subBox'); //选择所有的对象，返回数组
                    // 取到对象数组后，我们来循环检测它是不是被选中
                    var s = '';
                    for (var i = 0; i < obj.length; i++) {
                        if (obj[i].checked) {
                            $('#recieve .batch_code').eq(i).text(batch_code);
                        }
                    }
                    select_purchaseinfo();
                    //window.location.reload();
                } else {
                    layer.msg('哎呀，操作失败了！');
                }
            }
        });
    }*/
</script>


            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>