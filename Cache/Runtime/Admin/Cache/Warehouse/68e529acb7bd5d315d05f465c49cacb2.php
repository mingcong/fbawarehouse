<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>.form-control-static {
    padding-left: 10px;
}</style>
<div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">新建出库单</h3></div>
            <div class="panel-body">

                <form method="post" id="myform" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-2 control-label required">出库类型</label>
                        <div class="col-md-2">
                            <select class="form-control" name="type" style="width:180px;">
                                <option value="">--请选择--</option>
                                <?php if(is_array($deliveryorder_types)): $i = 0; $__LIST__ = $deliveryorder_types;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($val); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                        <div class="col-md-2"><p class="form-control-static selects"></p></div>

                        <label class="col-md-2 control-label required">SKU</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="sku" onblur="load_sku_info()"/>
                        </div>
                        <div class="col-md-2"><p class="form-control-static inputs"></p></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label required">中文名称</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="sku_name"/>
                        </div>
                        <div class="col-md-2"><p class="form-control-static inputs"></p></div>

                        <label class="col-md-2 control-label">规格</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="sku_standard"/>
                        </div>
                        <div class="col-md-2"><p class="form-control-static inputs"></p></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label required">储位</label>
                        <div class="col-md-2">
                            <select class="form-control" name="storage_position" style="width:180px;">
                                <option value="">--请选择--</option>
                            </select>
                        </div>

                        <div class="col-md-2"><p class="form-control-static selects "></p></div>

                        <label class="col-md-2 control-label required">出库时间</label>
                        <div class="col-md-2">
                            <input class="form-control form-date" type="text" name="delivery_date"
                                   style="cursor:pointer;" value="<?php echo date('Y-m-d') ?>" readonly>
                        </div>
                        <div class="col-md-2"><p class="form-control-static inputs"></p></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label required">数量</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="quantity"/></div>
                        <div class="col-md-2"><p class="form-control-static inputs"></p></div>

                        <label class="col-md-2 control-label">备注</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="remark"/></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label required">站点</label>
                        <div class="col-md-2">
                            <select class="form-control" name="site_id"
                                    data-name-group="common" style="width:180px;">
                                <option value="">-- 请选择 --</option>
                                <?php if(is_array($sites)): $i = 0; $__LIST__ = $sites;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($value); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                        <div class="col-md-2"><p class="form-control-static selects "></p></div>

                        <label class="col-md-2 control-label required">所属公司主体</label>
                        <div class="col-md-2">
                            <select name="enterprise_dominant" id="enterprise_dominant" class="form-control"
                                    data-name-group="common" style="width:180px;">
                                <option value="">--请选择--</option>
                                <?php if(is_array($enterprise_dominant)): $num = 0; $__LIST__ = $enterprise_dominant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($num % 2 );++$num;?><option value="<?php echo ($num); ?>"><?php echo ($val); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                        <div class="col-md-2"><p class="form-control-static selects "></p></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label required">是否退税</label>
                        <div class="col-md-2">
                            <select class="form-control" name="export_tax_rebate"
                                    data-name-group="common" style="width:180px;">
                                <option value="">-- 请选择 --</option>
                                <option value="1">出口退税</option>
                                <option value="0">非出口退税</option>
                            </select>
                        </div>
                        <div class="col-md-2"><p class="form-control-static selects "></p></div>

                        <div class="col-md-2 col-md-offset-2">
                            <button class="btn btn-primary" type="button" id="dismiss" onclick="create()">提交</button>
                            <button class="btn btn-white" type="button" onclick="reset()">重置</button>
                        </div>
                    </div>

            </div>



            </form>

            <div class="clearfix"></div>
            <hr>
            <h3>出库批量上传数据：<span class="text-danger">库存盘点请到 库存查询-》盘点库存（新）</span></h3>
            <form id="upload_form" class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-3 control-label required">批量上传</label>
                    <div class="col-md-3">
                        <input type="file" name="excel" class="form-control">
                    </div>
                    <div class="col-md-2"><p style="padding-top: 5px;">支持xls/xlsx格式</p></div>
                    <div class="col-md-2">
                        <button class="btn btn-info" type="button" onclick="batch_create()">上传</button>
                        <button class="btn btn-info" type="button" onclick="down_template()">下载模板</button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-7 control-label"></label>
                    <div class="col-md-6">
                        <span id="tip"></span>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
</div>
<script>
    /**
     * 重置
     * */
    function reset() {
        $('#myform_search').find('input').val('');
        $('#myform_search').find('select').val('');
    }
    /**
     * 加载sku信息
     */
    function load_sku_info() {
        $('.form-control-static').text('');
        //$('#myform input').val('');
        //$('#myform select').val('');

        $.ajax({
            type: "get",
            async: true,
            data: {'sku': $("input[name='sku']").val()},
            url: "<?php echo U('StockOut/load_sku_info');?>",
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.info);
                    return;
                } else if (data.flag == 1) {
                    //将数据写入到前端页面
                    if (data.sku_info.sku_name != '') {
                        $("input[name='sku_name']").val(data.sku_info.sku_name);
                    } else {
                        $('.form-control-static').eq(2).text('没有匹配到中文名').css('color', 'red');
                        $("#myform input").eq(1).val('');
                    }

                    if (data.sku_info.sku_standard != '') {
                        $("input[name='sku_standard']").val(data.sku_info.sku_standard);
                    } else {
                        $('.form-control-static').eq(3).text('没有匹配到规格').css('color', 'red');
                        $("#myform input").eq(2).val('');
                    }

                    if (data.sku_info.storage_position != null) {
                        var html = '<option value="">--请选择--</option>';
                        $.each(data.sku_info.storage_position, function (n, data) {
                            html += '<option value="' + data + '">' + data + '</option>';
                        });
                        $("select[name='storage_position']").html(html);
                    } else {
                        $('.form-control-static').eq(4).text('没有匹配到储位').css('color', 'red');
                        //$("#myform select").eq(1).val('');
                    }
                } else if (data.flag == -1) {
                    layer.msg('没有匹配到sku中文名,规格,储位信息');
                }
            }
        })
    }

    /**
     * 检查form中的值
     */
    function check() {
        var warning = ['类型不能为空', '储位不能为空', '站点不能为空','公司主体不能为空','是否退税不能为空'];
        var warning2 = ['sku不能为空', '中文名称不能为空', '', '出库时间不能为空', '数量不能为空'];

        var flag = true;
        $('#myform select').each(function (i) {
            if ($('select').eq(i).val() == '') {
                $('.selects').eq(i).text(warning[i]).css('color', 'red');
                flag = false;
            } else {
                $('.selects').eq(i).text('通过验证').css('color', 'green');
            }
        })
        $('#myform input').each(function (i) {
            if (i == 2 || i == 5) {
            } else if(i==4 && parseInt($('#myform input').eq(4).val())<=0){
                $('.inputs').eq(4).text('出库数量不能小于等于0').css('color', 'red');
                flag = false;
            }else if ($('#myform input').eq(i).val() == '') {
                $('.inputs').eq(i).text(warning2[i]).css('color', 'red');
                flag = false;
            } else {
                $('.inputs').eq(i).text('通过验证').css('color', 'green');
            }
        });
        return flag;
    }
    /**
     * 提交  创建特殊出库
     */
    function create() {
        var type = $.trim($("select[name='type']").val());
        var sku = $.trim($("input[name='sku']").val());
        var sku_name = $.trim($("input[name='sku_name']").val());
        var sku_standard = $.trim($("input[name='sku_standard']").val());
        var storage_position = $.trim($("select[name='storage_position']").val());
        var delivery_date = $.trim($("input[name='delivery_date']").val());
        var quantity = $.trim($("input[name='quantity']").val());
        var remark = $.trim($("input[name='remark']").val());
        var site_id = $.trim($("select[name='site_id']").val());
        var enterprise_dominant = $.trim($("select[name='enterprise_dominant']").val());
        var export_tax_rebate = $.trim($("select[name='export_tax_rebate']").val());
        var flag = check();
        if (flag) {
            $.ajax({
                type: "get",
                async: true,
                data: {
                    'type': type, 'sku': sku, 'sku_name': sku_name, 'sku_standard': sku_standard,
                    'storage_position': storage_position, 'delivery_date': delivery_date,
                    'quantity': quantity, 'remark': remark, 'site_id': site_id,'enterprise_dominant':enterprise_dominant,
                    'export_tax_rebate':export_tax_rebate
                },
                url: "<?php echo U('StockOut/create');?>",
                dataType: "json",
                success: function (data) {
                    if (data.status == 0) {
                        layer.msg(data.info);
                        return;
                    } else if (data.flag == 1) {
                        layer.msg(data.create_info);
                        //$("#dismiss").attr('display','none');
                    } else if (data.flag == -1) {
                        layer.msg(data.create_info);
                    }
                }
            })
        }
    }
    /**
     * 下载模板
     */
    function down_template() {
        window.location.href = '/fbawarehouse/index.php/Warehouse/StockOut/down_template';
    }

    /**
     * 批量上传
     */
    function batch_create() {
        var xls = new FormData();
        xls.append('file', $('#upload_form input[name="excel"]').get(0).files[0]);
        $.ajax({
            url: "<?php echo U('StockOut/batch_create');?>",
            type: 'POST',
            data: xls,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                if (data.status == 0) {
                    layer.msg(data.info);
                    return;
                } else if (data.flag == 1) {
                    layer.msg(data.message);
                    $("#upload_form input").val('');
                } else {
                    $("#tip").html(data.message).css('color', 'blue');
                    return;
                }
            }
        });
    }

</script>

            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>