<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <link href="/fbawarehouse/Public/images/logo.ico" rel="shortcut icon">
    <title><?php echo C('SITE_TITLE');?> - 后台管理系统</title>
    <base href="/fbawarehouse/index.php/Warehouse" />
    <!-- css -->
    <link rel="stylesheet" href="/fbawarehouse/Public/stylesheets/admin/lanren1.css">
    <link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/base.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/stylesheets/admin/layout.css">
<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/admin/asyncbox/skins/default.css">
<!--<link rel="stylesheet" type="text/css" href="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.css">-->

<!--yksui---->
<link href="/fbawarehouse/Public/yksui/css/zui.css?20161206" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/step.css?20170407" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/self.css?20161216" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/style.css?20161227" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/jquery.autocomplete.css" rel="stylesheet" />
<!---datatable css--->
<link href="/fbawarehouse/Public/yksui/lib/datatable10/css/jquery.dataTables.css?20161215" rel="stylesheet" />
<!------chosen css+js-->
<link href="/fbawarehouse/Public/yksui/lib/chosen/chosen.css?20161227" rel="stylesheet" />
<!------timedate css+js-->
<!--<link href="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.css?20161226" rel="stylesheet" />-->
<!--date--->
<link href="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.css?20161226" rel="stylesheet" />
<link href="/fbawarehouse/Public/yksui/css/autocomplete/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <!-- js -->
    <script src="/fbawarehouse/Public/javascripts/admin/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.lazyload.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/functions.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.form.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/asyncbox/asyncbox.js" type="text/javascript"></script>

<!--<script src="/fbawarehouse/Public/javascripts/admin/datepicker/datetimepicker_css.js" type="text/javascript"></script>-->
<script src="/fbawarehouse/Public/javascripts/admin/layer/layer.js" type="text/javascript"></script>
<!--<script src="/fbawarehouse/Public/javascripts/admin/gx_base.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/admin/jquery.watermark.js" type="text/javascript"></script>
<script src="/fbawarehouse/Public/javascripts/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>-->

<!--yksui---->

<script src="/fbawarehouse/Public/yksui/js/zui.js"></script>

<!--日期--->
<!--<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datetimepicker/datetimepicker.min.js"></script>-->

<!--date--->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/date/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/datePicker/WdatePicker.js"></script>

<!--常用--->
<script type="text/javascript" defer src="/fbawarehouse/Public/yksui/js/admin.js"></script>
<!--dynamic table-->
<!--<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="javascript" 
src="/fbawarehouse/Public/yksui/lib/datatable10/js/colResizable-1.5.min.js"></script>-->
<!------chosen css+js-->
<script type="text/javascript" src="/fbawarehouse/Public/yksui/lib/chosen/chosen.min.js"></script>

<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery.ui.autocomplete.min.js"></script>
<script src="/fbawarehouse/Public/yksui/js/autocomplete/jquery-ui-1.10.4.custom.js"></script>

<script type="text/javascript">
    $(window).resize(autoSize);
    $(function(){
        autoSize();
        $(".loginOut").click(function(){
            var url=$(this).attr("href");
            popup.confirm('你确定要退出吗？','你确定要退出吗',function(action){
                if(action == 'ok'){ window.location=url; }
            });
            return false;
        });
        var time=self.setInterval(function(){$("#today").html(date("Y-m-d H:i:s"));},1000);
    });
</script>

</head>

<body>
    <div class="wrap">
        <!-- header -->
        <div id="Top">
    <div class="logo">
        <a href="<?php echo U('Home/Index/index');?>">亚马逊国内转运仓库</a>
    </div>

<!--     <div class="help">
        <a href="#">使用帮助</a><span><a href="#">关于</a></span>
    </div> -->

    <!-- menu -->
    
    <div class="menu1">
    <nav class="navbar" role="navigation">
        <div class="container-fluid">
            <ul class="nav nav-secondary topnav">
                <?php if(is_array($main_menu)): $i = 0; $__LIST__ = $main_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i; if($i == 1): ?><li class="fisrt <?php echo activedLink($key, null, 'fisrt_current');?> ">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php elseif($i == count($main_menu)): ?>
                        <li class="end <?php echo activedLink($key, null, 'end_current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li>
                    <?php else: ?>
                        <li class="<?php echo activedLink($key, null, 'current');?>">
                            <span><a class="show-title-tip" href="<?php echo U($menu_item['target']);?>" title-tip="<?php echo ($menu_item['tip']); ?>"><?php echo ($menu_item['name']); ?></a></span>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
            </ul>
            <div class="clear clearfix"></div>
            <div class="pull-right topright">
                <i class="icon-time"></i> <span id="today"><?php echo date("Y-m-d H:i:s");?></span>
                <i class="icon-user"></i> 欢迎您，<?php echo ($_SESSION['current_account']['email']); ?>  &nbsp;&nbsp; <i class="icon-signout"></i> <a href="<?php echo U('Home/Public/logout');?>"  style="color: #FFF;"><span>&nbsp;</span>退出系统</a></li>
            </div>
        </div>
            <!-- 代码部分begin -->
        <?php if(empty($tasksList)): else: ?>
            <div class="asideNav">
                <div id="rightArrow"><a href="javascript:;" title="未完成任务列表"></a></div>
                <div id="floatDivBoxs">
                    <div class="floatDtt">未完成任务列表</div>
                    <div class="floatShadow">
                        <ul class="floatDqq">
                            <?php if(is_array($tasksList)): $i = 0; $__LIST__ = $tasksList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo ($vo["url"]); ?>" class="" target="_blank" > <li> <?php echo ($vo["msg"]); ?></li></a><?php endforeach; endif; else: echo "" ;endif; ?>
                        </ul>
                    </div>
                </div>
            </div><?php endif; ?>

    </nav>
</div>
<div class="clear clearfix"></div>


</div>
<div class="clear"></div>


        <!-- main -->
        <div class="mainBody">
            <!-- left -->
            <div id="Left">
    <div id="control"></div>
    <!--  <div class="subMenuList">
         <div class="itemTitle">
             常用操作
         </div>
         <ul>
             <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U($key);?>" class="show-title-tip"><?php echo ($menu_item); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
         </ul> -->
<div class="list-group">
    <div class="itemTitle">
        常用操作
    </div>
     <?php if(is_array($sub_menu)): $i = 0; $__LIST__ = $sub_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu_item): $mod = ($i % 2 );++$i;?><a href="<?php echo U($key);?>" class="list-group-item"><?php echo ($menu_item); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
    <!--
         <a href="#" class="list-group-item">用div >项目</a>
         <a href="#" class="list-group-item">待办</a>
         <a href="#" class="list-group-item">需求</a>
         <a href="#" class="list-group-item active">任务</a>
         <a href="#" class="list-group-item">Bug</a>
         <a href="#" class="list-group-item">用例</a>-->
     </div>


</div>


            <!-- right -->
            <div id="Right" >
                <ol class="breadcrumb mb10">
                    <!--<li><i class="icon icon-home"></i> 主页</li>-->
                    <li class="active"><i class="icon icon-home"></i><?php echo ($breadcrumbs); ?></li>
                </ol>
                <style>
    .tabliv {
        display: none;
    }

    .tabliv > table th, .tabliv > table td {
        padding: 8px;
        border-bottom: 1px solid #555;
    }
    .list-inline {
        padding-left: 0;
        list-style: none;
        margin-left: -5px;
    }
    .list-inline > li {
        display: inline-block;
        padding-left: 5px;
        padding-right: 5px;
    }
</style>

<div class="container-fluid">
    <div class="wrapper">
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">sku实时库存</h3></div>
            <div class="panel-body">
                <form action="/fbawarehouse/index.php/Warehouse/Inventory/sku_real_inventory" method="post" id="form_search">


                    <div class="col-sm-12 col-md-4  add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">所属公司主体</span>
                            <select name="enterprise_dominant" id="enterprise_dominant" class="form-control">
                                <option value="">--请选择--</option>
                                <?php if(is_array($enterprise_dominant)): $num = 0; $__LIST__ = $enterprise_dominant;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($num % 2 );++$num;?><option value="<?php echo ($num); ?>"><?php echo ($val); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-3  add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">是否退税</span>
                            <select class="form-control" name="export_tax_rebate"
                                    data-name-group="common">
                                <option value="">-- 请选择 --</option>
                                <option value="1">出口退税</option>
                                <option value="-1">非出口退税</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-3  add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">站点</span>
                            <select class="form-control" name="site_id"
                                    data-name-group="common" style="width:180px;">
                                <option value="">-- 请选择 --</option>
                                <?php if(is_array($sites)): $i = 0; $__LIST__ = $sites;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($value); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 col-md-4 add-space">
                        <div class="input-group input-group-md">
                            <span class="input-group-addon">SKU</span>
                            <textarea name="sku" id="sku" class="form-control" cols="5" rows="3" placeholder="例如：
A301
A401"><?php echo trim($_POST['sku']);?></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4  add-space">
                        <input class="btn btn-primary" type="submit" value="查询">
                        <input class="btn btn-primary" type="button" onclick="reset()" value="重置">
                        <input class="btn btn-success" type="button" value="下载" id="download">
                        <span>&nbsp;&nbsp;数据不超过2000行</span>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <hr>

            <table class="table table-bordered" id="table_content"
                   style="border-collapse:collapse;width: 98%;margin-left: 10px;">
                <thead>
                <tr>
                    <th>序号</th>
                    <th></th>
                    <th>sku</th>
                    <th>采购名称</th>
                    <th>sku实时库存</th>
                    <th>入库单可用库存</th>
                    <th>sku实际可用库存</th>
                    <th>占用库存</th>
                    <th>是否退税</th>
                    <th>站点</th>
                    <th>公司主体</th>
                </tr>
                </thead>
                <tbody id="div_content">
                <?php if(is_array($data)): $num = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($num % 2 );++$num;?><tr>
                        <td><?php echo ($num); ?></td>
                        <td id="tr<?php echo ($val["id"]); ?>" class="detailtr"><!--data-toggle="modal" data-target="btn_ck"
                            onclick="write_detail($str)"--><font color="#008b8b" style="cursor:pointer; ">展开</font></td>
                        <td><?php echo ($val["sku"]); ?></td>
                        <td><?php echo ($val["sku_name"]); ?></td>
                        <td>
                            <?php if($val['quantity'] != $val['avaliable_quantity']): ?><font color="red"><?php echo ($val["quantity"]); ?></font>
                                <?php else: ?>
                                <?php echo ($val["quantity"]); endif; ?>
                        </td>
                        <td>
                            <?php if($val['quantity'] != $val['avaliable_quantity']): ?><font color="red"><?php echo ($val["avaliable_quantity"]); ?></font>
                                <?php else: ?>
                                <?php echo ($val["avaliable_quantity"]); endif; ?>
                        </td>
                        <td><?php echo $val['avaliable_quantity']-$val['occupy_quantity']?></td>
                        <td><?php echo ($val["occupy_quantity"]); ?></td>
                        <td><?php echo ($val["export_tax_rebate"]); ?></td>
                        <td><?php echo ($val["site_id"]); ?></td>
                        <td><?php echo ($val["enterprise_dominant"]); ?></td>
                    </tr>
                    <tr>
                        <td id="tr<?php echo ($val["id"]); ?>s" class="tabliv" colspan="9">
                            <div class="alert alert-info" role="alert">
                                <span>前面为储位,后为储位对应的数量</span>
                                <ul class="list-inline">
                                    <?php if(is_array($val["details"])): $i = 0; $__LIST__ = $val["details"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><?php echo ($vo["storage_position"]); ?>：<?php echo ($vo["all_available_quantity"]); ?>&nbsp;&nbsp;&nbsp;&nbsp;</li><?php endforeach; endif; else: echo "" ;endif; ?>
                                </ul>
                            </div>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
            <div class="clearfix clear"></div>
            <div class="row" style="margin-top: 20px;padding-bottom: 15px; width: 98%;margin-left: 10px;">
                <div class="col-md-6 pull-left">
                    <label>每页 20 条记录
                        显示 1 到 20 项，共 <?php echo ($count); ?>项</label>
                </div>
                <div class="col-md-6">
                    <div class="page pull-right" style="padding: 0;margin: 0">
                        <?php echo ($page); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- 详情 -->
<div class="modal fade btn_ck">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">明细</h4>
            </div>
            <div class="modal-body">
                <table>
                    <thead>
                    <tr>
                        <th width="19%">储位</th>
                        <th width="2%">数量</th>
                    </tr>
                    </thead>
                    <tbody id="detail">

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    $(function () {
        $("select[name='export_tax_rebate']").val("<?php echo $_POST['export_tax_rebate']?>");
        $("select[name='enterprise_dominant']").val("<?php  echo $_POST['enterprise_dominant'];?>");
        $("select[name='site_id']").val("<?php  echo $_POST['site_id'];?>");
    });

    /**
     * 重置
     * */
    function reset() {
        $('#form_search').find('input').val('');
        $('#form_search').find('select').val('');
        $('#sku').val('');
    }
    /**
     * 下载
     */
    $("#download").click(function () {
        var sku = $("#sku").val();
        sku=sku.replace(/\r\n/g,",");
        sku=sku.replace(/\n/g,",");
        var enterprise_dominant = $("select[name='enterprise_dominant']").val();
        var export_tax_debate = $("select[name='export_tax_debate']").val();
        var site_id = $("select[name='site_id']").val();
        post('/fbawarehouse/index.php/Warehouse/Inventory/sku_real_inventory_download', {'sku':sku,'enterprise_dominant':enterprise_dominant,
            'export_tax_debate':export_tax_debate,'site_id':site_id});
    });

    function post(URL, PARAMS) {
        var temp = document.createElement("form");
        temp.action = URL;
        temp.method = "post";
        temp.style.display = "none";
        for (var x in PARAMS) {
            var opt = document.createElement("textarea");
            opt.name = x;
            opt.value = PARAMS[x];
            // alert(opt.name)
            temp.appendChild(opt);
        }
        document.body.appendChild(temp);
        temp.submit();
        return temp;
    }

    /**
     * 展开关闭
     */
    $(".detailtr").click(function () {
        var trid = $(this).attr("id");
        $("#" + trid + "s").toggle();
        if ($(this).text() == '展开') {
            $(this).text('关闭');
            $(this).css('color', 'red')
        } else {
            $(this).text('展开');
            $(this).css('color', '#008b8b')
        }
    });

    /*
    分页
     */
    $('.page a').click(function(){
        var tmpHref = $(this).attr('href');
        tmpHref = tmpHref.replace(/\/selCon\//,"");
        $("#form_search").attr("action", tmpHref);
        $("#form_search").submit();
        return false;
    });

    $("#selecting").click(function(){
        $("#form").attr("action", "/fbawarehouse/index.php/Warehouse/Inventory/sku_real_inventory/p/1");
        $("#form").submit();
    });

</script>







            </div>
        </div>
        <div class="clear"></div>

        <!-- footer -->
        <!--<div id="Bottom">
    © 2014 Easy-Admin，Github项目地址：<a target="_blank" href="https://github.com/happen-zhang/easy-admin" target="_blank" >happen-zhang</a> Easy-Admin后台管理系统 All rights reserved
</div>-->


    </div>
    <script src="https://qiyukf.com/script/14ab9ac89093fd3fcc7b5c632ee1d9d8.js" defer async></script>
    <script>
        $(function(){
            var flag=0;

            $('#rightArrow').on("click",function(){
                if(flag==1){
                    $("#floatDivBoxs").animate({right: '0px'},300);
                    $(this).animate({right: '300px'},300);
                    $(this).css('background-position','0px 0');
                    flag=0;
                }else{
                    $("#floatDivBoxs").animate({right: '-305px'},300);
                    $(this).animate({right: '-5px'},300);
                    $(this).css('background-position','-50px 0');
                    flag=1;
                }
            });
        });
    </script>
</body>
</html>