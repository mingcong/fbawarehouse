﻿<?php include 'common/top.html' ?>
<?php include 'common/nav.html' ?>
<div class="main-content">
    <div class="wrapper">
        <ol class="breadcrumb m0">
            <li><a href="#"><i class="icon icon-home"></i> 主页</a></li>
            <li class="active">录入箱唛信息</li>
        </ol>
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">列表</h3></div>
            <div class="panel-body">
           <div class="row">
                    <div class="col-md-6">
                        <h5>退税</h5>
                        <p class="m10">
                        <input name="BOXID[0]" type="text" class="form-control w50" placeholder="请输入箱数"
                                           required="required"><button type="button" class="btn">确定</button></p>
                        <table class="table table-bordered table-hover">
                            <thead>

                            <tr>
                                <th>序号</th>
                                <th> 重量<span class="fred">(kg)*</span></th>
                                <th> 长(cm)*宽(cm) *高(cm</th>
                                <th nowrap=""> 操作</th>
                            </tr>
                            </thead>
                            <tbody id="addtr">
                            <tr>
                                <td>1</td>
                                <td><input name="BOXID[0]" type="text" class="form-control w50" placeholder="请输入重量"
                                           required="required"></td>
                                <td><input name="LENGTH[0]" type="text" class="form-control w50" placeholder="请输入长"
                                           required="required">
                                <input name="WIDTH[0]2" type="text" class="form-control w50" placeholder="请输入宽"
                                           required="required">
                                <input name="HEIGHT[0]2" type="text" class="form-control w50" placeholder="请输入高"
                                           required="required"></td>
                                <td><a href="javascript:void(0)" onclick="Remove(this);"> 删除 </a> <a
                                        href="javascript:void(0)" onclick="copyline(this);"> 复制 </a>
                                    <a href="#"  data-toggle="modal" data-target=".inputmore">录入sku</a></td>
                            </tr>
                            </tbody>
                        </table>
                        <a id="xinzen" href="javascript:void(0)" onclick="addConsignerAddress()"> 新增一行 </a>


                    </div>
                    <div class="col-md-6">
                        <h5>非退税</h5>
                        <p class="m10">
                            <input name="BOXID[0]" type="text" class="form-control w50" placeholder="请输入箱数"
                                   required="required"><button type="button" class="btn">确定</button></p>
                        <table class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>序号</th>
                              <th> 重量<span class="fred">(kg)*</span></th>
                              <th> 长(cm)*宽(cm) *高(cm</th>
                              <th nowrap=""> 操作</th>
                            </tr>
                          </thead>
                          <tbody id="addtr3">
                            <tr>
                              <td>1</td>
                              <td><input name="BOXID[0]2" type="text" class="form-control w50" placeholder="请输入重量"
                                           required="required"></td>
                              <td><input name="LENGTH[0]2" type="text" class="form-control w50" placeholder="请输入长"
                                           required="required">
                                <input name="WIDTH[0]" type="text" class="form-control w50" placeholder="请输入宽"
                                           required="required">
                                <input name="HEIGHT[0]" type="text" class="form-control w50" placeholder="请输入高"
                                           required="required"></td>
                              <td><a href="javascript:void(0)" onClick="Remove(this);"> 删除 </a> <a
                                        href="javascript:void(0)" onClick="copyline(this);"> 复制 </a> <a href="#"  data-toggle="modal" data-target=".inputmore">录入sku</a></td>
                            </tr>
                          </tbody>
                        </table>
                        <a id="xinzen1" href="javascript:void(0)" onclick="addConsignerAddress()"> 新增一行 </a>

                    </div>
                    <div class="col-md-2 col-md-offset-10">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary ajax-submit"> 提交订单</button>
                        </div>
                    </div>

                </div>
                <form action="" id="search-form" class="search-form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">


                                <div class="col-sm-12 col-md-4  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">shipmengt ID</span>
                                        <input class="form-control " placeholder="请输入内容" name="globalOrder"
                                               data-name-group="" type="text" value="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">拣货编码</span>
                                        <input class="form-control " placeholder="请输入内容" name="platformOrder"
                                               data-name-group="common" type="text" value="">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2 add-space">
                                <input class="btn btn-primary jsSearchBtn" type="button" value="搜索">
                            </div>

                        </div>
                    </div>
                </form>
                <hr>
                <table class="table table-striped  table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>shipment ID</th>
                        <th>拣货单编码</th>
                        <th>SKU</th>
                        <th>账号</th>
                        <th>站点</th>
                        <th>虚拟SKU</th>
                        <th>产品名称</th>
                        <th>储位</th>
                        <th>拣货数量</th>
                        <th>是否退税</th>
                        <th>销售员</th>
                        <th>采购员</th>
                        <th>运输方式</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

                <div class="alert alert-info top20" role="alert">
                    <p>*注意美国站点外箱标准：标准尺寸的货物，包装的外箱每条边最长不能超过 25英寸，即不能超过 63.5 厘米，重量每箱不能超过 50磅，即22.67 公斤。</p></div>


            </div> <!---panel-body end--->
        </div> <!---panel end--->


    </div><!---wrapper end--->

</div><!---main-content end---->

<!-- 账号分仓对话框 -->
<div class="modal fade inputmore">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">录入sku</h4>
            </div>
            <div class="modal-body">
            <table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>序号</th>
       <th> SKU <span class="fred">*</span></th>
      <th> 数量 <span class="fred">*</span></th>
      <th nowrap=""> 操作</th>
    </tr>
  </thead>
  <tbody id="addtr2">
    <tr>
      <td>1</td>
      <td><input name="" type="text" class="form-control" placeholder="sku"
                                           required="required"></td>
      <td><input name="" type="text" class="form-control" placeholder="请输入数量"
                                           required="required"></td>
                                              <td><a href="#">新增</a></td>
    </tr>
  </tbody>
</table>
            
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>

<!-------------------------当前页面js引用处--------------------------------->

<?php include 'common/bottom.html' ?>
<script>
    $(function () {
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });
    });


</script>


</body>
</html>