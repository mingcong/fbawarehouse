﻿<?php include 'common/top.html' ?>
<!-------------------------当前页面css引用处或定义地方--------------------------------->
<?php include 'common/nav.html' ?>
<div class="main-content">
    <div class="wrapper">
        <ol class="breadcrumb m0">
            <li><a href="#"><i class="icon icon-home"></i> 主页</a></li>
            <li class="active">汇总列表</li>
        </ol>
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">查询</h3></div>
            <div class="panel-body">
                <form action="" id="search-form" class="search-form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-5">

                                <div class="col-sm-12 col-md-5  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">站点</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="orderState"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="0">US</option>
                                            <option value="10">UK</option>
                                            </select>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-5 add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">运输方式</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="orderSubject"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="OVERSEA">海运</option>
                                            <option value="OLDERP">空运</option>
                                        </select>
                                    </div>
                                </div>
                                <input class="btn btn-primary jsSearchBtn add-space" type="button" value="搜索">

                                <table width="70%" cellspacing="0" class="table table-striped  table-hover">
                                    <thead>
                                    <tr>
                                        <th width="20" class="text-center"><input id="checkAll" type="checkbox" /></th>
                                        <th width="30">拣货编码 </th>
                                        <th width="30">最佳到货时间</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                                        <td>ddddf</td>
                                        <td>20161111</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                                        <td>ddddf</td>
                                        <td>20161111</td>
                                    </tr>
                                    </tbody>
                                </table>
                               </div>
                            <!--<div class="col-md-1 text-center"><ul><li><i class="icon-long-arrow-left"></i></li>
                                    <li><i class="icon-long-arrow-right"></i></li>
                                </ul>
                                </div>-->
                            <div class="col-md-offset-1 col-md-5">
                                <h4>已选订单</h4>
                                <select multiple="" class="form-control mulselect b10">
                                    <option value="1">BN00001</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary" title="保存" id="sub">
                                        保存
                                    </button>
                                </div>






                            </div>

                        </div>
                    </div>
                </form>
                <hr>
                <table class="table table-striped  table-hover">
                  <thead>
                    <tr>
                      <th>序号</th>
                      <th>拣货单编码</th>
                      <th>SKU</th>
                      <th>账号</th>
                      <th>站点</th>
                      <th>虚拟SKU</th>
                      <th>产品名称</th>
                      <th>储位</th>
                      <th>拣货数量</th>
                      <th>是否退税</th>
                      <th>销售员</th>
                      <th>采购员</th>
                      <th>运输方式</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>US</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>UK</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>US</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>UK</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>US</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>UK</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>US</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>US</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>BNSUYQ</td>
                      <td>BNSUS161</td>
                      <td>是</td>
                      <td>UK</td>
                      <td>60001YQ</td>
                      <td>&nbsp;</td>
                      <td>100</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
          <div class="clearfix clear"></div>
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <label>每页 <select class=" input-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> 条记录
                            显示 1 到 10 项，共 57项</label>
                    </div>
                    <div class="col-md-6">
                        <ul class="pager pull-right">
                            <li class="previous"><a href="your/nice/url">上一页</a></li>
                            <li><a href="your/nice/url">1</a></li>
                            <li class="active"><a href="your/nice/url">2</a></li>
                            <li><a href="your/nice/url">3</a></li>
                            <li><a href="your/nice/url">4</a></li>
                            <li><a href="your/nice/url">5</a></li>
                            <li class="next"><a href="your/nice/url">下一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    </div><!---wrapper end--->

</div>


<!---model 弹出页面----->


<!--导入文件 -->
<div class="modal fade" id="btn_import">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件</h4>
            </div>

            <div class="modal-body form-horizontal">
                <div class="alert alert-info top20" role="alert">
                    <p>模板下载: <a href="template-2016-03-21.xls">模板</a></p></div>

                <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                    <div class="col-md-4"><input name="import_file" class="form-control" data-val="true"
                                                 data-val-required="请选择[上传文件]!" id="File" type="file" value="">
                    </div>
                </div>

                <div class="form-group">
                    <button id="submit-button" type="button" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                    </button>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>


<!--查看明细 -->
<div class="modal fade showdetail">
    <div class="modal-dialog  modal-fullscreen">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">查看明细</h4>
            </div>

            <div class="modal-body">
                <h1 class="text-center">*****备货需求批次号详情</h1>

                <div class="alert alert-primary with-icon">
                    <i class="icon-ok-sign"></i>

                    <div class="content">
                        <h4>renn0001(备货需求批次号)</h4>
                        <hr>
                        <>创建人：张三 创建时间：2016-12-12 状态：创建</>
                    </div>
                </div>
                <button class="btn btn-success b10">导出</button>
                <table class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                      <th>序号</th>
                        <th>状态</th>
                        <th>SKU</th>
                        <th>账号</th>
                        <th>是否分仓</th>
                        <th>站点</th>
                        <th>虚拟SKU</th>
                        <th>产品名称</th>
                        <th>需求数量</th>
                        <th>储位</th>
                        <th>预拣数量</th>
                        <th>是否退税</th>
                        <th>销售员</th>
                        <th>采购员</th>
                        <th>要求到货时间</th>
                        <th>运输方式</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>2</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>3</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>4</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>5</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>6</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>7</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>8</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    <tr>
                      <td>9</td>
                        <td>未发货</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>户外真空保龄球保温时尚水杯办公保温杯 黑色</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>是</td>
                        <td>张三</td>
                        <td>张三</td>
                        <td>2016-11-04 14:58:49</td>
                        <td>空运</td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>


<!-------------------------当前页面js引用处--------------------------------->
<?php include 'common/bottom.html' ?>
<script>
    $(function () {
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });
    });
</script>

</body>
</html>