﻿<?php include 'common/top.html' ?>
<!-------------------------当前页面css引用处或定义地方--------------------------------->
<style>
    th, td {
        white-space: nowrap;
    }

    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<?php include 'common/nav.html' ?>
<div class="main-content">
    <div class="wrapper">
        <ol class="breadcrumb m0">
            <li><a href="#"><i class="icon icon-home"></i> 主页</a></li>
            <li class="active">汇总列表</li>
        </ol>
        <div class="panel" id="nav">
            <div class="panel-heading"><h3 class="panel-title">查询</h3></div>
            <div class="panel-body">
                <form action="" id="search-form" class="search-form">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">

                                <div class="col-sm-12 col-md-3  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">所属备货需求批次号</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="orderSubject"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="OVERSEA">OVERSEA</option>
                                            <option value="OLDERP">OLDERP</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">shipmengt ID</span>
                                        <input class="form-control " placeholder="请输入内容" name="globalOrder"
                                               data-name-group="" type="text" value="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">拣货编码</span>
                                        <input class="form-control " placeholder="请输入内容" name="platformOrder"
                                               data-name-group="common" type="text" value="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">账号</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="orderSubject"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="OVERSEA">OVERSEA</option>
                                            <option value="OLDERP">OLDERP</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">站点</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="platformType"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="EB">EBAY</option>
                                            <option value="SM">速卖通</option>
                                            <option value="WH">WISH</option>
                                            <option value="YA">亚马逊</option>
                                            <option value="NE">NEWEGG</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-12 col-md-2  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">运输方式</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="salesAccount"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="12deeping" data-value="EB">
                                                海运
                                            </option>
                                            <option value="allgoodforver" data-value="WH">
                                                空运
                                            </option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">状态</span>
                                        <select class="chosen-select form-control" tabindex="-1" name="orderState"
                                                data-name-group="common">
                                            <option value="" class="empty-opt">-- 请选择 --</option>
                                            <option value="0">订单不存在</option>
                                            <option value="10">订单待初始化</option>
                                            <option value="20">订单确认</option>
                                            <option value="30">确认分仓</option>
                                            <option value="40">标记发货</option>
                                            <option value="50">通知仓库发货</option>
                                            <option value="110">订单完成</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-12 col-md-5  add-space">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">单据生成时间</span>
                                        <input class="form-control form-date" type="text" value="">
                                        <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                        <input class="form-control form-date" type="text" value="">
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-2 add-space">
                                <input class="btn btn-primary jsSearchBtn" type="button" value="搜索">
                                <button class="btn btn-success">导出</button>
                            </div>

                        </div>
                    </div>
                </form>
                <hr>
                <div class="fixed-table-toolbar b10">

                    <div id="toolbar" class="btn-group">
                        <button type="button" class="btn" data-toggle="modal" data-target="#btn_import">
                            导出
                        </button>
                        <button type="button" class="btn">录入箱唛信息
                        </button>
                        <button type="button" class="btn">
                            上传平台箱唛信息
                        </button>
                        <!--                        <button type="button" class="btn" data-toggle="modal" data-target="#btn_delete">账号分仓-->
                        <!--                        </button>-->
                        <!--                        <button type="button" class="btn" data-toggle="modal" data-target="#btn_check">销售核对</button>-->
                        <button type="button" class="btn">打印箱唛</button>
                    </div>
                </div>

                <table id="example" class="table table-striped  table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="20"><input id="checkAll" type="checkbox" /></th>
                        <th width="30">&nbsp;</th>
                        <th width="30">序号</th>
                        <th width="91">状态</th>
                        <th width="43">shipment ID</th>
                        <th width="74">拣货单编码</th>
                        <th width="61">所属备货需求批次号</th>
                        <th width="61">账号</th>
                        <th width="67">站点</th>
                        <th width="56">最佳到货时间</th>
                        <th width="33">运输方式</th>
                        <th width="69">起运地</th>
                        <th width="235">目的地</th>
                        <th width="56">亚马逊配送中心标识</th>
                        <th width="33"> 亚马逊配送中心地址</th>
                        <th width="56">计划单生成人</th>
                        <th>计划单生成时间</th>
                        <th width="17"> 操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>展开</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td> <div class="dropdown">
                                <button class="btn btn-xs" type="button" data-toggle="dropdown">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a title="查看/录入箱唛信息" href="#">查看/录入箱唛信息</a></li>
                                    <li><a title="上传发票" href="#">上传发票</a></li>
                                    <li><a title="下载发票" href="#">下载发票</a></li>
                                </ul>
                            </div></td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>US</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center"><input name="subBox" type="checkbox" /></td>
                        <td>&nbsp;</td>
                        <td>1</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>BNSUYQ</td>
                        <td>BNSUS161</td>
                        <td>是</td>
                        <td>UK</td>
                        <td>60001YQ</td>
                        <td>&nbsp;</td>
                        <td>100</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <div class="clearfix clear"></div>
            </div>
        </div>


    </div><!---wrapper end--->

</div>


<!---model 弹出页面----->


<!--导入文件 -->
<div class="modal fade" id="btn_import">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件</h4>
            </div>

            <div class="modal-body form-horizontal">
                <div class="alert alert-info top20" role="alert">
                    <p>模板下载: <a href="template-2016-03-21.xls">模板</a></p></div>

                <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                    <div class="col-md-4"><input name="import_file" class="form-control" data-val="true"
                                                 data-val-required="请选择[上传文件]!" id="File" type="file" value="">
                    </div>
                </div>

                <div class="form-group">
                    <button id="submit-button" type="button" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                    </button>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>




<!--导入文件 -->
<div class="modal fade" id="btn_detail">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">导入文件</h4>
            </div>

            <div class="modal-body form-horizontal">
                <div class="alert alert-info top20" role="alert">
                    <p>模板下载: <a href="template-2016-03-21.xls">模板</a></p></div>

                <div class="form-group"><label class="col-md-3 control-label required">上传文件</label>

                    <div class="col-md-4"><input name="import_file" class="form-control" data-val="true"
                                                 data-val-required="请选择[上传文件]!" id="File" type="file" value="">
                    </div>
                </div>

                <div class="form-group">
                    <button id="submit-button" type="button" class="col-md-offset-3 col-md-1 btn btn-danger">上传
                    </button>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>


<!-- 账号分仓对话框 -->
<div class="modal fade" id="btn_delete">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">账号分仓</h4>
            </div>
            <div class="modal-body"><p class="text-primary">你确定要分仓？</p></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>

<!-- 账号分仓对话框 -->
<div class="modal fade" id="btn_check">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">销售员核对</h4>
            </div>
            <div class="modal-body"><p class="text-primary">销售员确定已核对</p></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
            <button type="button" class="btn" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>


<!-------------------------当前页面js引用处--------------------------------->
<?php include 'common/bottom.html' ?>
<script>
    $(function () {
        $('select.chosen-select').chosen({
            no_results_text: '没有找到',    // 当检索时没有找到匹配项时显示的提示文本
            disable_search_threshold: 2, // 10 个以下的选择项则不显示检索框
            search_contains: true         // 从任意位置开始检索
        });
    });
</script>

<script>

    $(document).ready(function() {
        $('#example').dataTable( {
            "searching": true,
            "dom": 'rt<"bottom"ilp<"clear">>',
            "scrollX": true,
            "columnDefs": [
                {orderable: false, targets: 0},
                {orderable: false, targets: 1}
                // {orderable: false, targets: 26}
            ],//第一列与第二列禁止排序

            "language": {
                "url":"zh_CN.txt",
            }
        } );
    } );

</script>



</body>
</html>