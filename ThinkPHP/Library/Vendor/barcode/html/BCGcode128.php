<?php
define('IN_CB', true);
include('include/header.php');

$default_value['start'] = '';
$start = isset($_POST['start']) ? $_POST['start'] : $default_value['start'];
registerImageKey('start', $start);
registerImageKey('code', 'BCGcode128');

$vals = array();
for($i = 0; $i <= 127; $i++) {
    $vals[] = '%' . sprintf('%02X', $i);
}
$characters = array(
    'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'TAB', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI', 'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US',
    '&nbsp;', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
    '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
    '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', 'DEL'
);
?>

<ul id="specificOptions">
    <li class="option">
        <div class="title">
            <label for="start">開頭為</label>
        </div>
        <div class="value">
            <?php echo getSelectHtml('start', $start, array('NULL' => '自動', 'A' => 'Code 128-A', 'B' => 'Code 128-B', 'C' => 'Code 128-C')); ?>
        </div>
    </li>
</ul>

<div id="validCharacters">
    <h3>有效字符</h3>
    <?php $c = count($characters); for ($i = 0; $i < $c; $i++) { echo getButton($characters[$i], $vals[$i]); } ?>
</div>

<div id="explanation">
    <h3>說明</h3>
    <ul>
        <li>Code 128是一個高密度的字母數字組合.</li>
        <li>被全世界廣泛使用.</li>
        <li>Code 128被設計用來編碼128個ASCII碼.</li>
        <li>包括校验数字符号.</li>
        <li>Code 128A處理大寫字母<br />Code 128B處理大小寫字母<br />Code 128C處理兩組數字</li>
        <li>瀏覽器可能無法顯示類似(NUL, SOH, etc.)的字符,但是你可以將其寫入代碼即可.</li>
    </ul>
</div>

<?php
include('include/footer.php');
?>