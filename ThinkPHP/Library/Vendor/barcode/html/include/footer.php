<?php
if (!defined('IN_CB')) { die('您無權訪問該頁.'); }
?>

            <div class="output">
                <section class="output">
                    <h3>輸出</h3>
                    <?php
                        $finalRequest = '';
                        foreach (getImageKeys() as $key => $value) {
                            $finalRequest .= '&' . $key . '=' . urlencode($value);
                        }
                        if (strlen($finalRequest) > 0) {
                            $finalRequest[0] = '?';
                        }
                    ?>
                    <div id="imageOutput">
                        <?php if ($imageKeys['text'] !== '') { ?><img src="image.php<?php echo $finalRequest; ?>" alt="Barcode Image" /><?php }
                        else { ?>請填寫上方的配置.<?php } ?>
                    </div>
                </section>
            </div>
        </form>
    </body>
</html>