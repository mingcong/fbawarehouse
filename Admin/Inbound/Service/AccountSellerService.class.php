<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-16
 * Time: 上午9:37
 */
namespace Inbound\Service;
use Inbound\Service\PublicInfoService;
use Inbound\Model\AccountSellerModel;

class AccountSellerService {
    public $AccountSeller       = null;
    //查询记录总数
    public $count = 0;
    //页面展示包含页面样式
    public $page = 0;
    //数据
    public $data = array();

    public $nowtime;

    public function __construct($table = '',$param = array()) {
        $this->AccountSeller            = D('Inbound/AccountSeller','Model');
        $this->nowtime                  = date('Y-m-d H:i:s',time());
    }
    /**
     * @param array $_array
     * @param bool  $flag
     * @return mixed
     * 首页数据查询与显示
     */
    public function select_detail($_array = array(),$flag = TRUE) {
        $_array = empty($_array)?$_GET:$_array;
        if ($_array['account_id']) {
            $data['account_id'] = $_array['account_id'];
        }
        if ($_array['seller_ids']) {
            $data['seller_ids'] = array('like','%,'.$_array['seller_ids'].',%');
        }
        $accountseller  = $this->AccountSeller->get_this_model($data);
        if($flag){
            $AccountSeller_count       = clone $accountseller;
            $this->count               = $AccountSeller_count->count();//数量
            $Page                      = new \Org\Util\Page($this->count,20);// 实例化分页类 传入总记录数和每页显示的记录数
            $this->page                = $Page->show();// 分页显示输出

            $accountseller->order("account_id")
                ->limit($Page->firstRow.','.$Page->listRows);
        }
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $accountseller     = $accountseller->select();
        foreach ($accountseller as $k =>$v) {
            $accountseller[$k]['account_name'] =  PublicInfoService::get_accounts_by_id($v['account_id'],'name');
            $accountseller[$k]['seller_names'] =  $this->sellerNamesFromIds($v['seller_ids']);
            $accountseller[$k]['user_id'] =  PublicInfoService::get_user_name_by_id($v['user_id']);
        }
        return $accountseller;
    }

    /**
     * @param $data
     * @return mixed
     * 格式化
     */
    public function account_sellers_detail_data($data,$table = 'fba_account_sellers') {
        $sale_arr = PublicInfoService::salesmanGet();
        //var_dump ($sale_arr);exit;
        foreach($data as &$_data){
            //$_data['seller_id']             = PublicInfoService::get_user_name_by_id($_data['seller_id']);
            $_data['account_id']            = PublicInfoService::salesmanBindAccountsGet($table,$_data['account_id']);
            $sale_arr['seller']             = $sale_arr[$_data['seller']];
        }
        return $data;
    }
    /**
     * 描述: 销售员显示格式
     * 作者: kelvin
     */
    public function sellerNamesFromIds($ids) {
        $id = explode(',',$ids);
        $names = '';
        foreach ($id as $v) {
            $name = M('ea_admin', ' ', 'amazonadmin')
                ->where(array('id' => $v,'role_id'=>4))
                ->field('remark')->find();
            if($name){
                $names .= $name['remark'].',';
            }
        }
        return $names;
    }

     /**
      * 描述: 获取帐号下对应的销售
      * 作者: kelvin
      */
    public function getSellerFromAccountId($accountId) {
        if (!$accountId) {
            return FALSE;
        }
        $data['account_id'] = $accountId;
        $result = $this->AccountSeller->get_this_model($data)->find();
        return $result;
    }
    /**
     * 描述: 新增或修改
     * 作者: kelvin
     */
    public function addOrUpdateAccountSeller($account_id, $seller_id, $flag = TRUE) {
        if ($flag) {
            $result = $this->AccountSeller->edit($account_id, $seller_id);
        } else {
            $result = $this->AccountSeller->add($account_id, $seller_id);
        }
        return $result;
    }
    /**
     * 描述: 检查帐号销售员是否已经存在
     * 作者: kelvin
     */
    public function checkAccountAndSeller($account_id, $seller_id) {
        if (!$account_id) {
            return FALSE;
        }
        if (!$seller_id) {
            return FALSE;
        }
        $data['account_id'] = $account_id;
        $data['seller_ids'] = array('like', '%,'.$seller_id.',%');
        $result  = $this->AccountSeller->get_this_model($data)->find();
        return $result;
    }
    /**
     * 描述: 获取帐号销售员
     * 作者: kelvin
     */
    public function delAccountSeller($id) {
        $data['id'] = $id;
        $result = $this->AccountSeller->get_this_model($data)->find();
        $_array = array();
        if ($result) {
            $_array['id'] = $result['id'];
            $_array['account_id'] = $result['account_id'];
            $_array['account_name'] = PublicInfoService::get_accounts_by_id($result['account_id'],'name');
            if(preg_match_all('/[0-9]+/',$result['seller_ids'],$arr)){
                foreach($arr[0] as $k => $v){
                    $_array['seller'][$v] = M('ea_admin', ' ', 'amazonadmin')
                        ->where(array('id' => $v))
                        ->field('remark')->find();
                }
            }

        }
        return $_array;

    }
}