<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/12
 * Time: 9:49
 */
namespace Inbound\Service;

class TransferHopperAddressService
{
    public $transferHopperAddressModel = NULL;

    public function __construct($table = '',$param = array()) {
        $this->transferHopperAddressModel = D('TransferHopperAddress','Model');
    }

    /**
     * @param int $addressId
     * @return mixed
     * 查询所有中转仓库地址或者根据ID获取地址数组
     */
    public function getTransferHopperAddress ($addressId = 0) {
        return $this->transferHopperAddressModel->getTransferHopperAddress(intval($addressId));
    }
}