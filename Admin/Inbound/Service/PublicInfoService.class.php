<?php
/**
 * Created by PhpStorm.
 * User: yanghaize
 * Date: 16-12-30
 * Time: 下午2:47
 */

namespace Inbound\Service;

use Think\Model;

class PublicInfoService
{
    public static $model;

    protected static $_db = 'DB_FBAERP';

    protected static $_db1 = 'fbawarehouse';

    public static $enterprise_dominant = array();  //公司主体


    /**
     * 根据承运商id或是服务商获取启用的承运商和服务商所有信息
     * @param array (
     *              'id'=>服务商id,
     *              'carrier_id'=>'承运商id',
     *              )
     * @return array
     */
    public static function getServiceInfo($_array,$arry = array())
    {
        $model = M('','',self::$_db)
            ->table('fba_carrier a')
            ->join('LEFT JOIN fba_carrier_service b on a.id = b.carrier_id');
        $field = "a.name,a.pickingtime,a.paymethod,a.billingway,b.id,b.carrier_id,b.service_name,b.aging";
        if(isset($_array['id'])&&$_array['id']){
            $arry['b.id'] = $_array['id'];
        }
        if(isset($_array['carrier_id'])&&$_array['carrier_id']){
            $arry['b.carrier_id'] = $_array['carrier_id'];
        }

        $data = $model->where($arry)
            ->field($field)
            ->select();

        return $data?$data[0]:array();
    }

    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function getCompanyId($name)
    {
        return M('statusdics',' ',self::$_db1)
            ->where(array('table' => 'fba_prepare_needs_details'))
            ->where("value LIKE '".$name."'")
            ->getField('number');
    }

    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司id获取公司名称
     */
    public static function getCompanyName($number)
    {
        return M('statusdics',' ',self::$_db1)
            ->where(array('table' => 'fba_prepare_needs_details'))
            ->where("number LIKE '".$number."'")
            ->getField('value');
    }

    /**
     * @return array 公司主体
     * 获取公司主体数组
     */
    public static function get_company_array()
    {
        $arr = M('statusdics',' ',self::$_db1)
            ->where(array('table' => 'fba_prepare_needs_details'))
            ->where(array('colum_name' => 'enterprise_dominant'))
            ->getField('number,value');

        return $arr==NULL?array():$arr;
    }

    /**
     * @return mixed
     * 描述：获取销售状态数组
     */
    public static function getSaleStatus() {
        $status = M('statusdics', ' ', self::$_db1)
            ->where(array(
                'table' => 'api_account_seller_sku',
                'colum_name' => 'sale_status_id'))
            ->field('number,value')
            ->select();

        return $status;
    }

    public static function getSaleStatusName() {
        $status = M('statusdics', ' ', self::$_db1)
            ->where(array(
                'table' => 'api_account_seller_sku',
                'colum_name' => 'sale_status_id'))
            ->getField('number,value');

        return $status;
    }

    /**
     * @param $userId
     * @return array
     * 描述:获取销售员绑定的销售帐号
     */
    public static function salesmanBindAccountsGet($userId=null) {
        $accounts = M('fba_account_sellers', ' ', self::$_db1)
            ->table(self::$_db1 . '.fba_account_sellers AS fas')
            ->join('LEFT JOIN ' . self::$_db1 . '.amazonorder_accounts AS aa ON fas.account_id = aa.id')
            ->join('LEFT JOIN ' . self::$_db1 . '.amazonorder_account_troop_site AS aats ON aa.id = aats.account_id')
            ->join('LEFT JOIN ' . self::$_db1 . '.amazonorder_sites AS ass ON aats.site_id = ass.id')
            ->where('fas.seller_ids like "%,' .$userId .',%"')
            ->getField('aa.id,aa.name,ass.shorthand_code', true);

        return $accounts ? $accounts : array();
    }

    /**
     * @param $accountId
     * @return array
     * 描述：通过帐号获取绑定的销售员的ID和姓名
     */
    public static function accountIdBindSalesmanGet($accountId) {
        $salesmanIds = M('fba_account_sellers', ' ', self::$_db1)
            ->where(array('account_id' => $accountId))
            ->getField('seller_ids');

        $salesmanIds = explode(',', trim($salesmanIds, ','));

        if ($salesmanIds) {
            $salesmanInfo = array();
            foreach ($salesmanIds as $id) {
                $tempArray = array();
                $tempArray['id'] = $id;
                $tempArray['remark'] = M('ea_admin', ' ', 'amazonadmin')
                    ->where(array('id' => $id))
                    ->getField('remark');

                if (!$tempArray['remark']) continue;
                $salesmanInfo[] = $tempArray;
            }
            return !empty($salesmanInfo) ? $salesmanInfo : array();
        } else
            return array();


    }

    /**
     * @return mixed
     * 描述:获取销售员姓名
     */
    public static function salesmanGet() {
        $id = self::roleIdGet(3);
        $ids = implode(',',array_merge(array(3),$id));
        $salesman = M('ea_admin', ' ', 'fbawarehouse1')->where("role_id in ($ids)")->getField('id,remark');
        return $salesman ? $salesman : array();
    }
    /**
     * 描述:获取父角色所有子角色id
     * 作者:kelvin
     */
    public static function roleIdGet($role_id)
    {
        $rec = M('ea_role', ' ', 'fbawarehouse1')->where("pid = $role_id")->getField('id',true);
        $arr = array();
        foreach ($rec as $v){
            $arr[] = $v;
            $arr = array_merge($arr,self::roleIdGet($v));
        }
        return $arr;
    }
    /**
     * @param $accountId
     * @param $skuKeywords
     * @return array
     * 描述:根据关键字获取关联SKU列表
     */
    public static function accountSkuGet($accountId, $skuKeywords) {
        $map['account_id'] = $accountId;
        !empty($skuKeywords) && $map['private_sku'] = array('like', "$skuKeywords%");
        $skus = M('api_account_seller_sku', ' ', self::$_db1)
            ->where($map)
            ->group('private_sku')
            ->order('private_sku ASC')
            ->limit(15)
            ->getField('private_sku', true);
        return !empty($skus) ? $skus : array();
    }

    /**
     * @param $accountId
     * @param $sku
     * @return array
     * 描述:根据关键字获取关联SellerSKU列表
     */
    public static function accountSellerSkuGet($accountId, $sku) {
        $map['account_id'] = $accountId;
        $map['private_sku'] = $sku;
        $map['is_used'] = 1;

        $sellerSkus = M('api_account_seller_sku', ' ', self::$_db1)
            ->where($map)
            ->group('seller_sku')
            ->getField('seller_sku', true);

        return !empty($sellerSkus) ? $sellerSkus : array();
    }

    public static function accountSkuCheck($accountId, $sku) {
        $map['account_id'] = $accountId;
        $map['private_sku'] = $sku;

        $sku = M('api_account_seller_sku', ' ', self::$_db1)
            ->where($map)
            ->getField('private_sku');
        return !empty($sku) ? $sku : '';
    }

    /**
     * @param $accountId
     * @param $sku
     * @return string
     * 描述:获取SKU标题
     */
    public static function accountSkuTitleGet($sku) {
        $map['ssc.sku'] = $sku;
        $skuTitle = M('skusystem_sku_cnname', ' ', self::$_db1)
            ->table(self::$_db1 . '.skusystem_sku_cnname AS ssc')
            ->join('LEFT JOIN ' . self::$_db1 . '.skusystem_cnname AS sc ON sc.id = ssc.attr_id')
            ->where($map)
            ->getField('sc.name');

        return !empty($skuTitle) ? $skuTitle : '';
    }

    public static function getSiteIdByAccountId($accountId) {
        return M('amazonorder_account_troop_site',' ',self::$_db1)
            ->where("account_id = '".$accountId."'")
            ->getField('site_id');
    }

    public static function getMainAccountName() {
        return M('amazonorder_account_troop_site',' ',self::$_db1)
            ->table(self::$_db1 . '.amazonorder_account_troop_site AS aats')
            ->join('LEFT JOIN ' . self::$_db1 . '.amazonorder_account_troop AS aat ON aats.account_troop_id = aat.id')
            ->getField('aats.account_id,aat.name');
    }

    /**
     * @param $name
     * @return mixed id
     * 获取角色id
     */
    public static function get_role_id($name)
    {
        $admin_model = M('ea_admin',' ','amazonadmin');
        return $admin_model->where("remark = '".$name."'")
            ->getField('id');
    }

    /**
     * @param $id
     * @return mixed -用户名
     * 根据id获取用户名
     */
    public static function get_user_name_by_id($id)
    {
        $admin_model = M('ea_admin',' ','amazonadmin');
        return $admin_model->where(array('id' => $id))
            ->getField('remark');
    }
    /**
     * @param $id
     * @return mixed -用户名
     * 获取id,用户名
     */
    public static function get_user_name_and_id()
    {
        $admin_model = M('ea_admin',' ','amazonadmin');
        return $admin_model->where(1)
            ->getField('id,remark');
    }

    /**
     * @param $id
     * @return mixed -用户名
     * 根据id获取用户名
     */
    public static function get_user_id_by_name($name)
    {
        $admin_model = M('ea_admin',' ','amazonadmin');
        return $admin_model->where(array('remark' => $name))
            ->getField('id');
    }

    /**
     * @param $id
     * @return mixed
     * 根据状态id获取状态
     */
    public static function get_prepareneeds_status_by_id($id)
    {
        return M('statusdics',' ',self::$_db1)
            ->where(array('table' => 'fba_prepare_needs'))
            ->where(array('number' => $id))
            ->getField('value');
    }

    /**
     * @param $name
     * @return array
     * 获取站点数组
     */
    public static function get_site_array()
    {
        $arr = M('amazonorder_sites',' ',self::$_db1)
            ->where('is_used = 1')
            ->getField('id,shorthand_code');
        return $arr==NULL?array():$arr;
    }

    /**
     * @param $name
     * @return mixed
     * 根据站点获取站点id
     */
    public static function get_site_id($name)
    {
        return M('amazonorder_sites',' ',self::$_db1)
            ->where("shorthand_code = '".$name."'")
            ->getField('id');
    }
    public static function get_account_id($name)
    {
        return M('amazonorder_accounts',' ',self::$_db1)
            ->where("name = '".$name."'")
            ->getField('id');
    }

    /**
     * @return array 状态数组
     * 获取备货需求表的状态数组
     */
    public static function get_prepareneeds_status_array()
    {
        $arr = M('statusdics',' ',self::$_db1)
            ->where(array('table' => 'fba_prepare_needs'))
            ->where(array('colum_name' => 'status'))
            ->order("number")
            ->getField('number,value');
        return $arr==NULL?array():$arr;
    }

    /**
     * @return array 状态数组
     * 获取平台计划单的状态数组
     */
    public static function get_inbound_status_array($table,$status)
    {
        $arr = M('statusdics',' ',self::$_db1)
            ->where(array(
                'table'  => $table,
                'number' => $status
            ))
            ->getField('value');
        return $arr==NULL?array():$arr;
    }

    /**
     * @return array 状态数组
     * 获取平台计划单的状态
     */
    public static function get_inbound_status()
    {
        $arr = M('statusdics',' ',self::$_db1)
            ->where(array('table' => 'fba_inbound_shipment_plan'))
            ->where(array('colum_name' => 'status'))
            ->order('number asc')
            ->getField('number,value');
        return $arr==NULL?array():$arr;
    }

    /**
     * @return array
     * 获取帐号
     */
    public static function get_accounts()
    {
        $arr = M('amazonorder_accounts',' ',self::$_db1)
            ->field('id,name')
            ->order('id asc')
            ->select();
        return $arr==NULL?array():$arr;
    }

    /**
     * @return array 状态数组
     * 获取帐号
     */
    public static function get_accounts_by_id($account_id,$field)
    {
        $account_name= M('amazonorder_accounts',' ',self::$_db1)
            ->where(array('id'=>$account_id))
            ->getField($field);
        return $account_name==NULL?array():$account_name;
    }

    /**
     * @return array 状态数组
     * 获取$site_id
     */
    public static function get_siteid_by_accountid($account_id)
    {
        $site_id= M('amazonorder_account_troop_site',' ',self::$_db1)
            ->where(array('account_id'=>$account_id))
            ->getField('site_id');
        return $site_id==NULL?array():$site_id;
    }

    /**
     * @return array
     * 获取物流渠道数组
     */
    public static function get_transport_way_array()
    {
        $model = new Model();
        $arr = $model
            ->table(self::$_db1.'.fba_carrier_service a')
            ->join('LEFT JOIN '.self::$_db1.'.fba_carrier b on a.carrier_id=b.id')
            ->field('a.id,a.service_name,b.name')
            ->select();
        $datas = array();
        foreach ($arr as $k=>$v){
            $datas[$v['id']] = $v['name'].'-'.$v['service_name'];
        }
        return $datas;
    }

    /**
     * @return array
     * 获取目的仓地址数组
     */
    public static function get_ship_to_addresses_array()
    {
        $model = new Model();
        $arr = $model
            ->table(self::$_db1.'.api_ship_to_address')
            ->field("id,concat(CenterId,' ',Name,' ',AddressLine1,' ',DistrictOrCounty,' ',City,' ',StateOrProvinceCode,' ',CountryCode,' ',PostalCode) as addresses")
            ->select();
        $datas = array();
        foreach ($arr as $k=>$v){
            $datas[$v['id']] = trim($v['addresses']);
        }
        return $datas;
    }


    /**
     * @param $accountid
     * @param $fnsku
     * 获取title
     */
    public static function get_title_by_sellersku($accountid,$seller_sku){
        $title = M('api_account_seller_sku',' ',self::$_db1)
            ->where(array('account_id'=>$accountid,'seller_sku'=>$seller_sku))
            ->getField('title');
        return $title==NULL?'':$title;
    }

    /**
     * @param $accountId
     * @param array $sellerSku
     * @return mixed
     * 获取平台SKU信息
     */
    public static function accountSellerSkuInfoGet($accountId, $sellerSku = array()) {
        $options = array('account_id' => $accountId, 'is_used' => 1);
        !empty($sellerSku) && $options = array_merge($options, array('seller_sku' => array('IN', $sellerSku)));

        return M('api_account_seller_sku', ' ', self::$_db1)
            ->where($options)
            ->getField('seller_sku,fnsku,private_sku,asin,title', NULL);
    }
    /**
     * 描述: 根据seller_sku获取内部sku
     * 作者: kelvin
     */
    public static function get_sku_by_sellerSku($accountid,$seller_sku){
        $sku = M('api_account_seller_sku',' ',self::$_db1)
            ->where(array('account_id'=>$accountid,'seller_sku'=>$seller_sku))
            ->getField('private_sku');
        return $sku==NULL?'':$sku;
    }
    /**
     * 获得sku的相关信息
     * $param string  sku
     * return Array(
     * [sku] => 'sku',
     * [ename_long] =>'sku英文名',
     * [name] => 'sku中文名',
     * [item] => 'sku品名',
     * [sku_unit] => 'sku申报要素',
     * [hs_code] => 'sku海关编码',
     * [place] => '储位信息表'
     * )
     */
    public static function getSkuCnname($sku,$results = NULL)
    {
        // AND g.isavailable='1'
        if($sku){
            $sku     = mysql_escape_string(trim($sku));
            $sql     = "SELECT a.sku,c.ename_long,e.name,f.item,f.sku_unit,f.hs_code,h.place
                FROM `skus` as a
                LEFT JOIN sku_enname as b on a.sku=b.sku
                LEFT JOIN enname as c on b.attr_id=c.id
                LEFT JOIN sku_cnname as d on a.sku=d.sku
                LEFT JOIN cnname as e on d.attr_id=e.id
                LEFT JOIN sku_taxrate as f on a.sku=f.sku
                LEFT JOIN place_skus as g on a.sku=g.sku
                LEFT JOIN places as h on g.place_id=h.id
                WHERE  a.sku = '$sku'";
            $results = M('',' ','DB_SKU')->query($sql);
        }
        return $results[0]?$results[0]:array();
    }
    /**
     * 描述: 获取海关编码
     * 作者: kelvin
     */
     public function getSkuHsCode($sku) {
         if($sku){
             $sku     = mysql_escape_string(trim($sku));
             $sql     = "SELECT f.hs_code
                FROM `skus` as a
                LEFT JOIN sku_taxrate as f on a.sku=f.sku
                WHERE  a.sku = '$sku'";
             $results = M('',' ','DB_SKU')->query($sql);
         }
         return $results[0]?$results[0]['hs_code']:array();
     }
    /*
     * @param      $sku
     * @param null $results
     * 根据公司内部sku获取sku中文名
     */
    public static function getSkuCnname_bak($sku,$results = NULL)
    {
        if($sku){
            $attr_id = M('skusystem_sku_cnname',' ',self::$_db1)
                ->where(array('sku'=>$sku))
                ->getField('attr_id');
            $sku_cnname = M('skusystem_cnname',' ',self::$_db1)
                ->where(array('id'=>$attr_id))
                ->getField('name');
            return !empty($sku_cnname)?$sku_cnname:array();
        }
    }
    /**
     * 描述: 根据返仓状态显示名称
     * 作者: kelvin
     */
    public static function getNameForBackStatus($name,$number = NULL)
    {
        if ($number){
            return M('statusdics',' ',self::$_db1)
                ->where(array('table' => 'fba_shipment_back'))
                ->where("number = '$number' and colum_name = '".$name."'")
                ->getField('value');
        }else{
            return M('statusdics',' ',self::$_db1)
                ->where(array('table' => 'fba_shipment_back'))
                ->where("colum_name = '".$name."'")
                ->getField('number,value');
        }

    }
    /**
     * 描述: 根据状态显示名称
     * 作者: kelvin
     */
    public static function getNameForStatus($table, $colum, $number = NULL)
    {
        if ($number){
            return M('statusdics',' ',self::$_db1)
                ->where(array('table' => $table))
                ->where("number = '".$number."' and colum_name = '$colum'")
                ->order('number asc')
                ->getField('value');
        }else{
            return M('statusdics',' ',self::$_db1)
                ->where(array('table' => $table,'colum_name'=>$colum))
                ->order('number asc')
                ->getField('number,value');
        }

    }

    /**
     * 描述: 根据节点返回角色id
     * 作者: kelvin
     */
    public static function getRoleIdFromNodeId($nodeId)
    {
        $admin_model = M('ea_access',' ','amazonadmin');
        $role = $admin_model->where("node_id = '".$nodeId."'")
            ->getField('role_id', TRUE);
        array_push($role,'1');
        return $role;
    }
    
    /**
     * 描述: 获取任务列表
     * 作者: kelvin
     */
    public static function tasksList()
    {
        $sellerCheckMsg = array();
        $sellerPlanMsg = array();
        $sellerToWhMsg = array();
        $complianceNeedMsg = array();
        $logisticsNeedMsg = array();
        $logisticsPlanMsg = array();
        $pickMsg = array();
        $boxMsg = array();
        $saveBoxMsg = array();
        $pendMsg = array();
        $shipMsg = array();
        $examineMsg = array();
        $warehouseMsg = array();
        $sellerSaveMsg = array();
        $rejectsSaveMsg = array();
        $skuMsg = array();
        $shippedMsg = array();
        //待销售核对
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('check_sale')))) {
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $sellerNeed = M('prepare_needs_details', 'fba_', self::$_db1)
                        ->field('account_id,account_name,status,count(1) as num')
                        ->where("account_id in ($account) AND status = 10")
                        ->group('account_id')
                        ->select();
                    if ($sellerNeed) {
                        foreach ($sellerNeed as $key => $_arr) {
                            $sellerCheckMsg[$key]['url'] = 'Inbound/Prepareneeds/all_detail?account_id='.$_arr['account_id'].'&status='.$_arr['status'];
                            $sellerCheckMsg[$key]['msg'] = '('.$_arr['account_name'].')待销售审核：'.$_arr['num'] ;
                        }
                    }
                }
            }
        }
        //待平台计划
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('create_fba_stocking_plan')))) {
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $sellerPlan =  M('prepare_needs_details', 'fba_', self::$_db1)
                        ->field('account_id,account_name,status,count(1) as num')
                        ->where("account_id in ($account) AND status = 30")
                        ->group('account_id')
                        ->select();
                    if ($sellerPlan) {
                        foreach ($sellerPlan as $key => $_arr) {
                            $sellerPlanMsg[$key]['url'] = 'Inbound/Prepareneeds/all_detail?account_id='.$_arr['account_id'].'&status='.$_arr['status'];
                            $sellerPlanMsg[$key]['msg'] = '('.$_arr['account_name'].')待平台计划：'.$_arr['num'] ;
                        }
                    }
                }
            }
        }
        //待推送仓库
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('update_plan_and_datail_status')))) {
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $sellerPlan = M('inbound_shipment_plan','fba_',self::$_db1)
                        ->field('account_id,account_name,status,count(1) as num')
                        ->where("account_id in ($account) AND status = 10")
                        ->group('account_id')
                        ->select();
                    if ($sellerPlan) {
                        foreach($sellerPlan as $key => $_arr){
                            $sellerToWhMsg[$key]['url'] = 'Inbound/Inboundshipmentplan/index?account_id='.$_arr['account_id'].'&status='.$_arr['status'];
                            $sellerToWhMsg[$key]['msg'] = '('.$_arr['account_name'].')待推送仓库：'.$_arr['num'];
                        }
                    }
                }
            }
        }
        //待合规预审任务列表
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('compliancePreview')))) {
            $complianceNeed = M('prepare_needs_details', 'fba_', self::$_db1)
                ->field('account_id, status,count(1) as num')
                ->where('status = 15')
                ->group('status')
                ->select();
            if ($complianceNeed) {
                $complianceNeedMsg[0]['url'] = 'Inbound/Prepareneeds/all_detail?status='.$complianceNeed[0]['status'];
                $complianceNeedMsg[0]['msg'] = '待合规预审：'.$complianceNeed[0]['num'] ;
            }

        }
        //待物流预审任务列表
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('logisticsPreview')))) {
            $logisticsNeed = M('prepare_needs_details', 'fba_', self::$_db1)
                ->field('account_id, status,count(1) as num')
                ->where('status = 20')
                ->group('status')
                ->select();
            if ($logisticsNeed) {
                $logisticsNeedMsg[0]['url'] = 'Inbound/Prepareneeds/all_detail?status='.$logisticsNeed[0]['status'];
                $logisticsNeedMsg[0]['msg'] = '待物流预审：'.$logisticsNeed[0]['num'] ;
            }

        }
        //待录物流计划任务列表
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('createTransPlan')))) {
            $logisticsPlan =  M('inbound_shipment_plan', 'fba_', self::$_db1)
                ->field('status,count(1) as num')
                ->where('status = 40')
                ->group('status')
                ->select();
            if ($logisticsPlan) {
                $logisticsPlanMsg[0]['url'] = 'Inbound/Inboundshipmentplan/index?status='.$logisticsPlan[0]['status'];
                $logisticsPlanMsg[0]['msg'] = '待物流计划：'.$logisticsPlan[0]['num'];
            }
        }
        //待拣货任务列表
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('savePickSku')))) {
            $pickPlan =  M('inbound_shipment_plan', 'fba_', self::$_db1)
                ->field('status,count(1) as num')
                ->where('status = 15')
                ->group('status')
                ->select();
            if ($pickPlan) {
                $pickMsg[0]['url'] = 'Inbound/Inboundshipmentplan/index?status='.$pickPlan[0]['status'];
                $pickMsg[0]['msg'] = '待拣货确认：'.$pickPlan[0]['num'];
            }
        }
        //待录入箱唛任务列表
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('create_package_box')))) {
            $boxPlan =  M('inbound_shipment_plan', 'fba_', self::$_db1)
                ->field('status,count(1) as num')
                ->where('status = 20')
                ->group('status')
                ->select();
            if ($boxPlan) {
                $boxMsg[0]['url'] = 'Inbound/Inboundshipmentplan/index?status='.$boxPlan[0]['status'];
                $boxMsg[0]['msg'] = '待录入箱唛：'.$boxPlan[0]['num'];
            }
        }
        //待装箱确认
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('update_plan_and_datail_status_prepackage')))) {
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $saveBoxPlan = M('inbound_shipment_plan','fba_',self::$_db1)
                        ->field('account_id,account_name,status,count(1) as num')
                        ->where("account_id in ($account) AND status = 30")
                        ->group('account_id')
                        ->select();
                    if ($saveBoxPlan) {
                        foreach($saveBoxPlan as $key => $_arr){
                            $saveBoxMsg[$key]['url'] = 'Inbound/Inboundshipmentplan/index?account_id='.$_arr['account_id'].'&status='.$_arr['status'];
                            $saveBoxMsg[$key]['msg'] = '('.$_arr['account_name'].')待装箱确认：'.$_arr['num'];
                        }
                    }
                }
            }
        }
        //待物流计划
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('createTransPlan')))) {
            $logisticsPlan =  M('inbound_shipment_plan', 'fba_', self::$_db1)
                ->field('status,count(1) as num')
                ->where('status = 40')
                ->group('status')
                ->select();
            if ($logisticsPlan) {
                $logisticsPlanMsg[0]['url'] = 'Inbound/Transportplan/createTransPlan.html';
                $logisticsPlanMsg[0]['msg'] = '待物流计划：'.$logisticsPlan[0]['num'];
            }
        }
        //待发货
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('markship')))) {
            $shipPlanResult =  M('transport_plan', 'fba_', self::$_db1)
                ->where('a.status = 50')
                ->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b on a.id=b.transport_plan_id')
                ->field('a.id')
                ->group('a.id')
                ->select();
            $shipPlan[0]['status'] = 50;
            $shipPlan[0]['num'] = count($shipPlanResult);
//            $shipPlan =  M('transport_plan', 'fba_', self::$_db1)
//                ->field('status,count(1) as num')
//                ->where('status = 50')
//                ->group('status')
//                ->select();
            if ($shipPlan && $shipPlan[0]['num'] !=0) {
                $shipMsg[0]['url'] = 'Inbound/Transportplan/index.html?status='.$shipPlan[0]['status'];
                $shipMsg[0]['msg'] = '待发货：'.$shipPlan[0]['num'];
            }
        }
        //待总监确认
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('examineBackShipment')))) {
            $backExamine =  M('shipment_back', 'fba_', self::$_db1)
                ->field('back_status as status,count(1) as num')
                ->where('back_status = 10')
                ->group('back_status')
                ->select();
            if ($backExamine) {
                $examineMsg[0]['url'] = 'Inbound/Inboundshipmentplan/shipmentIdBackList?back_status='.$backExamine[0]['status'];
                $examineMsg[0]['msg'] = '返仓待销售总监审核：'.$backExamine[0]['num'];
            }
        }
        //待分配储位
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('saveBackShipment')))) {
            $warehouse =  M('shipment_back', 'fba_', self::$_db1)
                ->field('back_status as status,count(1) as num')
                ->where('back_status = 20')
                ->group('back_status')
                ->select();
            if ($warehouse) {
                $warehouseMsg[0]['url'] = 'Inbound/Inboundshipmentplan/shipmentIdBackList?back_status='.$warehouse[0]['status'];
                $warehouseMsg[0]['msg'] = '返仓待分配储位：'.$warehouse[0]['num'];
            }
        }
        //不良处理待销售选择
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('rejectsSellerSave')))) {
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $sellerSave = M('shipment_rejects', 'fba_', self::$_db1)
                        ->field('account_id,rejects_status as status,count(1) as num')
                        ->where("account_id in ($account) AND rejects_status = 10")
                        ->group('account_id')
                        ->select();
                    if ($sellerSave) {
                        foreach ($sellerSave as $key => $_arr) {
                            $sellerSaveMsg[$key]['url'] = 'Inbound/Inboundshipmentplan/shipmentIdRejectsList?account_id='.$_arr['account_id'].'&status='.$_arr['status'];
                            $sellerSaveMsg[$key]['msg'] = '不良处理待销售选择：'.$_arr['num'] ;
                        }
                    }
                }
            }
        }
        //不良处理待实物返仓
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('rejectsBackSave')))) {
            $rejectsSave =  M('shipment_rejects', 'fba_', self::$_db1)
                ->field('rejects_status as status,count(1) as num')
                ->where('rejects_status = 20')
                ->group('rejects_status')
                ->select();
            if ($rejectsSave) {
                $rejectsSaveMsg[0]['url'] = 'Inbound/Inboundshipmentplan/shipmentIdRejectsList?status='.$rejectsSave[0]['status'];
                $rejectsSaveMsg[0]['msg'] = '不良处理待实物返仓：'.$rejectsSave[0]['num'];
            }
        }
        //待销售更新sku
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('skuDownload')))) {
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $where = "account_id in ($account) AND (fnsku = '' or private_sku = '' or asin = '') AND is_used =1";
                    $sku_data = M('account_seller_sku','api_','DB_FBAERP')
                        ->field('account_id,count(1) as num')
                        ->where($where)
                        ->group('account_id')
                        ->select();
                    if ($sku_data) {
                        foreach ($sku_data as $key => $_arr) {
                            $skuMsg[$key]['url'] = 'Inbound/Inboundshipmentplan/show_sku_info?accountId='.$_arr['account_id'];
                            $skuMsg[$key]['msg'] = '('.PublicInfoService::get_accounts_by_id($_arr['account_id'],'name').')待更新sku：'.$_arr['num'] ;
                        }
                    }
                }
            }
        }
        //标记发货失败shipment
        if (in_array($_SESSION['current_account']['role_id'],self::getRoleIdFromNodeId(self::getIdFormNodeName('skuDownload')))) {
            //获取销售员信息
            $result  = self::salesmanBindAccountsGet($_SESSION['current_account']['id']);
            if ($result) {
                $account = implode(',', array_keys($result));
                if ($account) {
                    $where = "account_id in ($account) and seller_mark_shiped_status = 10";
                    $sku_data = M('shipment_shiped_failure','fba_','DB_FBAERP')
                        ->field('account_id,count(1) as num,shipmentid')
                        ->where($where)
                        ->group('account_id,shipmentid')
                        ->select();
                    if ($sku_data) {
                        foreach ($sku_data as $key => $_arr) {
                            $shippedMsg[$key]['url'] = 'Inbound/Inboundshipmentplan/index?shipmentid='.$_arr['shipmentid'];
                            $shippedMsg[$key]['msg'] = '('.PublicInfoService::get_accounts_by_id($_arr['account_id'],'name').')待平台标记发货'.$_arr['shipmentid'] ;
                        }
                    }
                }
            }
        }
        $allMsg = array_merge($sellerCheckMsg, $sellerPlanMsg ,$sellerToWhMsg, $logisticsNeedMsg,$complianceNeedMsg, $pickMsg, $boxMsg,
            $logisticsPlanMsg, $saveBoxMsg, $pendMsg, $shipMsg, $examineMsg, $warehouseMsg,$sellerSaveMsg,$rejectsSaveMsg,
            $skuMsg,$shippedMsg
        );
        return $allMsg;
    }
     /**
      * 描述: 根据节点名称获取节点id
      * 作者: kelvin
      */
    public static function getIdFormNodeName($name) {
        $admin_model = M('ea_node',' ','amazonadmin');
        $role = $admin_model->where("name = '".trim($name)."'")
            ->field('id')
            ->find();
        return $role?$role['id']:0;
    }

    /**
     * @param array $privateSku
     * @return mixed
     * 根据SKU数组查询在途库存
     */
    public static function getInternalOnWayStock ($privateSku = array()) {
        $sql = 'SELECT
          `pds`.`site_id`,
          `pds`.`sku`,
          `p`.`export_tax_rebate`,
          `p`.`enterprise_dominant`,
          SUM(
            `pds`.`quantity` - `pds`.`ware_quantity`
          ) AS `onwayStock`
        FROM
          `wms_purchaseorder_details_sites` AS `pds`
        LEFT JOIN
          `wms_purchaseorder_details` AS `pd`
        ON
          `pd`.id = `pds`.`purchaseorder_details_id`
        LEFT JOIN
          `wms_purchaseorders` AS `p`
        ON
          `p`.id = `pd`.`purchaseorder_id`
        WHERE
          `p`.`purchaseorder_date` > DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND `p`.`status` NOT IN(10, 70, 90, 100) AND `pd`.`status` NOT IN(10, 70, 90, 100)';
        !empty($privateSku) && $sql .= ' AND `pds`.`sku` IN (\'' . join('\', \'', $privateSku) . '\')';
        $sql .='GROUP BY
          `pds`.`site_id`,
          `pds`.`sku`,
          `p`.`enterprise_dominant`';

        $result = M('wms_purchaseorder_details_sites', ' ', 'fbawarehouse')->query($sql);
        return $result;
    }

    public static function getSiteActualInventory($privateSku = array()) {
        $result = M('warehouseorders', 'wms_', 'fbawarehouse')
            ->where(array('sku'=>array('IN', $privateSku), 'available_quantity' => array('gt',0)))
            ->group('sku,site_id,enterprise_dominant,export_tax_rebate')
            ->field("sku,sum(available_quantity) as available_quantity,site_id,export_tax_rebate,enterprise_dominant")
            ->order('warehouse_date ASC')
            ->select();
        return $result;
    }
    /*
     * @version 通过sku获取品牌
     * @param string $sku
     * return string
     */
    public static function getLogo($sku){
        if(!empty($sku)){
            $brandArr=M('skusystem_sku_brand',' ',self::$_db1)
                ->where("sku = '".$sku."' and is_ownbrand=1")
                ->getField('brand_id');
            if(!empty($brandArr)){
                $result=M('skusystem_ownbrand',' ',self::$_db1)
                ->where("id = ".$brandArr['brand_id'])
                ->getField('logo');
                if(!empty($result)){
                    return $result;
                }else{
                    return '';
                }
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过sku获取型号
     * @param string $sku
     * return string
     */
    public static function getTypeNumber($sku){
        if(!empty($sku)){
            $typeArr=M('skusystem_skus',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('type_number');
            if(!empty($typeArr)){
                return $typeArr;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过sku获取英文品名
     * @param string $sku
     * return string
     */
    public static function getEnItem($sku){
        if(!empty($sku)){
            $enArr=M('skusystem_sku_taxrate',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('sku,en_item,report_elements,item,hs_code,rate');
            if(!empty($enArr)){
                return $enArr[$sku];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过sku获取净重
     * @param string $sku
     * return string
     */
    public static function getWeight($sku){
        if(!empty($sku)){
            $weightArr=M('skusystem_weight',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('num');
            if(!empty($weightArr)){
                return $weightArr/1000;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    /*
     * @version 通过sku获取毛重
     * @param string $sku
     * return string
     */
    public static function getAsWeight($sku){
        if(!empty($sku)){
            $weightArr=M('skusystem_asweight',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('num');
            if(!empty($weightArr)){
                return $weightArr/1000;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    /*
     * @version 通过sku获取长
     * @param string $sku
     * return string
     */
    public static function getAsLength($sku){
        if(!empty($sku)){
            $weightArr=M('skusystem_aslength',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('num');
            if(!empty($weightArr)){
                return $weightArr;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    /*
     * @version 通过sku获取高
     * @param string $sku
     * return string
     */
    public static function getAsHeight($sku){
        if(!empty($sku)){
            $weightArr=M('skusystem_asheight',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('num');
            if(!empty($weightArr)){
                return $weightArr;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    /*
     * @version 通过sku获取宽
     * @param string $sku
     * return string
     */
    public static function getAsWidth($sku){
        if(!empty($sku)){
            $weightArr=M('skusystem_aswidth',' ',self::$_db1)
                ->where("sku = '".$sku."'")
                ->getField('num');
            if(!empty($weightArr)){
                return $weightArr;
            }else{
                return ''; 
           }
        }else{
            return '';
        }
    }
    /*
     * @version 通过sku获取体积
     * @param string $sku
     * return string
     */
    public static function getVolume($sku){
        if(!empty($sku)){
            $length=PublicInfoService::getAsLength($sku);
            
            $width=PublicInfoService::getAsWidth($sku);
            $height=PublicInfoService::getAsHeight($sku);
            return number_format($length*0.1*$width*0.1*$height*0.1,2);
        }else{
            return 0;
        }
    }      
    /*
     * @version 通过采购单号获取采购单信息
     * @param int $purchaseorder_id
     * return string
     */
    public static function getPurchase($purchaseorder_id){
        if(!empty($purchaseorder_id)){
            $purchaseArr=M('wms_purchaseorders',' ',self::$_db1)
                ->where("id = ".$purchaseorder_id)
                ->getField('id,purchase_id,enterprise_dominant as enterprise_dominant_other,export_tax_rebate');
            if(!empty($purchaseArr)){
                return $purchaseArr[$purchaseorder_id];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过采购单号获取采购单详情
     * @param int $purchaseorder_id
     * return string
     */
    public static function getPurchaseDetail($purchaseorder_id){
        if(!empty($purchaseorder_id)){
            $purchaseArr=M('wms_purchaseorder_details',' ',self::$_db1)
                ->where("purchaseorder_id = ".$purchaseorder_id)
                ->getField('purchaseorder_id,export_tax,sku_name,item,hs_code,quantity,single_price,money,supplier');
            if(!empty($purchaseArr)){
                return $purchaseArr[$purchaseorder_id];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过货件编码shipmentid获取备货需求信息
     * @param string $shipmentid
     * @param string $sku
     * return array
     */
    public static function getNeedInfo($shipmentid,$sku){
        $needArr=M('fba_prepare_needs_details_shipmentid',' ',self::$_db1)
            ->where("shipmentid = '".$shipmentid."'")
//            ->getField('prepare_needs_details_id');
            ->field('prepare_needs_details_id')
            ->select();
        if(!empty($needArr)){
            foreach ($needArr as $k=>$v){
                $result=M('fba_prepare_needs_details',' ',self::$_db1)
                    ->where("id = ".$v['prepare_needs_details_id']." and sku='".$sku."'")
                    ->getField('id,sku,site_id,account_name,site_id,create_time,single_ship,status,seller_id,buyer_id,remark,claim_arrive_time,create_user_id,seller_check_user_id,seller_check_time,logistic_check_user_id,logistic_check_time');
                if(!empty($result)){
                    return $result[$v['prepare_needs_details_id']];
                }else{
                    continue;
                }
            }
        }else{
            return '';
        }
    }
    /**
     * @version 通过colum_name和table获取信息
     * @param string $table
     * @param string $colum_name
     * return array
     */
    public static function getNeedStatusArr($table,$colum_name){
        $result = M('statusdics',' ',self::$_db1)
            ->where(array('table' => $table))
            ->where(array('colum_name' => $colum_name))
            ->getField('number,value');
        return $result==NULL?array():$result;
    }
    /*
     * @version 通过物流计划单transport_plan_id获取物流渠道
     * @param int $transport_plan_id
     * return string
     */
    public static function getTransportServiceName($transport_plan_id){
        $seviceArr = M('fba_transport_plan',' ',self::$_db1)
            ->where(array('id' => $transport_plan_id))
            ->getField('carrier_service_id');
        if(!empty($seviceArr)){
            $result = M('fba_carrier_service',' ',self::$_db1)
            ->where(array('id' => $seviceArr['carrier_service_id']))
            ->getField('service_name');
            if(!empty($result)){
                return $result;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过inbound_shipment_plan_id和sku获取采购单号
     * @param int $inbound_shipment_plan_id
     * @param string $sku
     * return int
     */
    public static function getPurchaseordersId($inbound_shipment_plan_id,$sku){
        if(!empty($sku) && !empty($inbound_shipment_plan_id)){
            $planArr=M('fba_inbound_shipment_plan_detail',' ',self::$_db1)
                ->where("sku = '".$sku."' and inbound_shipment_plan_id=".$inbound_shipment_plan_id)
                ->getField('deliveryorders_id');
            if(!empty($planArr)){
                $warehouseArr=M('wms_warehouse_deliveryorders',' ',self::$_db1)
                    ->where("sku = '".$sku."' and deliveryorders_id=".$planArr)
                    ->getField('warehouseorders_id');
                if(!empty($warehouseArr)){
                    $warehouseArr=M('wms_warehouseorders',' ',self::$_db1)
                        ->where("id=".$warehouseArr)
                        ->getField('purchaseorders_id');
                    return $warehouseArr;
                }else{
                    return '';
                }
                
                return $planArr;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    /*
     * @version 通过inbound_shipment_plan_id和package_box_id获取出库单ID
     * @param int $inbound_shipment_plan_id
     * @param string $package_box_id
     * return int
     */
    public static function getSkuByBox($inbound_shipment_plan_id,$package_box_id){
        if(!empty($package_box_id) && !empty($inbound_shipment_plan_id)){
            $boxArr=M('fba_package_box_details',' ',self::$_db1)
                ->where("package_box_id = ".$package_box_id." and inbound_shipment_plan_id=".$inbound_shipment_plan_id)
                ->getField('package_box_id,sku,declaration_element,seller_sku');
            return $boxArr[$package_box_id];
        }else{
            return 0;
        }
    }
    /*
     * @version 通过sku和site_id获取海外监管条件
     * @param string $sku
     * @param int $site_id
     * return string
     */
    public static function getOverseasSupervision($sku,$site_id){
        if(!empty($sku) && !empty($site_id)){
            $overseasSupervision=M('fba_sku_site_relation',' ',self::$_db1)
                ->where("sku = '".$sku."' and site_id=".$site_id)
                ->order('id desc')
                ->field('overseas_supervision,cargo_grade')
                ->find();
            return $overseasSupervision?$overseasSupervision:null;
        }else{
            return '';
        }
    }
    /*
     * @version 通过采购单号获取采购单详情
     * @param int $purchaseorder_id
     * @param string $sku
     * return string
     */
    public static function getPurchaseDetailOther($purchaseorder_id,$sku){
        if(!empty($purchaseorder_id) || !empty($sku)){
            $purchaseArr=M('wms_purchaseorder_details',' ',self::$_db1)
                ->where("purchaseorder_id = ".$purchaseorder_id." and sku='".$sku."'")
                ->getField('purchaseorder_id,real_tax_rate,export_tax,sku_name,item,hs_code,quantity,single_price,money,supplier');
            if(!empty($purchaseArr)){
                return $purchaseArr[$purchaseorder_id];
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /*
     * @version 通过供应商id获取名称
     * @param string $supplier_id
     * return string
     */
    public static function getSupplierName($supplier_id){
        if(!empty($supplier_id)){
            $supplierArr=M('skusystem_suppliers',' ',self::$_db1)
                ->where("id = ".$supplier_id)
                ->getField('name');
            if(!empty($supplierArr)){
                return $supplierArr;
            }else{
                return ''; 
           }
        }else{
            return '';
        }
    }
    /*
     * @version 获取出库单id
     */
    public static function getDeliveryordersId($inbound_shipment_plan_id,$sku){
        if(!empty($sku) && !empty($inbound_shipment_plan_id)){
            $planArr=M('fba_inbound_shipment_plan_detail',' ',self::$_db1)
                ->where("sku = '".$sku."' and inbound_shipment_plan_id=".$inbound_shipment_plan_id)
                ->getField('deliveryorders_id');
            return $planArr;
        }else{
            return '';
        }
    }
    /*
     * @version 通过出库单ID获取申报要素和数量
     */
    public static function getDeliveryordersInfo($deliveryorders_id){
        if(!empty($deliveryorders_id)){
            $deliveryordersArr=M('wms_deliveryorders',' ',self::$_db1)
                ->where("id = ".$deliveryorders_id)
                ->getField('quantity');
            if(!empty($deliveryordersArr)){
                return $deliveryordersArr;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
    /**
     * 描述: 标记发货问题池插入
     * 作者: kelvin
     */
    public function insertShipPool($accountId,$shipmentId) {
       $ship  = M('fba_shipment_shiped_failure',' ',self::$_db1);
       $data['account_id'] = $accountId;
       $data['shipmentid'] = $shipmentId;
       $data['ware_shiped_user_id'] = $_SESSION['current_account']['id'];
       $data['ware_shiped_time'] = date('Y-m-d H:i:s');
       $data['seller_mark_shiped_status'] = 10;
       $ship->add($data);
    }

    /**
     * 描述: 获取SKU物流属性名字
     * 作者：橙子
     */
    public static function get_sku_logic_name_array() {
        return M('skusystem_sku_logic_name', ' ', self::$_db1)
            ->getField('logic_id,logic_name');
    }

    public static function get_compliance_array() {
        return M('statusdics', ' ', self::$_db1)
            ->where(array('table' => 'fba_prepare_needs_details'))
            ->where(array('colum_name' => 'compliance'))
            ->getField('number,value');
    }
    /**
     * 描述:根据sku获取采购人员名称
     * 作者:kelvin
     */
    public static function getPuNameForSku($sku)
    {
        $skus = array('sku'=>$sku);
        $handle = curl_init();
//        $url = 'http://dev.skusystem.kokoerp.com/putcomplianceapi/putSkuUser';//测试
        $url = 'http://192.168.5.5:802/putcomplianceapi/putSkuUser';//正式
        curl_setopt($handle,CURLOPT_URL,$url);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($handle,CURLOPT_USERAGENT,'Fbawarehouse API/1.0');
        curl_setopt($handle,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($handle,CURLOPT_TIMEOUT,90);
        curl_setopt($handle,CURLOPT_MAXREDIRS,3);
        curl_setopt($handle,CURLOPT_POST,TRUE);
        curl_setopt($handle,CURLOPT_POSTFIELDS,http_build_query($skus));
        curl_setopt($handle,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_0);
        curl_setopt($handle,CURLOPT_HEADER,FALSE);    //不返回头信息

        $response = curl_exec($handle);
        if(curl_errno($handle)) {
            return null;
//            $re          = new \stdClass();
//            $re->code    = 100;
//            $re->message = '连接主机'.$url.'时发生错误: '.curl_error($handle)."<br/>";
//            curl_close($handle);
//            return $re->message;
        }
        curl_close($handle);
        $data = json_decode($response,true);
        if($data['status']==200){
            return $data['data'];
        }else{
            return null;
        }


    }
    
}