<?php
/**
 * Created by PhpStorm.
 * User: gxh
 * Date: 17-1-1
 * Time: 上午11:28
 */

namespace Inbound\Service;

class PackageboxService
{
    public $packageboxdetails = NULL;
    //查询记录总数
    public $count = 0;
    //页面展示包含页面样式
    public $page = 0;
    //数据
    public $packageboxdetails_data = array();

    public $packagebox_data = array();

    public $update_package_info_data = array();

    public $sku_detail = '';

    public $info = '';

    public function __construct($table = '',$param = array())
    {

        $this->packageboxdetails = D('Packageboxdetails','Model');

    }

    public function judge_quantity_package()
    {

    }
    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 平台计划单的创建
     */
    public function parse_package_data(&$_array = array())
    {
        $Inboundshipmentplandetail = D('Inboundshipmentplandetail','Model');

        $condition = array(
            'inbound_shipment_plan_id' => $_array[0]['inbound_shipment_plan_id']);

        $data = $Inboundshipmentplandetail->get_detail_model($condition,'*'
        )->group('sku')->getField('sku,seller_sku,sku_name,sku_virtual,position, quantity,hs_code,declaration_element,sku_cname,sum(quantity) as p_quantity');

//        print_r($_array);exit;
        if(!empty($_array))
        {
            foreach($_array as $key=>&$_detail)
            {
                if($_detail['sku_detail'] == '录入sku')
                {
                    unset($_array[$key]);
                    continue ;
                }

                //print_r($_detail['sku_detail']);exit;
                $this->sku_detail = explode("<br>",$_detail['sku_detail']);

                //unset($_detail['sku_detail']);

                $packageBoxDetails = array();
                foreach($this->sku_detail as $k =>$_sku_list)
                {
                    $sku_detail_list = explode(' : ',$_sku_list);

                    $data[$sku_detail_list[0]]['p_quantity'] = $data[$sku_detail_list[0]]['p_quantity']-$sku_detail_list[1];
                    $packageBoxDetails[] = array(
                        'sku' => $sku_detail_list[0],
                        'quantity' => $sku_detail_list[1],
                        'inbound_shipment_plan_id'=>$_detail['inbound_shipment_plan_id'],
                        'sku_name' => $data[$sku_detail_list[0]]['sku_name'],
                        'seller_sku' => $data[$sku_detail_list[0]]['seller_sku'],
                        'sku_virtual'=> $data[$sku_detail_list[0]]['sku_virtual'],
                        'position' => $data[$sku_detail_list[0]]['position'],
                        'hs_code'=> $data[$sku_detail_list[0]]['hs_code'],
                        'declaration_element' => $data[$sku_detail_list[0]]['declaration_element'],
                        'sku_cname' => $data[$sku_detail_list[0]]['sku_cname'],
                    );
                }

                $_detail['sku_detail'] = $packageBoxDetails;

            }
            foreach($data as $key=>&$_detail)
            {
                if ($_detail['quantity'] == 0) {
                    continue;
                }
                if($_detail['p_quantity'] == $_detail['quantity'])
                {
                    $this->info = array(
                        'flag'=>3,
                        'info'=>$key.'未装箱'
                    );
                    break;
                }elseif($_detail['p_quantity']<0)
                {
                    $this->info = array(
                        'flag'=>1,
                        'info'=>$key.'的数量过大'
                    );
                    break;
                }else if($_detail['p_quantity']>0){
                    $this->info = array(
                        'flag'=>1,
                        'info'=>$key.'的数量不足'
                    );
                    break;
                }
            }

//print_r($this->info);exit;
            if($this->info)
            {
                return $this->info;
            }

            foreach($_array as &$_detail)
            {
                $this->packageboxdetails_data = $_detail['sku_detail'];
                unset($_detail['sku_detail']);
                $this->create($_detail);
            }

            if($this->packageboxdetails->packagebox_detail_id){
                $Inboundshipmentplandetail->update(
                    array('id'=>$_array[0]['inbound_shipment_plan_id']),
                    array(
                        'status'=>30,
                        ),
                    $this->packageboxdetails->model);
                $this->info = array(
                    'flag'=>0,
                    'info'=>"创建成功",
                );

                $this->packageboxdetails->model->commit();
                $Inboundshipment_service = D('Inboundshipmentplan','Service');
                $res = $Inboundshipment_service->caculate_totalinfo_by_shipmentid($_array[0]['inbound_shipment_plan_id']);
            }else{
                $this->info = array(
                    'flag'=>-1,
                    'info'=>"创建失败",
                );
                $this->packageboxdetails->model->rollback();
            }

            return $this->info;
        }
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 平台计划单的创建
     */
    public function create(&$_array = array(),&$model = NULL)
    {
        $model                 = $model == NULL? $this->packageboxdetails->model:$model;
        $this->packagebox_data = array(
            'inbound_shipment_plan_id' => $_array['inbound_shipment_plan_id'],
            'customs_id'               => '',
            'package_code'             => '',
            'length'                   => $_array['length'],
            'width'                    => $_array['width'],
            'height'                   => $_array['height'],
            'weight'                   => $_array['weight'],
            'export_tax_rebate'        => $_array['export_tax_rebate'],
            'enterprise_dominant'      => '',
            'create_time'              => date('Y-m-d H:i:s'),
            'adder_id'                 => $_SESSION['current_account']['id'],
        );
        $this->packageboxdetails->add_by_table($this->packagebox_data,$model);


        if($this->packageboxdetails->packagebox_id){
            foreach($this->packageboxdetails_data as &$_detail)
            {
                $_detail['package_box_id'] = $this->packageboxdetails->packagebox_id;
            }
             //= array(
                /*'platform_plan_id'    => $plan_id,
                'package_box_id'      => $this->packageboxdetails->packagebox_id,
                'sku'                 => $_array[''],
                'sku_name'            => $_array[''],
                'sku_virtual'         => $_array[''],
                'position'            => $_array[''],
                'quantity'            => $_array[''],
                'remark'              => $_array[''],
                'hs_code'             => $_array[''],
                'declaration_element' => $_array[''],
                'sku_cname'           => $_array['']*/
            //);
            $this->packageboxdetails->add_detail_by_table($this->packageboxdetails_data,$model);
        }

        return $this->packageboxdetails->packagebox_detail_id;
    }

    /**
     *修改箱唛信息

     */
    public function update_package_info(&$_array = array(),&$model = NULL)
    {
        $model  = $model==NULL?$this->packageboxdetails->model:$model;
        $this->update_package_info_data = array(
            ''=>$_array[''],
            ''=>$_array[''],
            ''=>$_array[''],
            ''=>$_array[''],
        );
        $this->packageboxdetails->update_by_table($this->update_package_info_data,$model);
        if($this->packageboxdetails->packagebox_update_id){
            $model->commit();
        }else{
            $model->rollback();
        }
        return $this->packageboxdetails->packagebox_update_id;

    }

    /**
     * @param array $_array
     * @param bool  $flag
     * @return mixed
     * 首页数据查询与显示
     */
    public function select(&$_array = array(),$flag = TRUE)
    {
        $_array = empty($_array)?$_GET:$_array;

        $model = $this->packagebox->get_packagebox_model($_array);

        $packagebox_count = clone $model;

        if($flag){
            $this->count = $packagebox_count->count();//数量

            $Page = new \Org\Util\Page($this->count,20);// 实例化分页类 传入总记录数和每页显示的记录数(5)

            $this->page = $Page->show();// 分页显示输出

            $model->order("create_time desc")
                ->limit($Page->firstRow.','.$Page->listRows);
        }

        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $result = $model->select();

        //$result = D('Prepackage','Logic')->format_prepackage_data($result);

        return $result;
    }
}