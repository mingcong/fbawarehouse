<?php

namespace Inbound\Service;

use Home\Service\CommonService;

class VirtualFbaService extends CommonService {
    protected static $_db1 = 'fbawarehouse';
    /**
     * @param $status 店铺状态
     * @return mixed-店铺信息id=>name
     * 根据状态来获取店铺信息
     */
    public static function getAllStore($status = 10){
        
        $accounts = M('virtual_accounts',' ',self::$_db1)
            ->where("accStatus={$status}")
            ->getField('id,accName,siteId',true);
        
        return $accounts ? $accounts : array();
    }
    //判断是否存在sku
    public static function checkSku($sku){
        $sku = M('skusystem_skus',' ',self::$_db1)
            ->where("isavailable=1")
            ->where("sku='{$sku}'")
            ->getField('sku');
        
        return empty($sku) ? '' : $sku;
    }
    
    //通过accountId查找某一个属性值
    public static function getAttrById($accountId, $attr = 'siteId'){
        $attrVal = M('virtual_accounts',' ',self::$_db1)
            ->where("id={$accountId}")
            ->getField($attr);
        
        return empty($attrVal) ? '' : $attrVal;
    }
}