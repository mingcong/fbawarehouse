<?php
/**
 * Created by PhpStorm.
 * User: yanghaize
 * Date: 16-12-30
 * Time: 下午2:47
 * 公用的插件服务
 */

namespace Inbound\Service;
use Home\Service\CommonService;

class BarcodeService extends CommonService
{
    /**
     * 描述：条形码打印
     * 参数：
     *     $option : 数组{
     *         数组 {
     *              shipmentid  : 生成条形码的字符串,
     *              num         : 条形码数量,
     *              title       : 条形码的标题
     *          },...
     *      }
     * 返回： 打印的条形码页面
     * 作者：kelvin 2017-01-06
     */
    public function barcodePrint($option = array()){
        if(!isset($option) && !is_array($option) && empty($option)) return null;
        foreach($option as $k =>$v){
            if(empty($v['fnsku'])||empty($v['num'])||empty($v['title'])) return '参数有误';
        }

        $html = '<!DOCTYPE html>
                <html>
                <head lang="en">
                    <meta charset="UTF-8">
                    <title>商品条形码打印预览</title>
                    <style>
                        body{
                            margin: 0;
                        }
                    </style>
                </head>
                <body>';
        $new='New';
        $make = '';
        foreach($option as $k =>$v){
            if($v['site_id']==107){
                $make = 'MADE IN CHINA';
            }

            for($i = 0;$i<$v['num']+5;$i++){
                $html .= '<table STYLE="width: 70mm;height: 25mm;" cellspacing="0" cellpadding="0">       
                        <tr>
                            <td style="width: 7mm"></td>
                            <td style="text-align: left;width: 55mm"><img src="'.__ROOT__.'/ThinkPHP/Library/Vendor/barcode/html/image.php?filetype=PNG&dpi=300&scale=1&rotation=0&font_family=Arial.ttf&font_size=8
                                &text='.$v['fnsku'].'&thickness=22&start=A&code=BCGcode128" alt="barcode" style="width: 55mm;height: 6.9mm;margin-top:1.6mm"/>
                            </td>
                            <td style="width: 8mm"></td>
                        </tr>
                        <tr>
                            <td style="width: 7mm"></td>
                            <td style="text-align: center;width: 55mm;"><span style="font-size:10px;">'.$v['fnsku'].'</span></td>
                            <td style="width: 8mm"></td>
                        </tr>
                        <tr style="text-align: left">
                            <td style="width: 7mm"></td>
                            <td><span style="font-size: 9px;width: 55mm">'.$this->strMiddleReduceWordSensitive($v['site_id'],$v['title']).' </span></br>
                                <span style="font-size: 9px;">'.$new.'</span>                                
                                <span style="font-size: 9px;float:right;">'.$make.'</span>
                            </td>
                            <td style="width: 8mm"></td>
                        </tr>
                    </table>';
            }
        }
        $html .='</body></html>';
        return $html;
    }
    /*
     * 描述 : 字符串过长中间省略
     * 参数 :
     *      $string  字符串
     *      $max     字符串多长时开始替换
     *      $rep     替换样式
     * 返回 : 处理后的字符串
     * 作者：kelvin 2017-01-06
     * */
    public function strMiddleReduceWordSensitive($site_id, $string, $rep = '...') {
        if($site_id == 55) {
            return mb_substr($string, 0,35);
        }else{
            $max = 31;
            $strlen =  strlen($string);
            if ($strlen <= $max) return $string;

            $lengthtokeep = $max - strlen($rep);
            $start = 0;
            $end = 0;

            if (($lengthtokeep % 2) == 0) {
                $start = $lengthtokeep / 2;
                $end = $start;
            } else {
                $start = intval($lengthtokeep / 2);
                $end = $start + 1;
            }

            $i = $start;
            $tmp_string = $string;
            while ($i < $strlen) {
                if (isset($tmp_string[$i]) and $tmp_string[$i] == ' ') {
                    $tmp_string = substr($tmp_string, 0, $i) . $rep;
                    $return = $tmp_string;
                }
                $i++;
            }

            $i = $end;
            $tmp_string = strrev ($string);
            while ($i < $strlen) {
                if (isset($tmp_string[$i]) and $tmp_string[$i] == ' ') {
                    $tmp_string = substr($tmp_string, 0, $i);
                    $return .= strrev ($tmp_string);
                }
                $i++;
            }
            return mb_substr($string, 0, $start,'UTF-8') . $rep . mb_substr($string,-$end);
        }

    }
}