<?php
/**
 * Created by PhpStorm.
 * User: yanghaize
 * Date: 16-12-30
 * Time: 下午2:47
 * 公用的插件服务
 */

namespace Inbound\Service;
use Home\Service\CommonService;

class CarrierService extends CommonService
{
    /***
     * @param int $carrierServiceId
     * @return mixed
     * 提供承运商服务
     */
    public static function getCarrierService ($carrierServiceId = 0) {
        return D('Carrier')->getCarrierService($carrierServiceId);
    }


    public function carrierSearchName(){
        $carrier = D('Carrier','Model')->selectName();
        return $carrier;
    }
    /**
     * 获取承运商表和承运商服务表的数据
     */

    public function carrierSearch($param,$limit = '')
    {

        //print_r($param);die;
        $where = array();
        //$where = '1=1';
        if(!empty($param['name'])){
            $where['name'] = $param['name'];
        }
        if(!empty($param['pickingtime'])){
            $where['pickingtime'] = $param['pickingtime'];
        }
        if(!empty($param['paymethod'])){
            $where['paymethod'] = $param['paymethod'];
        }
        if(!empty($param['billingway'])){
            $where['billingway'] = $param['billingway'];
        }
        if(!empty($param['id'])){
            $where['id'] = $param['id'];
        }


        if(!empty($param['carrier_id'])){
            $where['carrier_id'] = $param['carrier_id'];
        }
        if(!empty($param['service_name'])){
            $where['service_name'] = $param['service_name'];
        }
        if(!empty($param['aging'])){
            $where['aging'] = $param['aging'];
        }
        if(!empty($param['is_used'])){
            $where['is_used'] = $param['is_used'];
        }

        $carrier = D('Carrier','Model')->selectAll($where,$limit);
        //var_dump($carrier);exit;
        return $carrier;

    }
    /**
     * 通过id获取渠道信息
     */
    public function getDetail($id)
    {
        //print_r($id);die;
        $where = array();
        if($id){
            $where['id'] = $id;
        }
        $carrierDetail = D('Carrier')->editCarrier($where);
        //echo D('Carrier')->getLastSql();exit;
        if($carrierDetail[0]['is_used']==1){
            $carrierDetail[0]['is_used'] = '开启';
        }else{
            if($carrierDetail[0]['is_used']==0){
                $carrierDetail[0]['is_used'] = '关闭';
            }
        }
        return $carrierDetail;
    }
    /**
     * 通过id获取渠道信息
     */
    public function getEdit($id)    {

        $carrierDetail = D('Carrier')->getEdit($id);
        return $carrierDetail;
    }

    /**
     * 修改渠道货代信息时，验证重复记录
     * @return string
     */

    public function findSameEditCarrier($name)
    {
        $carrier       = D('Carrier');
        $where         = array();
        $where['name'] = $name;
        $carrierInfo   = $carrier->where($where)
            ->find();
        return $carrierInfo;
    }


    /**
     * 通过类型和键搜索
     * @return string
     */
    public function getCarrierByIdAndName($name)
    {
        $carrier     = D('Carrier');
        $where       = "name='".$name."'";
        $carrierInfo = $carrier->where($where)
            ->find();
        return $carrierInfo;
    }
    /**
     * 添加货代以及服务
     * @return string
     */
    public function addCarrier($data)
    {
        $carrier = D('Carrier','Model');
        return $result = $carrier->add($data);
    }
    public function addCarrierService($carrierData){
        $num = 0;
        $carrier = M('fba_carrier_service',null,'DB_FBAERP');
        for($i=0;$i<count($carrierData['service_name']);$i++){
            $data['service_name'] = $carrierData['service_name'][$i];
            $data['aging'] = $carrierData['aging'][$i];
            $data['is_used'] = $carrierData['is_used'][$i];
            $data['carrier_id'] = $carrierData['carrier_id'];
            if($carrier->add($data)){
                $num +=1;
            }
        }
        if($num == count($carrierData['service_name'])){
            return true;
        }else{
            return FALSE;
        }
    }
    /**
     * 修改货代以及服务商状态
     * @return string
     */
    public function editCarrier($carrierData,$id)
    {

        $carrier     = D('Carrier');
        $where       = array();
        $where['id'] = $id;
        return $result = $carrier->where($where)
            ->save($carrierData);
        //var_dump ($result);exit;
    }
    public function editCarrierService($carrierData)
    {
        $num = 0;
        $carrier = M('fba_carrier_service',null,'DB_FBAERP');
        for($i=0;$i<count($carrierData['service_id']);$i++){
            $data['service_name'] = $carrierData['service_name'][$i];
            $data['aging'] = $carrierData['aging'][$i];
            $data['is_used'] = $carrierData['is_used'][$i];
            if($carrier->where("id = ".$carrierData['service_id'][$i])->save($data)){
                $num +=1;
            }
        }
        if($num == count($carrierData['service_id'])){
            return true;
        }else{
            return FALSE;
        }
    }









    /**
     * 改变货代承运商开启关闭状态
     */
    public function setCarrierStatus($carrier_service)
    {
        $carrier = D('Carrier');
        $carrier_service = where()
            ->setField(array('is_used=>1'));
    }

}