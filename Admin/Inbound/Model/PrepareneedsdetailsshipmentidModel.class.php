<?php
/**
 * Created by PhpStorm.
 * User: khq
 * Date: 17-1-10
 * Time: 下午10:19
 */
namespace Inbound\Model;
use Inbound\Model\CommoninterfaceModel;
class PrepareneedsdetailsshipmentidModel extends CommoninterfaceModel{

    public $obj = NULL;

    //备货需求表
    static $table = 'fba_prepare_needs_details_shipmentid';

    //数据库对象
    public $prepareneedsdetailsshipmentid = NULL;

    /**
     * PrepareneedsModel constructor.
     * @param string $table
     * @param array $param
     * 默认的构造方法
     */
    public function __construct($table = '',$param=array()){
        $this->prepareneedsdetailsshipmentid = M(self::$table,' ',$this->_db);
        parent::__construct();
    }

    public function create(&$_array = array())
    {
        $this->prepareneedsdetailsshipmentid = parent::create($_array, $this->model->table("$this->_db.".self::$table));
    }

    public function update($_array, $_param, &$model = NULL)
    {
        return parent::update($_array, $_param, $this->model->table("$this->_db.".self::$table)); // TODO:
    }

    /**
     * @param $needDetailId
     * @return int
     * 描述：根据备货需求ID获取shipment的状态
     */
    public function getShipmentStatusByNeedDetailId($needDetailId) {
        $status = $this->prepareneedsdetailsshipmentid
            ->table('fba_prepare_needs_details_shipmentid AS a')
            ->join('LEFT JOIN fba_inbound_shipment_plan AS b ON a.shipmentid = b.shipmentid')
            ->where(array('a.prepare_needs_details_id' => $needDetailId))
            ->getField('b.status');

        return $status ? $status : 0;

    }

    public function getComplianceInfo($shipmentId) {
        return $this->prepareneedsdetailsshipmentid
            ->table('fba_prepare_needs_details_shipmentid AS a')
            ->join('JOIN fba_prepare_needs_details AS b ON a.prepare_needs_details_id = b.id')
            ->join('JOIN fba_prepare_needs_sku_compliance AS c ON b.site_id = c.site_id AND b.sku = c.sku')
            ->where(array('a.shipmentid' => $shipmentId))
            ->getField('b.sku,b.compliance,c.comment');
    }
}