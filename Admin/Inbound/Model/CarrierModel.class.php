<?php
/**
 * Created by PhpStorm.
 * User: gxh
 * Date: 16-12-30
 * Time: 上午9:56
 */

namespace Inbound\Model;

use Think\Model;

class CarrierModel extends Model
{
    protected $connection = 'DB_FBAERP';
    protected $tablePrefix = 'fba_';

    /**
     * @param int $carrierServiceId
     * @return array|int|mixed
     * 提供承运商服务
     */
    public function getCarrierService($carrierServiceId = 0) {
        $carrierServicesCache = S('CarrierServicesCache');
        if(!$carrierServicesCache or !is_array($carrierServicesCache)){
            $this->join(
                array('fba_carrier_service ON fba_carrier.id = fba_carrier_service.carrier_id'),
                'LEFT'
            )->where(
                'fba_carrier . is_used = 1 AND fba_carrier_service . is_used = 1'
            );

            $result =  $this->getField('fba_carrier_service.id, fba_carrier.name, fba_carrier_service.service_name');

            foreach ($result as $row) {
                $carrierServicesCache[$row['id']] = $row['name'] . '-' . $row['service_name'];
            }

            S('CarrierServicesCache', $carrierServicesCache, 3600);
        }

        return $carrierServiceId == 0 ?
            $carrierServicesCache :
            (
                $carrierServicesCache[$carrierServiceId] ? $carrierServicesCache[$carrierServiceId] : 0
            );
    }
    /**
     * 查询承运商名称
     * @return 数据
     */
    public function selectName()
    {
        //print_r($_array);die;
        $carrier = M('fba_carrier',NULL,'DB_FBAERP');
        $data = $carrier
            ->select();
        //echo $carrier->getLastSql();exit;
        //var_dump ($data);exit;
        return $data;
    }

    /**
     * 查询承运商
     * @param  array $array 查询条件
     * @return 数据
     */
    public function select(&$_array = array(),$limit = '',$flag = TRUE)
    {
        //print_r($_array);die;
        $carrier = M('fba_carrier',NULL,'DB_FBAERP');
        $data = $carrier
            -> table('fba_carrier a ')
            ->join('INNER JOIN fba_carrier_service b on a.id = b.carrier_id')
            ->where($_array)
            -> field('a.name,a.pickingtime,a.pickingaddress,a.paymethod,a.billingway,a.is_used,a.id,b.carrier_id,b.service_name,b.aging')
            ->limit($limit)
            ->select();
        //echo $carrier->getLastSql();exit;
       //var_dump ($data);exit;
        return $data;
    }
    /**
     * 查询承运商
     * @param  array $array 查询条件
     * @return 数据
     */
    public function selectAll(&$_array = array(),$limit = '',$flag = TRUE)
    {
        //print_r($_array);die;
        $carrier = M('fba_carrier',NULL,'DB_FBAERP');
        $data = $carrier
            -> table('fba_carrier a ')
            ->join('LEFT JOIN fba_carrier_service b on a.id = b.carrier_id')
            ->where($_array)
            -> field('a.id,a.name,a.pickingtime,a.pickingaddress,a.paymethod,a.billingway,a.is_used,a.id,GROUP_CONCAT(b.service_name,"||",b.aging,"||",b.is_used) as service')
            ->limit($limit)
            ->group('a.id')
            ->select();
        //echo $carrier->getLastSql();exit;
       //var_dump ($data);exit;
        return $data;
    }
    /**
     * 添加承运商
     * @param $where
     * @param $data
     * @param $data1
     *
     */
    public function addCarrier($where,$data,$data1)
    {
        $carrier = M('fba_carrier',NULL,'DB_FBAERP');
        $carrier_service = M('fba_carrier_service',NULL,'DB_FBAERP');
        $result = $carrier->where($where)->add($data);
        $result1= $carrier_service->where($where)->add($data1);
        return ;


    }
    
    public function editCarrier(&$_array = array(),$limit = '',$flag = TRUE)
    {
        //print_r($_array);die;
        $carrier = M('fba_carrier',NULL,'DB_FBAERP');
        $data = $carrier
            -> table('fba_carrier a ')
            ->join('LEFT JOIN fba_carrier_service b on a.id = b.carrier_id')
            -> where('a.id = '.$_array['id'])
            -> field('a.name,a.pickingtime,a.pickingaddress,a.paymethod,a.billingway,a.is_used,b.id,b.carrier_id,b.service_name,b.aging')
            ->limit($limit)
            ->select();
        //var_dump ($carrier->getLastSql());exit;
        return $data;
    }
    public function getEdit($id)
    {
        //print_r($_array);die;
        $carrier = M('fba_carrier',NULL,'DB_FBAERP');
        $data = $carrier
            -> table('fba_carrier a ')
            ->join('LEFT JOIN fba_carrier_service b on a.id = b.carrier_id')
            -> where('a.id = '.$id)
            -> field('a.id,a.name,a.pickingtime,a.pickingaddress,a.paymethod,a.billingway,a.is_used,GROUP_CONCAT(b.id,"||",b.service_name,"||",b.aging,"||",b.is_used) as service')
            ->select();
        //var_dump ($carrier->getLastSql());exit;
        return $data;
    }












}