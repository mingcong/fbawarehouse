<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-16
 * Time: 上午9:35
 */
namespace Inbound\Model;
class ReportModel extends CommoninterfaceModel {
    public $AccountSeller   = null;
    static $table           = 'report_summary_inventory';
    public $accounts        = null;
    //销售员ID
    public $seller_ids      = '';
    //数据
    public $data = array();
    /**
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '',$param=array()){

        $this->AccountSeller= M(self::$table,'api_',$this->_db);
        parent::__construct();
    }

    /**
     * 获取model对象
     * 参数 查询条件
     * 返回 对象
     */
    public function get_this_model(&$_array) {
        $result =  $this->AccountSeller ->where('id=1')-> select();
        return $result;
    }
    /**
     * 描述: 添加
     * 作者: kelvin
     */
    public function add($account_id, $seller_ids) {
        $data['account_id'] = $account_id;
        $data['seller_ids'] = $seller_ids;
        $data['is_used'] = 1;
        $result =  $this->AccountSeller->add($data);
        return $result;
    }
    /**
     * 描述: 添加
     * 作者: kelvin
     */
    public function edit($account_id, $seller_ids) {
        $where['account_id'] = $account_id;
        $data['seller_ids'] = $seller_ids;
        $result =  $this->AccountSeller->where($where)->save($data);
        return $result;
    }
}