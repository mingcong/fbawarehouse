<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/12
 * Time: 9:51
 * 中转仓库地址模型
 */
namespace Inbound\Model;
use Think\Model;
class TransferHopperAddressModel extends Model{
    protected $connection = 'DB_FBAERP';
    protected $trueTableName = 'fba_transfer_hopper_address';

    /**
     * @return mixed|string|void
     * 获取中转仓库地址配置
     */
    public function getTransferHopperAddress($addressId = 0) {
        $transferHopperAddressCache = S('TransferHopperAddress');
        if(!$transferHopperAddressCache or !is_array($transferHopperAddressCache)){
            $address = $this->where('is_used = 1')->select();
            foreach ($address as $row) {
                $transferHopperAddressCache[$row['id']] = $row;
            }
            S('transferHopperAddress', $transferHopperAddressCache, 86400);
        }

        return $addressId === 0 ?
            $transferHopperAddressCache :
            (
                isset($transferHopperAddressCache[$addressId]) ? $transferHopperAddressCache[$addressId] : array()
            );
    }
}