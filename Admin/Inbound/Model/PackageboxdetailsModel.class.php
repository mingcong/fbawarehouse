<?php
/**
 * Created by PhpStorm.
 * User: gxh
 * Date: 17-1-1
 * Time: 上午11:22
 */

namespace Inbound\Model;

use Inbound\Model\PackageboxModel;

class PackageboxdetailsModel extends PackageboxModel
{

    public $Packageboxdetail = NULL;
    //备货需求表
    static $table = 'package_box_details';
    //备货需求ID
    public $packagebox_detail_id;
    //备货需求编码
    public $packagebox_detail_code;
    //需求表数据
    //数据
    public $packagebox_detail_data = array();

    public function  __construct($table = '',$param = array())
    {
        $this->Packageboxdetail = M(self::$table,'fba_',$this->_db);
        parent::__construct();
    }


    public function select_detail(&$_array = array(),$flag = TRUE)
    {
    }

    /**
     *
     */
    public function add_detail_by_table($_array = array(),&$model = NULL)
    {
        //插入平台计划单详情
        $this->packagebox_detail_id = $model->table($this->_db.'.fba_'.self::$table)
            ->addAll($_array);

        return $this->packagebox_detail_id;
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_detail_model(&$_array = array())
    {
        if($_array){
            $_array = array_filter($_array);
        }else{
            //$_array = array('status' => 10);
        }

        foreach($_array as $key => $_arr){
            switch($key){

                case 'create_time_from':
                    $_array['create_time'][] = array(
                        'gt',
                        $_arr
                    );
                    unset($_array['create_time_from']);
                    break;
                case 'create_time_to':
                    $_array['create_time'][] = array(
                        'lt',
                        $_arr
                    );
                    unset($_array['create_time_to']);
                    break;
            }
        }
        return $this->Packageboxdetail->where($_array);
    }
}