<?php
/**
 * Created by PhpStorm.
 */

namespace Inbound\Model;

use Inbound\Model\CommoninterfaceModel;

class PackageboxModel extends CommoninterfaceModel
{
    public $Packagebox = NULL;
    //备货需求表
    static $table = 'package_box';
    //备货需求ID
    public $packagebox_id;
    public $packagebox_update_id;
    //备货需求编码
    public $packagebox_code;
    //需求表数据
    //数据
    public $packagebox_data = array();

    public function __construct($table = '',$param = array())
    {
        $this->Packagebox = M(self::$table,'fba_',$this->_db);
        parent::__construct();
    }

    public function init()
    {
    }

    public function select(&$_array = array(),$flag = TRUE)
    {
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_this_model(&$_array = array())
    {
        if($_array){
            $_array = array_filter($_array);
        }else{
            //$_array = array('status' => 10);
        }

        foreach($_array as $key => $_arr){
            switch($key){

                case 'create_time_from':
                    $_array['create_time'][] = array(
                        'gt',
                        $_arr
                    );
                    unset($_array['create_time_from']);
                    break;
                case 'create_time_to':
                    $_array['create_time'][] = array(
                        'lt',
                        $_arr
                    );
                    unset($_array['create_time_to']);
                    break;
            }
        }
        return $this->Packagebox->where($_array);
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return array
     * 添加
     */
    public function add_by_table($_array = array(),&$model = NULL)
    {
        $this->packagebox_id = $model->table($this->_db.'.fba_'.self::$table)
            ->add($_array);

        return $this->packagebox_id;
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return array
     * 更新
     */
    public function update_by_table($_array = array(),&$model = NULL)
    {
        $this->packagebox_update_id = $model->table($this->_db.'.'.self::$table)
            ->save($_array);

        return $this->packagebox_update_id;
    }
}