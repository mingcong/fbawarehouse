<?php
/**
 * Created by PhpStorm.
 * User: khq
 * Date: 16-12-29
 * Time: 下午2:44
 */
namespace Inbound\Model;
use Inbound\Model\CommoninterfaceModel;

class PrepareneedsModel extends CommoninterfaceModel{

    public $obj = NULL;

    //备货需求表
    static $table = 'fba_prepare_needs';
    
    //数据库对象
    public $prepare_needs = NULL;
    
    //备货需求ID
    public $prepare_needs_id;
    
    //备货需求编码
    public $prepare_needs_code;
    
    //数据
    public $data = array();

    /**
     * PrepareneedsModel constructor.
     * @param string $table
     * @param array $param
     * 默认的构造方法
     * khq 2016.12.29
     */
    public function __construct($table = '',$param=array()){
        $this->prepare_needs = M(self::$table,' ',$this->_db);
        parent::__construct();
    }

    public function init(){

    }

    /**
     * @param array $_array
     * @param bool $flag
     * @return bool
     * 查询备货需求表的数据
     * khq 2016.12.29
     */
    public function select(&$_array = array(),$flag = TRUE)
    {
        $data = $this->prepare_needs
            ->where($_array)
            ->select();
        return $data;
    }

    /**
     * @param $batch_code
     * @return mixed
     * 根据备货需求编号查询备货信息
     */
    public function get_data_by_code($batch_code)
    {
        $data = $this->prepare_needs
            ->where(array('batch_code'=>$batch_code))
            ->find();
        return $data;
    }

    /**
     * @return mixed
     * 获取最近一次的批次号
     */
    public function get_batch_code()
    {
        return $this->prepare_needs
            ->order('batch_code DESC')
            ->limit(1)
            ->getfield('batch_code');
    }
    
    public function create(&$_array = array())
    {
        $this->prepare_needs_id = parent::create($_array, $this->model->table("$this->_db.".self::$table));
    }

    public function update($_array, $_param, &$model = NULL)
    {
        return parent::update($_array, $_param, $this->model->table("$this->_db.".self::$table)); // TODO:
    }
}