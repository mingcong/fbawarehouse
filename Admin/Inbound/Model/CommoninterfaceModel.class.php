<?php
/**
 * Created by PhpStorm.
 * User: liqiang
 * Date: 16-12-29
 * Time: 下午3:06
 */

namespace Inbound\Model;
use Think\Model;
class CommoninterfaceModel{
    //数据库
    public $_db     = 'fbawarehouse';

    static $table   = NULL;
    //公有model
    public $model   = NULL;

    public static $link;

    protected $host;

    /**数据库配置
     * 当我们调用一个权限上不允许调用的属性,和不存在的属性时,
     * __get魔术方法会自动调用
     * 参数$host:属性名
     * 返回标志符
     * */
    public function __get($host)
    {
        $this->host = array(
            'DB_HOST' => C('DB_HOST'),
            'DB_USER' => C('DB_USER'),
            'DB_PWD'  => C('DB_PWD'),
            'DB_NAME' => $this->_db,
        );
        return $this->$host;
    }

    public function __construct($table = '',$param=array())
    {
        $this->model = self::start_trans();
    }

    /**
     * 开启事务
     * @param  array  -初始化参数
     * @return object
     */
    public static function start_trans()
    {
        //开启事务
        $model = new Model();

        $model->startTrans();

        return $model;
    }

    /*
     *新增数据
     * @param  array $array 新增的数据
     * @param  obj   $model 事务对象
     * @return BOOL
     * */
    public function create(&$_array = array(),&$model = NULL)
    {
        $model = $model == NULL?$this->model:$model;

        return $model->add($_array);
    }

    /*
     * 批量创建数据
     * @param  array $array 新增的数据
     * @param  obj   $model 事务对象
     * @return BOOL
     * */
    public function batch_create(&$_array = array(),&$model = NULL)
    {
        $model = $model == NULL?$this->model:$model;

        return $model->addAll($_array);
    }

    /*
     * 更新数据
     * @param  array $array 条件
     * @param  array $param 参数
     * @param  obj   $model 事务对象
     * @return BOOL
     * */
    public function update($_array,$_param,&$model = NULL)
    {
        $model = $model == NULL?$this->model:$model;

        if (!empty($_array)) {
            $model->where($_array);
        }

        foreach ($_param as $key => $value) {
            $model->$key = $value;
        }

        return $model->save();
    }
} 