<?php
/**
 * Created by PhpStorm.
 * User: gxh
 * Date: 16-12-30
 * Time: 上午9:56
 */

namespace Inbound\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;

class InboundshipmentplanModel extends CommoninterfaceModel
{
    public $Inboundshipmentplan = NULL;
    //备货需求表
    static $table = 'inbound_shipment_plan';
    //数据库对象
    public $Inboundshipmentplan_data = NULL;
    //备货需求ID
    public $Inboundshipmentplan_id;
    //数据
    public $data = array();

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        //print_r($this->_db);exit;
        $this->Inboundshipmentplan = M(self::$table,'fba_',$this->_db);
        parent::__construct();
    }

    public function init()
    {
    }


    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_this_model(&$_array = array(),$flag=true)
    {
        if($flag){
            if($_array){
                $_array = array_filter($_array);
            }else{
                //$_array = array('status' => 10);
            }
            $_array = PrepareneedsService::trim_array($_array);
            if($_array['sku']){
                $sku = strtoupper($_array['sku']);
                $planId = M('inbound_shipment_plan_detail', 'fba_', 'DB_FBAERP')
                    ->where("sku = '$sku'")
                    ->getField('inbound_shipment_plan_id',TRUE);
                if($planId){
                    $_array['id'] = array('in', $planId);
                }else{
                    $_array['id']= 0;
                }
                unset($_array['sku']);
            }
            if ($_array['print_pick_note_user']) {
                if (trim($_array['print_pick_note_user']) == 1) {
                    $_array['print_pick_note_user'] = array('neq', 0);
                }
                if (trim($_array['print_pick_note_user']) == 2) {
                    $_array['print_pick_note_user'] = array('eq', 0);
                }
            }
            foreach($_array as $key => $_arr){
                switch($key){
                    case 'claim_arrive_time_from':
                        $_array['claim_arrive_time'][] = array(
                            'egt',
                            $_arr
                        );
                        unset($_array['claim_arrive_time_from']);
                        break;
                    case 'claim_arrive_time_to':
                        $_array['claim_arrive_time'][] = array(
                            'elt',
                            $_arr
                        );
                        unset($_array['claim_arrive_time_to']);
                        break;
                    case 'name':
                        unset($_array['name']);
                        break;
                }
            }
            return $this->Inboundshipmentplan->where($_array);
        }else{
            return $this->Inboundshipmentplan->where($_array);
        }
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 插入平台计划单
     */
    public function add_by_table($_array = array(),&$model = NULL)
    {
        $this->Inboundshipmentplan_id = $model
            ->table($this->_db.'.fba_'.self::$table)
            ->add($_array);
        return $this->Inboundshipmentplan_id;
    }

    /*
     * 查询某些字段值
     * */
    public function get_field_by_param($_array = array() , $_param = array('*'))
    {
        return $this->Inboundshipmentplan->where($_array)->field($_param)->select();
    }
    /**
     * 整理下载sql
     */
    public function manage_sql($model){
        $manage_sql = $model->order("create_time desc")->select();
        return $manage_sql->getLastSql();
    }
    /**
     *更新操作
     */
    public  function  update($_array = array(),$data = array(),&$model = NULL){
        $model = $model == NULL? $this->Inboundshipmentplan : $model->table($this->_db.".fba_".self::$table);
        $res = $model->where($_array)->save($data);
        return $res;
    }
    /*
     * @version fba_inbound_shipment_plan表和fba_inbound_shipment_plan_detail表拼装起来数组
     * @param array $(&$_array = array()
     * return array
     */
    public function get_plan_detail(&$_array = array()){
        $this->Inboundshipmentplan->table('fba_inbound_shipment_plan fisp')
            ->join('LEFT JOIN fba_inbound_shipment_plan_detail fispd on fisp.id = fispd.inbound_shipment_plan_id')
            ->join('LEFT JOIN wms_warehouse_deliveryorders wwd on wwd.deliveryorders_id = fispd.deliveryorders_id')
            ->join('LEFT JOIN wms_warehouseorders ww on ww.id = wwd.warehouseorders_id')
            ->field("fisp.*,fispd.*,ww.purchaseorders_id,ww.single_price,ww.money");
//            ->field("fisp.*,fispd.*");
        if(!empty($_array)){
            $_array = array_filter($_array);
        }else{
//            exit('请先选择筛选条件，再进行下载');
        }
        $_array = PrepareneedsService::trim_array($_array);
        if ($_array['print_pick_note_user']) {
            if (trim($_array['print_pick_note_user']) == 1) {
                $_array['fisp.print_pick_note_user'] = array('neq', 0);
            }
            if (trim($_array['print_pick_note_user']) == 2) {
                $_array['fisp.print_pick_note_user'] = array('eq', 0);
            }
        }
        if (!empty($_array['status'])) {
            $_array['fisp.status'] = array('eq', $_array['status']);
            unset($_array['status']);
        }
        if (!empty($_array['site_id'])) {
            $_array['fisp.site_id'] = array('eq', $_array['site_id']);
            unset($_array['site_id']);
        }
        foreach($_array as $key => $_arr){
            switch($key){
                case 'claim_arrive_time_from':
                    $_array['fisp.claim_arrive_time'][] = array(
                        'egt',
                        $_arr
                    );
                    unset($_array['claim_arrive_time_from']);
                    break;
                case 'claim_arrive_time_to':
                    $_array['fisp.claim_arrive_time'][] = array(
                        'elt',
                        $_arr
                    );
                    unset($_array['claim_arrive_time_to']);
                    break;
                case 'name':
                    unset($_array['name']);
                    break;
            }
        }
        return $this->Inboundshipmentplan->where($_array);
    }

}