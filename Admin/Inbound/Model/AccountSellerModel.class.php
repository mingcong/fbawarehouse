<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-16
 * Time: 上午9:35
 */
namespace Inbound\Model;
class AccountSellerModel extends CommoninterfaceModel {
    public $AccountSeller   = null;
    static $table           = 'account_sellers';
    public $accounts        = null;
    //销售员ID
    public $seller_ids      = '';
    //数据
    public $data = array();
    /**
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '',$param=array()){

        $this->AccountSeller= M(self::$table,'fba_',$this->_db);
        parent::__construct();
    }

    /**
     * 获取model对象
     * 参数 查询条件
     * 返回 对象
     */
    public function get_this_model(&$_array) {
        $_array['is_used'] = 1;
        $result =  $this->AccountSeller -> where($_array)->field('id,account_id,seller_ids,update_time,user_id');
        return $result;
    }
    /**
     * 描述: 添加
     * 作者: kelvin
     */
    public function add($account_id, $seller_ids) {
        $data['account_id'] = $account_id;
        $data['seller_ids'] = $seller_ids;
        $data['user_id'] = $_SESSION['current_account']['id'];
        $data['is_used'] = 1;
        $result =  $this->AccountSeller->add($data);
        return $result;
    }
    /**
     * 描述: 添加
     * 作者: kelvin
     */
    public function edit($account_id, $seller_ids) {
        $where['account_id'] = $account_id;
        $data['seller_ids'] = $seller_ids;
        $data['user_id'] = $_SESSION['current_account']['id'];
        $result =  $this->AccountSeller->where($where)->save($data);
        return $result;
    }
}