<?php
/**
 * Created by PhpStorm.
 * User: liqiang
 * Date: 16-12-29
 * Time: 下午2:39
 */
namespace Inbound\Model;

use Inbound\Service\PublicInfoService;
use Think\Model;

class TransportPlanModel
{
    //protected $db = 'fbawarehouse';
    protected $connection = 'DB_FBAERP';
    //protected $trueTableName = 'fba_transport_plan';
    protected $tablePrefix = '';//fba_
    /*public $obj              = NULL;
    //备货需求表
    static $table           = 'transport_plan';
    //备货需求ID
    public $transportplan_id;
    //备货需求编码
    public $transportplan_code;
    //需求表数据
    public $transportplan;
    //数据
    public $data             = array();*/

    /**
     * 初始化数据
     * @param  array  初始化参数
     * @return
     */
    public function init($array = array())
    {
        return TRUE;
    }

    /**
     * 查询物流计划单
     * @param  array $array 查询条件
     * @return 数据
     */
    public function select(&$_array = array(),$limit = '',$flag = TRUE)
    {
        $pubser              = new \Inbound\Service\PublicInfoService();
        $carrier_service_arr = PublicInfoService::get_transport_way_array();
        //var_dump($_array);exit;
        $trans = M('fba_transport_plan',NULL,'DB_FBAERP');
        //        var_dump(strlen($limit));
        //        exit;
        if(strlen($limit)>0){
            $data = $trans->where($_array)
                ->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b on a.id=b.transport_plan_id')
                ->field('a.*,sum(b.total_package) as sumboxs,sum(b.total_weight) as sumweight,sum(b.total_capacity) as sumvol')
                ->limit($limit)
                ->group('a.id')
                ->order(array('a.id' => 'desc'))
                ->select();
            //echo $trans->getLastSql();exit;
            for($i = 0;$i<count($data);$i++){
                /*$data[$i]['detail']  = $trans->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b JOIN fba_inbound_shipment_plan_detail c')
                    ->field('a.status,b.shipmentid,c.sku,c.sku_virtual,c.sku_name,c.position,c.quantity,c.export_tax_rebate,b.account_id,b.merchant_id')
                    ->where('a.id = b.transport_plan_id AND b.id=c.inbound_shipment_plan_id AND a.id='.$data[$i]['id'])
                    ->select();*/
                $data[$i]['create_user_id']     = PublicInfoService::get_user_name_by_id($data[$i]['create_user_id']);
                $data[$i]['pickup_op_user_id']  = PublicInfoService::get_user_name_by_id($data[$i]['pickup_op_user_id']);
                $_array['c.table']              = 'fba_inbound_shipment_plan';
                $data[$i]['detail']             = $trans->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b JOIN statusdics c JOIN amazonorder_sites d 
                    JOIN fba_carrier_service e JOIN fba_transfer_hopper_address f')
                    ->field('b.shipmentid,c.value,b.batch_code,b.account_name,d.shorthand_code,e.service_name,
                        f.name,b.ship_to_address_id,b.claim_arrive_time,b.total_package,b.total_weight,b.total_capacity')
                    ->where($_array)
                    ->where('a.id = b.transport_plan_id AND b.status=c.number AND b.site_id=d.id AND b.carrier_service_id=e.id 
                        AND b.tranfer_hopper_id=f.id AND a.id='.$data[$i]['id'])
                    ->select();
                $data[$i]['carrier_service_id'] = $carrier_service_arr[$data[$i]['carrier_service_id']];
                //echo $trans->getLastSql();exit;
            }
        }else{
            //echo "string1";exit;
            /*$data  = $trans->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b JOIN fba_inbound_shipment_plan_detail c')
                ->where($_array)
                ->where('a.id = b.transport_plan_id AND b.id=c.inbound_shipment_plan_id')
                ->field('a.status,a.transport_plan_code,a.certificate_code,a.carrier_service_id,a.create_user_id,a.create_time,a.expected_pickup_time,
                    a.pickup_op_user_id,a.pickup_op_time,a.arrive_port_date,a.take_off_date,a.arrive_time,a.customs_clearance_date,a.delivery_date,
                    b.shipmentid,c.sku,c.sku_virtual,c.sku_name,c.position,c.quantity,c.export_tax_rebate,b.account_id,b.merchant_id')
                ->select();*/
            $_array['c.table'] = 'fba_inbound_shipment_plan';
            $data              = $trans->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b JOIN statusdics c JOIN amazonorder_sites d 
                    JOIN fba_carrier_service e JOIN fba_transfer_hopper_address f')
                ->where($_array)
                ->where('a.id = b.transport_plan_id AND b.status=c.number AND b.site_id=d.id AND b.carrier_service_id=e.id AND b.tranfer_hopper_id=f.id')
                ->field('c.value,a.transport_plan_code,a.contract_no,a.invoice_code,a.certificate_code,a.carrier_service_id,a.create_user_id,a.create_time,
                    a.expected_pickup_time,a.ori_driver_name,a.ori_license_plate_num,a.ori_tracking_num,a.ori_seal_num,
                    a.pickup_op_user_id,a.pickup_op_time,a.arrive_port_date,a.take_off_date,a.arrive_time,a.customs_clearance_date,a.delivery_date,
                    b.shipmentid,b.batch_code,b.account_name,d.shorthand_code,e.service_name,f.name,b.ship_to_address_id,b.claim_arrive_time,
                    b.total_package,b.total_weight,b.total_capacity')
                ->group('a.id')
                ->select();
            for($i = 0;$i<count($data);$i++){
                $data[$i]['create_user_id']     = PublicInfoService::get_user_name_by_id($data[$i]['create_user_id']);
                $data[$i]['pickup_op_user_id']  = PublicInfoService::get_user_name_by_id($data[$i]['pickup_op_user_id']);
                $data[$i]['carrier_service_id'] = $carrier_service_arr[$data[$i]['carrier_service_id']];
            }
            //echo $trans->getLastSql();exit;
        }
//        var_dump($data);exit;
        return $data;
        //var_dump(self::field('id')->select()->getLastSql());exit;
    }
    /**
     * 查询物流计划单
     * @param  array $array 查询条件
     * @return 数据
     */
    public function selectOther(&$_array = array())
    {
        if(!empty($_array)){
            $_array = array_filter($_array);
        }else{
            //            exit('请先选择筛选条件，再进行下载');
        }
        $data = M('fba_transport_plan',NULL,'DB_FBAERP')
            ->table('fba_transport_plan ftp')
            ->join('LEFT JOIN fba_inbound_shipment_plan fisp on ftp.id = fisp.transport_plan_id')
            ->join('LEFT JOIN fba_package_box fpb on fpb.inbound_shipment_plan_id = fisp.id')
            ->field("ftp.*,fisp.shipmentid,fisp.shipmentid,fisp.ship_from_address_id,fisp.ship_to_address_id,fisp.destination_fullfillment_center_id,fpb.id as package_box_id,fpb.inbound_shipment_plan_id")
            ->where($_array)
            ->select();
        $carrier_service_arr = PublicInfoService::get_transport_way_array();
        $company_arr = PublicInfoService::get_company_array();
        $needStatusArr=PublicInfoService::getNeedStatusArr('fba_prepare_needs_details','status');
        $site_arr= PublicInfoService::get_site_array();
        $i=1;
        foreach ($data as &$_data){
//            $_data['pallet']='';
            $_data['carrier_service_id']= $carrier_service_arr[$_data['carrier_service_id']];

            if(!empty($_data['package_box_id'])){
                $boxData= M('package_box_details','fba_','DB_FBAERP')->where("package_box_id = ".$_data['package_box_id'])->select();
                $num=count($boxData);
                $_data['box_code']=$i."/".$num;
                $boxArr= PublicInfoService::getSkuByBox($_data['inbound_shipment_plan_id'],$_data['package_box_id']);
                $_data['sku']=$boxArr['sku'];
                $_data['declaration_element']=$boxArr['declaration_element'];
                $_data['seller_sku']=$boxArr['seller_sku'];
            }
            if(!empty($_data['sku'])){
                $_data['purchaseorders_id']= PublicInfoService::getPurchaseordersId($_data['inbound_shipment_plan_id'],$_data['sku']);
                $skuArr= PublicInfoService::getEnItem($_data['sku']);
                $_data['en_item']=$skuArr['en_item'];
                $_data['report_elements']=$skuArr['report_elements'];
                $_data['logo']= PublicInfoService::getLogo($_data['sku']);
                $_data['type_number']= PublicInfoService::getTypeNumber($_data['sku']);
                $_data['weight']= PublicInfoService::getWeight($_data['sku']);
                $_data['asweight']= PublicInfoService::getAsWeight($_data['sku']);
                $_data['volume']= PublicInfoService::getVolume($_data['sku']);
                $need_arr = PublicInfoService::getNeedInfo($_data['shipmentid'],$_data['sku']);
                $_data['need_account_name']=$need_arr['account_name'];
                $_data['need_site_id']= $site_arr[$need_arr['site_id']];
                $_data['need_claim_arrive_time']=$need_arr['claim_arrive_time'];
                $_data['need_seller_id']=PublicInfoService::get_user_name_by_id($need_arr['seller_id']);
                $_data['need_single_ship']=$need_arr['single_ship']==1?'是':'否';
                $_data['need_status']=$needStatusArr[$need_arr['status']];
                $_data['transport_carrier_service_id']=  PublicInfoService::getTransportServiceName($_data['transport_plan_id']);
                $_data['need_create_user_id']=PublicInfoService::get_user_name_by_id($need_arr['create_user_id']);
                $_data['need_create_time']=$need_arr['create_time'];
                $_data['need_seller_check_user_id']=PublicInfoService::get_user_name_by_id($need_arr['seller_check_user_id']);
                $_data['need_seller_check_time']=$need_arr['seller_check_time'];
                $_data['need_logistic_check_user_id']=PublicInfoService::get_user_name_by_id($need_arr['logistic_check_user_id']);
                $_data['need_logistic_check_time']=$need_arr['logistic_check_time'];
                $_data['need_remark']=$need_arr['remark'];
                $_data['need_cargo_grade']=$need_arr['cargo_grade'];
                $_data['overseas_supervision']=  PublicInfoService::getOverseasSupervision($need_arr['sku'], $need_arr['site_id']);
                $deliveryorders_id= PublicInfoService::getDeliveryordersId($_data['inbound_shipment_plan_id'],$_data['sku']);//出库单ID
                $_data['quantity']=PublicInfoService::getDeliveryordersInfo($deliveryorders_id);
            }else{
                $_data['purchaseorders_id']='';
                $_data['en_item']='';
                $_data['logo']='';
                $_data['type_number']='';
                $_data['weight']='';
                $_data['asweight']='';
                $_data['volume']='';
            }
            $skuArr= PublicInfoService::getEnItem($_data['sku']);
            if(!empty($_data['purchaseorders_id'])){
                $_data['en_item']=$skuArr['en_item'];
                $_data['report_elements']=$skuArr['report_elements'];
                $purchaseArr= PublicInfoService::getPurchase($_data['purchaseorders_id']);
                $_data['purchase_id']=PublicInfoService::get_user_name_by_id($purchaseArr['purchase_id']);
                $purchaseDetailArr= PublicInfoService::getPurchaseDetailOther($_data['purchaseorders_id'],$_data['sku']);
                $_data['sku_name']=$purchaseDetailArr['sku_name'];
                $_data['item']=$purchaseDetailArr['item'];
                $_data['hs_code']=$purchaseDetailArr['hs_code'];
                $_data['export_tax']=$purchaseDetailArr['export_tax'];
                $_data['quantity']=$purchaseDetailArr['quantity'];
                $_data['single_price']=$purchaseDetailArr['single_price']*(1+$purchaseDetailArr['real_tax_rate']);
                $_data['money']=$purchaseDetailArr['money'];
                $_data['export_tax_rebate']=$purchaseArr['export_tax_rebate']==1?'是':'否';
                $_data['supplier']=  PublicInfoService::getSupplierName($purchaseDetailArr['supplier']);
                $_data['enterprise_dominant_other']=$company_arr[$purchaseArr['enterprise_dominant_other']];
            }else{
//                $jieArr=PublicInfoService::test();
                //调接口
                $data['skuParam'] = array($_data['sku']);
                $handle = curl_init();
                //$url = 'http://192.168.5.6:703/commonapi/newerpapi/getHistoryPuInfo';
                $url = '';
                curl_setopt($handle,CURLOPT_URL,$url);
                curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE);
                curl_setopt($handle,CURLOPT_USERAGENT,'Fbawarehouse API/1.0');
                curl_setopt($handle,CURLOPT_CONNECTTIMEOUT,60);
                curl_setopt($handle,CURLOPT_TIMEOUT,90);
                curl_setopt($handle,CURLOPT_MAXREDIRS,3);
                curl_setopt($handle,CURLOPT_POST,TRUE);
                curl_setopt($handle,CURLOPT_POSTFIELDS,http_build_query($data));
                curl_setopt($handle,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_0);
                curl_setopt($handle,CURLOPT_HEADER,FALSE);    //不返回头信息
                $response = curl_exec($handle);
                if(curl_errno($handle)) {
                    $re          = new \stdClass();
                    $re->code    = 100;
                    $re->message = '连接主机'.$url.'时发生错误: '.curl_error($handle)."<br/>";
                    curl_close($handle);
                    return json_encode($re);
                }
                curl_close($handle);
                $jieArr=json_decode($response, true);
                $dataJie=$jieArr['data'][$_data['sku']];
                $_data['purchaseorders_id']=$dataJie['purchaseorder_id'];
                $_data['enterprise_dominant_other']=$company_arr[$dataJie['enterprise_dominant']];
                $_data['export_tax_rebate']=$dataJie['export_tax_rebate']==1?'是':'否';
                $_data['purchase_id']=$dataJie['nick'];
                $_data['sku_name']=$dataJie['sku_name'];
                $_data['supplier']= $dataJie['su_name'];
                $_data['single_price']=$dataJie['single_price'];
                $_data['item']=$purchaseDetailArr['item'];
                $_data['en_item']=$skuArr['en_item'];
                $_data['report_elements']=$skuArr['report_elements'];
                $_data['hs_code']=$skuArr['hs_code'];
                $_data['export_tax']=$skuArr['rate'];
            }
            $ship_from_address_id          = D('Api/Amazon/AccountSite','Service')->getShipFromAddress($_data['ship_from_address_id']);
            $_data['ship_from_address_id'] = $ship_from_address_id['addressStr'];
            $ship_to_address_id          = D('Api/Amazon/ShipToAddress','Service')->getShipToAddress($_data['ship_to_address_id']);
            $_data['ship_to_address_id'] = $ship_to_address_id['addressStr'];
            $i++;
        }
        return $data;
    }

    /*
     * 创建物流计划单
     * @plat_arr array 查询条件
     * @param obj
     * */
    public function createTransplans($plat_str)
    {
        $plat     = M('fba_inbound_shipment_plan',NULL,'DB_FBAERP');
        $trans    = M('fba_transport_plan',NULL,'DB_FBAERP');
        $platstatuserror = $plat->where("id in ($plat_str) AND status!=40")
            ->count();
        if ($platstatuserror>0){
            return false;
        } else {
            $platinfo = $plat->where("id in ($plat_str)")
                ->select();
            $platinfo = $platinfo[0];
            //var_dump($platinfo);exit;
            $numb=0;
            $numb=$trans->where("create_time like '".date('Y-m-d',time())."%'")->count(1);
            $numb=(int)$numb+1;
            $cangcode=$this->getNameById($platinfo['tranfer_hopper_id'],'code','fba_transfer_hopper_address');
            $toaddrid=$this->getNameById($platinfo['ship_to_address_id'],'CenterId','api_ship_to_address');
            $data['transport_plan_code']   = date('ymdhis').$platinfo['site_id'].$platinfo['carrier_service_id'];
            $data['contract_no']           = 'SC-'.$cangcode.'-FBA-'.date('ymd').'-'.$numb;//$toaddrid替换为FBA
            $data['invoice_code']          = 'CI-'.$cangcode.'-FBA-'.date('ymd').'-'.$numb;//$toaddrid替换为FBA
            $data['carrier_service_id']    = $platinfo['carrier_service_id'];
            $data['transport_supplier_id'] = $platinfo['transport_supplier_id'];
            $data['tranfer_hopper_id']     = $platinfo['tranfer_hopper_id'];
            $data['ship_to_address_id']    = $platinfo['ship_to_address_id'];
            $data['status']                = 50;
            $data['create_user_id']        = $_SESSION['current_account']['id'];
            $transid                       = $trans->add($data);
            //echo $trans->getLastSql();exit;
            //var_dump($transid);exit;
            if($transid>0){
                $result = $plat->where("id in ($plat_str) AND status=40")
                    ->save(array(
                        'transport_plan_id' => $transid,
                        'status'            => 50
                    ));
//                D('Api/Amazon/Client','Controller')->shipmentIdPostFormTransId($transid);
                return $transid;
            }else{
                return false;
            }
        }
    }

    /*
     * 修改物流计划单
     * @plat_arr array 查询条件
     * @param obj
     * */
    public function editTransPlan($where,$data)
    {
        $trans = M('fba_transport_plan',NULL,'DB_FBAERP');
        //$data['editTime']   = date('Y-m-d H:i:s',time());
        $data['pickup_op_user_id']      = $_SESSION['current_account']['id'];
        $result                         = $trans->where($where)
            ->save($data);
        $log_arr=array();
        $log_arr['transid']=$where['id'];
        $log_arr['opuserid']=$_SESSION['current_account']['id'];
        $log_arr['content']=$trans->getLastSql();
        //变更人及变更时间的记录
        M('transportplan_action_log',NULL,'DB_FBAERP')->add($log_arr);
        $platwhere['transport_plan_id'] = $where['id'];
        //$platdata['status']=$data['status'];
        if(!empty($data['status'])){
            return $this->upplat($platwhere,$data);
        }else{
            if($result){
                return array(
                    'status' => 200,
                    'msg' => $result,
                );
            }else{
                return array(
                    'status' => 500,
                    'msg' => $result,
                );
            }

        }
        //echo $trans->getLastSql();

    }

    /*
     * 从物流计划单中移除单个平台计划单
     * @where array 查询条件
     * @data array 数据
     * */
    public function upplat($where,$data,&$data_t = array())
    {
        $trans   = M('fba_transport_plan',NULL,'DB_FBAERP');
        $platser = new \Inbound\Service\InboundshipmentplanService();
        if($where['transport_plan_id']>0){
            $curstatus = $trans->where('id='.$where['transport_plan_id'])
                ->field('status')
                ->find();
            $curstatus = $curstatus[0]['status'];
        }
        if(!empty($data_t)){
            $trans->where("id=".$where['transport_plan_id'])
                ->save($data_t);
        }
        /*if (isset($data['status'])) {
            switch ($data['status'])
            {
            case 10:
              $data['status']=70;
              break;
            case 20:
              $data['status']=80;
              break;
            case 30:
              $data['status']=90;
              break;
            case 100:
              $data['status']=60;
              break;
            default:
              $data['status']=60;
            }
        }*/
        if(!empty($where['transport_plan_id'])){
            $where_p['transport_plan_id'] = $where['transport_plan_id'];
            $result                       = $platser->update($where_p,$data);
            if($curstatus!=60&&$data['status']==60){
                $result = $platser->update_shipment_status($where['transport_plan_id'],60);
                /*插入发货数据到wms_deliveryorders
                $deliverydata = array();
                $trans        = M('fba_transport_plan',NULL,'DB_FBAERP');
                $delivery_arr = $trans->table('fba_transport_plan a JOIN fba_inbound_shipment_plan b ON a.id = b.transport_plan_id
                    JOIN fba_inbound_shipment_plan_detail c ON b.id=c.inbound_shipment_plan_id JOIN skusystem_supplier_skus d
                    ON c.sku=d.sku JOIN fba_package_box e on b.id=e.inbound_shipment_plan_id')
                    ->where('a.id='.$where['transport_plan_id'])
                    ->field('b.account_name,d.supplier_id,c.sku,c.sku_name,d.size,c.position,c.quantity,d.price,a.shipfee,
                    b.shipmentid,e.id as package_box_id,c.fnsku,c.hs_code,c.declaration_element,c.enterprise_dominant,
                    c.export_tax_rebate')
                    ->select();
                for($i = 0;$i<count($delivery_arr);$i++){
                    $deliverydata[$i]['warehouseid']            = 102;
                    $deliverydata[$i]['crontrigger']            = 0;
                    $deliverydata[$i]['delivery_date']          = $data['pickup_op_time'];
                    $deliverydata[$i]['op_time']                = $data['pickup_op_time'];
                    $deliverydata[$i]['delivery_man']           = $data['pickup_op_user_id'];//操作人
                    $deliverydata[$i]['type']                   = 10;
                    $deliverydata[$i]['delivery_department']    = $delivery_arr[$i]['account_name'];//帐号id
                    $deliverydata[$i]['supplier_id']            = $delivery_arr[$i]['supplier_id'];
                    $deliverydata[$i]['sku']                    = $delivery_arr[$i]['sku'];
                    $deliverydata[$i]['sku_name']               = $delivery_arr[$i]['sku_name'];
                    $deliverydata[$i]['sku_standard']           = $delivery_arr[$i]['size'];
                    $deliverydata[$i]['storage_position']       = $delivery_arr[$i]['position'];
                    $deliverydata[$i]['quantity']               = $delivery_arr[$i]['quantity'];
                    $deliverydata[$i]['single_price']           = $delivery_arr[$i]['price'];
                    $deliverydata[$i]['money']                  = 0;
                    $deliverydata[$i]['cost']                   = $delivery_arr[$i]['price'];
                    $deliverydata[$i]['internationalship_cost'] = $delivery_arr[$i]['shipfee'];
                    $deliverydata[$i]['remark']                 = '';
                    $deliverydata[$i]['shipmentid']             = $delivery_arr[$i]['shipmentid'];
                    $deliverydata[$i]['package_box_id']         = $delivery_arr[$i]['package_box_id'];
                    $deliverydata[$i]['fnsku']                  = $delivery_arr[$i]['fnsku'];
                    $deliverydata[$i]['hs_code']                = $delivery_arr[$i]['hs_code'];
                    $deliverydata[$i]['declaration_element']    = $delivery_arr[$i]['declaration_element'];
                    $deliverydata[$i]['enterprise_dominant']    = $delivery_arr[$i]['enterprise_dominant'];
                    $deliverydata[$i]['export_tax_rebate']      = $delivery_arr[$i]['export_tax_rebate'];
                    $delivery        = M('wms_deliveryorders',NULL,'DB_FBAERP');
                    //$delivery->add($deliverydata[$i]);
                }*/
                /*$outStockser = D('Warehouse/StockOut', 'Service');
                $outStockser->create_deliveryorders($deliverydata);*/
            }
        }elseif(!empty($where['shipmentid'])){
            $where_p['shipmentid'] = $where['shipmentid'];
            $result                = $platser->update($where_p,$data);
        }
        //$plat                          = M('fba_inbound_shipment_plan',NULL,'DB_FBAERP');
        //$data['editTime']   = date('Y-m-d H:i:s',time());
        //$result                       = $plat->where($where)->save($data);
        //echo $trans->getLastSql();exit;
        return $result;
    }

    public function getCarrierServiceList($param = array('1' => '1'))
    {
        $trans = M('fba_carrier_service',NULL,'DB_FBAERP');
        $data  = $trans->where($param)
            ->select();
        //var_dump($data);exit;
        return $data;
    }

    /**
     * 查询平台计划单
     * @param  array $array 查询条件
     * @return 数据
     */
    public function getPlatplans(&$_array = array(),$limit = '',$flag = TRUE)
    {
        $trans             = M('fba_inbound_shipment_plan',NULL,'DB_FBAERP');
        $_array['c.table'] = 'fba_inbound_shipment_plan';
        $data              = $trans->table('fba_inbound_shipment_plan a JOIN fba_inbound_shipment_plan_detail b 
            JOIN statusdics c JOIN amazonorder_sites d JOIN fba_carrier_service e JOIN fba_transfer_hopper_address f')
            ->field('distinct(a.shipmentid),a.id,c.value,a.batch_code,a.account_name,d.shorthand_code,a.carrier_service_id,
                f.name,a.ship_to_address_id,a.claim_arrive_time,a.total_package,a.total_weight,a.total_capacity,a.remark')
            ->where($_array)
            ->where('a.id = b.inbound_shipment_plan_id AND a.status=c.number AND a.site_id=d.id AND a.tranfer_hopper_id=f.id')
            ->order('d.shorthand_code,a.claim_arrive_time')
            ->select();//AND a.carrier_service_id=e.id
        //echo $trans->getLastSql();exit;
        return $data;
    }

    /**
     * 查询物流计划单
     * @param  array $array 查询条件
     * @return 数据
     */
    public function getTransplan(&$_array = array())
    {
        $trans = M('fba_transport_plan',NULL,'DB_FBAERP');
        $data  = $trans->where($_array)
            ->order(array('id' => 'desc'))
            ->select();
        //echo $trans->getLastSql();exit;
        return $data;
    }

    /**
     * 通过id获取名称
     * @param  string 查询条件
     * @return string
     */
    public function getNameById($id,$field,$tablename)
    {
        $trans = M($tablename,NULL,'DB_FBAERP');
        $data  = $trans->where('id='.$id)
            ->field($field)
            ->find();
        //echo $trans->getLastSql();exit;
        return $data[$field];
    }
}
