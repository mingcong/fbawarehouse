<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/13
 * Time: 20:40
 */
namespace Inbound\Model;
use Think\Model;

class StatusdicsModel extends Model {
    protected $connection       =   'DB_FBAERP';
    protected $trueTableName    =   'statusdics';

    /**
     * @param $table
     * @param $column
     * @param null $number
     * @param bool $type
     * @return mixed
     * 状态表维护
     */
    public function getStatusdicsValue($table, $column, $number=NULL, $type=false) {
        $options = array();
        $options['table'] = $table;
        $options['colum_name'] = $column;

        if($number) {
            if(is_array($number)) {
                $options['number'] = array('IN', $number);
                $this->where($options);

                if($type)
                    return $this->getField('id, number');
                else
                    return $this->getField('number, value');
            }

            $options['number'] = $number;
            return $this->where($options)->getField('value');
        }

        $this->where($options);
        if($type)
            return $this->getField('id, number');
        else
            return $this->getField('number, value');
    }
}