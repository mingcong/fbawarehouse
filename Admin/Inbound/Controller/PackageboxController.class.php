<?php
/**
 * Created by PhpStorm.
 */

namespace Inbound\Controller;


use Home\Controller\CommonController;

class PackageboxController extends CommonController{
    //库存服务层对象
    public $packagebox = NULL;

    //条件
    public $condition = array();

    //标志符
    public $flag;


    public function __construct(){
        $this->packagebox = D('PackageboxService','Service');
        parent::__construct();

    }

    /**
     * 箱唛首页显示
     */
    public function index(){
        $this->condition = $_GET;
        $data = $this->packagebox->select($_GET,true);

        $this->assign('page',$this->Inboundshipmentplan->page);
        $this->assign('count',$this->Inboundshipmentplan->count);
        $this->assign('data',$data);
        $this->display();

    }

    /**
     * 创建箱唛信息
     */
    public function create($packagebox_data){
        $res = $this->packagebox->create($packagebox_data);
        if($res){

        }else{

        }

    }

    /*
     * 上传箱唛信息
     */
    public function upload_package(){
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $file = $_FILES['fileName'];
        $max_size = "2000000"; //最大文件限制（单位：byte）
        $fname = $file['name'];
        $file_name = $file['tmp_name'];

        $sheet_data=PublicPlugService::parse_excel($file_name);

        //将二位数组插入到箱唛表中  待续
        $this->create($sheet_data);

    }

    /**
     * 更改箱唛信息
     */
    public  function update_package_info(){
        $info = $_POST;
        $res = $this->packagebox->update_package_info($info);
        if($res){

        }else{

        }
    }




}