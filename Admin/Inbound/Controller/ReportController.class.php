<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-16
 * Time: 上午9:33
 */
namespace Inbound\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
class ReportController extends CommonController {
    //服务层对象
    public $AccountSeller   = null;
    public $accounts        = null;
    public $remark          = null;
    public $account_id        = null;
    public $report        = null;

    //条件
    public $condition = array();

    //标志符
    public $flag;

    /**
     * 默认构造方法
     */
    public function __construct()
    {
        $this->AccountSeller = D('Inbound/AccountSeller','Service');
        $this->report = D('Inbound/Report','Service');
        $this->account_id           = PublicInfoService::salesmanBindAccountsGet();
        $this->remark               = publicInfoService::salesmanGet();
        $this->accounts             = PublicInfoService::get_accounts();
        parent::__construct();
    }

    /**
     * 首页展示
     */
    public function index() {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $data = array();
        if($_GET){
            if($_GET['api']){
                if(!$_GET['account_id']){
                    echo "<script>alert('请选择帐号');history.go(-1);</script>";die;
                }
                $this->report->apiGet($_GET['account_id']);die;
            }
            $data = $this->report->select($_GET);
            if($_GET['down']){
                $this->download($data,$_GET);die;
            }
            if($_GET['pass']){
                $this->report->check($_GET);die;
            }
            if($_GET['agree']){
                $this->report->agreeDown($_GET);die;
            }
            if($_GET['unDown']){
                $this->report->unDown($_GET);die;
            }
            if($_GET['account_id']){
               $pass = $this->report->checkAgree($_GET);
               $this->assign('pass',$pass?$pass['pass']:0);
                $this->assign('remark',$pass?$pass['remark']:'');
            }

        }
        $date = array();
        for($i=0;$i<12;$i++){
            if(date('d')==31){
                $date[] = date('Y-m', strtotime(date('Y-m-01', strtotime("-$i month -3 day")) . '-1 day'));
            }else{
                $date[] = date('Y-m', strtotime(date('Y-m-01', strtotime("-$i month")) . '-1 day'));
            }
        }
        $this->assign('accounts',$this->accounts);
        $this->assign('page',$this->report->page);
        $this->assign('count',$this->report->count);
        $this->assign('data',$data);
        $this->assign('date',$date);
        $this->display();
    }
    /**
     * 数据导出
     */
    public function download($data = array(),$name = null){
        set_time_limit(0);
        ini_set('memory_limit','2048M');
        vendor('PHPExcel.PHPExcel');
        $tplExcel = new \PHPExcel();
        $tplExcel->getProperties()->setCreator("amazonorder")                                                               // Set properties
        ->setLastModifiedBy("amazonorder")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $start_date = date('Ym', strtotime(date($name['date'].'-01', strtotime('-1 month')) . '-1 day'));
        $end_date = str_replace('-','',$name['date']);
        if($name['account_id']){
            $account_name = PublicInfoService::get_accounts_by_id($name['account_id'],'name');
            $fileName = $account_name.'帐号进销存报表'.$start_date.'-'.$end_date.'.xlsx';
        }else{
            $fileName = '所有帐号进销存报表'.$start_date.'-'.$end_date.'.xlsx';
        }

        $tplExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $tplExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        $tplExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
        $tplExcel->setActiveSheetIndex(0)->getStyle('E1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $tplExcel->setActiveSheetIndex(0)->getStyle('K1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $tplExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
        $tplExcel->setActiveSheetIndex(0)->mergeCells('E1:J1');
        $tplExcel->setActiveSheetIndex(0)->mergeCells('K1:O1');
        $tplExcel->setActiveSheetIndex(0)->mergeCells('P1:R1');
        $tplExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '')
            ->setCellValue('E1', "本期入库")
            ->setCellValue('K1', "本期出库")
            ->setCellValue('P1', "");
        $tplExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', '帐号')
            ->setCellValue('B2', 'FBA(SKU)')
            ->setCellValue('C2', "公司(SKU)")
            ->setCellValue('D2', "期初库存")
            ->setCellValue('E2', "备货入库")
            ->setCellValue('F2', "退货入库")
            ->setCellValue('G2', "转仓入库")
            ->setCellValue('H2', "盘盈入库")
            ->setCellValue('I2', "其它入库")
            ->setCellValue('J2', "入库合计")
            ->setCellValue('K2', "销售出库")
            ->setCellValue('L2', "转仓出库")
            ->setCellValue('M2', "盘亏出库")
            ->setCellValue('N2', "其它出库")
            ->setCellValue('O2', "出库合计")
            ->setCellValue('P2', "实际期末库存")
            ->setCellValue('Q2', "理论期末库存")
            ->setCellValue('R2', "实际比理论相差");
        foreach($data as $k =>$v) {
            $num = $k+3;
            $tplExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$num, $v['account_name'])
                ->setCellValue('B'.$num, $v['sku'])
                ->setCellValue('C'.$num, $v['piv_sku'])
                ->setCellValue('D'.$num, $v['qckc'])
                ->setCellValue('E'.$num, $v['bhrk'])
                ->setCellValue('F'.$num, $v['thrk'])
                ->setCellValue('G'.$num, $v['zcrk'])
                ->setCellValue('H'.$num, $v['pyrk'])
                ->setCellValue('I'.$num, "")
                ->setCellValue('J'.$num, $v['rkhj'])
                ->setCellValue('K'.$num, $v['xsck'])
                ->setCellValue('L'.$num, $v['zcck'])
                ->setCellValue('M'.$num, $v['pkck'])
                ->setCellValue('N'.$num, $v['qtck'])
                ->setCellValue('O'.$num, $v['ckhj'])
                ->setCellValue('P'.$num, $v['qmkc'])
                ->setCellValue('Q'.$num, $v['llqmkc'])
                ->setCellValue('R'.$num, $v['diff']);
        }
//        $tplExcel->setActiveSheetIndex(0)->setCellValue('A2', ' ')->setCellValue('B2', ' ')->setCellValue('C2', ' ')
//            ->setCellValue('D2', ' ')->setCellValue('E2', ' ')->setCellValue('F2', ' ')->setCellValue('G2', ' ')
//            ->setCellValue('H2', ' ')->setCellValue('I2', ' ')->setCellValue('J2', ' ');

        $tplExcel->getActiveSheet()->setTitle('进销存报表');
        $tplExcel->setActiveSheetIndex(0);

        $PHPWriter = new \PHPExcel_Writer_Excel2007($tplExcel);
        ob_start();
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:attachment;filename="'.$fileName.'"');//输出模板名称
        header("Content-Transfer-Encoding: binary");
        header("Last-Modified:".gmdate("D, d M Y H:i:s")." GMT");
        header('Pragma: public');
        header('Expires: 30');
        header('Cache-Control: public');
        $PHPWriter->save('php://output');

        exit;
    }
}