<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-16
 * Time: 上午9:33
 */
namespace Inbound\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
class AccountSellerController extends CommonController {
    //服务层对象
    public $AccountSeller   = null;
    public $accounts        = null;
    public $remark          = null;
    public $account_id        = null;

    //条件
    public $condition = array();

    //标志符
    public $flag;

    /**
     * 默认构造方法
     */
    public function __construct()
    {
        $this->AccountSeller = D('Inbound/AccountSeller','Service');
        $this->account_id           = PublicInfoService::salesmanBindAccountsGet();
        $this->remark               = publicInfoService::salesmanGet();
        $this->accounts             = PublicInfoService::get_accounts();
        parent::__construct();
    }

    /**
     * 销售员帐号关系对应表首页展示
     */
    public function index() {
        $this->condition = $_GET;
        $data = $this->AccountSeller->select_detail($_GET);
        $this->assign('remark',$this->remark);
        $this->assign('accounts',$this->accounts);
        $this->assign('page',$this->AccountSeller->page);
        $this->assign('count',$this->AccountSeller->count);
        $this->assign('data',$data);
        $this->display();
    }
     public function addAccountSeller() {
         $account_id = I('post.account_id');
         $seller_id = I('post.seller_id');
         if (!$account_id) {
             echo "<script>alert('请选择帐号');history.go(-1);</script>";
             die;
         }
         if (!$account_id) {
             echo "<script>alert('请选择销售员');history.go(-1);</script>";
             die;
         }
         $arr = array();
         foreach ($seller_id as $k => $v) {
            if (!$this->AccountSeller->checkAccountAndSeller($account_id, $v)) {
                $arr[] = $v;
            }
         }
         $seller_id = implode(',', $arr);
         $data = $this->AccountSeller->getSellerFromAccountId($account_id);
         if($data){
             $seller_ids = $data['seller_ids'].$seller_id.',';
             $flag = TRUE;
         } else {
             $seller_ids = ','.$seller_id.',';
             $flag = FALSE;
         }
         $result = $this->AccountSeller->addOrUpdateAccountSeller($account_id, $seller_ids, $flag);
         if ($result) {
             echo "<script>alert('操作成功');window.location.href='index';</script>";
             die;
         } else {
             echo "<script>alert('操作失败');history.go(-1);</script>";
             die;
         }

     }
     /**
      * 描述: 解绑页面显示
      * 作者: kelvin
      */
     public function delAccountSeller() {
         C("LAYOUT_ON", FALSE);
         C('SHOW_PAGE_TRACE', false);
         $id = I('get.id');
         if ($id) {
             $result = $this->AccountSeller->delAccountSeller($id);
             $this->assign('result',$result);
             $this->display();
         }

     }
     /**
      * 描述: 解绑确认
      * 作者: kelvin
      */
     public function delAccountSellerSave() {
         $id = I('post.seller_ids');
         $account_id = I('post.account_id');
         if ($id) {
             $seller_id = implode(',', $id);
             $seller_ids = ','.$seller_id.',';
         }else{
             $seller_ids = '';
         }

         $result = $this->AccountSeller->addOrUpdateAccountSeller($account_id, $seller_ids,TRUE);
         if ($result) {
             echo "<script>alert('操作成功');parent.location.reload();</script>";
             die;
         } else {
             echo "<script>alert('操作失败');history.go(-1);</script>";
         }

     }
}