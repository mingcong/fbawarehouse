<?php
/**
 * Created by PhpStorm.
 * User: liqiang
 * Date: 16-12-29
 * Time: 上午11:16
 */
namespace Inbound\Controller;

use Home\Controller\CommonController;
use Inbound\Service\CarrierService;
use Inbound\Service\PublicPlugService;

class CarrierController extends CommonController
{
    /**
     * 物流渠道管理首页显示
     */
    public function index(){
        //var_dump(CarrierService::getCarrierService());exit;

        $carrier        = D('Carrier','Service');
        $name           = I('get.name');
        $pickingtime    = I('get.pickingtime');
        $paymethod      = I('get.paymethod');
        $billingway     = I('get.billingway');
        $pickingaddress = I('get.pickingaddress');
        $is_used        = I('get.is_used');


        $carrier_id     = I('get.carrier_id');
        $service_name   = I('get.service_name');
        $aging          = I('get.aging');



        $page           = I('get.p',1,'int');
        $param = array(
            'name'          => $name,
            'pickingtime'   => $pickingtime,
            'paymethod'     => $paymethod,
            'billingway'    => $billingway,
            'pickingaddress'=> $pickingaddress,
            'is_used'       => $is_used,
            'carrier_id'    => $carrier_id,
            'service_name'  => $service_name,
            'aging'         => $aging,


        );
        $pagesize = 5;         //每页大小
        $limit    = $pagesize*($page-1).','.$pagesize;
        //获取分页信息
        $carrierResult = $carrier->carrierSearch($param,$limit);
       // print_r($carrierResult);die;

        foreach($carrierResult as $k =>$v){
            $service = array();
            if(empty($v['service'])){
                continue;
            }
            $aa = explode(',',$v['service']);
            //print_r($aa);
            foreach($aa as $i => $s){
                $se = explode('||',$s);
                $service[$i]['service_name'] = $se[0];
                $service[$i]['aging'] = $se[1];
                $service[$i]['service_is_use'] = $se[2];
            }
           // print_r($service);
            $carrierResult[$k]['service']= $service;

        }//die;
        //var_dump($carrierResult);exit;
        $carrierCnt = count($carrier->carrierSearch($param));
        $cinfo = $carrier->carrierSearchName();
        //var_dump ($cinfo);exit;
        //实例化分页类 传入总记录数和每页显示的记录数(13)
        $Page = new \Think\Page($carrierCnt,$pagesize);
        $Page->setConfig('prev','上一页');
        $Page->setConfig('next','下一页');
        $Page->setConfig('first','首页');
        $Page->setConfig('last','末页');
        foreach($carrierResult as &$data){
            if($data['is_used']==1){
                $data['is_used']='开启';}
            elseif($data['is_used']==0){
                $data['is_used'] = '关闭';
            }
        }
        //分页显示输出
        $show = $Page->show();
        //var_dump($carrierResult[11]['name']);exit;
        $this->assign('cinfo',$cinfo);
        //分页内容
        $this->assign('carrierResult',$carrierResult);
        //搜索信息
        $this->assign('param',$param);
        //分页按钮
        $this->assign('Pager',$show);
        //当前页
        $this->assign('page',$page);
        //总页数
        $this->assign('pageSize',ceil($carrierCnt/$pagesize));
        //总记录数
        $this->assign('total',$carrierCnt);
        $this->display();

    }
    /**
     * 查看明细
     */
    public function getDetail()
    {

        //echo 123;exit;
        $id = I("post.id");
        if(!$id){
            $this->ajaxReturn('');
        }
        $carrier       = D('Carrier','Service');
        $carrierDetail = $carrier->getDetail($id);
        //var_dump($carrierDetail);exit;
        layout(FALSE);
        $this->assign('cdetail',$carrierDetail);
        $this->display();
    }


    /**
     * 货代修改弹窗
     */
    public function  editCarrier()
    {
        C("LAYOUT_ON",FALSE);
        $id = I('get.id');
        if($id){
            $carrier       = D('Carrier','Service');
            //            $carrierDetail = $carrier->getDetail($id);
            $carrierDetail = $carrier->getEdit($id);
            foreach($carrierDetail as $k =>$v){
                $aa = explode(',',$v['service']);
                foreach($aa as $i => $s){
                    $se = explode('||',$s);
                    $service[$i]['service_id'] = $se[0];
                    $service[$i]['service_name'] = $se[1];
                    $service[$i]['aging'] = $se[2];
                    $service[$i]['service_is_use'] = $se[3];
                }
                //print_r($service);die;
                $carrierDetail[$k]['service']=$service;

            }
//            print_r($carrierDetail);die;
            $this->assign('cdetail',$carrierDetail);
            $this->display('Carrier/editCarrier');
        }

    }

    /**
     * 修改处理
     */
    public function doEditCarrier()
    {
        //$this->ajaxReturn(1);die;
        //获取参数
        $service_is_used = array();
        $name           = I('post.name');
        $pickingtime    = I('post.pickingtime');
        $pickingaddress    = I('post.pickingaddress');
        $paymethod      = I('post.paymethod');
        $id             = I('post.id');
        $billingway     = I('post.billingway');
        $is_used        = I('post.is_used');
        $service_id     = I('post.service_id');
        $service_name   = I('post.service_name');
        $aging          = I('post.aging');
        for($i=0;$i< count($service_id);$i++){
            $service_is_used[]  = I('post.service_is_used'.$i);
        }
        //print_r($service_is_used);die;
        //定义变量
        $carrier        = D('Carrier','Service');
        $carrierArr     =   array(
            'name'          => $name,
            'pickingtime'   => $pickingtime,
            'paymethod'     => $paymethod,
            'billingway'    => $billingway,
            'is_used'       => $is_used,
            'pickingaddress'=> $pickingaddress,
            //'id'            => $id,
        );

        $serviceArr     =   array(
            'service_id'    => $service_id,
            'service_name'  => $service_name,
            'aging'         => $aging,
            'is_used'       => $service_is_used,
        );

        //var_dump ($serviceArr);exit;
        //校验参数
        if(empty($name)){     //名称为空
            echo '<script>alert("承运商名称为必填项");parent.location.reload();</script>';
            die;
        }
        if(empty($pickingaddress)){       //结款方式为空
            echo '<script>alert("地点为必填项");parent.location.reload();</script>';
            die;
        }

        if(empty($paymethod)){       //结款方式为空
            echo '<script>alert("结款方式为必填项");parent.location.reload();</script>';
            die;
        }
        if(empty($billingway)){      //计费方式为空
            echo '<script>alert("计费方式为必填项");parent.location.reload();</script>';
            die;
        }

        if(empty($pickingtime)){      //提货时间为空
            echo '<script>alert("提货时间为必填项");parent.location.reload();</script>';
            die;
        }
        $res = $carrier->editCarrier($carrierArr,$id);
        //$serviceArr['carrier_id'] = $res;
        //echo ($res);exit;
        $res1=$carrier->editCarrierService($serviceArr);
        //echo ($res1);exit;
        if(1){
            echo '<script>alert("修改成功");parent.location.reload();</script>';
            die;
        }else{
            echo '<script>alert("修改失败");parent.location.reload();</script>';
            die;
        }


    }

    /**
     * 货代添加弹窗
     */
    public function  addCarrier()
    {
        C("LAYOUT_ON",FALSE);
        $this->display('Carrier/addCarrier');
    }


    /**
     * 添加处理
     */
    public function doAddCarrier()
    {
        $service_is_used = array();
        $name           = I('post.name');
        $pickingtime    = I('post.pickingtime');
        $pickingaddress    = I('post.pickingaddress');
        $paymethod      = I('post.paymethod');
        $id             = I('post.id');
        $billingway     = I('post.billingway');
        $is_used        = I('post.is_used');
        $service_name   = I('post.service_name');
        $aging          = I('post.aging');
        for($i=0;$i< count($service_name);$i++){
            $service_is_used[]  = I('post.service_is_used'.$i);
        }
        //print_r($service_is_used);die;
        //定义变量
        $carrier        = D('Carrier','Service');
        $carrierArr     =   array(
            'name'          => $name,
            'pickingtime'   => $pickingtime,
            'paymethod'     => $paymethod,
            'billingway'    => $billingway,
            'is_used'       => $is_used,
            'pickingaddress'=> $pickingaddress,
            //'id'            => $id,
        );

        $serviceArr     =   array(
            'service_name'  => $service_name,
            'aging'         => $aging,
            'is_used'       => $service_is_used,
        );

        if(empty($name)){     //名称为空
            echo '<script>alert("承运商名称为必填项");parent.location.reload();</script>';
            die;
        }
        if(empty($pickingaddress)){       //结款方式为空
            echo '<script>alert("地点为必填项");parent.location.reload();</script>';
            die;
        }

        if(empty($paymethod)){       //结款方式为空
            echo '<script>alert("结款方式为必填项");parent.location.reload();</script>';
            die;
        }
        if(empty($billingway)){      //计费方式为空
            echo '<script>alert("计费方式为必填项");parent.location.reload();</script>';
            die;
        }

        if(empty($pickingtime)){      //提货时间为空
            echo '<script>alert("提货时间为必填项");parent.location.reload();</script>';
            die;
        }

        //搜索相同的货代名称
        $sameInfo = $carrier->getCarrierByIdAndName($name);
        if(!empty($sameInfo)){
            echo '<script>alert("承运商名称已存在");parent.location.reload();</script>';
            die;
        }else{
            $res = $carrier->addCarrier($carrierArr);
            $serviceArr['carrier_id'] = $res;
            $res1=$carrier->addCarrierService($serviceArr);
            if($res && $res1){
                echo '<script>alert("添加成功");parent.location.reload();</script>';
                die;
            }else{
                echo '<script>alert("添加失败");parent.location.reload();</script>';
                die;
            }
        }

    }


    /**
     *新增一行
     *
     */
    public function addConsignerAddress()
    {

    }



}