<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/9
 * Time: 17:16
 */
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class AccountSiteService extends CommonService{
    protected $accountSiteModel = NULL;

    public function __construct() {
        $this->accountSiteModel = D('Api/Amazon/AccountSite','Model');
    }

    public function getAccountShipFromAddress ($accountId) {
        return $this->accountSiteModel->getAccountShipFromAddress($accountId);
    }

    public function getSiteIdByAccountId($accountId) {
        return $this->accountSiteModel->getSiteIdByAccountId($accountId);
    }
    public function getAccountShipFromAddressID($accountId) {
        return $this->accountSiteModel->getAccountShipFromAddressId($accountId);
    }

    public function getShipFromAddress($shipFromId = 0) {
        return $this->accountSiteModel->formatAddress($this->accountSiteModel->getShipFromAddress($shipFromId));
    }
}