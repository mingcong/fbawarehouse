<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/1
 * Time: 19:55
 */
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class AsinService extends CommonService{
    protected $asinModel = NULL;
    protected $reportUnsuppressdInventoryModel = NULL;

    public function __construct(){
        $this->asinModel = D('Api/Amazon/Asin','Model');
        $this->reportUnsuppressdInventoryModel = D('Amazon\ReportUnsuppressedInventory');
    }

    /**
     * 描述：从api_report_unsuppressed_inventory抓取数据到api_asins表中
     * 参数：
     */
    public function insertToAsin(){
        $asinsData = $this->reportUnsuppressdInventoryModel->selectInsertToAsin();
        $this->asinModel->insert($asinsData);
    }
}