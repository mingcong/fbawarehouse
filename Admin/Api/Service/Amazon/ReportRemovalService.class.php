<?php
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class ReportRemovalService extends CommonService{
    protected $reportRemovalModel = NULL;
    public $count = 0;
    public $page = 0;
    public function __construct(){
        $this->reportRemovalModel = D('Amazon\ReportRemoval');
    }

    public function selectData($options){
        $reportRemovalData = $this->reportRemovalModel->selectData($options);

        return (!empty($reportRemovalData)) ? $reportRemovalData : false;
    }

    public function downloadData($options){
        $reportRemovalData = $this->reportRemovalModel->selectData($options);

        $downloadData = $reportRemovalData['reportDataLists'];
        $this->export($downloadData,"物流移除订单详情报告_".date('Y-m-d',time()));

    }

    /**
     * @param $data
     * @param $excelFileName
     * 描述：字符串下载 Excel
     */
    public function export($data,$excelFileName){
        set_time_limit(0);
        ini_set('memory_limit','1024M');
        $firstLine = array(
            'name'              => '账号',
            'sku'               => '平台sku',
            'private_sku'       => '公司sku',
            'site'              => '站点',
            'disposed_quantity' => '移除数量',
            'shipped_quantity'  => '已完成转仓数量',
            'in_process_quantity' => '转仓在途数量',
            'request_date'      => '请求时间',
            'last_updated_date' => '最后更新时间'
        );
        /* 获取字段名称 */
        $keys    = array_keys($firstLine);
        $content = "";
        $content .= "<table border='1'><tr>";
        foreach($firstLine as $_pre){
            $content .= "<td>$_pre</td>";
        }
        $content .= "</tr>";
        foreach($data as $_list){
            $content .= "<tr>";
            foreach($keys as $key){
                $content .= "<td style='vnd.ms-excel.numberformat:@'>".$_list[$key]."</td>";
            }
            $content .= "</tr>";
        }
        $content .= "</table>";
        header("Content-type:application/vnd.ms-execl;charset=gb2312");
        header("Content-Disposition:attactment;filename=".$excelFileName.".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $content;
        exit;
    }
}