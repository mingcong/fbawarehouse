<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/9
 * Time: 19:47
 */
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class AccountsService extends CommonService{
    protected $accountsModel = NULL;

    public function __construct() {
        $this->accountsModel = D('Api/Amazon/Accounts','Model');
    }

    /**
     * @return mixed
     * 获取所有的账号
     */
    public function getAccounts() {
        return $this->accountsModel->getAccounts();
    }

    /**
     * @param $accountIds
     * @return mixed
     * 根据账号ID数组返回账号信息数组
     */
    public function getAccountsByIds($accountIds) {
        return $this->accountsModel->getAccountsByIds($accountIds);
    }
}