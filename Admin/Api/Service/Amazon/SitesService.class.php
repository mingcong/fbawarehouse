<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/9
 * Time: 17:14
 */
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class SitesService extends CommonService{
    protected $sitesModel = NULL;

    protected $accountsModel = NULL;

    public function __construct() {
        $this->sitesModel = D('Api/Amazon/Sites','Model');
    }

    /**
     * @return mixed
     * 获取站点数组列表
     */
    public function getSites ($siteId = 0) {
        return $this->sitesModel->getSites($siteId);
    }
}