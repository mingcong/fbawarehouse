<?php
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class ReportReceiptsService extends CommonService{
    protected $reportReceiptsModel = NULL;
    public $count = 0;
    public $page = 0;
    public function __construct(){
        $this->reportReceiptsModel = D('Amazon\ReportReceipts');
    }

    public function selectData($options){
        $reportReceiptsData = $this->reportReceiptsModel->selectData($options);

        return (!empty($reportReceiptsData)) ? $reportReceiptsData : false;
    }

    public function downloadData($options){
        $reportReceiptsData = $this->reportReceiptsModel->selectData($options);

        $downloadData = $reportReceiptsData['reportDataLists'];
        $this->export($downloadData,"商品首次入库时间报表_".date('Y-m-d',time()));

    }

    /**
     * @param $data
     * @param $excelFileName
     * 描述：字符串下载 Excel
     */
    public function export($data,$excelFileName){
        set_time_limit(0);
        ini_set('memory_limit','1024M');
        $firstLine = array(
            'name'        => '账号',
            'sku'         => '平台sku',
            'private_sku' => '公司sku',
            'site'        => '站点',
            'date'        => '首次入库时间'
        );
        /* 获取字段名称 */
        $keys    = array_keys($firstLine);
        $content = "";
        $content .= "<table border='1'><tr>";
        foreach($firstLine as $_pre){
            $content .= "<td>$_pre</td>";
        }
        $content .= "</tr>";
        foreach($data as $_list){
            $content .= "<tr>";
            foreach($keys as $key){
                $content .= "<td style='vnd.ms-excel.numberformat:@'>".$_list[$key]."</td>";
            }
            $content .= "</tr>";
        }
        $content .= "</table>";
        header("Content-type:application/vnd.ms-execl;charset=gb2312");
        header("Content-Disposition:attactment;filename=".$excelFileName.".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $content;
        exit;
    }
}