<?php

namespace Api\Service\Amazon;
use Home\Service\CommonService;

class ReportStockSaleService extends CommonService{
    protected $reportStockSaleModel = NULL;
    public $count = 0;
    public $page = 0;
    public function __construct(){
        $this->reportStockSaleModel = D('Amazon\ReportStockSale');
    }

    public function selectData($options){
        $reportStockSaleData = $this->reportStockSaleModel->selectData($options);

        return (!empty($reportStockSaleData)) ? $reportStockSaleData : false;
    }

    public function downloadData($options){
        $reportStockSaleData = $this->reportStockSaleModel->selectData($options);

        $downloadData = $reportStockSaleData['reportDataLists'];
        $this->export($downloadData,"库存销量报表_".date('Y-m-d',time()));

    }

    /**
     * @param $data
     * @param $excelFileName
     * 描述：字符串下载 Excel
     */
    public function export($data,$excelFileName){
        set_time_limit(0);
        ini_set('memory_limit','1024M');
        $firstLine = array(
            'name'      => '账号',
            'sku'       => '公司sku',
            'skucnname' => '产品名称',
            'daysale'   => 'FBA日销量',
            'stock'     => 'FBA可用库存',
            'shipping'  => 'FBA在途库存',
        );
        /* 获取字段名称 */
        $keys    = array_keys($firstLine);
        $content = "";
        $content .= "<table border='1'><tr>";
        foreach($firstLine as $_pre){
            $content .= "<td>$_pre</td>";
        }
        $content .= "</tr>";
        foreach($data as $_list){
            $content .= "<tr>";
            foreach($keys as $key){
                $content .= "<td style='vnd.ms-excel.numberformat:@'>".$_list[$key]."</td>";
            }
            $content .= "</tr>";
        }
        $content .= "</table>";
        header("Content-type:application/vnd.ms-execl;charset=gb2312");
        header("Content-Disposition:attactment;filename=".$excelFileName.".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $content;
        exit;
    }



}