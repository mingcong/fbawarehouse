<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/27
 * Time: 18:54
 */
namespace Api\Service\Amazon;
use Home\Service\CommonService;

class ServerService extends CommonService {
    protected $accountSellerSkuModel = NULL;
    protected $accountService = null;

    public function __construct() {
        $this->accountSellerSkuModel = D('Amazon\AccountSellerSku');
        $this->accountModel = D('Api/Amazon/Accounts','Model');
        $this->siteService = D('Amazon\Sites', 'Service');
    }

    /**
     * @param array $options
     * @return array
     * 给客服系统提供的数据支撑
     */
    public function getAccountSellerSku ($options = array()) {
        $sqlOptions = array();
        $accountSellerSkuData = array(
            'timeOptions'=>array(),
            'onSaleOptions'=>array(),
        );

        isset($options['startTime']) && $sqlOptions[] = array('EGT', $options['startTime']);
        isset($options['endTime']) && $sqlOptions[] = array('ELT', $options['endTime']);

        if(!empty($sqlOptions)) {
            $sqlOptions = array(
                'add_time' => $sqlOptions,
            );
            $accountSellerSkuData['timeOptions'] = $this->accountSellerSkuModel->getAccountSellerSku($sqlOptions);
        }

        if(isset($options['onSale'])) {
            $sqlOptions = array(
                'is_used' => $options['onSale']
            );

            $accountSellerSkuData['onSaleOptions'] = $this->accountSellerSkuModel->getAccountSellerSku($sqlOptions);
        }

        return $accountSellerSkuData;
    }

    public function getSkuSellerSku ($skuParam, $skuType) {
        $re = array();

        $skuTypes = array('PRIVATE_SKU', 'SELLER_SKU');
        $requestSkuColumn = strtolower($skuType);
        array_splice($skuTypes, array_search($skuType, array('PRIVATE_SKU', 'SELLER_SKU')), 1);
        $goalSkuColumn = strtolower(current($skuTypes));

        $sqlOptions = array(
            $requestSkuColumn => array('IN', $skuParam),
            'is_used' => 1,
        );
        $results = $this->accountSellerSkuModel->selectSku($sqlOptions, array('private_sku', 'seller_sku'));

        foreach ($results as $result) {
            $re[$result[$requestSkuColumn]][] = $result[$goalSkuColumn];
        }

        return $re;
    }

    public function getAccountSiteData() {
        $re = array();
        $re['accounts'] = $this->accountModel->getField('id AS accountId, name AS accountName');
        $re['sites'] = $this->siteService->getSites();
        $sql = 'SELECT `a`.`id` AS `accountId`,`a`.`name` AS `accountName`, `s`.`id` AS `siteId`, `s`.`shorthand_code` AS `siteName`
        FROM `amazonorder_account_troop_site` AS `t`
        LEFT JOIN amazonorder_accounts AS `a` ON `t`.`account_id` = `a`.`id`
        LEFT JOIN `amazonorder_sites` AS `s` ON `s`.`id` = `t`.`site_id` WHERE 1';
        $relation = M('amazonorder_accounts',' ','fbawarehouse')->query($sql);
        foreach ($relation as $_rel) {
            if(empty($_rel['siteId'])) {
                $_rel['siteId'] = '';
                $_rel['siteName'] = 'noData';
            }
            $re['relation'][$_rel['accountId']] = $_rel;
        }

        return $re;
    }
}