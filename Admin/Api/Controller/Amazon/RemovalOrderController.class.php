<?php
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class RemovalOrderController extends CommonController{
    public $reportRemovalService = NULL;
    public $accountsService         = NULL;

    public function __construct(){
        $this->reportRemovalService = D('Amazon\ReportRemoval','Service');
        $this->accountsService         = D('Amazon\Accounts','Service');
        parent::__construct();
    }

    /**
     * 描述：首页展示
     */
    public function index(){

        //$condition = $_GET;
        $accounts = $this->accountsService->getAccounts();

        //$reportStockSaleData = $this->reportStockSaleService($condition);
        $this->assign('accounts',$accounts);

        $this->display();
    }

    /**
     * 描述：获取报告
     */
    public function getReport(){
        $condition = array_filter($_POST);
        if (!empty($condition)) {
            $reportRemovalData = $this->reportRemovalService->selectData($condition);
            echo json_encode($reportRemovalData);
        }
        exit;

    }

    /**
     * 描述：下载报告
     */
    public function exportReport(){
        $options = array_filter($_POST);
        $options['download'] = 1;

        $this->reportRemovalService->downloadData($options);
    }
}