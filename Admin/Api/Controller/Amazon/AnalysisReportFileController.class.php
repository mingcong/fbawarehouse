<?php
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class AnalysisReportFileController {
    protected $reportService = NULL;

    public function __construct() {
        $this->reportService = D('Amazon\Report', 'Service');
    }

    public function analysisFile() {
        $reportId = $_GET['reportId'] ? $_GET['reportId'] : NULL;
        $this->reportService->analyzeUnAnalyzeFile($reportId);
    }
}