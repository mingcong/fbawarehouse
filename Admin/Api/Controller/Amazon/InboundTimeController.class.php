<?php
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class InboundTimeController extends CommonController{
    public $reportReceiptsService = NULL;
    public $accountsService         = NULL;

    public function __construct(){
        $this->reportReceiptsService = D('Amazon\ReportReceipts','Service');
        $this->accountsService         = D('Amazon\Accounts','Service');
        parent::__construct();
    }

    /**
     * 描述：首页展示
     */
    public function index(){

        //$condition = $_GET;
        $accounts = $this->accountsService->getAccounts();

        //$reportStockSaleData = $this->reportStockSaleService($condition);
        $this->assign('accounts',$accounts);

        $this->display();
    }

    /**
     * 描述：获取报告
     */
    public function getReport(){

        $reportReceiptsData = $this->reportReceiptsService->selectData($_POST);

        echo json_encode($reportReceiptsData);
        exit;

    }

    /**
     * 描述：下载报告
     */
    public function exportReport(){
        $options = $_POST;
        $options['download'] = 1;

        $this->reportReceiptsService->downloadData($options);
    }
}