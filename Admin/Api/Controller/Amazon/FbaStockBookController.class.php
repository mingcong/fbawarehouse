<?php
//到最后，亚马逊数据还是要形成报表
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class FbaStockBookController extends CommonController {
    public $fbaStockBookService = NULL;
    public $accountsService = NULL;
    public $error = array();

    public function __construct() {
        $this->fbaStockBookService = D('Amazon\FbaStockBook', 'Service');
        $this->accountsService = D('Amazon\Accounts', 'Service');

        parent::__construct();
    }

    public function index() {
        $this->assign('totalData', $this->fbaStockBookService->pieceSiteData());
        $this->display();
    }

    public function site() {
        $this->assign('accounts', $this->fbaStockBookService->accounts);
        $this->assign('sites', $this->fbaStockBookService->sites);
        $this->assign('siteData', $this->fbaStockBookService->pieceAccountData(
            !empty($_GET['siteId']) ? $_GET['siteId'] : 0,
            !empty($_GET['accountId']) ? $_GET['accountId'] : 0
            )
        );
        $this->assign('get', $_GET);
        $this->assign('page',$this->fbaStockBookService->page);
        $this->assign('count',$this->fbaStockBookService->count);
        $this->display();
    }

    public function account() {
        $this->assign('accounts', $this->fbaStockBookService->accounts);
        $this->assign('sites', $this->fbaStockBookService->sites);

        $options = array();
        !empty($_GET['accountId']) && $options['accountId'] = $_GET['accountId'];
        !empty($_GET['siteId']) && $options['siteId'] = $_GET['siteId'];
        !empty($_GET['privateSku']) && $options['privateSku'] = $_GET['privateSku'];

        $this->assign('get', $_GET);
        $this->assign('accountData', $this->fbaStockBookService->pieceDetailData($options, $_GET));
        $this->assign('page',$this->fbaStockBookService->page);
        $this->assign('count',$this->fbaStockBookService->count);
        $this->display();
    }

    public function download() {
        $options = array();
        if($_GET['type'] == 'detail') {
            empty($_GET['accountId']) || $options['accountId'] = $_GET['accountId'];
            empty($_GET['siteId']) || $options['siteId'] = $_GET['siteId'];
            empty($_GET['privateSku']) || $options['privateSku'] = $_GET['privateSku'];
        }
        $_GET['type'] == 'detail' ?
        $this->fbaStockBookService->pieceDetailData($options, $_GET) :
            $this->fbaStockBookService->pieceAccountData(intval($_GET['siteId']), intval($_GET['accountId']));
    }
}