<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/1
 * Time: 19:56
 * 货件计划测试类
 */
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class InboundController extends CommonController{
    protected $inboundService = NULL;

    public function __construct() {
        $this->inboundService = D('Amazon\Inbound', 'Service');
    }

    /**
     * 测试获取货件信息
     */
    public function getInboundList () {
        $account_id = 51;
        $options = array(
            'ShipmentIdList' => array(
                //'FBA49SMHJ4',
                //'FBA48WQKF0',
            ),
            'ShipmentStatusList' => array(
                'CLOSED',
            ),
        );
        $shipmentidList = $this->inboundService->getInboundShipments($account_id, $options);

        print_r($shipmentidList);
        exit;
    }
    /**
     * 测试创建货件入库计划
     */
    public function createInboundShipmentPlan () {
        $account_id = 51;
        $options = array(
            'ShipFromAddress' => array(
                'Name'=>'Youkeshu (Shenzhen) Wangluokeji Youxiangongsi',
                'AddressLine1'=>'Pingan Blv No.1 China South Road , Pinghu Town',
                'AddressLine2'=>'',
                'City'=>'Shenzhen',
                'StateOrProvinceCode'=>'Guangdong',
                'PostalCode'=>'518000',
                'CountryCode'=>'CN',
            ),
            'LabelPrepPreference' => 'SELLER_LABEL',
            'shipmentPlanItems' => array(
                array(
                    'SellerSKU'=>'AF@@_AM118700',
                    'Quantity' => '600',
                ),
                array(
                    'SellerSKU'=>'AF_AM29181@@',
                    'Quantity' => '100',
                ),
            ),
        );
        $shipmentidList = $this->inboundService->createInboundShipmentPlan($account_id, $options);

        print_r($shipmentidList);
        exit;
    }
    /**
     * 测试创建货件入库信息
     */
    public function createInboundShipment () {
        $account_id = 51;
        $options = array(
            'ShipmentId' => array(
                'FBA49SNNYH',
            ),
            'InboundShipmentHeader' => array(
                'ShipmentName' => '20170105',
                'ShipFromAddress' => array(
                    'Name'=>'DongguanTw',
                    'AddressLine1'=>'huanandadao yihao huananguoji pige piju yuanfuliao wuliuqu',
                    'AddressLine2'=>'',
                    'City'=>'Shenzhen',
                    'StateOrProvinceCode'=>'Guangdong',
                    'PostalCode'=>'518111',
                    'CountryCode'=>'CN',
                ),
                'DestinationFulfillmentCenterId' => 'FTW1',
                'LabelPrepPreference' => 'SELLER_LABEL',
                'AreCasesRequired' => false,
                'ShipmentStatus' => 'WORKING',

            ),
            'InboundShipmentItems' => array(
                array(
                    'SellerSKU' => 'AF_AM29181@@',
                    'QuantityShipped' => 210,
                ),
                array(
                    'SellerSKU' => 'AF_465451@@',
                    'QuantityShipped' => 10,
                ),
            ),
        );
        $shipMentidList = $this->inboundService->createInboundShipment($account_id, $options);
        print_r($shipMentidList);
        exit;
    }
    /**
     * 更新货件入库信息
     */
    public function updateInboundShipment () {
        $account_id = 51;
        $options = array(
            'ShipmentId' => '',
            'InboundShipmentHeader' => array(
                'ShipmentName' => '',
                'ShipFromAddress' => array(
                    'Name'=>'DongguanTw',
                    'AddressLine1'=>'huanandadao yihao huananguoji pige piju yuanfuliao wuliuqu Guangdong',
                    'AddressLine2'=>'',
                    'City'=>'Shenzhen',
                    'StateOrProvinceCode'=>'Longgang',
                    'PostalCode'=>'518111',
                    'CountryCode'=>'CN',
                ),
                'DestinationFulfillmentCenterId' => '',
                'LabelPrepPreference' => '',
                'AreCasesRequired' => '',
                'ShipmentStatus' => '',

            ),
            'InboundShipmentItems' => array(
                array(
                    'ShipmentId' => '',
                    'SellerSKU' => 'AF_AM29181@@',
                    'FulfillmentNetworkSKU' => 'X0014X4YSR',
                    'QuantityShipped' => 210,
                    'QuantityReceived' => '',
                    'QuantityInCase' => '',
                ),
                array(
                    'ShipmentId' => '',
                    'SellerSKU' => 'AF_465451@@',
                    'FulfillmentNetworkSKU' => 'X0014X4YSR',
                    'QuantityShipped' => 10,
                    'QuantityReceived' => '',
                    'QuantityInCase' => '',
                ),
            ),
        );
        $shipmentidList = $this->inboundService->updateInboundShipment($account_id, $options);
        print_r($shipmentidList);
        exit;
    }
    /**
     * 返回入库货件的当前运输信息
     * */

    public function getTransportContent()
    {
        $account_id = 51;
        $options = array(
            'ShipmentId' => 'FBA47LX9DR',
        );
        $shipmentidList = $this->inboundService->getTransportContent($account_id, $options);

        print_r($shipmentidList);
        exit;
    }
    /**
     * 返回用于打印入库货件包裹标签的 PDF 文档数据
     */
    public function getPackageLabels()
    {
        $account_id = 51;
        $options = array(
            'ShipmentId' => 'FBA47LX9DR',
            'PageType' => 'PackageLabel_Plain_Paper',
            'NumberOfPackages' => '11',
        );
        $shipmentidList = $this->inboundService->getPackageLabels($account_id, $options);
        $file    = 'test.zip';
        $file    = str_replace('','',$file);
        $fp      = fopen($file,'w');
        fputs($fp,base64_decode($shipmentidList['PdfDocument']));
        fclose($fp);
        $zip = zip_open($file);
        if($zip){
            while ($zip_entry = zip_read($zip)){
                #预览pdf
                $name = time();
                zip_entry_open($zip,$zip_entry);
                $tpdf = zip_entry_read($zip_entry,zip_entry_filesize($zip_entry));
                $file = $name.'.pdf';
                $file = str_replace('','',$file);
                $fp   = fopen($file,'w');
                fputs($fp,$tpdf);
                fclose($fp);
                $file = $name.".pdf";
                header('Content-type: application/pdf');
                header('filename='.$file);
                readfile($file);
                exit;
                //下载
                //header('Content-Type:pdf'); //指定下载文件类型
                //header('Content-Disposition: attachment; filename="'.$filename.'"'); //指定下载文件的描述
                //header('Content-Length:'.filesize($filename)); //指定下载文件的大小
                //eadfile($filename);
            }
        }
        print_r($shipmentidList);
        exit;
    }
    /*
     * 取消之前确认的使用亚马逊合作承运人配送入库货件的请求
     * */
    public function voidTransportRequest(){
        $account_id = 51;
        $options = array(
            'ShipmentId' => '',
            'LastUpdatedAfter' => '',
            'LastUpdatedBefore' => '',
        );
        $shipmentidList = $this->inboundService->voidTransportRequest($account_id, $options);
        print_r($shipmentidList);
        exit;
    }
    /*
     * 返回指定入库货件的商品列表或在指定时间段内更新的商品列表
     * */
    public function listInboundShipmentItems(){
        $account_id = 51;
        $options = array(
            'ShipmentId' => 'FBA49SMHJ4 ',
//            'LastUpdatedAfter' => gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z",time()-360*24*30),
//            'LastUpdatedBefore' => gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z",time()),
        );
        $shipmentidList = $this->inboundService->listInboundShipmentItems($account_id, $options);
        print_r($shipmentidList);
        exit;
    }
    /*
     * 返回“配送入库货件 API”部分的运行状态
     * */
    public function getServiceStatus(){
        $account_id = 51;
        $options = array();
        $shipmentIdList = $this->inboundService->getServiceStatus($account_id, $options);
        print_r($shipmentIdList);
        exit;
    }
}