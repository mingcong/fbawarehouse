<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/27
 * Time: 17:47
 * 请求外部系统的客户端
 */

namespace Api\Controller\Amazon;

use Inbound\Service\PublicInfoService;
header("Content-Type: text/html; charset=utf-8");
class ClientController
{

    public function __construct() {
        $this->status  =  PublicInfoService::get_inbound_status();
    }
    /**
     * 描述: 关务系统物流计划单推送接口，物流计划单发货后将数据推送到关务系统手动推送
     * 作者: kelvin
     */
    public function test() {
        $code = $_GET['transport_plan_code']?'1708230911101067':'1708230911101067';
        if($code){
            $model = M('transport_plan','fba_','fbawarehouse')->where('transport_plan_code = 170627030407550')->getField('id',true);
            if(count($model)==0){
                echo '数据为空';die;
            }
            foreach ($model as $id){
                $plan =  M('inbound_shipment_plan','fba_','fbawarehouse')->where("transport_plan_id = $id")->getField('shipmentid',TRUE);
                foreach ($plan as $planId){
                    $this->shipmentIdPost($planId);
                }
                $this->pushTransportInfo($id);
            }
        }else{
            echo '参数不存在';die;
        }
    }
    /**
     * 描述: 根据物流计划单id推送平台计划单
     * 作者: kelvin
     */
    public function shipmentIdPostFormTransId($transId) {
        $plan =  M('inbound_shipment_plan','fba_','fbawarehouse')->where("transport_plan_id = $transId")->getField('shipmentid',TRUE);
        foreach ($plan as $planId){
            $this->shipmentIdPost($planId);
        }
        return TRUE;
    }
    /**
     * 描述: 物流计划单推送接口
     * 作者: kelvin
     */
    public function pushTransportInfo($tranId = null) {
        header("Content-Type: text/html; charset=utf-8");
//        $tranId = 716;
//        dump($tranId);die;
        $data = array();
        if(!$tranId){
            echo 'Parameter error';
            return false;
        }
        $model = M('','','fbawarehouse');
        $tran = $model->table('fba_transport_plan')
            ->where("id = $tranId")
            ->field('transport_plan_code,invoice_code,contract_no,create_time,status,destination_site_id,clearance_port')
            ->find();
        $plan = $model->table('fba_inbound_shipment_plan')
            ->where("transport_plan_id = $tranId")
            ->field('id,shipmentid,site_id')
            ->select();
        $planId = array();
        $shipmentId = array();
        foreach($plan as $p){
            $planId[] = $p['id'];
            $shipmentId[] = $p['shipmentid'];
        }
        $shipmentId = implode("','",$shipmentId);
        $sql = "SELECT
              `shipmentDetail`.`sku`, `shipmentDetail`.`seller_sku`, `shipmentDetail`.`quantity`, `shipmentDetail`.seller_sku,
              `shipmentDetail`.`enterprise_dominant`, `shipment`.`shipmentid`, wd.warehouseorders_id,
              wd.quantity AS `fromWareQty`, pd.money/pd.`quantity` AS `exportSinglePrice`, su.name,pu.id as puId
            FROM
              `fba_inbound_shipment_plan_detail` AS `shipmentDetail`
            LEFT JOIN
              `fba_inbound_shipment_plan` AS `shipment`
            ON
              shipment.id = `shipmentDetail`.`inbound_shipment_plan_id`
            LEFT JOIN
              wms_warehouse_deliveryorders AS wd
            ON
              (
                wd.deliveryorders_id = `shipmentDetail`.deliveryorders_id
              )
            LEFT JOIN
              wms_warehouseorders AS w
            ON
              w.id = wd.warehouseorders_id
            LEFT JOIN
              wms_purchaseorder_details AS pd
            ON
              (
                pd.purchaseorder_id = w.purchaseorders_id AND pd.sku = w.sku
              )
            LEFT JOIN
              wms_purchaseorders AS pu
            ON
              pu.id = pd.purchaseorder_id
            LEFT JOIN
              skusystem_suppliers AS su
            ON
              su.id = w.supplier_id
            WHERE
              `shipment`.`shipmentid` in ('$shipmentId')";
        $result = $model->query($sql);
        $detail = array();
        foreach($result as $ship =>$met){
            $detail[$met['shipmentid']][$met['seller_sku']] = $met;
        }
        $planId = implode(',',$planId);
        $boxes = $model->table('fba_package_box')
            ->where("inbound_shipment_plan_id in ($planId)")
            ->field('inbound_shipment_plan_id,id,length,width,height,weight,export_tax_rebate')
            ->select();
        $box = array();
        $boxId = array();
        foreach($boxes as $b => $x){
            $box[$x['inbound_shipment_plan_id']][] = $x;
            $boxId[] = $x['id'];
        }
        $boxId = implode(',',$boxId);
        $skus = $model->table('fba_package_box_details')
            ->where("package_box_id in($boxId)")
            ->field('id,package_box_id,sku,seller_sku,sku_name,quantity')
            ->select();
        $sku =array();
        foreach($skus as $sk => $ku){
            $sku[$ku['package_box_id']][] = $ku;
        }
        $status = PublicInfoService::get_inbound_status();
        $site = PublicInfoService::get_site_array();
        $data['transportPlanCode'] = $tran['transport_plan_code'];
        $data['invoiceCode'] = $tran['invoice_code'];
        $data['contractNo'] = $tran['contract_no'];
        $data['createTime'] = $tran['create_time'];
        $data['status'] = $status[$tran['status']];
        $data['country'] = $tran['destination_site_id']?$site[$tran['destination_site_id']]:'';
        $data['clearance_port'] = $tran['clearance_port']?$tran['clearance_port']:'';
        $data['number'] = count($boxes);
        foreach($plan as $k =>&$v){
            $data['tranPlanDetail'][$k]['shipmentid'] = $v['shipmentid'];
            foreach($box[$v['id']] as $y =>$c){
                $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxId']     = $c['id'];
                $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['export_tax_rebate']     = $c['export_tax_rebate'];
                $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxLength'] = $c['length'];
                $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxWidth']  = $c['width'];
                $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxHeight'] = $c['height'];
                $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxWeight'] = $c['weight'];
                foreach($sku[$c['id']] as $s =>$u) {
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['sku'] = $u['sku'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['skuName'] = $u['sku_name'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['sellerSku'] = $u['seller_sku'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['quantity'] = $u['quantity'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['warehouseorders_id'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['warehouseorders_id']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['warehouseorders_id']:'';
//                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['skuExportPrice'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['exportSinglePrice']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['exportSinglePrice']:'';
//                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['suName'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['name']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['name']:'';
//                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['enterpriseDominant'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['enterprise_dominant']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['enterprise_dominant']:'';
//                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['puId'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['puId']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['puId']:'';
                }
            }
        }
        $sign = $this->signatureKey(array('transportPlanCode'=>$data['transportPlanCode']),'fba');
        $info = array(
            'sign' =>$sign,
            'detail'=>$data
        );
        $handle = curl_init();
        $url = 'http://customs.kokoerp.com/api/index.php?c=api_transportall&a=getFbaTransport';
        curl_setopt($handle,CURLOPT_URL,$url);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($handle,CURLOPT_USERAGENT,'Fbawarehouse API/1.0');
        curl_setopt($handle,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($handle,CURLOPT_TIMEOUT,90);
        curl_setopt($handle,CURLOPT_MAXREDIRS,3);
        curl_setopt($handle,CURLOPT_POST,TRUE);
        curl_setopt($handle,CURLOPT_POSTFIELDS,http_build_query($info));
        curl_setopt($handle,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_0);
        curl_setopt($handle,CURLOPT_HEADER,FALSE);    //不返回头信息

        $response = curl_exec($handle);
        if(curl_errno($handle)) {
            $re          = new \stdClass();
            $re->code    = 100;
            $re->message = '连接主机'.$url.'时发生错误: '.curl_error($handle)."<br/>";
            curl_close($handle);
            return $re->message;
        }
        curl_close($handle);
        return TRUE;

//        exit;
    }
    /**
     * @version 根据必要参数生成密钥
     * @param array
     * param=array(
     *      "transportPlanCode" : 物流计划单 ,
     * )
     * @return  密钥
     */
    public function signatureKey($param,$key){
        ksort($param);
        $string = '';
        foreach ($param as $k => $v) {
            $string .= $k . '=' . urlencode($v);
        }
        $sign = hash_hmac("md5", strtolower($string), $key);
        return $sign;
    }
    /**
     * 描述: 关务系统物流计划单抓取接口
     * 作者: kelvin
     */
    public function getTransportInfo() {
        $model = M('','','fbawarehouse');
//        $code = array('170627030407550','170822085213213','1708230911101067');
        $code = json_decode($_GET['data']);
        $codes = implode("','",$code);
        $tranId = $model->table('fba_transport_plan')
            ->where("transport_plan_code in ('$codes')")
            ->getfield('id,status,transport_plan_code',true);
//            ->select();
        if(!$code){
            $msg['state']  = 400;
            $msg['info'] = 'The Parameter is empty';
            $msg['data'] = '';
            echo json_encode($msg);die;
        }
        $all = array();
        $delList = array();
        foreach($tranId as $n =>$t){
            if($t['status']==100){
                $all[] = $t['transport_plan_code'];
                continue;
            }
            $data = array();
            $tran       = $model->table('fba_transport_plan')
                ->where("id = $n")
                ->field('transport_plan_code,invoice_code,contract_no,create_time,status,destination_site_id,clearance_port')
                ->find();
            $plan       = $model->table('fba_inbound_shipment_plan')
                ->where("transport_plan_id = $n")
                ->field('id,shipmentid,site_id')
                ->select();
            $planId     = array();
            $shipmentId = array();
            foreach($plan as $p){
                $planId[]     = $p['id'];
                $shipmentId[] = $p['shipmentid'];
            }
            $shipmentId = implode("','",$shipmentId);
            $sql        = "SELECT
                  `shipmentDetail`.`sku`, `shipmentDetail`.`seller_sku`, `shipmentDetail`.`quantity`, 
                  `shipmentDetail`.`enterprise_dominant`, `shipment`.`shipmentid`, wd.warehouseorders_id,
                  wd.quantity AS `fromWareQty`, pd.money/pd.`quantity` AS `exportSinglePrice`, su.name,pu.id as puId
                FROM
                  `fba_inbound_shipment_plan_detail` AS `shipmentDetail`
                LEFT JOIN
                  `fba_inbound_shipment_plan` AS `shipment`
                ON
                  shipment.id = `shipmentDetail`.`inbound_shipment_plan_id`
                LEFT JOIN
                  wms_warehouse_deliveryorders AS wd
                ON
                  (
                    wd.deliveryorders_id = `shipmentDetail`.deliveryorders_id
                  )
                LEFT JOIN
                  wms_warehouseorders AS w
                ON
                  w.id = wd.warehouseorders_id
                LEFT JOIN
                  wms_purchaseorder_details AS pd
                ON
                  (
                    pd.purchaseorder_id = w.purchaseorders_id AND pd.sku = w.sku
                  )
                LEFT JOIN
                  wms_purchaseorders AS pu
                ON
                  pu.id = pd.purchaseorder_id
                LEFT JOIN
                  skusystem_suppliers AS su
                ON
                  su.id = w.supplier_id
                WHERE
                  `shipment`.`shipmentid` in ('$shipmentId')";
            $result     = $model->query($sql);
            $detail     = array();
            foreach($result as $ship => $met){
                $detail[$met['shipmentid']][$met['seller_sku']] = $met;
            }
            $planId = implode(',',$planId);
            $boxes  = $model->table('fba_package_box')
                ->where("inbound_shipment_plan_id in ($planId)")
                ->field('inbound_shipment_plan_id,id,length,width,height,weight,export_tax_rebate')
                ->select();
            $box    = array();
            $boxId  = array();
            foreach($boxes as $b => $x){
                $box[$x['inbound_shipment_plan_id']][] = $x;
                $boxId[]                               = $x['id'];
            }
            $boxId = implode(',',$boxId);
            $skus  = $model->table('fba_package_box_details')
                ->where("package_box_id in($boxId)")
                ->field('id,package_box_id,sku,seller_sku,sku_name,quantity')
                ->select();
            $sku   = array();
            foreach($skus as $sk => $ku){
                $sku[$ku['package_box_id']][] = $ku;
            }
            $status = $this->status;
            $site                      = PublicInfoService::get_site_array();
            $data['transportPlanCode'] = $tran['transport_plan_code'];
            $data['invoiceCode']       = $tran['invoice_code'];
            $data['contractNo']        = $tran['contract_no'];
            $data['createTime']        = $tran['create_time'];
            $data['status']            = $status[$tran['status']];
            $data['country']           = $tran['destination_site_id']?$site[$tran['destination_site_id']]:'';
            $data['clearance_port']    = $tran['clearance_port']?$tran['clearance_port']:'';
            $data['number']            = count($boxes);
            foreach($plan as $k => &$v){
                $data['tranPlanDetail'][$k]['shipmentid'] = $v['shipmentid'];
                foreach($box[$v['id']] as $y => $c){
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxId']             = $c['id'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['export_tax_rebate'] = $c['export_tax_rebate'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxLength']         = $c['length'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxWidth']          = $c['width'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxHeight']         = $c['height'];
                    $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxWeight']         = $c['weight'];
                    foreach($sku[$c['id']] as $s => $u){
                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['sku'] = $u['sku'];
                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['skuName'] = $u['sku_name'];
                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['sellerSku'] = $u['seller_sku'];
                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['quantity'] = $u['quantity'];
                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['warehouseorders_id'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['warehouseorders_id']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['warehouseorders_id']:'';
//                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['skuExportPrice'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['exportSinglePrice']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['exportSinglePrice']:'';
//                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['suName'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['name']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['name']:'';
//                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['enterpriseDominant'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['enterprise_dominant']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['enterprise_dominant']:'';
//                        $data['tranPlanDetail'][$k]['shipmentidDetail'][$y]['boxDetail'][$s]['puId'] = $detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['puId']?$detail[$data['tranPlanDetail'][$k]['shipmentid']][$u['seller_sku']]['puId']:'';
                    }
                }
            }

            $all[] = $data;
        }
        if(count($all)>0){
            $msg['state']  = 200;
            $msg['info'] = 'success';
            $msg['data'] = $all;
        }else{
            $msg['state']  = 300;
            $msg['info'] = 'The Data is empty';
            $msg['data'] = '';
        }
        echo json_encode($msg);die;
    }
    /**
     * 描述: 关务系统平台计划单推送接口
     * 作者: kelvin
     */
    public function shipmentIdPost($shipmentId = NULL) {
        $shipmentId = "FBA15BRT14LT";
        header("Content-Type: text/html; charset=utf-8");
        $model = M('','','fbawarehouse');
        if(!$shipmentId){
            return FALSE;
        }
        $sql = "SELECT
              `shipment`.`shipmentid`, `shipment`.`transport_plan_id`, `shipmentDetail`.`sku` ,`shipmentDetail`.`sku_name`, `shipmentDetail`.`seller_sku`, 
              `shipmentDetail`.`quantity`, `shipmentDetail`.`export_tax_rebate`,`tran`.transport_plan_code,
              `shipmentDetail`.`enterprise_dominant`, wd.warehouseorders_id,`shipment`.`status`,`shipment`.`create_time`,
              wd.quantity AS `fromWareQty`, pd.money/pd.`quantity` AS `exportSinglePrice`, su.name,pu.id as puId
            FROM
              `fba_inbound_shipment_plan_detail` AS `shipmentDetail`
            LEFT JOIN
              `fba_inbound_shipment_plan` AS `shipment`
            ON
              shipment.id = `shipmentDetail`.`inbound_shipment_plan_id`
            LEFT JOIN  `fba_transport_plan` AS `tran` ON `tran`.id = `shipment`.transport_plan_id
            LEFT JOIN 
              wms_warehouse_deliveryorders AS wd
            ON
              (
                wd.deliveryorders_id = `shipmentDetail`.deliveryorders_id
              )
            LEFT JOIN
              wms_warehouseorders AS w
            ON
              w.id = wd.warehouseorders_id
            LEFT JOIN
              wms_purchaseorder_details AS pd
            ON
              (
                pd.purchaseorder_id = w.purchaseorders_id AND pd.sku = w.sku
              )
            LEFT JOIN
              wms_purchaseorders AS pu
            ON
              pu.id = pd.purchaseorder_id
            LEFT JOIN
              skusystem_suppliers AS su
            ON
              su.id = w.supplier_id
            WHERE
              `shipment`.`shipmentid` = '$shipmentId'";
        $result = $model->query($sql);
        $status = $this->status;
        $data = array();
        foreach($result as $r) {
            if(!isset($data[$r['shipmentid']])) {
                $data[$r['shipmentid']]['shipmentid'] = $r['shipmentid'];
                $data[$r['shipmentid']]['transportPlanCode'] = $r['transport_plan_code']?$r['transport_plan_code']:'';
                $data[$r['shipmentid']]['shipmentidStatus'] = $status[$r['status']];
                $data[$r['shipmentid']]['shipmentidCreateTime'] = $r['create_time'];
            }

            if(!isset($data[$r['shipmentid']][$r['seller_sku']])) {
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['sku'] = $r['sku'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['skuName'] = $r['sku_name'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['sellerSku'] = $r['seller_sku'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['sellerSkuQty'] = $r['quantity'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['exportTaxRebate'] = $r['export_tax_rebate'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['enterpriseDominant'] = PublicInfoService::getCompanyName($r['enterprise_dominant']);
            }

            $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['fromPurchaseorder'][] = array(
                'purchaseorderId'=>$r['puId']?$r['puId']:$this->getPurchaseOrder($r['warehouseorders_id']),
                'fromWareQty'=>$r['fromWareQty']?$r['fromWareQty']:' ',
                'exportSinglePrice'=>$r['exportSinglePrice']?$r['exportSinglePrice']:$this->getPurchaseOrder($r['warehouseorders_id'],'exportSinglePrice'),
                'supplierName'=>$r['name']?$r['name']:$this->getPurchaseOrder($r['warehouseorders_id'],'suName'),
                'warehouseorderId'=>$r['warehouseorders_id']?$r['warehouseorders_id']:' '
            );

        }
        foreach($data as $K => $v){
            $data[$K]['shipmentidDetails'] = array_values($v['shipmentidDetails']);
        }
        $sign = $this->signatureKey(array('shipmentid'=>$shipmentId),'fba');
        $info = array(
            'sign' =>$sign,
            'detail'=>array_values($data)
        );
        $handle = curl_init();
        $url = 'http://customs.kokoerp.com/api/index.php?c=api_transportall&a=getFbaShipment';//测试
        curl_setopt($handle,CURLOPT_URL,$url);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($handle,CURLOPT_USERAGENT,'Fbawarehouse API/1.0');
        curl_setopt($handle,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($handle,CURLOPT_TIMEOUT,90);
        curl_setopt($handle,CURLOPT_MAXREDIRS,3);
        curl_setopt($handle,CURLOPT_POST,TRUE);
        curl_setopt($handle,CURLOPT_POSTFIELDS,http_build_query($info));
        curl_setopt($handle,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_0);
        curl_setopt($handle,CURLOPT_HEADER,FALSE);    //不返回头信息

        $response = curl_exec($handle);
        if(curl_errno($handle)) {
            $re          = new \stdClass();
            $re->code    = 100;
            $re->message = '连接主机'.$url.'时发生错误: '.curl_error($handle)."<br/>";
            curl_close($handle);
            return $re->message;
        }
        curl_close($handle);
        return TRUE;

    }
    /**
     * 描述: 关务系统平台计划单抓取接口
     * 作者: kelvin
     */
    public function shipmentIdGet() {
        header("Content-Type: text/html; charset=utf-8");
        $data = json_decode($_GET['data'],TRUE);
        if (!$data) {
            $msg['state']  = 400;
            $msg['info'] = 'The Parameter is empty';
            $msg['data'] = '';
        } else{
            $detail = array();
            foreach($data as $k=>$v){
                $detail[] = $this->getDataFromShipmentIdAndSellerSku($v['shipmentId'],$v['sellerSku']);
            }
            if(count($detail)==0){
                $msg['state']  = 300;
                $msg['info'] = 'The data is empty';
                $msg['data'] = '';
            }else{
                $msg['state']  = 200;
                $msg['info'] = 'success';
                $msg['data'] = $detail;
            }

        }
        echo json_encode($msg);die;
    }
    /**
     * 描述: 根据ShipmentId和SellerSku查询平台计划单接口所需信息
     * 作者: kelvin
     */
    public function getDataFromShipmentIdAndSellerSku($shipmentId,$sellerSku) {
        $model = M('','','fbawarehouse');
        $sql = "SELECT
              `shipment`.`shipmentid`, `shipment`.`transport_plan_id`, `shipmentDetail`.`sku` ,`shipmentDetail`.`sku_name`, `shipmentDetail`.`seller_sku`, 
              `shipmentDetail`.`quantity`, `shipmentDetail`.`export_tax_rebate`,`tran`.transport_plan_code,`rejects`.rej_qty,
              `shipmentDetail`.`enterprise_dominant`, wd.warehouseorders_id,`shipment`.`status`,`shipment`.`create_time`,
              wd.quantity AS `fromWareQty`, pd.money/pd.`quantity` AS `exportSinglePrice`, su.name,pu.id as puId
            FROM
              `fba_inbound_shipment_plan_detail` AS `shipmentDetail`
            LEFT JOIN
              `fba_inbound_shipment_plan` AS `shipment`
            ON
              shipment.id = `shipmentDetail`.`inbound_shipment_plan_id`
            LEFT JOIN  `fba_transport_plan` AS `tran` ON `tran`.id = `shipment`.transport_plan_id
            LEFT JOIN  `fba_shipment_rejects` AS `rejects` ON  `rejects`.inbound_shipment_plan_details_id = shipmentDetail.id
            LEFT JOIN 
              wms_warehouse_deliveryorders AS wd
            ON
              (
                wd.deliveryorders_id = `shipmentDetail`.deliveryorders_id
              )
            LEFT JOIN
              wms_warehouseorders AS w
            ON
              w.id = wd.warehouseorders_id
            LEFT JOIN
              wms_purchaseorder_details AS pd
            ON
              (
                pd.purchaseorder_id = w.purchaseorders_id AND pd.sku = w.sku
              )
            LEFT JOIN
              wms_purchaseorders AS pu
            ON
              pu.id = pd.purchaseorder_id
            LEFT JOIN
              skusystem_suppliers AS su
            ON
              su.id = w.supplier_id
            WHERE
              `shipment`.`shipmentid` ='$shipmentId'
            AND `shipmentDetail`.`seller_sku` = '$sellerSku'";
        $result =  $model->query($sql);
        $status = $this->status;
        $data = array();
        foreach($result as $r) {
            if(!isset($data[$r['shipmentid']])) {
                $data[$r['shipmentid']]['shipmentid'] = $r['shipmentid'];
                $data[$r['shipmentid']]['transportPlanCode'] = $r['transport_plan_code']?$r['transport_plan_code']:'';
                $data[$r['shipmentid']]['shipmentidStatus'] = $status[$r['status']];
                $data[$r['shipmentid']]['shipmentidCreateTime'] = $r['create_time'];
            }


            if(!isset($data[$r['shipmentid']][$r['seller_sku']])) {
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['sku'] = $r['sku'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['rejectsQty'] = $r['rej_qty']?$r['rej_qty']:0;
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['skuName'] = $r['sku_name'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['sellerSku'] = $r['seller_sku'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['sellerSkuQty'] = $r['quantity'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['exportTaxRebate'] = $r['export_tax_rebate'];
                $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['enterpriseDominant'] = PublicInfoService::getCompanyName($r['enterprise_dominant']);
            }
            $data[$r['shipmentid']]['shipmentidDetails'][$r['seller_sku']]['fromPurchaseorder'][] = array(
                'purchaseorderId'=>$r['puId']?$r['puId']:$this->getPurchaseOrder($r['warehouseorders_id']),
                'fromWareQty'=>$r['fromWareQty']?$r['fromWareQty']:' ',
                'exportSinglePrice'=>$r['exportSinglePrice']?$r['exportSinglePrice']:$this->getPurchaseOrder($r['warehouseorders_id'],'exportSinglePrice'),
                'supplierName'=>$r['name']?$r['name']:$this->getPurchaseOrder($r['warehouseorders_id'],'suName'),
                'warehouseorderId'=>$r['warehouseorders_id']?$r['warehouseorders_id']:' '
            );

        }
        foreach($data as $K => $v){
            $data[$K]['shipmentidDetails'] = array_values($v['shipmentidDetails']);
        }
        $data = array_values($data);
        return $data[0];

    }
    /**
     * 描述: 根据入库单获取采购单信息
     * 作者: kelvin
     */
    public function getPurchaseOrder($wareId = NULL,$name = 'puId') {
        $model = M('','','fbawarehouse');
        $str = ' ';
        if(isset($wareId) && $wareId){
            $result = $model->table('fba_shipment_purchaseorder')->where("warehouseorders_id = '$wareId'")->find();
            if($result){
                if($name == 'exportSinglePrice'){
                    return $result['export_single_price'];//含税单价
                }elseif ($name == 'puId'){
                    return $result['purchaseorder_id'];//采购单
                }elseif ($name == 'suName'){
                    return PublicInfoService::getSupplierName($result['supplier_id']);//供应商id
                }else{
                    return $str;
                }
            }else{
                return $str;
            }
        }else{
            return $str;
        }
    }
}