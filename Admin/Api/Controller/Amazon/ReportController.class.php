<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/1
 * Time: 19:56
 */
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class ReportController extends CommonController{
    protected $reportService = NULL;

    public function __construct() {
        $this->reportService = D('Amazon\Report', 'Service');
        parent::__construct();
    }

    public function getReportRequest(){
        $accountId = 51;
        $options = array(
            'ReportType' => !empty($_GET['reportType']) ? $_GET['reportType'] : '_GET_AFN_INVENTORY_DATA_BY_COUNTRY_'
        );

        $reportRequestResult = $this->reportService->getRequestReport($accountId,$options);

        print_r($reportRequestResult);exit;
        return $reportRequestResult;
    }

    public function getReportRequestList(){
        $accountId = 51;
        $options['ReportRequestIdList'] = array(
            'Id'=>array('52278017172')
        );

        $getReportRequestListResult = $this->reportService->getReportRequestList($accountId,$options);

        print_r($getReportRequestListResult);exit;
        return $getReportRequestListResult;
    }

    public function getReportList(){
        $accountId = 2;
        $options['ReportRequestIdList'] = array(
            'Id'=>array('56106017172')
        );

        $getReportListResult = $this->reportService->getReportList($accountId,$options);

        print_r($getReportListResult);exit;
        return $getReportListResult;
    }

    public function getReport(){
        $accountId = 51;
        $options['report_type'] = 4;
        $getReportResult = $this->reportService->getReport($options,$accountId);

        print_r($getReportResult);exit;
        return $getReportResult;
    }

    public function test(){

        $m = D('Amazon\AccountSellerSku','Service');
        $m->insertToAccountSellerSku();
        exit;

    }
}