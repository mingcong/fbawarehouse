<?php
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;

class CrontabController {
    protected $reportService = NULL;
    protected $crontabService = NULL;

    public function __construct() {
        $this->reportService = D('Amazon\Report', 'Service');
        $this->inboundReport = D('Inbound/Report','Service');
        $this->inventory    = D('Warehouse/Inventory', 'Service');
        $this->skuService = D('Amazon\Title', 'Service');
        $this->crontabService = D('Amazon\Crontab', 'Service');
    }
    /****************计划任务******************/
    /**
     * 描述：跑销量的计划任务，一天一次，凌晨
     */
    public function saleReportCron(){
        $options['report_type'] = 1;
        $this->reportService->getReport($options);
    }

    /**
     * 描述：跑在途库存的计划任务，一天一次，三点
     */
    public function unsuppressedInventoryReportCron(){
        $options['report_type'] = 2;
        $this->reportService->getReport($options);
    }

    /**
     * 描述：跑库存的计划任务，一天两次,六点跑一次，十二点跑一次
     */
    public function inventoryReportCron(){
        $options['report_type'] = 3;
        $this->reportService->getReport($options);
    }

    /**
     * 描述：跑入库时间，一周跑一次
     */
    public function receiptsReportCron(){
        $options['report_type'] = 4;
        $this->reportService->getReport($options);
    }

    /**
     * 描述：跑物流移除订单的计划任务，
     */
    public function removalOrderReportCron(){
        $options['report_type'] = 5;
        $this->reportService->getReport($options);
    }
    //期初库存
    public function startCurrentInventoryReportCron() {
        set_time_limit(0);
        $accounts = PublicInfoService::get_accounts();
        foreach($accounts as $v) {
            $accountId = $v['id'];
        $site = PublicInfoService::get_siteid_by_accountid($accountId);
        $map['account_id'] = $accountId;
        $map['LEFT(snapshot_date,7)'] = date('Y-m', strtotime(date('Y-m-01', strtotime('-1 month')) . '-1 day'));
        if(date('d')==31){
            $map['LEFT(snapshot_date,7)'] = date('Y-m', strtotime(date('Y-m-01', strtotime('-1 month -1 day')) . '-1 day'));
        }
        $data = M('api_report_current_inventory', ' ', 'fbawarehouse')
            ->where($map)
            ->find();
        if (!empty($data)) {
            continue;
        }
        $options['report_type'] = 6;
        //日本站点
        if($site==55){
            $options['StartDate'] = date('Y-m-d H:i:s', strtotime(date('Y-m-01 00:00:00', strtotime('-1 month')) . '-1 day')-3600*9);
            $options['EndDate'] = date('Y-m-d H:i:s', strtotime(date('Y-m-01 23:59:59', strtotime('-1 month')) . '-1 day')-3600*9);
        }
        else{
            $options['StartDate'] = date('Y-m-d H:i:s', strtotime(date('Y-m-01 00:00:00', strtotime('-1 month')) . '-1 day')+3600*8);
            $options['EndDate'] = date('Y-m-d H:i:s', strtotime(date('Y-m-01 23:59:59', strtotime('-1 month')) . '-1 day')+3600*8);
        }
        $this->reportService->getReport($options,$accountId);
        }

    }
    //期末库存
    public function endCurrentInventoryReportCron() {
        set_time_limit(0);
        $accounts = PublicInfoService::get_accounts();
        foreach($accounts as $v) {
            $accountId = $v['id'];
            $site = PublicInfoService::get_siteid_by_accountid($accountId);
            $map['account_id'] = $accountId;
            $map['LEFT(snapshot_date,7)'] = date('Y-m', strtotime('-1 month'));
            if(date('d')==31){
                $map['LEFT(snapshot_date,7)'] = date('Y-m', strtotime('-1 month -1 day'));
            }
            $data = M('api_report_current_inventory', ' ', 'fbawarehouse')
                ->where($map)
                ->find();
            if (!empty($data)) {
                continue;
            }
            $options['report_type'] = 6;
            if($site==55){
                $options['StartDate'] = date('Y-m-d H:i:s',strtotime(date('Y-m-d 00:00:00', strtotime(date('Y-m-01') . '-1 day')))-3600*9);
                $options['EndDate'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d 23:59:59', strtotime(date('Y-m-01') . '-1 day')))-3600*9);
            }else{
                $options['StartDate'] = date('Y-m-d H:i:s',strtotime(date('Y-m-d 00:00:00', strtotime(date('Y-m-01') . '-1 day')))+3600*8);
                $options['EndDate'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d 23:59:59', strtotime(date('Y-m-01') . '-1 day')))+3600*8);
            }
            $this->reportService->getReport($options,$accountId);
        }
    }
    //出入库明细
    public function fbaInventoryReportCron() {
        set_time_limit(0);
        $accounts = PublicInfoService::get_accounts();
        foreach($accounts as $v) {
            $accountId = $v['id'];
            $site = PublicInfoService::get_siteid_by_accountid($accountId);
            $map['account_id'] = $accountId;
            $map['LEFT(snapshot_date,7)'] = date('Y-m', strtotime('-1 month'));
            if(date('d')==31){
                $map['LEFT(snapshot_date,7)'] = date('Y-m', strtotime('-1 month -1 day'));
            }
            $data = M('api_report_summary_inventory', ' ', 'fbawarehouse')
                ->where($map)
                ->find();
            if (!empty($data)) {
                continue;
            }
            $options['report_type'] = 7;
            if($site==55) {
                $options['StartDate'] = date('Y-m-d',strtotime(date('Y-m-01 H:i:s', strtotime(date('Y-m-01') . '-1 day')))-3600*9);
                $options['EndDate'] = date('Y-m-d',strtotime(date('Y-m-d 23:59:59', strtotime(date('Y-m-01') . '-1 day')))-3600*9);
            }else{
                $options['StartDate'] = date('Y-m-d',strtotime(date('Y-m-01 H:i:s', strtotime(date('Y-m-01') . '-1 day')))+3600*8);
                $options['EndDate'] = date('Y-m-d',strtotime(date('Y-m-d 23:59:59', strtotime(date('Y-m-01') . '-1 day')))+3600*8);
            }
            $this->reportService->getReport($options,$accountId);
        }
    }
    public function listingsReportCron() {
        set_time_limit(0);
        $options['report_type'] = 8;
        $this->reportService->getReport($options);
    }
    public function getSellerSku() {
        $this->skuService->getReport();
    }
    //fba进销存报表
    public function inboundReport() {
        set_time_limit(0);
        $this->inboundReport->apiGet();
    }
    /**
     * 描述: 每天一次的FBA库存数据监控
     * 作者: kelvin
     */
    public function fbaInventoryMonitoring() {
        set_time_limit(0);
        $this->inventory->fbaInventoryAdd();
    }

    /**
     * 更新shipmentid的已完成状态
     */
    public function updateShipmentidCompleteStatus() {
        set_time_limit(0);
        if(empty($_GET)){echo 'hehe';exit;}

        $params = array($_GET['accountId']=>array(explode(',', $_GET['shipmentids'])));
        $this->crontabService->updateShipmentidCompleteStatus($params);
    }

    /**
     * 描述：每月1号3点跑库存成本的期初数据
     * 作者：橙子
     */
    public function crontabQichu() {
        set_time_limit(0);
        $this->inventory->crontab_qichu();
    }

    /**
     * 自动计算平台Sku的销售状态
     */
    public function autoCalculateListingSaleStatus () {
        set_time_limit(0);
        $this->crontabService->autoCalculateListingSaleStatus();
    }

    /**
     * 从抓下来的库存数据转到临时表来形成报表
     */
    public function fbaStockMakeTable() {
        set_time_limit(0);
        $this->crontabService->fbaStockMakeTable();
    }

    /**
     * 亚马逊物流货件报告
     */
    public function getFulfilledShipmentsReportRequest(){
        set_time_limit(0);
        $_GET['sp'] = 1;
        $accountId = !empty($_GET['accountId']) ? $_GET['accountId'] : null;
        $options = array(
            'StartDate' => '2017-06-01 08:00:00',
            'EndDate' => '2017-07-01 08:00:00',
            'report_type' => 9
        );

        $this->reportService->getReport($options, $accountId);
    }

    /**
     * 亚马逊物流货件报告
     */
    public function getFulfilledShipmentsReportRequestOtz(){
        set_time_limit(0);
        $_GET['sp'] = 1;
        $accountId = 120;
        $options = array(
            'StartDate' => '2017-06-01 08:00:00',
            'EndDate' => '2017-07-01 08:00:00',
            'report_type' => 9
        );

        $this->reportService->getReport($options, $accountId);
    }

    /**
     * 亚马逊物流货件报告
     */
    public function getFulfilledShipmentsReportRequestTtn(){
        set_time_limit(0);
        $_GET['sp'] = 1;
        $accountId = 339;
        $options = array(
            'StartDate' => '2017-06-01 08:00:00',
            'EndDate' => '2017-07-01 08:00:00',
            'report_type' => 9
        );

        $this->reportService->getReport($options, $accountId);
    }

    /**
     * 亚马逊物流货件报告
     */
    public function getFulfilledShipmentsReportRequestTtf(){
        set_time_limit(0);
        $_GET['sp'] = 1;
        $accountId = 334;
        $options = array(
            'StartDate' => '2017-06-01 08:00:00',
            'EndDate' => '2017-07-01 08:00:00',
            'report_type' => 9
        );

        $this->reportService->getReport($options, $accountId);
    }

    /**
     * 亚马逊物流货件报告
     */
    public function getFulfilledShipmentsReportRequestTts(){
        set_time_limit(0);
        $_GET['sp'] = 1;
        $accountId = 327;
        $options = array(
            'StartDate' => '2017-06-01 08:00:00',
            'EndDate' => '2017-07-01 08:00:00',
            'report_type' => 9
        );

        $this->reportService->getReport($options, $accountId);
    }

    /**
     * 报告接口测试专用方法
     */
    public function getReportRequestCommon(){
        set_time_limit(0);
        $_GET['sp'] = 1;
        if(empty($_GET['AccountId'])) {
            echo 'AccountId缺失';
            exit;
        }
        if(empty($_GET['ReportType'])) {
            echo 'ReportType缺失';
            exit;
        }
        $accountId = intval($_GET['AccountId']);
        $options = array(
            'StartDate' => !empty($_GET['StartDate']) ? $_GET['StartDate'] : date('Y-m-d', strtotime('-30 day')),
            'EndDate' => !empty($_GET['EndDate']) ? $_GET['EndDate'] : date('Y-m-d'),
            'report_type' => intval($_GET['ReportType'])
        );

        $this->reportService->getReport($options, $accountId);
    }
}