<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/2/27
 * Time: 17:47
 * 对外系统服务端
 */
namespace Api\Controller\Amazon;

class ServerController
{
    /**
     * 亚马逊客服系统提供ASIN
     */
    public function supportAccountSellerSku() {
        $re = $options = array();
        !empty($_POST['startTime']) && $options['startTime'] = $_POST['startTime'];
        !empty($_POST['endTime']) && $options['endTime'] = $_POST['endTime'];
        isset($_POST['onSale']) && $options['onSale'] = intval($options['onSale']);

        $re['code'] = 0;
        $re['message'] = 'Connect OK';
        $re['data'] = D('Amazon\Server', 'Service')->getAccountSellerSku($options);

        echo json_encode($re);
        exit;
    }

    /**
     * 根据SellerSku获取sku
     */
    public function postSkuSellerSku() {
        $re = $options = array();
        if(empty($_POST['skuType']) ||
            !in_array($_POST['skuType'], array('PRIVATE_SKU', 'SELLER_SKU')) ||
            empty($_POST['skuParam'])
        ) {
            $re['code'] = 801;
            $re['message'] = '参数缺失';
        } else {
            $re['code'] = 0;
            $re['message'] = 'Connect OK';
            $re['data'] = D('Amazon\Server', 'Service')->getSkuSellerSku($_POST['skuParam'], $_POST['skuType']);
        }

        echo json_encode($re);
        exit;
    }

    /**
     * 提供账号站点数据
     */
    public function postAccountSiteData() {
        $re = $options = array();

        $re['code'] = 0;
        $re['message'] = 'Connect OK';
        $re['data'] = D('Amazon\Server', 'Service')->getAccountSiteData();

        echo json_encode($re);
        exit;
    }

    /**
     * @return string
     * 给小鹏测试用的
     */
    public function postSkuSellerSkuTest() {
        $data['skuType'] = 'PRIVATE_SKU';
        $data['skuParam'] = array('AM51300', 'AM100302');
        $handle = curl_init();
        $url = 'http://dev.fbawarehouse.youkeshu.com/index.php/Api/Amazon/Server/postSkuSellerSku';
        curl_setopt($handle,CURLOPT_URL,$url);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($handle,CURLOPT_USERAGENT,'Fbawarehouse API/1.0');
        curl_setopt($handle,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($handle,CURLOPT_TIMEOUT,90);
        curl_setopt($handle,CURLOPT_MAXREDIRS,3);
        curl_setopt($handle,CURLOPT_POST,TRUE);
        curl_setopt($handle,CURLOPT_POSTFIELDS,http_build_query($data));
        curl_setopt($handle,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_0);
        curl_setopt($handle,CURLOPT_HEADER,FALSE);    //不返回头信息

        $response = curl_exec($handle);

        if(curl_errno($handle)) {
            $re          = new \stdClass();
            $re->code    = 100;
            $re->message = '连接主机'.$url.'时发生错误: '.curl_error($handle)."<br/>";
            curl_close($handle);
            return json_encode($re);
        }

        curl_close($handle);
        print_r(json_decode($response, true));
        exit;
    }
}