<?php
namespace Api\Controller\Amazon;
use \Home\Controller\CommonController;

class InventorySaleController extends CommonController{

    public $reportStockSaleService = NULL;
    public $accountsService         = NULL;

    public function __construct(){
        $this->reportStockSaleService = D('Amazon\ReportStockSale','Service');
        $this->accountsService         = D('Amazon\Accounts','Service');
        parent::__construct();
    }

    /**
     * 描述：首页展示
     */
    public function index(){

        //$condition = $_GET;
        $accounts = $this->accountsService->getAccounts();

        //$reportStockSaleData = $this->reportStockSaleService($condition);
        $this->assign('accounts',$accounts);

        $this->display();
    }

    /**
     * 描述：获取报告
     */
    public function getReport(){

        $reportStockSaleData = $this->reportStockSaleService->selectData($_POST);

        echo json_encode($reportStockSaleData);
        exit;

    }

    /**
     * 描述：下载报告
     */
    public function exportReport(){
        $options = $_POST;
        $options['download'] = 1;

        $this->reportStockSaleService->downloadData($options);
    }
}