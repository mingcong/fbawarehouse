<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/7
 * Time: 21:49
 */
namespace Api\Model\Amazon;

class SitesModel extends  CommonModel {
    //数据库配置
    protected $connection       =   'DB_FBAERP';
    // 实际数据表名（包含表前缀）
    protected $trueTableName    =   'amazonorder_sites';

    /**
     * @return bool|mixed
     * 站点简码数组
     */
    public function getSiteCodes($siteId = 0) {
        $siteCodesCache = S('SiteCodes');

        if($siteId && !isset($siteCodesCache[$siteId]))
            $siteCodesCache = array();

        if(!$siteCodesCache or !is_array($siteCodesCache)){
            $sites = $this->where('is_used = 1')->select();
            foreach ($sites as $row) {
                $siteCodesCache[$row['id']] = $row['shorthand_code'];
            }
            S('SiteCodes', $siteCodesCache, 86400);
        }

        return $siteId == 0 ? $siteCodesCache : (isset($siteCodesCache[$siteId]) ? $siteCodesCache[$siteId] : '');
    }

    /**
     * @return bool|mixed
     * 站点数组
     */
    public function getSites($siteId = 0) {
        $sitesCache = S('Sites');

        if($siteId && !isset($sitesCache[$siteId]))
            $sitesCache = array();

        if(!$sitesCache or !is_array($sitesCache)){
            $sites = $this->where('is_used = 1')->select();
            foreach ($sites as $row) {
                $sitesCache[$row['id']] = $row['shorthand_code'] . '-' . $row['country_name'];
            }
            S('Sites', $sitesCache, 86400);
        }

        return $siteId == 0 ? $sitesCache : (isset($sitesCache[$siteId]) ? $sitesCache[$siteId] : '');
    }

    /**
     * @param string $saleChannel
     * @return array|bool|mixed
     * （根据后缀）获取销量表后缀
     */
    public function getSaleChannel ($saleChannel = '') {
        $sitesSaleChannelCache = S('SitesSaleChannel');

        if($saleChannel && !isset($sitesSaleChannelCache[$saleChannel]))
            $sitesSaleChannelCache = array();

        if(!$sitesSaleChannelCache or !is_array($sitesSaleChannelCache)){
            $sites = $this->where('sale_channel != \'\'')->select();
            foreach ($sites as $row) {
                $sitesSaleChannelCache[$row['sale_channel']] = $row['id'];
            }
            S('SitesSaleChannel', $sitesSaleChannelCache, 86400);
        }

        return $saleChannel === '' ?
            $sitesSaleChannelCache :
            (
                isset($sitesSaleChannelCache[$saleChannel]) ? $sitesSaleChannelCache[$saleChannel] : false
            );
    }

    /**
     * @param string $country
     * @return array|bool|mixed
     * 描述：通过国家简称获取 siteId
     */
    public function getShortHandCode($country = ''){
        $siteShortHandCodeCache = S('siteShortHandCode');

        if($country && !isset($siteShortHandCodeCache[$country]))
            $siteShortHandCodeCache = array();

        if(!$siteShortHandCodeCache or !is_array($siteShortHandCodeCache)){
            $sites = $this->select();
            foreach ($sites  as $row) {
                $siteShortHandCodeCache[$row['shorthand_code']] =$row['id'];
            }
            S('siteShortHandCode', $siteShortHandCodeCache, 86400);
        }

        return $country === '' ?
            $siteShortHandCodeCache :
            (
                isset($siteShortHandCodeCache[$country]) ? $siteShortHandCodeCache[$country] : false
            );
    }
}