<?php
namespace Api\Model\Amazon;

class ReportReceiptsModel extends CommonModel{
    // 数据表前缀
    protected $tablePrefix   = 'api_';
    // 数据库配置
    protected $connection    = 'fbawarehouse';
    // 数据表名（不包含表前缀）
    protected $tableName     = 'report_receipts';

    protected $trueTableName = 'api_report_receipts';

    /**
     * @param $rowLists
     * @param $tableName
     * 描述：插入解析数据
     */
    public function insertData($tableName,$rowLists){

        $sql = 'SELECT `create_time` FROM `' . $tableName . '` ORDER BY `id` DESC LIMIT 1';
        $maxCreateTime = $this->query($sql);
        $maxCreateTime = !empty($maxCreateTime) ? $maxCreateTime[0]['create_time'] : '';
        if(!empty($maxCreateTime) && ((time() - strtotime($maxCreateTime)) > 86400 * 10)) {
            $sql = 'TRUNCATE TABLE  `' . $tableName . '`';
            $this->query($sql);
        }

        if(!empty($rowLists)) {
            $rowListsChunks = array_chunk($rowLists, 500);
            foreach($rowListsChunks as $rowListsChunk) {
                $this->addAll($rowListsChunk);
            }
        }
    }

    /**
     * @param array $options
     * @return bool|mixed
     * 描述：根据条件查询数据
     */
    public function selectData($options = array()){
        $sql = "SELECT `arr`.`account_id`,`ac`.`name`,`arr`.`sku`,`aass`.`private_sku`,
                `as`.`shorthand_code` AS `site`,min(`arr`.`received_date`) AS `date`
                FROM `api_report_receipts` AS `arr`
                LEFT JOIN `amazonorder_accounts` AS `ac` ON `arr`.`account_id` = `ac`.`id`
                LEFT JOIN `api_account_seller_sku` AS `aass` ON `arr`.`sku` = `aass`.`seller_sku` AND `aass`.`account_id` = `arr`.`account_id`
                LEFT JOIN `amazonorder_account_troop_site` AS `aats` ON `aats`.`account_id` = `arr`.`account_id`
                LEFT JOIN `amazonorder_sites` AS `as` ON `as`.`id` = `aats`.`site_id`
                WHERE `arr`.`account_id` != 0  ";

        $getCountSql = "SELECT * FROM `api_report_receipts` AS `arr` 
                        WHERE `arr`.`account_id` != 0  ";
        $condition = '';

        (isset($options['accountId']) && $options['accountId']) && $condition .= " AND `arr`.`account_id` = " . $options['accountId'];
        (isset($options['sku']) && $options['sku']) && $condition .= " AND `arr`.`sku` = '" . $options['sku'] . "'";

        $countArr = $this->query($getCountSql.$condition . " GROUP BY `arr`.`account_id`,`arr`.`sku`");
        $count = count($countArr);

        $page =$options['page'];
        $pageSize =20;

        $limit = $pageSize;
        $offset = ($page-1)*$pageSize;

        $total = $count ? $count : 0;
        $totalPage = ceil($total/$pageSize);

        $condition .= " GROUP BY `arr`.`account_id`,`arr`.`sku`";

        /*下载和页面展示的区分*/
        if(!(isset($options['download']) && $options['download'] == 1)){
            $condition .= ' LIMIT ' . $offset . ',' . $limit;
        }

        $reportDataLists = $this->query($sql.$condition);

        $reportReceiptsModel = array();
        $reportReceiptsModel['reportDataLists']       = $reportDataLists;

        $reportReceiptsModel['total']     = $total;
        $reportReceiptsModel['totalPage'] = $totalPage;
        $reportReceiptsModel['page']      = $page;
        $reportReceiptsModel['pageSize']  = $pageSize;

        return $reportReceiptsModel;
    }
}