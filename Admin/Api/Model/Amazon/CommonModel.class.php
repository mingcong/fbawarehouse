<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/3
 * Time: 17:31
 */
namespace Api\Model\Amazon;
use Think\Model;

class CommonModel extends Model {
    /**
     * @param $goal
     * @param string $defFolder
     * 根据自定义的文件夹引入第三方接口文件
     */
    public function vendorApi ($goal, $defFolder = 'AmazonApi/') {
        return vendor($goal, VENDOR_PATH . $defFolder);
    }

    public static function trimArray($arr) {
        if (!is_array($arr)) {
            return $arr;
        }
        try {
            while (list($key, $value) = each($arr)) {
                if (is_array($value)) {
                    $arr[$key] = self::trimArray($value);
                } else {
                    $arr[$key] = trim($value);
                }
            }
        } catch (Exception $e){
            echo $e->getMessage();
        }
        return $arr;
    }

    public static function &slashesDeep(&$data, $func = 'addslashes') {
        $waitList = array(&$data);                                                                                      //待处理列表

        do {
            $wk = key($waitList);
            $wv = &$waitList[$wk];
            unset($waitList[$wk]);

            if( is_array($wv) ) {
                $result = array();                                                                                      //结果列表
                foreach($wv as $k => &$v) {
                    $result[$func($k)] = &$v;
                    $waitList[] = &$v;
                }
                $wv = $result;
            } else if( is_string($wv) ) {
                $wv = $func($wv);
            }
        } while( !empty($waitList) );

        return $data;
    }

    /**
     * @param array $address
     * @return array
     * 格式化地址
     */
    public function formatAddress (&$address = array()) {
        if(empty($address))return array();

        if(!isset($address['Name'])) {
            $ad = array();
            foreach ($address as $rowId=>$row) {
                if(!empty($row['amazon_address_dec'])) {
                    $ad[$rowId] = $row['amazon_address_dec'];
                    continue;
                }
                $ad[$rowId] = '';
                $ad[$rowId] .= isset($row['Name']) && !empty($row['Name']) ? $row['Name'] . "<br/>" : '';
                $ad[$rowId] .= isset($row['AddressLine1']) && !empty($row['AddressLine1']) ? $row['AddressLine1'] . "<br/>" : '';
                $ad[$rowId] .= isset($row['AddressLine2']) && !empty($row['AddressLine2']) ? $row['AddressLine2'] . "<br/>" : '';
                $ad[$rowId] .= isset($row['Province']) && !empty($row['Province']) ? $row['Province'] . "," : '';
                $ad[$rowId] .= isset($row['StateOrProvinceCode']) && !empty($row['StateOrProvinceCode']) ? $row['StateOrProvinceCode'] . "," : '';
                $ad[$rowId] .= isset($row['CountryCode']) && !empty($row['CountryCode']) ? $row['CountryCode'] : '';
                $ad[$rowId] .= isset($row['PostalCode']) && !empty($row['PostalCode']) ? $row['PostalCode'] : '';
            }

            return $ad;
        } else {
            if(!empty($address['amazon_address_dec'])) {
                $ad = $address['amazon_address_dec'];
            } else {
                $ad = isset($address['Name']) && !empty($address['Name']) ? $address['Name'] . "<br/>" : '';
                $ad .= isset($address['AddressLine1']) && !empty($address['AddressLine1']) ? $address['AddressLine1'] . "<br/>" : '';
                $ad .= isset($address['AddressLine2']) && !empty($address['AddressLine2']) ? $address['AddressLine2'] . "<br/>" : '';
                $ad .= isset($address['Province']) && !empty($address['Province']) ? $address['Province'] . "," : '';
                $ad .= isset($address['StateOrProvinceCode']) && !empty($address['StateOrProvinceCode']) ? $address['StateOrProvinceCode'] . "," : '';
                $ad .= isset($address['CountryCode']) && !empty($address['CountryCode']) ? $address['CountryCode'] : '';
                $ad .= isset($address['PostalCode']) && !empty($address['PostalCode']) ? $address['PostalCode'] : '';
            }
        }

        $address['addressStr'] = $ad;

        return $address;
    }
}