<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/3
 * Time: 16:57
 * 接口日志模型
 */
namespace Api\Model\Amazon;

class LogModel extends CommonModel {
    //数据库配置
    protected $connection       =   'DB_FBAERP';
    // 实际数据表名（包含表前缀）
    protected $trueTableName    =   'log_amazon_api';

    /**
     * API调用日志
     */
    public function apiLog($apiName,$account,$options,$request,$response)
    {
        return $this->add(
            array(
                'account_id' => $account['id'],
                'account_name' => $account['name'],
                'api_name' => $apiName,
                'api_options' => json_encode($options),
                'api_request' => print_r($request,TRUE),
                'api_response' => print_r($response,TRUE),
                'api_error' => '',
                'create_time' => date('Y-m-d H:i:s')
            )
        );
    }

    /**
     * 调用API失败
     */
    public function apiErrorLog($apiName,$account,$options,$request,$ex){
        $error                           = array();
        $error['Message']                = $ex->getMessage();
        $error['StatusCode']             = $ex->getStatusCode();
        $error['ErrorCode']              = $ex->getErrorCode();
        $error['ErrorType']              = $ex->getErrorType();
        $error['RequestId']              = $ex->getRequestId();
        $error['XML']                    = $ex->getXML();
        $error['ResponseHeaderMetadata'] = $ex->getResponseHeaderMetadata();

        return $this->add(
            array(
                'account_id' => $account['id'],
                'account_name' => $account['name'],
                'api_name' => $apiName,
                'api_options' => json_encode($options),
                'api_request' => print_r($request,TRUE),
                'api_response' => '',
                'api_error' => json_encode($error),
                'create_time' => date('Y-m-d H:i:s')
            )
        );
    }
}