<?php
namespace Api\Model\Amazon;
use Think\Exception;
class ReportRemovalModel extends CommonModel{
    // 数据表前缀
    protected $tablePrefix   = 'api_';
    // 数据库配置
    protected $connection    = 'fbawarehouse';
    // 数据表名（不包含表前缀）
    protected $tableName     = 'report_removal';

    protected $trueTableName = 'api_report_removal';

    /**
     * @param $rowLists
     * @param $tableName
     * 描述：插入解析数据
     */
    public function insertData($tableName,$rowLists){

        /*$sql = 'SELECT `create_time` FROM `' . $tableName . '` ORDER BY `id` DESC LIMIT 1';
        $maxCreateTime = $this->query($sql);
        $maxCreateTime = !empty($maxCreateTime) ? $maxCreateTime[0]['create_time'] : '';
        if(!empty($maxCreateTime) && ((time() - strtotime($maxCreateTime)) > 86400 * 7)) {
            $sql = 'TRUNCATE TABLE  `' . $tableName . '`';
            $this->query($sql);
        }*/

        /*if(!empty($rowLists)) {
            $rowListsChunks = array_chunk($rowLists, 500);
            foreach($rowListsChunks as $rowListsChunk) {
                $this->addAll($rowListsChunk);
            }
        }*/
        if(!empty($rowLists)) {
            foreach($rowLists as $row) {
                try {
                    $this->add($row);
                } catch(Exception $ex) {
                    continue;
                }
            }
        }
    }

    /**
     * @param array $options
     * @return bool|mixed
     * 描述：根据条件查询数据
     */
    public function selectData($options = array()){
        $sql = "SELECT `arr`.`account_id`,`ac`.`name`,`arr`.`sku`,`aass`.`private_sku`,
                `as`.`shorthand_code` AS `site`,`arr`.`disposed_quantity`,`arr`.`shipped_quantity`,
                `arr`.`in_process_quantity`,`arr`.`request_date`,`arr`.`last_updated_date`
                FROM `api_report_removal` AS `arr`
                LEFT JOIN `amazonorder_accounts` AS `ac` ON `arr`.`account_id` = `ac`.`id`
                LEFT JOIN `api_account_seller_sku` AS `aass` ON `arr`.`sku` = `aass`.`seller_sku` AND `aass`.`account_id` = `arr`.`account_id`
                LEFT JOIN `amazonorder_account_troop_site` AS `aats` ON `aats`.`account_id` = `arr`.`account_id`
                LEFT JOIN `amazonorder_sites` AS `as` ON `as`.`id` = `aats`.`site_id`
                WHERE `arr`.`account_id` != 0  ";

        $getCountSql = "SELECT COUNT(0) AS `count` FROM `api_report_removal` AS `arr`
                        WHERE `arr`.`account_id` != 0 ";
        $condition = '';

        (isset($options['accountId']) && $options['accountId']) && $condition .= " AND `arr`.`account_id` = " . $options['accountId'];
        (isset($options['sku']) && $options['sku']) && $condition .= " AND `arr`.`sku` = '" . $options['sku'] . "'";
        (isset($options['removal_time_from'])) && $condition .= " AND `arr`.`request_date` >= '" . $options['removal_time_from'] . "'";
        (isset($options['removal_time_to'])) && $condition .= " AND `arr`.`request_date` <= '" . $options['removal_time_to'] . "'";

        $countArr = $this->query($getCountSql.$condition);
        $count = $countArr[0]['count'];

        $page =$options['page'];
        $pageSize =20;

        $limit = $pageSize;
        $offset = ($page-1)*$pageSize;

        $total = $count ? $count : 0;
        $totalPage = ceil($total/$pageSize);

        /*下载和页面展示的区分*/
        if(!(isset($options['download']) && $options['download'] == 1)){
            $condition .= ' LIMIT ' . $offset . ',' . $limit;
        }

        $reportDataLists = $this->query($sql.$condition);

        $reportRemovalModel = array();
        $reportRemovalModel['reportDataLists']       = $reportDataLists;

        $reportRemovalModel['total']     = $total;
        $reportRemovalModel['totalPage'] = $totalPage;
        $reportRemovalModel['page']      = $page;
        $reportRemovalModel['pageSize']  = $pageSize;

        return $reportRemovalModel;
    }
}