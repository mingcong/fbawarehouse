<?php
/**
 * 账号模型
 */
namespace Api\Model\Amazon;

class AccountsModel extends CommonModel
{
    // 数据表前缀
    protected $tablePrefix      =   'amazonorder_';
    //数据库配置
    protected $connection       =   'fbawarehouse';
    // 数据表名（不包含表前缀）
    protected $tableName        =   'accounts';
    protected $trueTableName    =   'amazonorder_accounts';

    /**
     * @return mixed|string|void
     * 获取数据库所有的账号列表
     */
    public function getAccounts() {
        $accountsCache = S('Accounts');
        if(!$accountsCache or !is_array($accountsCache)){
            $accounts = $this->select();
            foreach ($accounts as $account) {
                $accountsCache[$account['id']] = $account;
            }
            S('Accounts', $accountsCache, 7200);
        }

        return $accountsCache;
    }

    /**
     * @param $accountIds
     * @return array
     * 根据账号ID数组返回数组信息
     */
    public function getAccountsByIds ($accountIds) {
        $accountsInfo = array();
        if(is_array($accountIds) && !empty($accountIds)) {
            foreach ($accountIds as $accountId) {
                $accountsInfo[$accountId] = $this->getAccountById($accountId);
            }
        }

        return $accountsInfo;
    }

    /**
     * @param $accountId
     * @return bool
     * 根据账号ID获取账号一维数组
     */
    public function getAccountById( $accountId ) {
        $accounts = $this->getAccounts();
        if(!isset($accounts[$accountId])) {
            $accountsCache = array();
            $accounts = $this->select();
            foreach ($accounts as $account) {
                $accountsCache[$account['id']] = $account;
            }
            S('Accounts', $accountsCache, 7200);
        }
        return isset($accounts[$accountId]) ? $accounts[$accountId] : FALSE;
    }
}