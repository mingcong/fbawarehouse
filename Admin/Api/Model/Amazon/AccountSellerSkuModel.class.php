<?php
namespace Api\Model\Amazon;

class AccountSellerSkuModel extends CommonModel {
    protected $connection       =   'DB_FBAERP';
    protected $trueTableName        =   'api_account_seller_sku';

    /**
     * @param $accountId
     * @param array $sellerSku
     * @return array
     * 根据账号站点ID（或指定部分SellerSku）返回SellerSku对应的公司SKU
     */
    public function getActualSkuByAccountIdAndSellerSku($accountId, $sellerSku = array()) {
        $options = array();
        $options['account_id'] = $accountId;
        $options['is_used'] = 1;
        !empty($sellerSku) && $options['seller_sku'] = array('IN', self::slashesDeep($sellerSku));
        return $this->field('private_sku, seller_sku')
            ->where($options)
            ->getField('seller_sku, private_sku', NULL);
    }

    public function insertData($accountSellerSkuData){
        if(empty($accountSellerSkuData)) return false;

        foreach ($accountSellerSkuData as $data){
            $skuData = $this->where(array(
                'seller_sku' => $data['seller_sku'],
                'account_id' => $data['account_id']
            ))->select();

            if($skuData){
                $this->where(array(
                    'seller_sku' => $data['seller_sku'],
                    'account_id' => $data['account_id']
                ))->field('title')->save(array('title' => $data['title']));
            }else{
                $this->add($data);
            }
        }
        /*$sql = 'INSERT INTO `api_account_seller_sku`(`account_id`, `site_id`,`seller_sku`,`fnsku`, `asin`, `title`) VALUES';

        foreach ($accountSellerSkuData as $data){
            $sql .= '(\''. join('\',\'',$data) . '\'),';
        }
        $sql = rtrim($sql,',');
        $sql .= 'ON DUPLICATE KEY UPDATE `title`=VALUES(`title`)';

        $this->query($sql);*/
    }
    public function selectSku($date, $columnStr = ''){
        $columnStr == '' || $this->field($columnStr);
        return $this->where($date)->select();
    }

    /**
     * @param $accountId
     * @param array $sellerSku
     * @return mixed
     * 通过账号或者sellersku找到ASIN
     */
    public function getAsinBySellerSku($accountId, $sellerSku = array()) {
        $options = array();
        $options['account_id'] = $accountId;
        !empty($sellerSku) && $options['seller_sku'] = array('IN', $sellerSku);
        return $this->where($options)->getField('seller_sku, asin');
    }

    /**
     * @param array $options
     * @return mixed
     * 给客服系统提供的数据支撑
     */
    public function getAccountSellerSku ($options = array()) {
        if(!empty($options)) {
            $sqlOptions = array();
            foreach ($options as $k=>$op) {
                $sqlOptions[$this->trueTableName . '.' .$k] = $op;
            }
        }

        $this->table($this->trueTableName)
            ->field('name, shorthand_code, seller_sku, fnsku, private_sku, asin, ' . $this->trueTableName . '.is_used')
            ->join(' LEFT JOIN amazonorder_sites ON amazonorder_sites.id = ' .
                $this->trueTableName . '.site_id')
            ->join('LEFT JOIN amazonorder_accounts ON amazonorder_accounts.id = ' .
                $this->trueTableName . '.account_id');
        !empty($sqlOptions) && $this->where($sqlOptions);

        return $this->select();
    }

    public function getAllSku() {
        $sql = 'SELECT
                  b.name,
                  c.shorthand_code,
                  a.`seller_sku`,
                  a.`private_sku`,
                  a.`asin`,
                  a.`fnsku`,
                  a.`first_received_date`,
                  st.value,
                  IF(a.`is_used` = 1, "是", "否") AS "is_used"
                FROM
                  `api_account_seller_sku` AS a
                LEFT JOIN
                  amazonorder_accounts AS b
                ON
                  a.account_id = b.id
                LEFT JOIN
                  amazonorder_sites AS c
                ON
                  a.site_id = c.id
                LEFT JOIN
                  statusdics AS st
                ON
                  st.number = a.`sale_status_id` AND st.colum_name = "sale_status_id"
                WHERE
                  1';
        return $this->query($sql);
    }
}
