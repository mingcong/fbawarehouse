<?php
namespace Api\Model\Amazon;
use Think\Exception;
class ReportCurrentInventoryModel extends CommonModel{
    // 数据表前缀
    protected $tablePrefix   = 'api_';
    // 数据库配置
    protected $connection    = 'fbawarehouse';
    // 数据表名（不包含表前缀）
    protected $tableName     = 'report_current_inventory';

    protected $trueTableName = 'api_report_current_inventory';

    /**
     * @param $rowLists
     * @param $tableName
     * 描述：插入解析数据
     */
    public function insertData($tableName,$rowLists){
        /*if(!empty($rowLists)) {
            $rowListsChunks = array_chunk($rowLists, 500);
            foreach($rowListsChunks as $rowListsChunk) {
                $this->addAll($rowListsChunk);
            }
        }*/
        if(!empty($rowLists)) {
            foreach($rowLists as $row) {
                try {
                    $this->add($row);
                } catch(Exception $ex) {
                    continue;
                }
            }
            return TRUE;
        }

    }

    /**
     * @param array $options
     * @return bool|mixed
     * 描述：根据条件查询数据
     */
    public function selectData($options = array()){
        $data = $this->where($options)->select();

        return isset($data) ? $data : false;
    }

}