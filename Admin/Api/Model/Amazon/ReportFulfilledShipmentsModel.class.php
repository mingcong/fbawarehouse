<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/9/8
 * Time: 16:39
 */
namespace Api\Model\Amazon;
use Think\Exception;

class ReportFulfilledShipmentsModel extends CommonModel{
    // 数据表前缀
    protected $tablePrefix   = 'api_';
    // 数据库配置
    protected $connection    = 'fbawarehouse';
    // 数据表名（不包含表前缀）
    protected $tableName     = 'report_fulfilled_shipments';

    protected $trueTableName = 'api_report_fulfilled_shipments';
}