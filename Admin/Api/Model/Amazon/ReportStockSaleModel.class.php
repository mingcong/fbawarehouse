<?php
namespace Api\Model\Amazon;

class ReportStockSaleModel extends CommonModel{
    // 数据表前缀
    protected $tablePrefix   = 'fba_';
    // 数据库配置
    protected $connection    = 'DB_FBAERP';
    // 数据表名（不包含表前缀）
    protected $tableName     = 'report_stock_sale';

    protected $trueTableName = 'fba_report_stock_sale';

    /**
     * @param $aveSaleLists
     * @param $dayShuttle
     * 描述：插入销量数据
     */
    public function insertSaleData($aveSaleLists,$dayShuttle){
        foreach ($aveSaleLists as $aveSaleList) {
            $sql = 'INSERT INTO `fba_report_stock_sale` (`account_id`, `sku`, `' . $dayShuttle . 'daysale`) VALUES';
            $sql .= '(\'' . join('\', \'', $aveSaleList) . '\')';
            $sql .= ' ON DUPLICATE KEY UPDATE `' . $dayShuttle . 'daysale` = ' . $aveSaleList[$dayShuttle . 'daysale'];

            $this->query($sql);
        }
    }

    /**
     * @param $stockLists
     * 描述：插入库存数据
     */
    public function insertInventoryData($stockLists){
        foreach($stockLists as $stock) {
            $sql = 'INSERT INTO `fba_report_stock_sale` (`account_id`, `sku`, `stock`) VALUES ';
            $sql .= '(\'' . join('\', \'', $stock) . '\')';
            $sql .= ' ON DUPLICATE KEY UPDATE `stock` = ' . $stock['stock'];

            $this->query($sql);

        }
    }

    /**
     * @param $shippingList
     * 描述：插入在途库存数据
     */
    public function insertShippingData($shippingList){
        foreach($shippingList as $shipping) {
            $sql = 'INSERT INTO `fba_report_stock_sale` (`account_id`, `sku`, `shipping`) VALUES ';
            $sql .= '(\'' . join('\', \'', $shipping) . '\')';
            $sql .= ' ON DUPLICATE KEY UPDATE `shipping` = ' . $shipping['shipping'];

            $this->query($sql);

        }
    }

    /**
     * @param $options
     * @return bool|mixed
     * 描述：查询数据
     */
    public function selectData($options){

        $sql = "SELECT `rss`.`account_id`,`ac`.`name`,`rss`.`sku`,`cn`.`name` AS `skucnname`,`rss`.`7daysale` AS `daysale`,`rss`.`stock`,`rss`.`shipping`
                FROM `fba_report_stock_sale` AS `rss`
                LEFT JOIN `amazonorder_accounts` AS `ac` ON `rss`.`account_id` = `ac`.`id`
                LEFT JOIN `skusystem_sku_cnname` AS `sc` on `rss`.`sku` = `sc`.`sku`
                LEFT JOIN `skusystem_cnname` AS `cn` ON `sc`.`attr_id` = `cn`.`id` 
                WHERE `rss`.`account_id` != 0  AND `rss`.`sku` <> ''";

        $getCountSql = "SELECT count(*) AS count FROM `fba_report_stock_sale` AS `rss` WHERE `rss`.`account_id` != 0  AND `rss`.`sku` <> '' ";
        $condition = '';

        (isset($options['accountId']) && $options['accountId']) && $condition .= " AND `rss`.`account_id` = " . $options['accountId'];
        (isset($options['sku']) && $options['sku']) && $condition .= " AND `rss`.`sku` = '" . $options['sku'] . "'";

        $countArr = $this->query($getCountSql.$condition);

        if(isset($options['order']) && $options['order']){
            $orderArr = explode(':',$options['order']);
            $condition .= " ORDER BY $orderArr[0] $orderArr[1]";
        }

        $page =$options['page'];
        $pageSize =20;

        $limit = $pageSize;
        $offset = ($page-1)*$pageSize;

        $total = $countArr[0]['count'] ? $countArr[0]['count'] : 0;
        $totalPage = ceil($total/$pageSize);

        /*下载和页面展示的区分*/
        if(!(isset($options['download']) && $options['download'] == 1)){
            $condition .= ' LIMIT ' . $offset . ',' . $limit;
        }

        $reportDataLists = $this->query($sql.$condition);

        $reportStockSaleModel = array();
        $reportStockSaleModel['reportDataLists']       = $reportDataLists;

        $reportStockSaleModel['total']     = $total;
        $reportStockSaleModel['totalPage'] = $totalPage;
        $reportStockSaleModel['page']      = $page;
        $reportStockSaleModel['pageSize']  = $pageSize;

        return $reportStockSaleModel;
    }

}