<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/10
 * Time: 16:03
 */
namespace Api\Model\Amazon;

class ShipToAddressModel extends CommonModel {
    // 数据库配置
    protected $connection = 'DB_FBAERP';
    protected $trueTableName = 'api_ship_to_address';

    /**
     * @param $siteId
     * @param $options
     * @return bool|mixed
     * 目的地址录入
     */
    public function getOrInsertShipToAddress ($siteId, $options) {
        if(empty($siteId))return false;

        $options = self::trimArray($options);

        $result = $this->table($this->trueTableName)->where(
            array(
                'site_id' => $siteId,
                'CenterId' => $options['CenterId'],
            )
        )->getField('id');
        if($result){
            D('Api/Amazon/Log','Model')->apiLog('getOrInsertShipToAddressModel',$result,$options,$siteId?$siteId:'空',time());
            return $result;
        }else{
            $options['site_id'] = $siteId;
            $sql = 'INSERT INTO `api_ship_to_address` ';
            $sql .= '(`' . join('`, `', array_keys($options)) . '`)';
            $sql .= 'VALUES';
            $sql .= '(\'' . join('\', \'', $options) .'\') ';
//            $sql .= 'ON DUPLICATE KEY UPDATE ';
//            foreach ($options as $key=>$val) {
//                $sql .= '`' . $key . '` = \'' . $val .'\', ';
//            }
//            $this->query(rtrim($sql, ','));
            $this->query($sql);

            return $this->where(
                array(
                    'site_id' => $siteId,
                    'CenterId' => $options['CenterId'],
                )
            )->getField('id');
        }
    }

    /**
     * @param int $siteId
     * @return array|int|mixed
     * 根据站点获取该站点下的所有仓库地址数组
     */
    public function getShipToAddress($siteId = 0) {
        $shipToAddressCache = S('ShipToAddressCache');

        if($siteId && !isset($shipToAddressCache[$siteId]))
            $shipToAddressCache = array();

        if(!$shipToAddressCache or !is_array($shipToAddressCache)){
            $shipToAddress = $this->where('site_id != 0')->select();
            foreach ($shipToAddress as $row) {
                $shipToAddressCache[$row['site_id']][$row['id']] = $row;
            }

            S('ShipToAddressCache', $shipToAddressCache, 3600);
        }
        return $siteId == 0 ?
            $shipToAddressCache :
            (
                $shipToAddressCache[$siteId] ? $shipToAddressCache[$siteId] : array()
            );
    }

    /**
     * @param $shipToAddressId
     * @return array|int|mixed
     * 根据ShipToAddressId获取该目的仓地址数组
     */
    public function getShipToAddressById ($shipToAddressId = 0) {
        $idShipToAddressCache = S('IdShipToAddressCache');

        if($shipToAddressId && !isset($idShipToAddressCache[$shipToAddressId]))
            $idShipToAddressCache = array();

        if(!$idShipToAddressCache or !is_array($idShipToAddressCache)){
            $shipToAddress = $this->where('site_id != 0')->select();
            foreach ($shipToAddress as $row) {
                $idShipToAddressCache[$row['id']] = $row;
            }

            S('IdShipToAddressCache', $idShipToAddressCache, 3600);
        }

        return $shipToAddressId == 0 ?
            $idShipToAddressCache :
            (
            $idShipToAddressCache[$shipToAddressId] ? $idShipToAddressCache[$shipToAddressId] : array()
            );
    }

    /**
     * @param $siteId
     * @param $centerId
     * @return array
     * 根据站点和仓库唯一标示判断是否存在
     */
    public function checkSiteCenterId ($siteId, $centerId) {
        $shipToAddress = $this->where("site_id = " . intval($siteId) . " AND CenterId = '" . $centerId . "'")->select();

        return !empty($shipToAddress) ? $shipToAddress[0] : array();
    }
}