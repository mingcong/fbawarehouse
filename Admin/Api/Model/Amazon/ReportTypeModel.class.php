<?php
namespace Api\Model\Amazon;

class ReportTypeModel extends CommonModel {
    // 数据表前缀
    protected $tablePrefix      =   'api_';
    // 数据库配置
    protected $connection       =   'fbawarehouse';
    // 数据表名（不包含表前缀）
    protected $tableName        =   'report_type';

    protected $trueTableName    =   'api_report_type';

    public function selectReportType($options = array()) {
        $reportType = $this->where($options)->select();
        return $reportType;
    }

    /**
     * @return mixed|string|void
     * 获取数据库所有的报告类型列表
     */
    public function getReportType() {
        $reportTypeCache = S('ReportType');
        if(!$reportTypeCache or !is_array($reportTypeCache)){
            $reportTypes = $this->select();
            foreach ($reportTypes as $reportType) {
                $reportTypeCache[$reportType['id']] = $reportType;
            }
            S('ReportType', $reportTypeCache, 3600);
        }

        return $reportTypeCache;
    }


    /**
     * @param $accountId
     * @return bool
     * 根据 $reportType 获取报告类型的一维数组
     */
    public function getReportTypeByID($reportTypeId) {
        $reportTypes = $this->getReportType();
        return isset($reportTypes[$reportTypeId]) ? $reportTypes[$reportTypeId] : FALSE;
    }
}