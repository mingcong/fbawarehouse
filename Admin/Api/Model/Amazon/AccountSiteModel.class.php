<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/7
 * Time: 21:55
 */
namespace Api\Model\Amazon;

class AccountSiteModel extends CommonModel
{
    //数据库配置
    protected $connection       =   'DB_FBAERP';
    protected $trueTableName = 'amazonorder_account_troop_site';

    /**
     * @param int $shipFromId
     * @return array|mixed
     * 根据始发地址ID获取始发仓数组
     */
    public function getShipFromAddress ($shipFromId = 0) {
        $idShipFromAddressCache = S('IdShipFromAddressCache');
        if(!$idShipFromAddressCache or !is_array($idShipFromAddressCache)){
            $shipFromAddress = M('ship_from_address', 'amazonorder_', 'DB_FBAERP')->select();
            foreach ($shipFromAddress as $row) {
                $idShipFromAddressCache[$row['id']] = $row;
            }

            S('IdShipFromAddressCache', $idShipFromAddressCache, 3600);
        }

        return $shipFromId == 0 ?
            $idShipFromAddressCache :
            (
            $idShipFromAddressCache[$shipFromId] ? $idShipFromAddressCache[$shipFromId] : array()
            );
    }

    /**
     * @param $accountId
     * @return mixed
     * 根据账号ID获取站点ID
     */
    public function getSiteIdByAccountId($accountId) {
        $result = $this->accountIdSiteIdCache();
        return isset($result[$accountId]) ? $result[$accountId] : 0;
    }

    /**
     * @return mixed
     * 站点ID账号ID对应关系缓存
     */
    public function accountIdSiteIdCache() {
        $accountIdSiteIdCache = S('AccountIdSiteIdCache');
        if(!$accountIdSiteIdCache or !is_array($accountIdSiteIdCache)){
            S('AccountIdSiteIdCache', $accountIdSiteIdCache = $this->getField('account_id, site_id'), 3600);
        }

        return $accountIdSiteIdCache;
    }

    /**
     * @param $accountId
     * @return mixed
     * 根据账号ID返回该账号下兄弟账号ID和saleChannel对应关系数组
     */
    public function getSaleChannelAccountIds($accountId) {
        return $this->table($this->trueTableName)
            ->join(array('amazonorder_sites ON amazonorder_sites.id = ' .
                $this->trueTableName . '.site_id'), 'LEFT')
            ->where($this->trueTableName . '.account_troop_id = ' . $this->getAccountTroop(intval($accountId)))
            ->getField('sale_channel, account_id');
    }

    /**
     * @param $accountId
     * @return mixed
     * 根据账号ID返回该账号下兄弟账号ID和站点简称对应关系数组
     */
    public function getShorthandCodeAccountIds($accountId) {
        return array_merge(
            array('UK'=>106),
            $this->table($this->trueTableName)
            ->join(array('amazonorder_sites ON amazonorder_sites.id = ' .
                $this->trueTableName . '.site_id'), 'LEFT')
            ->where($this->trueTableName . '.account_troop_id = ' . $this->getAccountTroop(intval($accountId)))
            ->getField('shorthand_code, account_id')
        );
    }

    /**
     * @return mixed|string|void
     * 根据accountID获取始发地址ID
     */
    public function getShipFromIdAddressByAccountId($accountId = 0){
        $AccountsShipFromAddressCache = S('AccountsShipFromAddress');
        if(!$AccountsShipFromAddressCache or !is_array($AccountsShipFromAddressCache)){
            $shipFromAddressId = $this->field('account_id, ship_from_address_id')->select();
            foreach ($shipFromAddressId as $row) {
                $AccountsShipFromAddressCache[$row['account_id']] = $row['ship_from_address_id'];
            }
            S('AccountsShipFromAddress', $AccountsShipFromAddressCache, 3600);
        }

        return $accountId == 0 ?
            $AccountsShipFromAddressCache :
            (
                $AccountsShipFromAddressCache[$accountId] ? $AccountsShipFromAddressCache[$accountId] : 0
            );
    }

    /**
     * @param $accountId
     * @return array
     * 根据账号ID返回始发地址数组
     */
    public function getAccountShipFromAddress($accountId){
        $address = $this->table($this->trueTableName)
            ->join(array('amazonorder_ship_from_address ON amazonorder_ship_from_address.id = ' .
                $this->trueTableName . '.ship_from_address_id'), 'LEFT')
            ->where($this->trueTableName . '.account_id = ' . intval($accountId))
            ->select();
        return !empty($address) ? $address[0] : array();
    }
    /**
     * @param $accountId
     * @return array
     * 根据账号ID返回始发地址Id
     */
    public function getAccountShipFromAddressId($accountId){
        $address = $this->table($this->trueTableName)
            ->join(array('amazonorder_ship_from_address ON amazonorder_ship_from_address.id = ' .
                         $this->trueTableName . '.ship_from_address_id'), 'LEFT')
            ->where($this->trueTableName . '.account_id = ' . intval($accountId))
            ->getField('id');
        return !empty($address) ? $address : null;
    }

    /**
     * @param $accountId
     * @param $siteId
     * @return int
     * 根据一个accountID和一个销售报告后缀，能够返回该账号的兄弟账号ID
     */
    public function getAccountIdBySaleChannel ($accountId, $saleChannel) {
        return $this->getAccountIdBySiteId($accountId, D('Amazon\Sites')->getSaleChannel($saleChannel));
    }

    /**
     * @param $accountId
     * @param $country
     * @return int
     * 根据一个账号ID和一个库存报告后缀，能够返回该账号的兄弟账号ID
     */
    public function getAccountIdByCountry ($accountId,$country) {
        return $this->getAccountIdBySiteId($accountId, D('Amazon\Sites')->getShortHandCode($country));
    }

    /**
     * @param $accountId
     * @param $siteId
     * @return int
     * 根据一个账号ID和一个站点ID，能够返回该账号的兄弟账号ID
     */
    public function getAccountIdBySiteId ($accountId, $siteId) {
        $siteId = intval($siteId);
        $accountTroopId = $this->getAccountTroop($accountId);
        if(!$accountTroopId)return false;//没有录入系统

        $accountsTroopSitesCache = S('AccountsTroopSites');
        if($siteId && !isset($accountsTroopCache[$accountTroopId][$siteId]))
            $accountsTroopSitesCache = array();

        if(!$accountsTroopSitesCache or !is_array($accountsTroopSitesCache)){
            $data = $this->field('account_id, account_troop_id, site_id')->select();
            foreach ($data as $row) {
                $accountsTroopSitesCache[$row['account_troop_id']][$row['site_id']] = $row['account_id'];
            }
            S('AccountsTroopSites', $accountsTroopSitesCache, 86400);
        }

        return isset($accountsTroopSitesCache[$accountTroopId][$siteId]) ? $accountsTroopSitesCache[$accountTroopId][$siteId] : 0;
    }

    /**
     * @return mixed
     * 获取每个账号的主账号名称ID
     */
    public function getAccountTroop($accountId = 0) {
        $accountsTroopCache = S('AccountsTroop');

        if($accountId && !isset($accountsTroopCache[$accountId]))
            $accountsTroopCache = array();

        if(!$accountsTroopCache or !is_array($accountsTroopCache)){
            $accountsTroop = $this->field('account_id, account_troop_id')->select();
            foreach ($accountsTroop as $row) {
                $accountsTroopCache[$row['account_id']] = $row['account_troop_id'];
            }
            S('AccountsTroop', $accountsTroopCache, 3600);
        }

        return $accountId == 0 ?
            $accountsTroopCache :
            (
                isset($accountsTroopCache[$accountId]) ? $accountsTroopCache[$accountId] : 0
            );
    }

    /**
     * @return bool|mixed
     * 根据站点ID返回该站点下的account_id
     */
    public function getAccountsBySiteId($siteId) {
        $siteAccountsCache = S('siteAccountsCache');
        if(!$siteAccountsCache or !is_array($siteAccountsCache)){
            $sites = $this->field('')->where()->select();
            foreach ($sites as $row) {
                $siteAccountsCache[$row['id']] = $row['shorthand_code'];
            }
            S('Sites', $siteAccountsCache, 86400);
        }

        return $siteAccountsCache;
    }

    /**
     * @param $siteId
     * @return array
     * 根据accountId返回该站点下的兄弟账号ID
     */
    public function getBrotherAccountIds ($accountId) {
        return $this->where(
            array(
                'account_troop_id'=>$this->getAccountTroop($accountId),
            )
        )->getField('account_id', TRUE);
    }

    /**
     * @param $siteId
     * @return array
     * 根据SiteId返回该站点下的账号信息数组
     */
    public function getSiteIdAccountsInfo ($siteId) {
        $siteIdAccountsId = $this->getSiteIdsAccountsIds();
        if(isset($siteIdAccountsId[$siteId]))
            return D('Amazon\Accounts')->getAccountsByIds($siteIdAccountsId[$siteId]);

        return array();
    }

    /**
     * @return mixed
     * 获取站点ID获取对应的可用的AccountId数组
     */
    public function getSiteIdsAccountsIds () {
        $siteIdAccountsIdsCache = S('siteIdAccountsIds');
        if(!$siteIdAccountsIdsCache or !is_array($siteIdAccountsIdsCache)){
            $siteIdAccountsIds = $this->field('account_id, site_id')
                ->where(array('is_used'=>1))->select();
            foreach ($siteIdAccountsIds as $row) {
                if(isset($siteIdAccountsIdCache[$row['site_id']])) {
                    $siteIdAccountsIdsCache[$row['site_id']][] = $row['account_id'];
                } else {
                    $siteIdAccountsIdsCache[$row['site_id']] = array($row['account_id']);
                }

            }
            S('Sites', $siteIdAccountsIdCache, 86400);
        }

        return $siteIdAccountsIdCache;
    }
}