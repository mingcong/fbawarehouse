<?php
/**
 * Created by PhpStorm.
 * User: gqc
 * Date: 2017/1/7
 * Time: 20:11
 */
namespace Api\Model\Amazon;

class ServiceUrlModel extends  CommonModel {
    //数据库配置
    protected $connection       =   'DB_FBAERP';
    // 实际数据表名（包含表前缀）
    protected $trueTableName    =   'api_marketplace_service';

    /**
     * @return bool|mixed
     * 根据API服务模块返回API根地址
     */
    public function getApiBelongUrls($siteId = 0) {
        if(empty($this->apiBelong))return false;
        $apiBelongCache = S('MarketplaceService_' . $this->apiBelong);
        if(!$apiBelongCache or !is_array($apiBelongCache)){
            $serviceUrls = $this->where(
                array(
                    'api_belong'=>$this->apiBelong
                )
            )->select();
            foreach ($serviceUrls as $row) {
                $apiBelongCache[$row['site_id']] = $row['protocol'] . '://' . $row['domain_name'];
                !empty($row['api_module']) && $apiBelongCache[$row['site_id']] .=  '/' . $row['api_module'];
                !empty($row['api_version']) && $apiBelongCache[$row['site_id']] .=  '/' . $row['api_version'];
            }
            S('MarketplaceService_' . $this->apiBelong, $apiBelongCache, 86400);
        }

        return $siteId === 0 ?
            $apiBelongCache :
            (
                isset($apiBelongCache[$siteId]) ? $apiBelongCache[$siteId] : false
            );
    }
}