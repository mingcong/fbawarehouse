<?php
$systemConfig = include('Common/Conf/system_config.php');
$menuConfig = include('menu_config.php');
$backupConfig = include('backup_config.php');
$securityConfig = include('security_config.php');
$mailConfig = include('mail_config.php');

//合规部擦
$specialConfig = include('special_config.php');


$appConfig =  array(
    // 调试页
    'SHOW_PAGE_TRACE' =>false,

    // 默认模块和Action
    'MODULE_ALLOW_LIST' => array('Home','Inbound','Warehouse','Api'),
    'DEFAULT_MODULE' => 'Home',

    // 默认控制器
    'DEFAULT_CONTROLLER' => 'Public',

    // 分页列表数
    'PAGE_LIST_ROWS' => 10,

    // 开启布局
    'LAYOUT_ON' => true,
    'LAYOUT_NAME' => '../../Home/View/Common/layout',

    // error，success跳转页面
    'TMPL_ACTION_ERROR' => 'Home@Common:dispatch_jump',
    'TMPL_ACTION_SUCCESS' => 'Home@Common:dispatch_jump',

    // 菜单项配置
    'MENU' => $menuConfig,
    'BACKUP' => $backupConfig,
    'MAIL' => $mailConfig,

    // 系统保留表名
    'SYSTEM_TBL_NAME' => 'model,models,filed,fileds,admin,admins',
    // 系统保留菜单名
    'SYSTEM_MENU_NAME' => '首页,模型,数据',

    // 文件上传根目录
    'UPLOAD_ROOT' =>  'Public/uploads/',
    // 系统公用配置目录
    'COMMON_CONF_PATH' => WEB_ROOT . 'Common/Conf/',

    // sku基础资料api网址,生产环境中填生产地址,开发环境中填开发的地址
//    'SKUSYSTEM_URL'     =>  'http://192.168.45.62:802',   //海泽的本地环境
    'SKUSYSTEM_URL'     =>  'http://192.168.5.5:802',
);

return array_merge($appConfig, $systemConfig, $securityConfig, $mailConfig, $specialConfig);
