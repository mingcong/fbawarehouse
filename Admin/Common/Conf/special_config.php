<?php
/**
 * 应对业务奇葩业务增加的配置文件
 * User: gqc
 * Date: 2018/1/12
 * Time: 14:35
 */
//合规部门规定某些账号数据行用红色展示
$complianceSet = array(
    'compliance' => array(432,433,434,435,436,437,438,439)
);

return $complianceSet;