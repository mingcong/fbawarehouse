<?php

$modelMenu = include('model_menu.php');

if (false === $modelMenu) {
    $modelMenu = array();
}

// 菜单项配置
$systemMenu = array(
    // 后台首页
    'Index' => array(
        'name' => '首页',
        'target' => 'Home/Index/index',
        'sub_menu' => array(
            array('item' => array('Index/index' => '系统信息')),
            array('item' => array('Index/editPassword' => '修改密码')),
            array('item' => array('Index/siteEdit' => '站点信息')),
            array('item' => array('Cache/index' => '清除缓存'))
        )
    ),
    // 缓存管理
    'Cache' => array(
        'name' => '缓存管理',
        'target' => 'Cache/index',
        'mapping' => 'Index',
        'sub_menu' => array(
            array('item' => array('Cache/index' => '缓存列表'))
        )
    ),

    // 数据管理
    'Admins' => array(
        'name' => '管理员权限',
        'target' => 'Home/Admins/index',
        'sub_menu' => array(
            array('item' => array('Admins/index' => '管理员信息')),
            array('item' => array('Roles/index' => '角色管理')),
            array('item' => array('Nodes/index' => '节点管理')),
            array('item' => array('Admins/add' => '添加管理员')),
            array('item' => array('Roles/add' => '添加角色')),
            array('item'=>array('Admins/edit'=>'编辑管理员信息'),'hidden'=>true),
            array('item' => array('Roles/edit'=>'编辑角色信息'),'hidden'=>true)
        )
    ),

    // 角色管理
    'Roles' => array(
        'name' => '角色管理',
        'target' => 'Home/Roles/index',
        'mapping' => 'Admins',
        'sub_menu' => array(
            array('item' => array('Roles/index' => '角色列表')),
            array('item' => array('Roles/add' => '添加角色')),
            array('item' => array('Roles/edit' => '编辑角色信息'),'hidden'=>true),
            array('item' => array('Roles/assignAccess' => '分配权限'),
                  'hidden'=>true)
        )
    ),

    // 节点管理
    'Nodes' => array(
        'name' => '节点管理',
        'target' => 'Home/Nodes/index',
        'mapping' => 'Admins',
        'sub_menu' => array(
            array('item' => array('Nodes/index' => '节点列表'))
        )
    ),
/*
    // 模型管理
    'Models' => array(
        'name' => '模型管理',
        'target' => 'Home/Models/index',
        'sub_menu' => array(
            array('item' => array('Models/index' => '模型列表')),
            array('item' => array('Models/add' => '添加模型')),
            array('item' => array('Models/show' => '模型信息'),'hidden' => true),
            array('item' => array('Models/edit' => '编辑模型'),'hidden' => true),
        )
    ),

    // 字段管理
    'Fields' => array(
        'name' => '字段管理',
        'target' => 'Home/Fields/edit',
        'mapping' => 'Models',
        'sub_menu' => array(
            array('item' => array('Fields/add' => '添加字段')),
            array('item' => array('Fields/edit' => '编辑字段')),
        )
    ),

    // 数据管理
    'Data' => array(
        'name' => '数据管理',
        'target' => 'Home/Data/backup',
        'sub_menu' => array(
            array('item' => array('Data/backup' => '数据备份')),
            array('item' => array('Data/restore' => '数据导入')),
            array('item' => array('Data/zipList' => '数据解压')),
            array('item' => array('Data/optimize' => '数据优化'))
        )
    ),*/

    //备货需求管理
    'Prepareneeds' => array(
        'name' => '备货需求管理',
        'target' => 'Inbound/StockingApply/index',
        'sub_menu' => array(
            array('item' => array('Inbound/StockingApply/index' => '备货需求申请')),
            //array('item' => array('Inbound/Prepareneeds/index' => '备货需求汇总列表')),
            array('item' => array('Inbound/Prepareneeds/all_detail' => '备货需求明细管理')),
            array('item' => array('Inbound/StockingApply/occupiedInventory' => '库存占用查询')),
            array('item' => array('Inbound/SingleShipmentidGrab/index' => 'Shipmentid抓取')),
            array('item' => array('Inbound/AccountSeller/index' => '销售员帐号对应关系表')),
            array('item' => array('Inbound/Prepareneeds/pcSearch' => 'PC单号查询')),
            //array('item' => array('Inbound/Prepareneeds/create' => '批量导入备货需求')),
            ////array('item' => array('Inbound/Prepareneeds/create_storage' => '批量导入预捡数量储位')),
        )
    ),
    //销售员帐号对应关系表
    'AccountSeller' => array(
        'name' => '备货需求管理',
        'target' => 'Inbound/StockingApply/index',
        'mapping' => 'Prepareneeds',
        'sub_menu' => array(
            array('item' => array('Inbound/AccountSeller/index' => '销售员帐号对应关系表')),
        )
    ),
    //备货需求申请
    'StockingApply' => array(
        'name' => '备货需求申请',
        'target' => 'Inbound/StockingApply/index',
        'mapping' => 'Prepareneeds',
        'sub_menu' => array(
            array('item' => array('Inbound/StockingApply/index' => '备货需求申请')),
            array('item' => array('Inbound/StockingApply/occupiedInventory' => '库存占用查询'))
        )
    ),
    //单个Shipmentid抓取
    'SingleShipmentidGrab' => array(
        'name' => '备货需求管理',
        'target' => 'Inbound/StockingApply/index',
        'mapping' => 'Prepareneeds',
        'sub_menu' => array(
            array('item' => array('Inbound/SingleShipmentidGrab/index' => 'Shipmentid抓取'))
        )
    ),

    // FBA平台计划单
    'Inboundshipmentplan' => array(
        'name' => 'FBA平台计划单',
        'target' => 'Inbound/Inboundshipmentplan/index',
        'sub_menu' => array(
            array('item' => array('Inbound/Inboundshipmentplan/index' => 'FBA平台计划单')),
            array('item' => array('Inbound/Inboundshipmentplan/package_box_index' => '录入箱唛信息')),
            // array('item' => array('Inbound/Inboundshipmentplan/select_detail' => '平台计划单明细')),
            array('item' => array('Inbound/Inboundshipmentplan/select_package_info' => '装箱查询')),
            array('item' => array('Inbound/Inboundshipmentplan/show_sku_info' => '平台SKU列表')),
            array('item' => array('Inbound/Inboundshipmentplan/shipmentIdBack' => 'ShipmentId返仓申请')),
            array('item' => array('Inbound/Inboundshipmentplan/shipmentIdBackList' => 'Shipmentid返仓列表')),
            array('item' => array('Inbound/Inboundshipmentplan/shipmentIdRejects' => 'ShipmentId不良申请')),
            array('item' => array('Inbound/Inboundshipmentplan/shipmentIdRejectsList' => 'ShipmentId不良申请处理')),
            array('item' => array('Inbound/Inboundshipmentplan/shipmentIdSingle' => 'Shipmentid采购单信息补录')),
        ),
    ),

    //物流计划单管理
    'Transportplan' => array(
        'name' => '物流计划单管理',
        'target' => 'Inbound/Transportplan/index',
        'sub_menu' => array(
            array('item' => array('Inbound/Transportplan/index' => '物流计划单列表')),
            array('item' => array('Inbound/Transportplan/batchedittrans' => '批量物流追踪')),
            array('item' => array('Inbound/Transportplan/createTransPlan' => '新建物流计划单')),
            array('item' => array('Inbound/Carrier/index' => '物流渠道管理列表')),
            array('item' => array('Inbound/Transportplan/exportRebateStatistics' => '出口退税物流统计表')),
            array('item' => array('Inbound/Transportplan/fbaHead' => 'FBA头程分摊计算报表')),
        )
    ),
    //物流渠道管理
    'Carrier' => array(
        'name' => '物流计划单管理',
        'target' => 'Inbound/Carrier/index',
        'mapping' => 'Transportplan',
        'sub_menu' => array(
            array('item' => array('Inbound/Carrier/index' => '物流渠道管理列表')),
        )
    ),
    //FBA库存销量管理
    'Amazon/InventorySale' => array(
        'name' => 'FBA库存销量管理',
        'target' => '/Api/Amazon/InventorySale/index',
        'sub_menu' => array(
            array('item' => array('Api\Amazon/InventorySale/index' => 'FBA库存销量列表')),
            array('item' => array('Api\Amazon/InboundTime/index' => 'LISTING首次入FBA时间')),
            array('item' => array('Api\Amazon/RemovalOrder/index' => '物流移除订单详情报告')),
            array('item' => array('Inbound/Report/index' => '亚马逊物流库存事件详情报告')),
            array('item' => array('Api\Amazon/FbaStockingArgs/index' => 'FBA库存销量表(FBA备货参数)')),
            array('item' => array('Api\Amazon/FbaStockingArgs/sku_quantity' => 'FBA订单销量统计')),

            //20180103gqc
            array('item' => array('Api\Amazon/FbaStockBook/index' => 'FBA平台站点库存汇总')),
            array('item' => array('Api\Amazon/FbaStockBook/site' => 'FBA平台账号库存汇总')),
            array('item' => array('Api\Amazon/FbaStockBook/account' => 'FBA平台账号库存明细'))
        )
    ),

    // Listing首次入库时间
    'Amazon/InboundTime' => array(
        'name' => 'FBA库存销量管理',
        'target' => '/Api/Amazon/InboundTime/index',
        'mapping' => 'Amazon/InventorySale',
        'sub_menu' => array(
            array('item' => array('Api\Amazon/InboundTime/index' => 'LISTING首次入FBA时间')),
        )
    ),

    // 物流移除订单报告
    'Amazon/RemovalOrder' => array(
        'name' => 'FBA库存销量管理',
        'target' => '/Api/Amazon/RemovalOrder/index',
        'mapping' => 'Amazon/InventorySale',
        'sub_menu' => array(
            array('item' => array('Api\Amazon/RemovalOrder/index' => '物流移除订单详情报告'))
        )
    ),
    //FBA库存销量表(FBA备货参数)
    'Amazon/FbaStockingArgs' => array(
        'name' => 'FBA库存销量管理',
        'target' => '/Api/Amazon/FbaStockingArgs/index',
        'mapping' => 'Amazon/InventorySale',
        'sub_menu' => array(
            array('item' => array('Api\Amazon/FbaStockingArgs/index' => 'FBA库存销量表(FBA备货参数)')),
            array('item' => array('Api\Amazon/FbaStockingArgs/sku_quantity' => 'FBA订单销量统计'))
        )
    ),
    //
    'Report' => array(
        'name' => 'FBA库存销量管理',
        'target' => '/Api/Amazon/FbaStockingArgs/index',
        'mapping' => 'Amazon/InventorySale',
        'sub_menu' => array(
            array('item' => array('Inbound/Report/index' => '亚马逊物流库存事件详情报告'))
        )
    ),
    //FBA平台汇总数据展示(gqc20180103)
    'Amazon/FbaStockBook' => array(
        'name' => 'FBA平台库存通表',
        'target' => '/Api/Amazon/FbaStockBook/index',
        'mapping' => 'Amazon/InventorySale',
        'sub_menu' => array(
            array('item' => array('Api\Amazon/FbaStockBook/index' => 'FBA平台站点库存汇总')),
            array('item' => array('Api\Amazon/FbaStockBook/site' => 'FBA平台账号库存汇总')),
            array('item' => array('Api\Amazon/FbaStockBook/account' => 'FBA平台账号库存明细'))
        )
    ),
    /*
    'Test' => array(
        'name' => '中转仓(原始界面)',
        'target' => 'Warehouse/Test/index',
        //'mapping' => 'Amazon/InventorySale',
        'sub_menu' => array(
            array('item' => array('/Warehouse/Test/index' => '一')),
            array('item' => array('/Warehouse/Test/two' => '二')),
            array('item' => array('/Warehouse/Test/three' => '三')),
            array('item' => array('/Warehouse/Test/four' => '四')),
            array('item' => array('/Warehouse/Test/five' => '五')),
            array('item' => array('/Warehouse/Test/six' => '六')),
            array('item' => array('/Warehouse/Test/seven' => '七')),
            array('item' => array('/Warehouse/Test/eight' => '八')),
            array('item' => array('/Warehouse/Test/nine' => '九')),
            array('item' => array('/Warehouse/Test/ten' => '十')),
            array('item' => array('/Warehouse/Test/eleven' => '十一')),
            array('item' => array('/Warehouse/Test/twelve' => '十二')),
            array('item' => array('/Warehouse/Test/thirteen' => '十三')),
            array('item' => array('/Warehouse/Test/fourteen' => '十四')),
            array('item' => array('/Warehouse/Test/fifteen' => '十五')),
            array('item' => array('/Warehouse/Test/sixteen' => '十六')),
        )
    ),*/

    // 扫描收货
    'Purchaseorders' => array(
        'name' => '扫描收货',
        'target' => 'Warehouse/Purchaseorders/index',
        'sub_menu' => array(
            array('item' => array('Warehouse/Purchaseorders/purchased_wait_recieve' => '已采购待收货的采购单')),
            array('item' => array('Warehouse/Purchaseorders/index' => '扫描收货')),
            array('item' => array('Warehouse/Purchaseorders/recieved_wait_storage' => '已收货待入库的采购单')),
            //array('item' => array('Warehouse/Purchaseorders/recieve_pictureupload' => '供应商送货单图片录入')),
            array('item' => array('Warehouse/Purchaseorders/recieve_details' => '收货明细报表')),
        )
    ),
    //质检模块
    'CheckOrders' => array(
        'name' => '质检模块',
        'target' => 'Warehouse/CheckOrders/attr_confirm_index',
        'sub_menu' => array(
            array('item' => array('Warehouse/CheckOrders/attr_confirm_index' => '属性确认')),
            array('item' => array('Warehouse/CheckOrders/check_standard_index' => 'SKU质检标准确认')),
            array('item' => array('Warehouse/CheckOrders/scan_check_index' => '扫描质检')),
            array('item' => array('Warehouse/CheckOrders/checked_orders_report' => '质检报表')),
            array('item' => array('Warehouse/CheckOrders/check_orders_details_report' => '质检明细报表')),
        )
    ),
    //不良品
    'BadProducts' => array(
        'name' => '不良品',
        'target' => 'Warehouse/BadProducts/index',
        'sub_menu' => array(
            array('item' => array('Warehouse/BadProducts/index' => '不良品查询')),
            array('item' => array('Warehouse/BadProducts/bad_products_handle' => '不良品处理')),
            array('item' => array('Warehouse/BadProducts/handlequality' => '不良品实物确认处理')),
            array('item' => array('Warehouse/BadProducts/backWarehouse' => '手动录入不良品'))
        )
    ),

    //入库出库
    'StockIn' => array(
        'name' => '入库',
        'target' => 'Warehouse/StockIn/Qc_StockIn',
        'sub_menu' => array(
            array('item' => array('Warehouse/StockIn/Qc_StockIn' => '质检入库')),
            array('item' => array('Warehouse/StockIn/index' => '特殊入库')),
            array('item' => array('Warehouse/StockIn/StockIn_Detail' => '入库明细报表')),
            array('item' => array('Warehouse/StockIn/add_storage' => '新建储位')),
            array('item' => array('Warehouse/StockIn/storage_lists' => '储位列表')),
            array('item' => array('Warehouse/StockIn/warehouse_import' => '期初库存导入')),
        )
    ),
    //入库出库
    'StockOut' => array(
        'name' => '出库',
        'target' => 'Warehouse/StockOut/index',
        'sub_menu' => array(
            array('item' => array('Warehouse/StockOut/index' => '出库')),
            array('item' => array('Warehouse/StockOut/delivery_details' => '出库明细报表')),
        )
    ),
    //库存模块
    'Inventory' =>  array(
        'name' => '库存',
        'target' => 'Warehouse/Inventory/index',
        'sub_menu' => array(
            array('item' => array('Warehouse/Inventory/index' => '查询单个sku的库存')),
            array('item' => array('Warehouse/Inventory/sku_real_inventory' => 'sku实时库存')),
            array('item' => array('Warehouse/Inventory/pandian' => '库存盘点')),
            array('item' => array('Warehouse/Inventory/move_inventory' => '站点调拨')),
            array('item' => array('Warehouse/Inventory/move_storage' => '储位移位')),
            array('item' => array('Warehouse/Inventory/shiftrecord' => '库存移位记录')),
            array('item' => array('Warehouse/Inventory/fbaInventory' => 'FBA库存数据监控')),
            array('item' => array('Warehouse/InventoryCost/index' => '库存期初成本报表')),
        )
    ),
    //库存成本
    'InventoryCost' =>  array(
    'name' => '库存',
    'target' => 'Warehouse/InventoryCost/index',
    'mapping' => 'Inventory',
    'sub_menu' => array(
        array('item' => array('Warehouse/InventoryCost/index' => '库存期初成本报表')),
       )
),

    //虚拟FBA模块
    /*'VirtualFba' =>  array(
        'name' => '虚拟FBA',
        'target' => 'Inbound/VirtualFba/prepare',
        'sub_menu' => array(
            array('item' => array('Inbound/VirtualFba/prepare' => '备货需求申请')),
        )
    ),*/
);

return array_merge($systemMenu, $modelMenu);
