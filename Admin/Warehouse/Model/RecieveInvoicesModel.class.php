<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;
use Warehouse\Service\PublicdataService;

class RecieveInvoicesModel extends CommoninterfaceModel
{
    //数据库对象
    public $Recieve_invoices = NULL;
    //收货表
    static $table = 'recieve_invoices';
    //数据
    public $Recieve_invoices_data = NULL;
    //收货表ID
    public $Recieve_invoices_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        //print_r($this->_db);exit;
        $this->Recieve_invoices = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_recieve_invoices_model(&$_array = array(),$flag = TRUE)
    {

        if($_array){
            $status = false;
            if($_array['export_tax_rebate']==='0'){
                $status=true;    //非退税设为0的后果,多三行代码
            }
            $_array = array_filter($_array);
            if($status){
                $_array['export_tax_rebate'] = 0;
            }
        }
        $_array = PrepareneedsService::trim_array($_array);
        $this->Recieve_invoices->table('wms_recieve_invoices a')
            ->join('wms_recieve_details b on a.id = b.recieve_invoice_id')
            ->field('a.`id` as mainid,a.`purchase_id`,a.`purchaseorder_id`,a .`recieve_man`,a.`store`,a.`recieve_time`,
            a.`export_tax_rebate`,a.`enterprise_dominant`,b.`site_id`,a.`supplier`,b.`id`,b.`sku`,b.`sku_name`,b.`purchase_quantity`,
            b.`arrival_quantity`,b.`sku_standard`,b.`storage_position`,b.`batch_code`');
        foreach($_array as $key => $_arr){
            switch($key){
                case 'recieve_time_from':
                    $_array['a.recieve_time'][] = array(
                        'egt',
                        $_arr
                    );
                    unset($_array['recieve_time_from']);
                    break;
                case 'recieve_time_to':
                    $_array['a.recieve_time'][] = array(
                        'elt',
                        $_arr
                    );
                    unset($_array['recieve_time_to']);
                    break;
                case 'purchase_id':
                    $_array['a.purchase_id'] = PublicdataService::get_user_id(trim($_arr));
                    unset($_array['purchase_id']);
                    break;
                case 'supplier':
                    $_array['a.supplier'] = PublicdataService::get_supplier_id(trim($_arr));
                    unset($_array['supplier']);
                    break;
                case 'export_tax_debate':
                    $_array['a.export_tax_debate'] = $_arr;
                    unset($_array['export_tax_debate']);
                    break;
                case 'sku':
                    $_array['b.sku'] = $_arr;
                    unset($_array['sku']);
                    break;
                case 'enterprise_dominant':
                    $_array['a.enterprise_dominant'] = $_arr;
                    unset($_array['enterprise_dominant']);
                    break;
                case 'purchaseorder_id':
                    $_array['a.purchaseorder_id'] = $_arr;
                    unset($_array['purchaseorder_id']);
                    break;
            }
        }
        return $this->Recieve_invoices->where($_array);
    }

    /**
     * @param array $_array
     * @return mixed
     *已收货待入库的采购单过滤查询条件
     */
    public function get_recieve_purchase_model(&$_array = array(),$flag = TRUE,$field = TRUE)
    {
        if($_array){
            $_array = array_filter($_array);
        }
        $this->Recieve_invoices->table('wms_recieve_invoices a')
            ->join('wms_recieve_details b on a.id = b.recieve_invoice_id')
            ->join('wms_purchaseorders c on a.purchaseorder_id =c.id ')
            ->join('wms_purchaseorder_details d on a.purchaseorder_id = d.purchaseorder_id');
        if($field){
            $this->Recieve_invoices
                ->field('a.`id` as mainid,a.`purchase_id`,a.`purchaseorder_id`,a .`recieve_man`,a.`store`,a.`recieve_time`,
            a.`export_tax_rebate`,a.`enterprise_dominant`,b.`site_id`,a.`supplier`,b.`id`,b.`sku`,b.`sku_name`,b.`purchase_quantity`,
            b.`arrival_quantity`,b.`check_count`,b.`sku_standard`,b.`storage_position`,b.`batch_code`,d.`status`,
            c.`transfer_hopper_id`')
                ->where(" c.status>=20 and c.status<70 and d.status>=20 and d.status<70 and d.sku = b.`sku`");
        }else{
            $this->Recieve_invoices
                ->field('a.`id` as mainid,b.`id`')
                ->where(" c.status>=20 and c.status<70 and d.status>=20 and d.status<50 and d.sku = b.`sku`");
        }


        $_array = PrepareneedsService::trim_array($_array);
        foreach($_array as $key => $_arr){
            switch($key){
                case 'id':
                    $_array['a.purchaseorder_id'] = $_arr;
                    unset($_array['id']);
                    break;
                case 'batch_code':
                    $_array['b.batch_code'] = $_arr;
                    unset($_array['batch_code']);
                    break;
                case 'purchaseorder_id':
                    $_array['a.purchaseorder_id'] = $_arr;
                    unset($_array['purchaseorder_id']);
                    break;
                case 'sku':
                    $_array['b.sku'] = $_arr;
                    unset($_array['sku']);
                    break;
                case 'status':
                    $_array['d.status'] = $_arr;
                    unset($_array['status']);
                    break;
                case 'recieve_time_from':
                    $_array['a.recieve_time'][] = array(
                        'egt',
                        $_arr
                    );
                    unset($_array['recieve_time_from']);
                    break;
                case 'recieve_time_to':
                    $_array['a.recieve_time'][] = array(
                        'elt',
                        $_arr
                    );
                    unset($_array['recieve_time_to']);
                    break;
            }
        }
        return $this->Recieve_invoices->where($_array);
    }

    /**
     * @param array $_array
     * @return mixed
     * 收获修改数量
     */
    public function get_recieveinfo_model(&$_array = array())
    {
        if($_array){
            $_array = array_filter($_array);
        }
        $this->Recieve_invoices->table('wms_recieve_invoices a')
            ->join('wms_recieve_details b on a.id = b.recieve_invoice_id')
            ->join('wms_purchaseorders c on a.purchaseorder_id =c.id ')
            ->join('wms_purchaseorder_details d on a.purchaseorder_id = d.purchaseorder_id')
            //->join('wms_purchaseorder_details_sites e on b.sku=e.sku')
            ->field('a.`id` as mainid,a.`purchase_id`,a.`purchaseorder_id`,a .`recieve_man`,a.`store`,a.`recieve_time`,
        a.`export_tax_rebate`,a.`enterprise_dominant`,a.`supplier`,b.`id`,b.`sku`,b.`sku_name`,b.`purchase_quantity`,
        b.`arrival_quantity`,b.`check_count`,b.`sku_standard`,b.`storage_position`,b.`batch_code`,b.`site_id`,d.`status`,
        c.`transfer_hopper_id`,d.`recieve_quantity`')
            ->where(" c.status>=20 and c.status<70 and d.status>=20 and d.status<70 and d.sku = b.`sku` 
            and b.check_count=0");
        $data =  $this->Recieve_invoices->where($_array)->select();
        return $data;
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 插入收货明细表   get_recieve_purchase_model
     */
    public function add_recieve_invoices_table($_array = array(),&$model = NULL)
    {
        $this->Recieve_invoices_id = $model->table($this->_db.'.wms_'.self::$table)
            ->add($_array);
        return $this->Recieve_invoices_id;
    }

    /**
     *更新操作
     */
    public function update_recieve_invoices($_array = array(),$data = array(),&$model = NULL)
    {
        $model = $model==NULL?$this->Recieve_invoices:$model->table($this->_db.".wms_".self::$table);
        $res   = $model->where($_array)
            ->save($data);
        return $res;
    }
}