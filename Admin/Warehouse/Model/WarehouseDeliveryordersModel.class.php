<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/2/25
 * Time: 15:37
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;

class warehouseDeliveryordersModel extends CommoninterfaceModel
{
    //数据库对象
    public $warehouse_deliveryorders = NULL;
    //入库出库关联表表
    static $table = 'warehouse_deliveryorders';
    //数据
    public $warehouse_deliveryorders_data = NULL;
    //入库出库关联表ID
    public $warehouse_deliveryorders_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        //print_r($this->_db);exit;
        $this->warehouse_deliveryorders = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 入库出库关联表
     */
    public function add_warehouse_deliveryorders(&$_array = array(),$model = NULL)
    {
        $model = $model==NULL?$this->warehouse_deliveryorders:$model->table($this->_db.".wms_".self::$table);

        $this->warehouse_deliveryorders_id = $model->table($this->_db.'.wms_'.self::$table)
            ->add($_array);
        return $this->warehouse_deliveryorders_id;
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 批量插入入库出库关联表
     */
    public function addAll_warehouse_deliveryorders(&$_array = array(),$model = NULL)
    {
        $model = $model==NULL?$this->warehouse_deliveryorders:$model->table($this->_db.".wms_".self::$table);
        $res = $model->table($this->_db.'.wms_'.self::$table)
            ->addAll($_array);
        return $res;
    }

    /**
     *更新操作
     */
    public function update_warehouse_deliveryorders($_array = array(),$data = array(),&$model = NULL)
    {
        $model = $model==NULL?$this->warehouse_deliveryorders:$model->table($this->_db.".wms_".self::$table);
        $res   = $model->where($_array)
            ->save($data);
        return $res;
    }


}