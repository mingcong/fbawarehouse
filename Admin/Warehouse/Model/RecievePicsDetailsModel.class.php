<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;
use Warehouse\Service\PublicdataService;

class RecievePicsDetailsModel extends CommoninterfaceModel
{
    //数据库对象
    public $Recieve_pics_details = NULL;
    //收货表
    static $table = 'recieve_pics_details';
    //数据
    public $Recieve_pics_details_data = NULL;
    //收货表ID
    public $Recieve_pics_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        //print_r($this->_db);exit;
        $this->Recieve_pics_details = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_recieve_pics_details_model(&$_array = array(),$flag = TRUE)
    {
        $this->Recieve_pics_details->table('wms_recieve_pics_details');
        $array=array();

        foreach($_array as $key=>$value){
            switch($key){
                case 'id':
                    !empty($value)?$array['id'] = $value:'';
                    break;
                case 'recieve_manual_pic_id':
                    !empty($value)?$array['recieve_manual_pic_id'] = $value:'';
                    break;
                case 'uploader_id':
                    !empty($value)?$array['uploader_id'] = $value:'';
                    break;
                case 'upload_time_from':
                    !empty($value)?$array['upload_time'][] = array(
                        'egt',
                        $value
                    ):'';
                    break;
                case 'upload_time_to':
                    !empty($value)?$array['upload_time'][] = array(
                        'elt',
                        $value
                    ):'';
                    break;
            }
        }
        return $this->Recieve_pics_details->where($array)
            ->getfield('`id`, `recieve_manual_pic_id`, `uploader_id`, `upload_time`, `url`');
    }
    public function add_recieve_pics_details($_array = array())
    {
        $this->Recieve_pics_id = $this->Recieve_pics_details->table($this->_db.'.wms_'.self::$table)
            ->add($_array);
        return $this->Recieve_pics_id;
    }

    public function delPic($picurl){
        $opsign=$this->Recieve_pics_details->table($this->_db.'.wms_'.self::$table)->where(array('url'=>$picurl))->delete();
        return $opsign;
    }
}