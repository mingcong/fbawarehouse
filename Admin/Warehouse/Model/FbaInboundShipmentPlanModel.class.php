<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/3/3
 * Time: 14:53
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;

class FbaInboundShipmentPlanModel extends CommoninterfaceModel
{
    //数据库对象
    public $plan_details = NULL;
    //收货明细表
    static $table = 'inbound_shipment_plan';

    public $plan_details_id;


    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->plan_details = M(self::$table,'fba_',$this->_db);
        parent::__construct();
    }

    /**
     * 非出口退税
     * sku订单占用未发的数量
     * @param $sku
     */
    public function get_sku_occupied_num(&$_array)
    {
        $model = $this->plan_details;

        $where_map = array(
            'fba_inbound_shipment_plan_detail.sku'                     =>      $_array['sku'],
            'fba_inbound_shipment_plan_detail.enterprise_dominant'     =>      $_array['enterprise_dominant'],
            'fba_inbound_shipment_plan_detail.export_tax_rebate'       =>  0,
            'fba_inbound_shipment_plan.status'                         => array(array('ELT', 15),array('EGT', 10)),
            'fba_inbound_shipment_plan.site_id'                        =>      $_array['site_id'],
        );
        $res = $model
            ->join('fba_inbound_shipment_plan_detail ON fba_inbound_shipment_plan.id=fba_inbound_shipment_plan_detail.inbound_shipment_plan_id','LEFT')
            ->where($where_map)
            ->getField('SUM(fba_inbound_shipment_plan_detail.quantity) as occupiedNum');
        return $res;
    }
    /**
     * 出口退税
     * sku订单占用未发的数量
     * @param $sku
     */
    public function get_tax_sku_occupied_num(&$_array)
    {
        $model = $this->plan_details;

        $where_map = array(
            'fba_inbound_shipment_plan_detail.sku'                     =>      $_array['sku'],
            'fba_inbound_shipment_plan_detail.enterprise_dominant'     =>      $_array['enterprise_dominant'],
            'fba_inbound_shipment_plan_detail.export_tax_rebate'       =>  1,
            'fba_inbound_shipment_plan.status'                         => array(array('ELT', 15),array('EGT', 10)),
            'fba_inbound_shipment_plan.site_id'                        =>      $_array['site_id'],
        );
        $res = $model
            ->join('fba_inbound_shipment_plan_detail ON fba_inbound_shipment_plan.id=fba_inbound_shipment_plan_detail.inbound_shipment_plan_id','LEFT')
            ->where($where_map)
            ->getField('SUM(fba_inbound_shipment_plan_detail.quantity) as occupiedNum');
        return $res;
    }
}