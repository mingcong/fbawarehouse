<?php
/**
 * Created by PhpStorm.
 * User: khq
 * Date: 17-3-1
 * Time: 下午3:30
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
class BadProductsModel extends CommoninterfaceModel
{

    public $obj             = NULL;

    static $table           = 'bad_product_delivery';
    
    //数据库对象
    public $badProducts     = NULL;
    
    public $dealInvoices    = NULL;
    //数据
    public $data            = array();

    /**
     * PrepareneedsdetailModel constructor.
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '',$param=array()){

        $this->badProducts = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }
    /**
     * 根据不良品处理单id获取不良品出库单信息
     * khq 2017.3.6
     */
    public function get_bad_product_delivery_byid($id)
    {
        return $this->badProducts
            ->where(array('unqualified_id'=>$id))
            ->select();
    }
    public function update($_array, $_param, &$model = NULL) {
        return parent::update($_array, $_param, $this->model->table("$this->_db."."wms_".self::$table)); // TODO:
    }

    /**
     * @param $id
     * @return mixed
     * 根据不良品出库单id获取出库单信息
     * kqh 2017.3.15
     */
    public function bad_product_data($id) {
        return $this->badProducts
            ->where(array('id'=>$id))
            ->find();
    }

    /**
     * @return mixed
     * 获取凭证最大值
     */
    public function get_max_pur_evidence() {
        return $this->badProducts
            ->getField('MAX(pur_evidence) AS maxid');
    }

    /**
     * @param $_array
     * @return mixed
     * 获取不良品出库单和质检明细表数据model
     */
    public function bad_products_and_check_detail_model(&$_array) {
        $model = $this->badProducts;
        $res   = $model->join(
            'wms_check_quality_details CD ON wms_bad_product_delivery.check_detail_id=CD.id','LEFT')
            ->where($_array)
            ->field('wms_bad_product_delivery.*,CD.invoice_date,CD.remark,CD.check_man');
        return $res;
    }
    /**
     * @param array $_array -
     * @return mixed
     * 批量创建不良品出库单
     * khq 2017.3.6
     */
    public function batch_create(&$_array = array())
    {
        return parent::batch_create($_array, $this->model->table("$this->_db."."wms_".self::$table)); // TODO:
    }
}