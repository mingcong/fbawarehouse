<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;
use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;

class BatchesModel extends CommoninterfaceModel
{
    //数据库对象
    public $batches = NULL;
    //收货表
    static $table = 'batches';
    //数据
    public $batches_data = NULL;
    //批次号表ID
    public $batches_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->batches = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @return mixed
     * 获取最近一次的批次号
     */
    public function get_batch_code()
    {
        return $this->batches
            ->order('batch_code DESC')
            ->limit(1)
            ->getfield('batch_code');
    }

    /**
     * @return mixed
     * 获取最近一次的批次号
     *
     */
    public function add_batch_code($data)
    {
        $res = $this->batches
            ->add($data);
        return $res;
    }



}