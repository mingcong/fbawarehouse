<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;
use Inbound\Service\PrepareneedsService;
use Warehouse\Model\PurchaseOrdersModel;

class PurchaseOrderDetailsModel extends PurchaseOrdersModel
{
    //数据库对象
    public $Purchaseorder_details = NULL;
    //采购单明细表
    static $table = 'purchaseorder_details';
    //数据
    public $Purchaseorder_details_data = NULL;
    //采购单明细表ID
    public $Purchaseorder_details_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->Purchaseorder_details = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_purchaseorder_details_model(&$_array = array(),$flag=true)
    {
        if($flag){
            if($_array){
                $_array = array_filter($_array);
            }else{
                //$_array = array('status' => 10);
            }
            $_array = PrepareneedsService::trim_array($_array);
            foreach($_array as $key => $_arr){
                switch($key){
                    case 'details_date_from':
                        $_array['details_date'][] = array(
                            'egt',
                            $_arr
                        );
                        unset($_array['details_date_from']);
                        break;
                    case 'details_date_to':
                        $_array['details_date'][] = array(
                            'elt',
                            $_arr
                        );
                        unset($_array['etails_date_to']);
                        break;
                }
            }
            return $this->Purchaseorder_details->where($_array);
        }else{
            return $this->Purchaseorder_details->where($_array);
        }
    }

    /**
     *更新操作
     */
    public  function  update_purchaseorder_details($_array = array(),$data = array(),&$model = NULL){
        $model = $model == NULL? $this->Purchaseorder_details : $model->table($this->_db.".wms_".self::$table);
        $res = $model->where($_array)->save($data);
        return $res;
    }

    /**
     * @param $_array
     * @return mixed
     * 获取采购明细的信息
     * khq 2017.3.15
     */
    public function get_purchaseorder_detail($_array) {
        return $this->Purchaseorder_details
            ->where($_array)
            ->find();
    }

    public function check_purchase_order($_array) {
        $map['pd.sku'] = $_array['sku'];
        $map['p.enterprise_dominant'] = $_array['enterprise_dominant'];

        $map['p.export_tax_rebate'] = 1;
        $map['p.status'] = array('neq', 100);
        $map['pd.ware_quantity'] = array('gt', 0);


        $result = $this->Purchaseorder_details
            ->table('wms_purchaseorder_details AS pd')
            ->join('wms_purchaseorders AS p ON p.id = pd.purchaseorder_id')
            ->where($map)
            ->select();echo $this->Purchaseorder_details->getLastSql();exit;

        return !empty($result) ? $result : array();
    }
}