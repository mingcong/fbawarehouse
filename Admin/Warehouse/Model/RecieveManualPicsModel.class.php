<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;
use Warehouse\Service\PublicdataService;

class RecieveManualPicsModel extends CommoninterfaceModel
{
    //数据库对象
    public $Recieve_manual_pics = NULL;
    public $Recieve_pics_details = NULL;
    //收货表
    static $Recieve_manual_pics_table = 'recieve_manual_pics';
    static $Recieve_pics_details_table = 'recieve_pics_details';
    //数据
    public $Recieve_manual_pics_data = NULL;
    //收货表ID
    public $Recieve_manual_pics_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        //print_r($this->_db);exit;
        $this->Recieve_manual_pics = M(self::$Recieve_manual_pics_table,'wms_',$this->_db);
        $this->Recieve_pics_details = M(self::$Recieve_pics_details_table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_recieve_manual_pics_model(&$_array = array(),$flag = TRUE)
    {
        $this->Recieve_manual_pics->table('wms_recieve_manual_pics a');
        $array=array();
        foreach($_array as $key=>$value){
            $detail_array=array();
            switch($key){
                case 'id':
                    !empty($value)?$array['id'] = $value:'';
                    break;
                case 'purchase_id':
                    !empty($value)?$array['purchaseorder_id'] = $value:'';
                    break;
                case 'recieve_invoices':
                    !empty($value)?$array['recieve_invoice_id'] = $value:'';
                    break;
                case 'reciever_id':
                    !empty($value)?$array['reciever_id'] = PublicdataService::get_user_id(trim($value)):'';
                    break;
                case 'recieve_time_from':
                    !empty($value)?$array['recieve_time'][] = array(
                        'egt',
                        $value
                    ):'';
                    break;
                case 'recieve_time_to':
                    !empty($value)?$array['recieve_time'][] = array(
                        'elt',
                        $value
                    ):'';
                    break;
                case 'uploader_id':
                    !empty($value)?$uploader_id = PublicdataService::get_user_id(trim($value)):'';
                    $ids=$this->Recieve_pics_details->where(array('uploader_id'=>$uploader_id))
                        ->field('recieve_manual_pic_id')->select();
                    if(!empty($ids)){
                        $idsstring='';
                        foreach($ids as $id){
                            $idsstring.=empty($idsstring)?$id['recieve_manual_pic_id']:','.$id['recieve_manual_pic_id'];
                        }
                        $array['id'][] = array(
                            'IN',$idsstring
                        );
                    }
                    break;
                case 'upload_time_from':
                    if(!empty($value)){
                        $ids=$this->Recieve_pics_details->where(array('upload_time'=>array(
                            'egt',
                            $value
                        )))->field('recieve_manual_pic_id')->select();
                        $idsstring='';
                        foreach($ids as $id){
                            $idsstring.=empty($idsstring)?$id['recieve_manual_pic_id']:','.$id['recieve_manual_pic_id'];
                        }
                        $array['id'][] = !empty($idsstring)?
                            array('IN',$idsstring):0;
                    }
                    break;
                case 'upload_time_to':
                    if(!empty($value)){
                        $ids=$this->Recieve_pics_details
                            ->where(array('upload_time'=>array(
                                'elt',
                                $value)))
                            ->field('recieve_manual_pic_id')
                            ->select();
                        $idsstring='';
                        foreach($ids as $id){
                            $idsstring.=empty($idsstring)?$id['recieve_manual_pic_id']:','.$id['recieve_manual_pic_id'];
                        }
                        $array['id'][] = !empty($idsstring)?
                            array('IN',$idsstring):0;
                    }
                    break;
            }
        }
        $infos=$this->Recieve_manual_pics->where($array)
            ->getfield('`id`, `purchaseorder_id`, `recieve_invoice_id`, `pics_path`, `reciever_id`, `recieve_time`');
        foreach($infos as &$info){
            $details=$this->Recieve_pics_details->field('url, uploader_id, upload_time')
                ->where(array('recieve_manual_pic_id'=>$info['id']))
                ->select();
            foreach($details as &$detail){
                $detail['uploader']=PublicdataService::get_user_name($detail['uploader_id']);
            }
            $info['details']=$details;
        }
        return $infos;
    }
    public function add_recieve_manual_pics($_array = array())
    {
        $this->Recieve_manual_pics_id = $this->Recieve_manual_pics
            ->add($_array);
        return $this->Recieve_manual_pics_id;
    }
    public function count(){

    }
}