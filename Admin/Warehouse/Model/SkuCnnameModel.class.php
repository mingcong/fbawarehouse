<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/3/6
 * Time: 10:23
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;

class SkuCnnameModel extends CommoninterfaceModel
{

    //数据库对象
    public $sku_cnname = null;
    //收货明细表
    static $table = 'sku_cnname';

    public $sku_cnname_id;


    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct( $table = '', $param = array() )
    {
        $this->sku_cnname = M( self::$table, 'skusystem_', $this->_db );
        parent::__construct();
    }

    /**
     * 获取所有的sku
     * sku=>sku
     *
     * @return mixed
     */
    public function get_skus()
    {
        $model = $this->sku_cnname;

        return $model->getField( 'sku AS skuu, sku' );
    }

    /**
     * 获取sku中文名
     * sku=>name
     *
     * @param null $sku
     *
     * @return mixed
     */
    public function get_skus_info( $sku = null )
    {
        $model = $this->sku_cnname;

        if ( is_string( $sku ) ) {
            return $model->join( 'skusystem_cnname ON skusystem_cnname.id=skusystem_sku_cnname.attr_id', 'LEFT' )
                         ->where( array( 'sku' => $sku ) )
                         ->getField( 'name' );
        } elseif ( is_array( $sku ) ) {
            $where = array( 'sku' => array( 'IN', $sku ) );
            return $model->join( 'skusystem_cnname ON skusystem_cnname.id=skusystem_sku_cnname.attr_id', 'LEFT' )
                         ->where( $where )
                         ->field( 'sku, name' )
                         ->select();
        } else {
            return $model->join( 'skusystem_cnname ON skusystem_cnname.id=skusystem_sku_cnname.attr_id', 'LEFT' )
                         ->field( 'sku, name' )
                         ->select();
        }
    }

    /**
     * 获取sku的状态和所在仓库
     *
     * @param array $skus
     *
     * @return mixed
     */
    public function get_sku_warehouse_and_status( &$skus = array() )
    {
        $model = M( 'statuss', 'skusystem_', $this->_db );

        $where_map = array(
            'skusystem_place_skus.sku'   => array( 'IN', $skus ),
            'skusystem_status_skuss.sku' => array( 'IN', $skus ),
        );

        return $model->join( 'skusystem_status_skuss ON skusystem_status_skuss.status_id=skusystem_statuss.id', 'LEFT' )
                     ->join( 'skusystem_place_skus ON skusystem_place_skus.sku=skusystem_status_skuss.sku', 'LEFT' )
                     ->join( 'skusystem_places ON skusystem_places.id=skusystem_place_skus.place_id', 'LEFT' )
                     ->join( 'skusystem_warehouse ON skusystem_warehouse.id=skusystem_places.warehouse_id', 'LEFT' )
                     ->where( $where_map )
                     ->field( 'skusystem_place_skus.sku, skusystem_statuss.status, skusystem_warehouse.location' )
                     ->find();
    }
}