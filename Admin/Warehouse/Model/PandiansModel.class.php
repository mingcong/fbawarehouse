<?php
/**
 * Created by PhpStorm.
 * User: khq
 * Date: 17-3-9
 * Time: 下午3:37
 */
namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
class PandiansModel extends CommoninterfaceModel
{

    public $obj = NULL;

    static $table = 'pandians';

    //数据库对象
    public $pandians = NULL;
    
    //数据
    public $data = array();

    /**
     * PrepareneedsdetailModel constructor.
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '', $param = array())
    {
        $this->pandians = M(self::$table, 'wms_', $this->_db);
        parent::__construct();
    }

    public function update($_array, $_param, &$model = NULL)
    {
        return parent::update($_array, $_param, $this->model->table("$this->_db." . "wms_" . self::$table)); // TODO:
    }

    /**
     * @param array $_array -
     * @return mixed
     * 批量创建盘点数据
     * khq 2017.3.9
     */
    public function batch_create(&$_array = array())
    {
        return parent::batch_create($_array, $this->model->table("$this->_db." . "wms_" . self::$table)); // TODO:
    }
}