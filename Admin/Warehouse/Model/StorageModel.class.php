<?php

namespace Warehouse\Model;

use Think\Model;
use Inbound\Model\CommoninterfaceModel;

class StorageModel extends CommoninterfaceModel
{
    //数据库
    public $_db = 'fbawarehouse';

    public $storage = NULL;
    public $storage_sku = NULL;

    //查询记录总数
    public $count           = 0;
    //页面展示包含页面样式
    public $page            = 0;

    static $table_storage = 'storage';
    static $table_storage_sku = 'storage_sku';



    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        parent::__construct();
        $this->storage = M(self::$table_storage,'wms_',$this->_db);
        $this->storage_sku = M(self::$table_storage_sku,'wms_',$this->_db);
    }


    /**
     * @param array $_array
     * @return array
     * 添加数据
     */
    public function add_storage($_array = array())
    {
        $result = $this->storage->add($_array);
        return $result;
    }


    /**
     * @param array $_array
     * @param bool $flag
     * @return array
     * 获取储位数据列表
     */
    public function get_storage_data($where = array())
    {
        //过滤掉数组中的空元素，但保留0，有些查询需要用到0
        $where = array_filter($where, function($val) {
            if ($val === '') {   
                return false;      
            }      
            return true; 
        });

        //绑定sku查询
        if (isset($where['bind_sku']) && $where['bind_sku'] != '') {
            $storage_position_arr = $this->storage_sku->field('storage_position')->where(array('sku' => $where['bind_sku']))->select();
            if (!$storage_position_arr) {
                return array();
            }
            $storage_position_arr = i_array_column($storage_position_arr, 'storage_position');
            $where['storage_position'] = array('in', $storage_position_arr);
            unset($where['bind_sku']);
        }

        //储位条数
        $this->count = $this->storage->where($where)->count();   
        $Page = new \Org\Util\Page($this->count,20);    
        $this->page = $Page->show();

        //结果集
        $result = $this->storage->where($where)->order("create_time desc")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        $result = $result ? $this->format_storage_data($result) : array();
        return $result;
    }


    /**
     * @param $data
     * @return mixed
     * 格式化储位数据
     */
    public function format_storage_data($data)
    {
        $storage_postion_arr = i_array_column($data, 'storage_position');
        $storage_sku = $this->storage_sku->field('storage_position, sku')
                            ->where(array('storage_position' => array('in', $storage_postion_arr)))
                            ->select();

        $bind_sku = array();
        for ($i = 0; $i < count($data); $i++) { 
            $data[$i]['bind_sku'] = '';
            for ($j = 0; $j < count($storage_sku); $j++) { 
                if ($storage_sku[$j]['storage_position'] == $data[$i]['storage_position']) {
                    $data[$i]['bind_sku'] .= $storage_sku[$j]['sku'] . ',';
                }
            }
            $data[$i]['bind_sku'] = rtrim($data[$i]['bind_sku'], ',');
        }
        return $data;
    }



    /**
     * @param array $where
     * @return array
     * 根据条件获取储位数据
     */
    public function get_storage_by_where($where)
    {
        $data = $this->storage->where($where)->find();
        return $data;
    }


    /**
     * @param array $data
     * @param array $where
     * @return array
     * 保存储位数据
     */
    public function save_storage($data, $where)
    {
        if ($where) {
           $result = $this->storage->where($where)->save($data);
        }
        return $result;
    }


    /**
     * @param array $where
     * @return array
     * 根据条件获取储位sku绑定数据
     */
    public function get_storage_sku_by_where($where, $field = '*')
    {
        $data = $this->storage_sku->field($field)->where($where)->select();
        return $data;
    }


    /**
     * @param array $data
     * @param array $where
     * @return array
     * 根据条件更新储位sku绑定数据
     */
    public function save_storage_sku($data, $where)
    {
        $result = $this->storage_sku->where($where)->save($data);
        return $result;
    }



    /**
     * @param array $_array
     * @return array
     * 储位绑定sku
     */
    public function add_storage_sku($data)
    {
        $result = $this->storage_sku->addAll($data);
        return $result;
    }


    /**
     * @param array $where
     * @return array
     * 删除储位绑定sku
     */
    public function del_storage_sku($where)
    {
       $result = $this->storage_sku->where($where)->delete();
        return $result;
    }


    /**
     * @param array $where
     * @return array
     * 查询sku库数量
     */
    public function get_sku_count($where)
    {
        $count = M('skus',' ','DB_SKU')->where($where)->count();
        return $count;
    }


    /**
     * @param array $where
     * @return array
     * 根据条件查询sku库
     */
    public function get_sku_info($where, $field = '*')
    {
        $result = M('skus',' ','DB_SKU')->field($field)->where($where)->select();
        return $result;
    }


    /**
     * @param where $where
     * @return int
     * 获取储位入库可使用量
     */
    public function get_storage_availiable($where)
    {
        $sum = M('warehouseorders', 'wms_', $this->_db)->where($where)->sum('available_quantity');
        return $sum ? $sum : 0;
    }

    /**
     * @param $where
     * @return mixed
     * 根据站点,公司主体分组获取可用库存
     * khq 2017.4.11
     */
    public function get_site_availiable($where){
         return M('warehouseorders', 'wms_', $this->_db)
            ->where($where)
            ->group('site_id,storage_position,enterprise_dominant,export_tax_rebate')
            ->field("sum(available_quantity) as available_quantity,storage_position,site_id,export_tax_rebate,enterprise_dominant,sku_name")
            ->order('warehouse_date ASC')
            ->select();
    }

    /**
     * @param $where -公司主体,退税方式,sku
     * @return mixed
     */
    public function get_all_site_availiable($where){
        
        return M('warehouseorders', 'wms_', $this->_db)
            ->where($where)
            ->group('site_id')
            ->field("sum(available_quantity) as available_quantity,site_id")
            ->order('warehouse_date ASC')
            ->select();
        
    }

    /**
     * @param array $where
     * @param string $field
     * @return array
     * 根据条件获取储位列表
     */
    public function get_storage_lists($where, $field = '*')
    {
        $data = $this->storage->field($field)->where($where)->select();
        return $data;
    }


    /**
     * @param $where
     * @return mixed
     * 分组获取入库单数据
     */
    public function get_move_warehouseorders($where = array(), $field = '*', $group){
        return M('warehouseorders', 'wms_', $this->_db)
            ->field($field)
            ->where($where)
            ->group($group)
            ->select();
    }
}