<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;
use Inbound\Service\PrepareneedsService;

class PurchaseOrderDetailsSitesModel extends PurchaseOrderDetailsModel
{
    //数据库对象
    public $purchaseorder_details_sites = NULL;
    //采购订单站点明细表
    static $table = 'purchaseorder_details_sites';

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->purchaseorder_details_sites = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     *更新操作
     */
    public  function  update_purchaseorder_details_sites($_array = array(),$data = array(),&$model = NULL){
        $model = $model == NULL? $this->purchaseorder_details_sites : $model->table($this->_db.".wms_".self::$table);
        $res = $model->where($_array)->save($data);
        return $res;
    }


    /**
     *更新 加 操作
     */
    public  function  setInc_purchaseorder_details_sites($_array = array(),$data = array(),&$model = NULL){
        $model = $model == NULL? $this->purchaseorder_details_sites : $model->table($this->_db.".wms_".self::$table);
        $res = $model->where($_array)->setInc($data['column'],$data['data']);
        return $res;
    }

    /**
     *更新 减 操作
     */
    public  function  setDec_purchaseorder_details_sites($_array = array(),$data = array(),&$model = NULL){
        $model = $model == NULL? $this->purchaseorder_details_sites : $model->table($this->_db.".wms_".self::$table);
        $res = $model->where($_array)->setDec($data['column'],$data['data']);
        return $res;
    }
}