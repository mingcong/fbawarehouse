<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/3/6
 * Time: 14:47
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;

class FbaPrepareNeedsDetailsModel extends CommoninterfaceModel
{

    //数据库对象
    public $prepare_needs_details = NULL;
    //收货明细表
    static $table = 'prepare_needs_details';

    public $prepare_needs_details_id;


    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->prepare_needs_details = M(self::$table,'fba_',$this->_db);
        parent::__construct();
    }

    /**
     * sku备货占用未发数量
     * @param     $sku
     * @param int $tax_flag 是否出口退税
     * @return mixed
     */
    public function get_sku_occupied_num(&$_array, $tax_flag=0)
    {
        $model = $this->prepare_needs_details;

        $where_map = array(
            'sku'                  =>  $_array['sku'],
            'enterprise_dominant'  =>  $_array['enterprise_dominant'],
            'site_id'              => $_array['site_id'],
            'export_tax_rebate'    =>  $tax_flag,
            'status'               =>  array(array('ELT', 30),array('EGT', 10)),
        );
        $res = $model
            ->where($where_map)
            ->getField('SUM(needs_quantity) as occupiedNum');
        return $res;
    }

}