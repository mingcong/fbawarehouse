<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/3/3
 * Time: 09:17
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;

class RealInventoriesModel extends CommoninterfaceModel
{

    //数据库对象
    public $real_inventories = NULL;
    //收货明细表
    static $table = 'real_inventories';

    public $real_inventories_id;


    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->real_inventories = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_real_inventories_model(&$_array = array(),$flag = TRUE)
    {
        if($flag){
            if($_array){
                $_array = array_filter($_array);
            }
            if(!empty($_array["print_status"]) and '-1'==$_array["print_status"]){
                $_array["print_status"] = 0;
            }
            if(!empty($_array["export_tax_rebate"]) and '-1'==$_array["export_tax_rebate"]){
                $_array["export_tax_rebate"] = 0;
            }
            $_array = PrepareneedsService::trim_array($_array);
            foreach($_array as $key => $_arr){
                switch($key){
                    case 'create_time_from':
                        $_array['invoice_date'][] = array(
                            'egt',
                            $_arr
                        );
                        unset($_array['create_time_from']);
                        break;
                    case 'create_time_to':
                        $_array['invoice_date'][] = array(
                            'elt',
                            $_arr
                        );
                        unset($_array['create_time_to']);
                        break;
                }
            }
            return $this->real_inventories->where($_array);
        }else{
            return $this->real_inventories->where($_array);
        }
    }

    /**
     * 非出口退税
     * 获取实时库存
     * @param $sku
     * @return mixed
     */
    public function get_sku_real_inventories(&$_array)
    {
        $model = $this->real_inventories;
        $where_map = array(
            'sku'   =>  $_array['sku'],
            'enterprise_dominant'   => $_array['enterprise_dominant'],
            'site_id'=> $_array['site_id'],
            'export_tax_rebate' => 0,
        );
        $res = $model->where($where_map)
            ->getField('quantity');
        return $res;
    }

    /**
     * 出口退税
     * 获取实时库存
     * @param $sku
     * @return mixed
     */
    public function get_tax_sku_real_inventories(&$_array)
    {
        $model = $this->real_inventories;
        $where_map = array(
            'sku'   =>  $_array['sku'],
            'enterprise_dominant'   => $_array['enterprise_dominant'],
            'site_id'=> $_array['site_id'],
            'export_tax_rebate' => 1,
        );
        $res = $model->where($where_map)
            ->getField('quantity');
        return $res;
    }

    /**
     * @param $_array
     * @return mixed
     * 统计相同sku,主体,退税方式下的所有站点的实时库存
     * khq 2017.4.16
     */
    public function get_Site_Quantity(&$_array)
    {
        $model = $this->real_inventories;
        $where_map = array(
            'sku'   =>  $_array['sku'],
            'enterprise_dominant'   => $_array['enterprise_dominant'],
            'export_tax_rebate' => $_array['export_tax_rebate'],
        );
        
        return $model->where( $where_map )->group( 'site_id' )
            ->select();
    }
}