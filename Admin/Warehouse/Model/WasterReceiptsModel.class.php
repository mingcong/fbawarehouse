<?php
/**
 * Created by PhpStorm.
 * User: khq
 * Date: 17-3-15
 * Time: 下午22:30
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
class WasterReceiptsModel extends CommoninterfaceModel
{

    public $obj             = NULL;

    static $table           = 'waster_receipts';

    //数据库对象
    public $wasterReceipts     = NULL;

    //数据
    public $data            = array();

    /**
     * PrepareneedsdetailModel constructor.
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '',$param=array()){

        $this->wasterReceipts = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }
    public function create(&$_array = array())
    {
        $this->wasterReceipts_id = parent::create($_array, $this->model->table("$this->_db."."wms_".self::$table));
    }
    public function update($_array, $_param, &$model = NULL) {
        return parent::update($_array, $_param, $this->model->table("$this->_db."."wms_".self::$table)); // TODO:
    }
}