<?php

/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:50
 */
namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;
use Warehouse\Service\PublicdataService;

class PurchaseOrdersModel extends CommoninterfaceModel
{
    //数据库对象
    public $Purchaseorders = NULL;
    //采购单表
    static $table = 'purchaseorders';
    //数据
    public $Purchaseorders_data = NULL;
    //采购单ID
    public $Purchaseorders_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->Purchaseorders = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @return mixed
     * 过滤查询条件
     */
    public function get_purchaseorders_model(&$_array = array(),$flag)
    {
        if($_array){
            $_array = array_filter($_array);
        }else{

        }
        $this->Purchaseorders->table('wms_purchaseorders a')
            ->join('wms_purchaseorder_details b on a.id = b.purchaseorder_id')
            ->join('wms_purchaseorder_details_sites c on a.id = c.purchaseorders_id and b.id=c.purchaseorder_details_id')
            ->field('a.id,a.purchaseid,a.purchase_id,a.`supplier_id`,a.store,a.enterprise_dominant,a.transfer_type,a.`supplier_id`,
            a. `export_tax_rebate`,a.`store`,a.`purchase_time`, a.`createtype`,a.`remark`,a.`status` as mainstatus,
            a.transfer_hopper_id,a.warehouseid as mainwarehouseid,a.crontrigger as maincrontrigger,
            b.`sku_standard`,b.`status`,b.`quantity` as total_quantity,b.`recieve_quantity` as total_recieve_quantity,
            b.`ware_quantity` as total_ware_quantity,b.`return_quantity` as total_return_quantity,b.`check_quantity`,
            b.`remark` as details_remark,b.warehouseid,b.crontrigger,b.storage_position,b.id as detailsid, b.`sku`,
            b.`sku_name`,c.id as cid,c.site_id,c.quantity,c.`recieve_quantity`,c.`check_quantity`,c.`return_quantity`,
            c.`ware_quantity`')
            ->where("a.status>=10 and a.status<=20 and b.status>=10 and b.status<70");
        /*if($flag){
            //$this->Purchaseorders->where("a.status>=10 and a.status<=20 and b.status>=10 and b.status<70");
        }else{
            //$this->Purchaseorders->where("a.status=20 and b.status>=20 and b.status<50");
        }*/
        $_array = PrepareneedsService::trim_array($_array);//去除两端空格
        foreach($_array as $key => $_arr){
            switch($key){
                case 'purchase_time_from':
                    $_array['a.purchase_time'][] = array(
                        'egt',
                        $_arr
                    );
                    unset($_array['purchase_time_from']);
                    break;
                case 'purchase_time_to':
                    $_array['a.purchase_time'][] = array(
                        'elt',
                        $_arr
                    );
                    unset($_array['purchase_time_to']);
                    break;
                case 'sku':
                    $_array['b.sku'] = $_arr;
                    unset($_array['sku']);
                    break;
                case 'supplier_id':
                    $_array['a.supplier_id'] = PublicdataService::get_supplier_id(trim($_arr));
                    unset($_array['supplier_id']);
                    break;
                case 'purchase_id':
                    $_array['a.purchase_id'] = PublicdataService::get_user_id(trim($_arr));
                    unset($_array['purchase_id']);
                    break;
                case 'id':
                    $_array['a.id'] = $_arr;
                    unset($_array['id']);
                    break;
                case 'detailsid':
                    $_array['b.id'][] = array(
                        'in',
                        $_arr
                    );
                    unset($_array['detailsid']);
                    break;
                case 'cid':
                    $_array['c.id'][] = array(
                        'in',
                        $_arr
                    );
                    unset($_array['cid']);
                    break;

            }
        }
        return $this->Purchaseorders->where($_array);
    }


    /**
     *更新操作
     */
    public function update_purchaseorders($_array = array(),$data = array(),&$model = NULL)
    {
        $model = $model==NULL?$this->Purchaseorders:$model->table($this->_db.".wms_".self::$table);
        $res   = $model->where($_array)
            ->save($data);
        return $res;
    }

    /**
     * 非出口退税
     * 获取在途库存
     * @param sku
     * @return mixed
     */
    public function onway_inventory(&$_array)
    {
        $model = $this->Purchaseorders;
        $where_map = array(
            'wms_purchaseorder_details.sku'                     =>      $_array['sku'],
            'wms_purchaseorders.enterprise_dominant'            =>      $_array['enterprise_dominant'],
            'wms_purchaseorders.store'                          =>      $_array['site_id'],
            'wms_purchaseorder_details.status'                  =>      array('NOT IN', array(10,70,90,100)),
            'wms_purchaseorders.status'                         =>      array('NOT IN', array(10,70,90,100)),
            'wms_purchaseorders.export_tax_rebate'              =>      0,
        );

        $res = $model
            ->join('wms_purchaseorder_details ON wms_purchaseorders.id=wms_purchaseorder_details.purchaseorder_id','LEFT')
            ->where($where_map)
            ->getField('SUM( wms_purchaseorder_details.`quantity` - wms_purchaseorder_details.`ware_quantity`) AS onwayNum');
        return $res;
    }

    /**
     * 出口退税
     * 获取在途库存
     * @param sku
     * @return mixed
     */
    public function tax_onway_inventory(&$_array)
    {
        $model = $this->Purchaseorders;
        $where_map = array(
            'wms_purchaseorder_details.sku'                     =>      $_array['sku'],
            'wms_purchaseorders.enterprise_dominant'            =>      $_array['enterprise_dominant'],
            'wms_purchaseorders.store'                          =>      $_array['site_id'],
            'wms_purchaseorder_details.status'                  =>      array('NOT IN', array(10,70,90,100)),
            'wms_purchaseorders.status'                         =>      array('NOT IN', array(10,70,90,100)),
            'wms_purchaseorders.export_tax_rebate'              =>      1,
        );
        $res = $model
            ->join('wms_purchaseorder_details ON wms_purchaseorders.id=wms_purchaseorder_details.purchaseorder_id','LEFT')
            ->where($where_map)
            ->getField('SUM( wms_purchaseorder_details.`quantity` - wms_purchaseorder_details.`ware_quantity`) AS onwayNum');
        return $res;
    }

    /**
     * @param array $privateSku
     * @return mixed
     * 根据SKU数组查询在途库存
     */
    public function getInternalOnWayStock ($privateSku = array()) {
        $model = $this->Purchaseorders;
        $where_map = array(
            'wms_purchaseorder_details.sku'                     =>      array('IN', array_unique($privateSku)),
            'wms_purchaseorders.status'                         =>      array('NOT IN', array(10,70,90,100)),
            'wms_purchaseorder_details.status'                  =>      array('NOT IN', array(10,70,90,100)),
        );
        return $model
            ->join('wms_purchaseorder_details ON wms_purchaseorders.id=wms_purchaseorder_details.purchaseorder_id','LEFT')
            ->where($where_map)
            ->group(
                'wms_purchaseorder_details.sku,
                wms_purchaseorders.enterprise_dominant'
            )->field(
                'wms_purchaseorder_details.sku,
                wms_purchaseorders.enterprise_dominant,
                SUM( wms_purchaseorder_details.`quantity` - wms_purchaseorder_details.`ware_quantity`) AS onwayNum'
            )->select();
    }

}