<?php

namespace Warehouse\Model;

use Think\Model;
use Inbound\Model\CommoninterfaceModel;

class ShiftrecordModel extends CommoninterfaceModel
{
    //数据库
    public $_db = 'fbawarehouse';

    //查询记录总数
    public $count           = 0;
    //页面展示包含页面样式
    public $page            = 0;

    static $table_shiftrecord = 'shiftrecord';



    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        parent::__construct();
        $this->shiftrecord = M(self::$table_shiftrecord,'wms_',$this->_db);
    }




    /**
     * @param array $where
     * @return array
     * 获取库存移位记录数据列表
     */
    public function get_shiftrecord_data($where = array())
    {
        //过滤掉数组中的空元素，但保留0，有些查询需要用到0
        $where = array_filter($where, function($val) {
            if ($val === '') {   
                return false;      
            }      
            return true; 
        });

        //记录条数
        $this->count = $this->shiftrecord->where($where)->count();   
        $Page = new \Org\Util\Page($this->count,20);    
        $this->page = $Page->show();

        //结果集
        $result = $this->shiftrecord->where($where)->order("create_time desc")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        return $result;
    }

}