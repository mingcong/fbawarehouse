<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/2/23
 * Time: 16:58
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;

class AttrLogModel extends CommoninterfaceModel
{
    //数据库对象
    public $attrLog = NULL;
    //属性确认日志表
    static $table = 'attr_log';
    //数据
    public $recieveDetails_data = NULL;
    //采购单明细表ID
    public $attrLogId;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        $this->attrLog = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     *插入物流属性确认日志
     */
    public function addLog(&$_array = array())
    {
        $data = array(
            'purchaseorder_id'  =>  $_array['poId'],
            'recieve_detail_id'  =>  $_array['recieveDetailId'],
            'att_time'  =>  date('Y-m-d H:i:s'),
            'user_id'   =>  $_SESSION['current_account']['id'],
            'sku'   =>  $_array['sku'],
            'logic_v'   =>  $_array['skuLogistic'],
        );
        $this->attrLog->add($data);
    }
}