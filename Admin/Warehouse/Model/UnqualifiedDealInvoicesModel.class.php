<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/2/25
 * Time: 15:37
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;

class UnqualifiedDealInvoicesModel extends CommoninterfaceModel
{
    //数据库对象
    public $unqualified_deal_invoices = NULL;
    //收货明细表
    static $table = 'unqualified_deal_invoices';
    //数据
    public $unqualified_deal_invoices_data = NULL;
    //收货明细表ID
    public $unqualified_deal_invoices_id;

    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '',$param = array())
    {
        //print_r($this->_db);exit;
        $this->unqualified_deal_invoices = M(self::$table,'wms_',$this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     * @param null  $model
     * @return mixed
     * 插入收货明细表
     */
    public function add_unqualified_deal_invoices(&$_array = array(),$model = NULL)
    {
        $model = $model==NULL?$this->unqualified_deal_invoices:$model->table($this->_db.".wms_".self::$table);

        $this->unqualified_deal_invoices_id = $model->table($this->_db.'.wms_'.self::$table)
            ->add($_array);
        return $this->unqualified_deal_invoices_id;
    }

    /**
     *更新操作
     */
    public function update_unqualified_deal_invoices($_array = array(),$data = array(),&$model = NULL)
    {
        $model = $model==NULL?$this->unqualified_deal_invoices:$model->table($this->_db.".wms_".self::$table);
        $res   = $model->where($_array)
            ->save($data);
        return $res;
    }
    /**
     * @param $id
     * @return mixed
     * 根据不合格处理单id获取指定信息
     * khq 2017.3.6
     */
    public function get_purchase_man_by_param($id,$param)
    {
        return $this->unqualified_deal_invoices
            ->where(array('id'=>$id))
            ->getField($param);
    }
    public function update($_array, $_param, &$model = NULL)
    {
        return parent::update($_array, $_param, $this->model->table("$this->_db."."wms_".self::$table)); // TODO:
    }

}