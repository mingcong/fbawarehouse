<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/2/25
 * Time: 12:08
 */

namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
use Inbound\Service\PrepareneedsService;
use Warehouse\Service\PublicdataService;

class CheckQualityDetailsModel extends CommoninterfaceModel
{
    //数据库对象
    public $checkQualityDetails = null;
    //收货明细表
    static $table = 'check_quality_details';

    public $check_quality_details_id;


    /**
     * @param string $table
     * @param array  $param
     */
    public function __construct($table = '', $param = array())
    {
        $this->checkQualityDetails = M(self::$table, 'wms_', $this->_db);
        parent::__construct();
    }

    /**
     * @param array $_array
     *
     * @return mixed
     * 过滤查询条件
     */
    public function get_check_details_model(&$_array = array(), $flag = true)
    {
        if ($flag) {
            if ($_array) {
                $_array = array_filter($_array);
            }
            if (!empty($_array["print_status"]) and '-1' == $_array["print_status"]) {
                $_array["print_status"] = 0;
            }
            if (!empty($_array["export_tax_rebate"]) and '-1' == $_array["export_tax_rebate"]) {
                $_array["export_tax_rebate"] = 0;
            }
            $_array = PrepareneedsService::trim_array($_array);
            foreach ($_array as $key => $_arr) {
                switch ($key) {
                    case 'create_time_from':
                        $_array['invoice_date'][] = array(
                            'egt',
                            $_arr
                        );
                        unset($_array['create_time_from']);
                        break;
                    case 'create_time_to':
                        $_array['invoice_date'][] = array(
                            'elt',
                            $_arr
                        );
                        unset($_array['create_time_to']);
                        break;
                    case 'supplier_id':
                        $_array['supplier_id'] = PublicdataService::get_supplier_id($_array['supplier_id']);
                        break;
                    case 'sku':
                        $_array['wms_check_quality_details.sku'] = $_array['sku'];
                        unset($_array['sku']);
                        break;
                    case 'batch_code':
                        $_array['wms_check_quality_details.batch_code'] = $_array['batch_code'];
                        unset($_array['batch_code']);
                        break;
                    case 'purchaseorder_id':
                        $_array['wms_check_quality_details.purchaseorder_id'] = $_array['purchaseorder_id'];
                        unset($_array['purchaseorder_id']);


                        break;
                }

            }
            return $this->checkQualityDetails->where($_array);
        } else {
            return $this->checkQualityDetails->where($_array);
        }
    }

    /**
     * 添加数据
     *
     * @param array $_array
     * @param null  $model
     */
    public function add_check_quality_details(&$_array = array(), $model = null)
    {
        $model = $model == null ? $this->checkQualityDetails : $model->table($this->_db.".wms_".self::$table);

        $this->check_quality_details_id = $model->table($this->_db.'.wms_'.self::$table)
            ->add($_array);
        return $this->check_quality_details_id;
    }

    /**
     * 添加数据
     *
     * @param array $_array
     * @param null  $model
     */
    public function addAll_check_quality_details(&$_array = array(), $model = null)
    {
        $model = $model == null ? $this->checkQualityDetails : $model->table($this->_db.".wms_".self::$table);

        $this->check_quality_details_id = $model->table($this->_db.'.wms_'.self::$table)
            ->addAll($_array);
        return $this->check_quality_details_id;
    }

    /**
     * 更新表
     *
     * @param array $_array
     * @param array $data
     * @param null  $model
     *
     * @return mixed
     */
    public function update_check_quality_details(&$_array = array(), &$data = array(), $model = null)
    {
        $model = $model == null ? $this->checkQualityDetails : $model->table($this->_db.".wms_".self::$table);
        $res   = $model->where($_array)
            ->save($data);
        return $res;
    }

    /**
     * 获取质检明细表和采购单表的数据
     *
     * @param $_array
     *
     * @return mixed
     */
    public function get_check_detail_and_purchase_info(&$_array)
    {
        $model = $this->checkQualityDetails;

        $res = $model->join(
            'wms_purchaseorders ON wms_purchaseorders.id=wms_check_quality_details.purchaseorder_id', 'LEFT'
        )
            ->where($_array)
            ->field(
                'wms_check_quality_details.*, wms_purchaseorders.*, wms_check_quality_details.remark AS check_remark'
            )
            ->select();
        return $res;
    }
    /**
     * @param $id
     * @return mixed
     * 根据质检明细id获取质检信息
     * khq 2017.3.16
     */
    public function check_detail_by_id($id) {
        return $this->checkQualityDetails
            ->where(array('id'=>$id))
            ->find();
    }


}