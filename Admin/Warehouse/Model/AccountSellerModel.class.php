<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-14
 * Time: 上午9:09
 */
namespace Warehouse\Model;

use Inbound\Model\CommoninterfaceModel;
class AccountSellerModel extends CommoninterfaceModel {
    public $AccountSeller   = null;
    static $table           = 'fba_account_sellers';
    public $accounts        = null;
    //销售员ID
    public $seller_ids      = '';
    //数据
    public $data = array();
    /**
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '',$param=array()){

        $this->AccountSeller= M(self::$table,'',$this->_db);
        parent::__construct();
    }

}