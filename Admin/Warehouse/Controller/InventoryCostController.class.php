<?php
namespace Warehouse\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
use Inbound\Service\PublicPlugService;
use Warehouse\Service\BaseInfoService;
use Warehouse\Service\PublicdataService;
class InventoryCostController extends CommonController{
    public $inventory = NULL;
    public $stockOut = NULL;
    public $stockIn = NULL;


    public function __construct() {
        parent::__construct();
        $this->inventory    = D('Inventory', 'Service');
        //$this->storage      = D('Storage', 'Model');
        $this->stockIn      = D('StockIn', 'Service');


    }

    public function index() {
        $stock_in_type = BaseInfoService::stock_in_type();
        $stock_out_type = BaseInfoService::stock_out_type();
        $flag = false;

        $type_length['in'] = count($stock_in_type);
        $type_length['out'] = count($stock_out_type);

        if ($_GET) {
            if($_GET['download']) {
                $flag = true;
                unset($_GET['download']);
            }
            $inventoryCostDetail = $this->inventory->getInventoryCostDetail($_GET, $flag);

            if ($flag) {
                $this->inventory->downloadInventoryCostDetail($inventoryCostDetail);
            }

            $this->assign('inventoryCostDetail',$inventoryCostDetail);
            $this->assign('page',$this->inventory->page);
            $this->assign('count',$this->inventory->count);
        }

        $this->assign('stock_in_type',$stock_in_type);
        $this->assign('stock_out_type',$stock_out_type);
        $this->assign('type_length',$type_length);
        $this->assign('enterprise_dominant',PublicInfoService::get_company_array());

        $this->display();
    }

    public function upload_cost() {
        $sheetData = PublicPlugService::parse_excel();                            //解析excel内容
        if(!$sheetData) {
            echo "<script>alert('文件内容解析有误，请检查文件内容！');history.go(-1)</script>";
            exit;
        }
        if(!PublicPlugService::$flag){                                            //选择了上传文件,状态返回false
            if(count($sheetData)>=2){
                $check_result = $this->stockIn->check_upload_data($sheetData);
                if (!$check_result['status']) {
                    echo "<script>alert('". $check_result['message'] ."');history.go(-1)</script>";
                    exit;
                } else {
                    $update_date = $this->stockIn->check_upload_cost($sheetData);
                    $message = $this->stockIn->update_cost($update_date);
                    echo "<script>alert('". $message ."');history.go(-1)</script>";
                    exit;
                }

            } else {
                echo "<script>alert('文件为空，请检查文件内容！');history.go(-1)</script>";
                exit;
            }
        } else {
            echo "<script>alert('请检上传文件！');history.go(-1)</script>";
            exit;
        }
    }

    public function warehouseCostDownload() {
        set_time_limit(0);
        ini_set('memory_limit','1024M');
        vendor('PHPExcel.PHPExcel');

        if ($_GET['fileName'] && $_GET['warehouse_date']) {
            $month = $_GET['warehouse_date'];
            $fileName = $_GET['fileName'];

            $result = $this->stockIn->get_cost_zero_value($month);

            $PHPExcel = new \PHPExcel();

            $firstLine = array(
                "id" => "入库单明细ID",
                "warehouse_date" => "入库时间",
                "sku" => "SKU",
                "enterprise_dominant" => "主体",
                "warehouse_quantity" => "入库数量",
                "type" => "入库类型",
                "cost" => "入库成本单价",
            );
            $objActSheet =$PHPExcel->setActiveSheetIndex(0);
            $objActSheet->setTitle($fileName);

            $r = 'A';
            foreach($firstLine as $v){
                $objActSheet->setCellValue($r.'1',$v);
                $r++;
            }
            $i = 2;
            foreach($result as $value)
            {
                /* excel文件内容 */
                $j = 'A';
                foreach ($firstLine as $key => $v) {
                    $objActSheet->setCellValue($j.$i,$value[$key]);
                    $j++;
                }
                $i++;
            }

            ob_end_clean();
            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
            header('Content-Disposition: attachment;filename='.$fileName.'.xls');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
            $objWriter->save('php://output');
        } else {
            echo "<script>alert('上传失败，请重试！');history.go(-1)</script>";
            exit;
        }
    }
}