<?php

namespace Warehouse\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
use Inbound\Service\PublicPlugService;
use Warehouse\Service\PublicdataService;


class StockOutController extends CommonController
{
    //服务层对象
    public $stockOut = NULL;

    //条件
    public $condition = array();

    //标志符
    public $flag;

    /**
     * StockOutController constructor.
     */
    public function __construct()
    {
        $this->stockOut = D('StockOut','Service');
        parent::__construct();
    }

    /**
     * 出库首页
     * 批量生成出库单
     */
    public function  index(){
        $deliveryorder_types = PublicdataService::get_deliveryorders_type();
        unset($deliveryorder_types[10]);//排除正常销售出库
        unset($deliveryorder_types[150]);//站点调拨
        unset($deliveryorder_types[130]);//移位出库
        $this->assign('sites',PublicInfoService::get_site_array());
        $this->assign('enterprise_dominant',PublicInfoService::get_company_array());
        $this->assign('deliveryorder_types',$deliveryorder_types);
        $this->display();
    }

    /**
     * 根据sku获取sku信息
     */
    public function load_sku_info(){
//        $sku_info = $this->stockOut->load_sku_info(trim($sku));
//        print_r($sku_info);
        if($_POST){
            if($_POST['site_id']!=''){
                $where['site_id'] = $_POST['site_id'];
            }
            if($_POST['export_tax_rebate']!=''){
                $where['export_tax_rebate'] = $_POST['export_tax_rebate'];
            }
            if($_POST['enterprise_dominant']!=''){
                $where['enterprise_dominant'] = $_POST['enterprise_dominant'];
            }
            $where['sku'] = $_POST['sku'];
            $res = M('real_inventories','wms_','fbawarehouse')->where($where)->find();
            $rec = D('Inventory', 'Service')->format_inventories_data(array($res),false);
            if($res){
                echo json_encode(array(
                    'flag'     => 1,
                    'info' => $rec[0],
                ));
            }else{
                echo json_encode(array(
                    'status'     => -1,
                ));
            }
        }elsE{
            $sku_info = $this->stockOut->load_sku_info(trim($_GET['sku']));
            if($sku_info){
                echo json_encode(array(
                    'flag'     => 1,
                    'sku_info' => $sku_info,
                ));
            }else{
                echo json_encode(array(
                    'flag'     => -1,
                ));
            }
        }

    }


    /**
     * 创建特殊出库单
     */
    public function create(){
        $res = $this->stockOut->create_formate($_GET);
        if(array_key_exists('status', $res[0])){
            echo json_encode(array(
                'flag'     => 1,
                'create_info' => $res[0]['msg'],
            ));
        }else{
            echo json_encode(array(
                'flag'     => -1,
                'create_info' =>$res
            ));
        }
    }

    /**
     *下载上传出库单的模板
     */
    public function down_template()
    {
        set_time_limit(0);
        vendor('PHPExcel.PHPExcel.IOFactory');
        $this->objReader  = \PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel      = new \PHPExcel();

        $objActSheet =$objPHPExcel->setActiveSheetIndex(0);
        $objActSheet->setCellValue('A1', '出库类型');
        $objActSheet->setCellValue('B1', 'SKU');
        $objActSheet->setCellValue('C1', '数量');
        $objActSheet->setCellValue('D1', '出库时间');
        $objActSheet->setCellValue('E1', '储位');
        $objActSheet->setCellValue('F1', '备注');
        $objActSheet->setCellValue('G1', '站点');
        $objActSheet->setCellValue('H1', '公司主体');
        $objActSheet->setCellValue('I1', '是否退税');

        $objActSheet->setCellValue('A2', '盘亏');
        $objActSheet->setCellValue('B2', 'A404');
        $objActSheet->setCellValue('C2', '20');
        $objActSheet->setCellValue('D2', '2017-03-04');
        $objActSheet->setCellValue('E2', 'MH-XXXX');
        $objActSheet->setCellValue('F2', '我是备注');
        $objActSheet->setCellValue('G2', 'US');
        $objActSheet->setCellValue('H2', '深圳市有棵树科技股份有限公司');
        $objActSheet->setCellValue('I2', '出口退税');

        $objActSheet->setCellValue('A3', '礼物送人');
        $objActSheet->setCellValue('B3', 'A401');
        $objActSheet->setCellValue('C3', '25');
        $objActSheet->setCellValue('D3', '2017-03-04');
        $objActSheet->setCellValue('E3', 'MH-XXXX');
        $objActSheet->setCellValue('F3', '我是备注1');
        $objActSheet->setCellValue('G3', 'GB');
        $objActSheet->setCellValue('H3', '深圳市有棵树电子商务有限公司');
        $objActSheet->setCellValue('I3', '非出口退税');

        $objPHPExcel->getActiveSheet()->setTitle('delivery_special');
        $objPHPExcel->setActiveSheetIndex(0);
        $day=date("m-d");
        ob_end_clean();
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        header('Content-Disposition: attachment;filename="'.$day.'出库单.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @return array
     * 批量上传
     */
    public function batch_create()
    {
       set_time_limit(0);
        if($_FILES){
            $sheetData = PublicPlugService::parse_excel();   //解析excel内容
            if(!PublicPlugService::$flag){    //选择了上传文件,状态返回false
                if(count($sheetData)>=2){
                    $check_data = $this->stockOut->check_upload_data($sheetData); // 检查导入的数据合法性
                    if($check_data['status']){
                        $result = $this->stockOut->upload_platform($sheetData);
                        if(array_key_exists('status', $result[0])){
                            echo json_encode(array(
                                'flag'     => 1,
                                'message' => $result[0]['msg'],
                            ));
                        }else{
                            echo json_encode(array(
                                'flag'     => -3,
                                'message'  =>$result,
                            ));
                        }
                    }else{
                        echo json_encode(array('flag'=>-2,'message'=>$check_data['message']));
                    }
                }else{
                    echo json_encode(array('flag'=>-1,'message'=>"文件内容为空"));
                }
            }else{
                echo json_encode(array('flag'=>-1,'message'=>"请选择上传文件"));
            }
        }else{
            echo json_encode(array('flag'=>-1,'message'=>"请选择上传文件"));
        }
    }
    

    /**
     * 出库明细
     */
    public function delivery_details(){
        $this->condition = $_GET;
        $data            = $this->stockOut->delivery_details($this->condition,TRUE);

        $this->assign('enterprise_dominant',PublicInfoService::get_company_array());
        $this->assign('type',PublicdataService::get_deliveryorders_type());
        $this->assign('page',$this->stockOut->page);
        $this->assign('count',$this->stockOut->count);
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 出库明细下载
     */
    public function delivery_details_download()
    {
        $this->condition = $_GET;
        $data            = $this->stockOut->delivery_details($_GET,FALSE);
        D('PurchaseOrders','Service')->download($data,'delivery_details_download');
    }

    /* public function test(){
          $arr = Array
         (
             0 => Array
             (
                 'type' => 20,
                 'sku' => 'Y520',
                 'quantity' => 25,
                 'delivery_date' => '2017-03-04',
                 'storage_position' => array(
                     'MK4-04'=>10,
                     'MH1-02'=>17
                 ),
                 'remark' => '我是备注',
                 'op_time' => '2017-03-25 16:27:08',
                 'delivery_man' => 1,
                 'sku_name' => 'WH-B05 厨房秤',
                 'sku_standard' => '外观：1.核对产品中文名称，检查产品是否与图相符；2.测量尺寸是否与资料相符，是否缺画.
 功能：1.装上电池，显示屏显0为良品。',
                 'internationalship_cost' => '',
                 'delivery_department' => '',
                 'shipmentid' => '',
                 'package_box_id' => '',
                 'fnsku' => '',
                 'hs_code' => '',
                 'declaration_element' => '',
                 'export_tax_rebate'      => 1,
                 'enterprise_dominant'    => 2,
             ),

            1 => Array
             (
                 'type' => 30,
                 'sku' => 'Y520',
                 'quantity' => 10,
                 'delivery_date' => '2017-03-04',
                 'storage_position' => 'MK4-04',
                 'remark' => '我是备注',
                 'op_time' => '2017-03-25 16:27:08',
                 'delivery_man' => 1,
                 'sku_name' => 'WH-B05 厨房秤',
                 'sku_standard' => '外观：1.核对产品中文名称，检查产品是否与图相符；2.测量尺寸是否与资料相符，是否缺画.
 功能：1.装上电池，显示屏显0为良品。',
                 'internationalship_cost' => '',
                 'delivery_department' => '',
                 'shipmentid' => '',
                 'package_box_id' => '',
                 'fnsku' => '',
                 'hs_code' => '',
                 'declaration_element' => '',
             ),

             2 => Array
             (
                 'type' => 80,
                 'sku' => 'Y520',
                 'quantity' => 15,
                 'delivery_date' => '2017-03-04',
                 'storage_position' => 'MK4-04',
                 'remark' => '我是备注',
                 'op_time' => '2017-03-25 16:27:08',
                 'delivery_man' => 1,
                 'sku_name' => 'WH-B05 厨房秤',
                 'sku_standard' => '外观：1.核对产品中文名称，检查产品是否与图相符；2.测量尺寸是否与资料相符，是否缺画.
 功能：1.装上电池，显示屏显0为良品。',
                 'internationalship_cost' => '',
                 'delivery_department' => '',
                 'shipmentid' => '',
                 'package_box_id' => '',
                 'fnsku' => '',
                 'hs_code' => '',
                 'declaration_element' => '',
             ),

             3 => Array
             (
                 'type' => 30,
                 'sku' => 'A408',
                 'quantity' => 10,
                 'delivery_date' => '2017-03-06',
                 'storage_position' => 'MH1-02',
                 'remark' => '我是备注4',
                 'op_time' => '2017-03-25 16:27:08',
                 'delivery_man' => 1,
                 'sku_name' => '大2kg秤',
                 'sku_standard' => '',
                 'internationalship_cost' => '',
                 'delivery_department' => '',
                 'shipmentid' => '',
                 'package_box_id' => '',
                 'fnsku' => '',
                 'hs_code' => '',
                 'declaration_element' => '',
             ),

             4 => Array
             (
                 'type' => 80,
                 'sku' => 'A408',
                 'quantity' => 10,
                 'delivery_date' => '2017-03-06',
                 'storage_position' => 'MH1-02',
                 'remark' => '我是备注4',
                 'op_time' => '2017-03-25 16:27:08',
                 'delivery_man' => 1,
                 'sku_name' => '大2kg秤',
                 'sku_standard' => '',
                 'internationalship_cost' => '',
                 'delivery_department' => '',
                 'shipmentid' => '',
                 'package_box_id' => '',
                 'fnsku' => '',
                 'hs_code' => '',
                 'declaration_element' => '',
             ),
        );
        echo '<pre/>';
        $re  = $this->stockOut->create_deliveryorders_by_picking_list($arr);
        print_r($re);exit;

    }*/
}