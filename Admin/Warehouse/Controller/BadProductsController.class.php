<?php
/**
 * Created by PhpStorm.
 * User: khq
 * Date: 17-3-1
 * Time: 下午3:18
 */

namespace Warehouse\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
use Warehouse\Service\PublicdataService;

class BadProductsController extends CommonController
{

    //服务层对象
    public $badProducts = NULL;

    //条件
    public $condition = array();

    public $time      = NULL;
    //标志符
    public $flag;

    /**
     * PurchaseordersController constructor.
     */
    public function __construct()
    {
        $this->badProducts = D('badProducts','Service');
        $this->time = date('Y-m-d H:i:s');
        parent::__construct();
    }

    /**
     * 不良品实物确认初始界面
     * khq 2017.3.15
     */
    public function handlequality() {
        if($_GET){
            $this->condition = $_GET;
            if($_GET['downcsv']){                                                          //详情导出
                unset($_GET['downcsv']);
                $this->badProducts->download_bad_products();
            }
            $detail = $this->badProducts->bad_products_detail($_GET,true);
            $this->assign('result',$detail);
        }
        $status_arr = PublicdataService::unqualified_deal_way();
        $this->assign('status',$status_arr);
        $this->assign('page', $this->badProducts->page);
        $this->assign('count', $this->badProducts->count);
        $this->assign('sites',PublicInfoService::get_site_array());
        $this->display();
    }
    /**
     * 不良品打印
     * khq 2017.3.17
     */
    public function printquality() {
        $datas = $this->badProducts->print_quality();                                         //更新打印状态
        $this->assign('result',$datas['result']);
        $this->assign('evidence',$datas['pur_evidence']);
        $this->assign('user_name',$_SESSION['current_account']['remark']);
        $this->display();
    }
    /**
     * 不良品实物确认处理
     * khq 2017.3.17
     */
    public function tijiao() {
        $result = $this->badProducts->confirm_bad_product();
        $json_array  = array();
        switch ($result){
            case 200:
                $json_array = array(
                    'status' => 'Y',
                    'message'=> '处理成功'
                );
                break;
            case 402:
                $json_array = array(
                    'status' => 'N',
                    'message'=> '让步接收时,创建入库单失败'
                );
            default:
                $json_array = array(
                    'status' => 'N',
                    'message'=> '更新失败'
                );
        }
        echo json_encode($json_array);
        exit;
    }
    /**
     * 不良品入库单页面
     * khq 2017.3.1
     */
    public function index()
    {
        if($_GET){
            $this->condition = $_GET;
            if($_GET['downcsv']){                                                          //详情导出
                unset($_GET['downcsv']);
                $this->badProducts->download_bad_products();
            }
            $detail = $this->badProducts->select_unqualified_detail($_GET,true);
            $this->assign('result',$detail);
        }
        $status_arr = PublicdataService::get_bad_products_status();
        $this->assign('status',$status_arr);
        $this->assign('sites',PublicInfoService::get_site_array());
        $this->assign('page', $this->badProducts->page);
        $this->assign('count', $this->badProducts->count);
        $this->display();
    }

    /**
     * 不良品处理初始界面
     * khq 2017.3.2
     */
    public function bad_products_handle()
    {
        if($_GET){
            $this->condition = $_GET;
            if($_GET['downcsv']){                                                          //详情导出
                unset($_GET['downcsv']);
                $this->badProducts->download_bad_products();
            }
            $detail = $this->badProducts->select_unqualified_detail($_GET,true);
            $this->assign('result',$detail);
        }
        $way        = PublicdataService::unqualified_deal_way();
        $status_arr = PublicdataService::get_bad_products_status();
        $this->assign('way',$way);
        $this->assign('status',$status_arr);
        $this->assign('page', $this->badProducts->page);
        $this->assign('count', $this->badProducts->count);
        $this->display();
    }
    /**
     * ajax查询不良品入库单
     * khq 2017.3.2
     */
    public function ajax_select_detail()
    {
        $detail_data = $this->badProducts->ajax_select_detail(array('id'=>$_GET['id']));
        $json_array = array(
            'status' => 'Y',
            'message'=> $detail_data
        );
        echo json_encode($json_array);
        exit;
    }
    /**
     * 编辑单个不良品入库信息
     * khq 2017.3.2
     */
    public function ajax_edit()
    {
        $edit_status = $this->badProducts->ajax_edit();
        $json_array  = array();
        switch ($edit_status){
            case 200:
                $json_array = array(
                    'status' => 'Y',
                    'message'=> '更新成功'
                );
                break;
            case 402:
            default:
                $json_array = array(
                    'status' => 'N',
                    'message'=> '更新失败'
                );
        }
        echo json_encode($json_array);
        exit;
    }
    /**
     * 不良品处理操作
     * khq 2017.3.6
     */
    public function deal()
    {
        $check_status = $this->badProducts->check_deal();
        if($check_status ==401){
            $json_array = array(
                'status' => 'N',
                'message'=> '处理数量大于不良品数量,请重新确认'
            );
            echo json_encode($json_array);
            exit;
        }
        $status = $this->badProducts->deal();
        switch ($status){
            case 200:
                $json_array = array(
                    'status' => 'Y',
                    'message'=> '处理成功'
                );
                break;
            case 404:
            default:
                $json_array = array(
                    'status' => 'N',
                    'message'=> '处理失败'
                );
                break;
        }
        echo json_encode($json_array);
        exit;
    }
    public function test()
    {
        $arr = $this->badProducts->test();
        print_r($arr);exit;
    }
}