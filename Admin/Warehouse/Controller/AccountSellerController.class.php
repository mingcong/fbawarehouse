<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-13
 * Time: 下午2:45
 */
namespace Warehouse\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
class AccountSellerController extends CommonController {
    //服务层对象
    public $AccountSeller   = null;
    public $accounts        = null;
    public $remark          = null;
    

    //条件
    public $condition = array();

    //标志符
    public $flag;

    /**
     * 默认构造方法
     */
    public function __construct()
    {
        //$this->AccountSeller = D('AccountSeller','Service');
        //$this->accounts       = PublicInfoService::salesmanBindAccountsGet();
        $this->remark               = publicInfoService::salesmanGet();
        $this->accounts             = PublicInfoService::get_accounts();
        parent::__construct();
    }

    /**
     * 销售员帐号关系对应表首页展示
     */
    public function index() {
        //var_dump(publicInfoService::salesmanGet());exit;
        //var_dump(publicInfoService::salesmanBindAccountsGet());exit;
        $this->condition = $_GET;
        //$data = $this->AccountSeller->select($_GET,true);
        $this->assign('remark',$this->remark);
        $this->assign('accounts',$this->accounts);
        $this->assign('page',$this->AccountSeller->page);
        $this->assign('count',$this->AccountSeller->count);
        //$this->assign('data',$data);

        $this->display();
    }
}