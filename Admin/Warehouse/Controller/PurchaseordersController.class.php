<?php
/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 上午9:41
 */

namespace Warehouse\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
use Warehouse\Model\RecieveManualPicsModel;
use Warehouse\Model\RecievePicsDetailsModel;
use Warehouse\Service\PublicdataService;
use Warehouse\Service\PurchaseordersService;

class PurchaseordersController extends CommonController
{
    //服务层对象
    public $Purchaseorders = NULL;

    //条件
    public $condition = array();

    //标志符
    public $flag;

    /**
     * PurchaseordersController constructor.
     */
    public function __construct()
    {
        $this->Purchaseorders = D('PurchaseOrders','Service');
        parent::__construct();
    }

    /**
     * 扫描收货页面
     */
    public function index()
    {
        $this->display();
    }

    /**
     * 供应商送货单图片录入控制器
     */
    public function recieve_pictureupload(){
        $this->display();
    }

    /**
     * 查询防范
     */
    public function select_recievepicinfo(){
        $this->condition=$_GET;
        $data=$this->Purchaseorders->recieve_pictureinfo($this->condition);
        if(!empty($data)){
            echo json_encode(array(
                'flag'=>1,
                'info'=>$data
            ));
            exit;
        }
        echo json_encode(array(
            'flag'=>-1,
            'info'=>$data
        ));
        exit;
    }

    /**
     * 添加图片页面控制器
     */
    public function multiupload_imgs(){
        C("LAYOUT_ON", FALSE);
        C('SHOW_PAGE_TRACE', false);
        $this->assign('id',$_GET['id']);
        $this->display('multiupload_imgs');
    }

    /**
     * 删除图片
     */
    function delPic(){
        $opsign=$this->Purchaseorders->delPic($_GET['picurl']);
        $this->ajaxReturn($opsign);
    }
    /**
     * 上传图片控制器
     */
    public function addPics(){

        if(empty($_POST['id'])){

        }
        $picModel=new RecieveManualPicsModel();
        $picInfo=$picModel->get_recieve_manual_pics_model($_POST);
        $picId=array_keys($picInfo);
        $picId=$picId[0];
        if(empty($_FILES['fileselect']['tmp_name'])){

        }

        $photo_types=array('image/jpg', 'image/jpeg','image/png','image/pjpeg','image/gif','image/bmp','image/x-png');//定义上传格式
        $max_size=30720000; //上传照片大小限制,默认700k
        $photo_folder="pic/upload/"; //上传照片路径
        ///////////////////////////////////////////////////开始处理上传
        if(!file_exists($photo_folder))//检查照片目录是否存在
        {
            mkdir($photo_folder, 0777, true); //mkdir("temp/sub, 0777, true);
        }

        $infos=array();
        for($i=0;$i<count($_FILES['fileselect']['tmp_name']);$i++){
            if(is_uploaded_file($_FILES['fileselect']['tmp_name'][$i])){

                $upfile=$_FILES['fileselect'];
                $name=$upfile['name'][$i];
                $type=$upfile['type'][$i];
                $size=$upfile['size'][$i];
                $tmp_name=$upfile['tmp_name'][$i];

                if($size > $max_size)//检查文件大小
                {
                    $infos[$name]='大小不符合要求，最大为30M！';
                    continue;
                }

                if(!in_array($type, $photo_types))//检查文件类型
                {
                    $infos[$name]='文件类型错误！';
                    continue;
                }

                if(!file_exists($photo_folder))//照片目录
                    mkdir($photo_folder);
                $pinfo=pathinfo($name);
                $photo_type=$pinfo['extension'];//上传文件扩展名
                $photo_server_folder = $photo_folder
                    .$picInfo[$picId]['purchaseorder_id']."_".$picInfo[$picId]['recieve_invoice_id']
                    ."_".date("YmdHis")."_".$i.".".$photo_type;//以当前时间和7位随机数作为文件名，这里是上传的完整路径

                $picInsertInfo=$this->Purchaseorders
                    ->create_recieve_pic_details(
                        array(
                            'recieve_manual_pic_id'=>$picId,
                            'url'=>$photo_server_folder,
                        )
                    );
                if(!$picInsertInfo){
                    $infos[$name]='数据保存出错！';
                    continue;
                }

                if(!move_uploaded_file ($tmp_name, $photo_server_folder)) {
                    $infos[$name]='文件保存出错！';
                    continue;
                }
            }
        }
        $this->ajaxReturn($infos);
    }

    /**
     * 根据采购单号获取采购单信息
     */
    public function select_purchaseinfo()
    {
        $this->condition = array('id' => trim($_GET['purchase_id']));
        $data            = $this->Purchaseorders->purchased_wait_recieve($this->condition,FALSE);
        if($data[0]['mainstatus']!=20){
            echo json_encode(array(
                'flag' => -1,
                'info' => $data[0]['mainstatus']
            ));
            exit;
        }
        echo json_encode(array(
            'flag' => 1,
            'info' => $data
        ));
        exit;
    }


    /**
     * 生成批次号
     */
    public function create_batch_code()
    {
        $batch_code = $this->Purchaseorders->create_batch_code();
        echo json_encode(array(
            'status'     => 1,
            'batch_code' => $batch_code
        ));
    }

    /**
     * 收货确认
     */
    public function check()
    {
        $arrival_quantity = explode(',',trim($_GET['arrival_quantity']));
        $detailsid        = explode(',',rtrim($_GET['detailsid'],','));
        $cids             = explode(',',rtrim($_GET['cids'],','));
        $batch_code       = trim($_GET['batch_code']);
        foreach($arrival_quantity as $key => $value){
            if($value===''){
                unset($detailsid[$key]);
                unset($cids[$key]);
            }
        }
        $arrival_quantity = array_filter($arrival_quantity);
        if(count($arrival_quantity)==0){
            echo json_encode(array('flag' => 2));exit;
        }else{
            $arrival_quantity = array_values($arrival_quantity);//重新对key值进行排列
            $this->condition  = array(
                'id'        => trim($_GET['mainid']),
                'detailsid' => implode(',',$detailsid),
                'cid'       => implode(',',$cids),
            );
            $recieve_data     = $this->Purchaseorders->check($this->condition,FALSE);
            $res              = $this->Purchaseorders->create_recieve($recieve_data,$batch_code,$arrival_quantity);
            if($res){
                echo json_encode(array('flag' => 1));exit;
            }else{
                echo json_encode(array('flag' => -1));exit;
            }
        }
    }



    /**
     * 根据采购单号获取收货*/
    public function get_recieveinfo_by_id(){
        $this->condition = array('purchaseorder_id'=>trim($_GET['purchaseorder_id']));
        $recieveinfo = $this->Purchaseorders->get_recieveinfo_by_id($this->condition,false);
        if($recieveinfo){
            echo json_encode(array('flag' => 1,'info'=>$recieveinfo));exit;
        }else{
            echo json_encode(array('flag' => -1));exit;
        }

    }

    /**
     * 获取当前采购单指定sku的收货信息
     */
    public function  get_recieveinfo(){
        $this->condition = array(
            'b.sku'=>strtoupper(trim($_GET['sku'])),
            'a.purchaseorder_id'=>trim($_GET['purchase_id']),
            'b.site_id'=>PublicInfoService::get_site_id(trim($_GET['site_id']))
        );
        $sku_recieveinfo = $this->Purchaseorders-> get_recieveinfo($this->condition);
        if($sku_recieveinfo){
            echo json_encode(array('flag' => 1,'info'=>$sku_recieveinfo));exit;
        }else{
            echo json_encode(array('flag' => -1));exit;
        }
    }

    /**
     * 修改
     */
    public function change_by_detailid(){
        $this->condition = $_GET;
        $info = $this->Purchaseorders-> find_check_info($this->condition);
        if($info){
            echo json_encode(array('flag' => 1));exit;
        }else{
            echo json_encode(array('flag' => -1));exit;
        }
    }


    /**
     * 绑定批次号
     */
    public function update_batch_code(){
        $res = $this->Purchaseorders->update_batch_code($_GET);
        if($res){
            echo json_encode(array('flag' => 1));exit;
        }else{
            echo json_encode(array('flag' => -1));exit;
        }
    }

    /**
     * 检查批次号输入是否正确
     */
    public function check_batch_code(){
        $res = $this->Purchaseorders->check_batch_code(trim($_GET['batch_code']));
        if($res){
            echo json_encode(array('flag' => 1));exit;
        }else{
            echo json_encode(array('flag' => -1));exit;
        }
    }

    /**
     * 已采购待收货的采购单页面
     */
    public function purchased_wait_recieve()
    {
        $this->condition = $_GET;
        $data            = $this->Purchaseorders->purchased_wait_recieve($this->condition,TRUE);
        $this->assign('page',$this->Purchaseorders->page);
        $this->assign('count',$this->Purchaseorders->count);
        $this->assign('data',$data);

        $this->display();
    }

    /**
     *  已采购待收货的采购单下载
     */
    public function purchased_wait_recieve_download()
    {
        $this->condition = $_GET;
        $data            = $this->Purchaseorders->purchased_wait_recieve($_GET,FALSE);
        $this->Purchaseorders->download($data,'purchased_wait_recieve_download');
    }

    /**
     * 已收货待入库的采购单页面
     */
    public function recieved_wait_storage()
    {
        $this->condition = $_GET;
        $data            = $this->Purchaseorders->get_recieved_wait_storage($this->condition,TRUE);

        $this->assign('page',$this->Purchaseorders->page);
        $this->assign('count',$this->Purchaseorders->count);
        $this->assign('data',$data);

        $this->display();
    }

    /**
     * 已收货待入库的采购单下载
     */
    public function recieved_wait_storage_download(){
        $this->condition = $_GET;
        $data            = $this->Purchaseorders->get_recieved_wait_storage($this->condition,false);
        $this->Purchaseorders->download($data,'recieved_wait_storage_download');
    }

    /**
     * 已收货明细页面
     */
    public function recieve_details()
    {
        $this->condition = $_GET;
        $data            = $this->Purchaseorders->get_recieveinfo_by_id($this->condition,TRUE);

        $this->assign('enterprise_dominant',PublicInfoService::get_company_array());
        $this->assign('page',$this->Purchaseorders->page);
        $this->assign('count',$this->Purchaseorders->count);
        $this->assign('data',$data);
        $this->assign('sum',array_sum(PublicdataService::i_array_column($data,'arrival_quantity')));

        $this->display();
    }

    /**
     * 已收货明细下载
     */
    public function recieve_details_download(){
        $this->condition = $_GET;
        $data            = $this->Purchaseorders->get_recieveinfo_by_id($this->condition,false);
        $this->Purchaseorders->download($data,'recieve_details_download');
    }



    function test()
    {
        /*$arr = array(
            'batch_code'       => 'PC2017022308',
            'recieve_details_id' => '1,2,3,',
        );
        echo '<pre/>';
        print_r($this->update_batch_code($arr));exit;*/
    }


}