<?php
/**
 * Created by PhpStorm.
 * User: liqiang
 * Date: 16-12-29
 * Time: 上午11:00
 */

namespace Warehouse\Controller;
use Home\Controller\CommonController;

class InventoryController extends CommonController {
    public function index() {
        $result = $this->getPagination('Admin');

        $this->assign('admins', $result['data']);
        $this->assign('rows_count', $result['total_rows']);
        $this->assign('page', $result['show']);
        $this->display();
    }


} 