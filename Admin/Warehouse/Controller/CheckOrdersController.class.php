<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/2/22
 * Time: 15:51
 */

namespace Warehouse\Controller;

use Home\Controller\CommonController;
use Inbound\Service\PublicInfoService;
use Warehouse\Service\PublicdataService;

class CheckOrdersController extends CommonController
{
    //服务层对象
    public $checkOrders = null;

    //条件
    public $condition = array();

    //标志符
    public $flag;

    //物流属性
    public $logictype = array(
        '1',
        '101',
        '201',
        '202',
        '203',
        '204',
        '205',
        '301',
        '302',
        '401',
        '402',
        '404',
        '405',
        '406',
        '407',
        '408',
        '409',
        '501',
        '502',
        '503',
        '601',
        '701',
        '702',
        '801'
    );
    //物流属性对应的中文名
    public $logicNames = array(
        '1'   => '普货产品',
        '101' => '无电池的电子产品',
        '201' => '小剪刀类产品',
        '202' => '螺丝刀工具类产品',
        '203' => '折刀类产品',
        '204' => '管制刀具类产品',
        '205' => '大剪刀类产品',
        '301' => '强磁性产品',
        '302' => '弱磁性产品',
        '401' => '手表类产品',
        '402' => '纽扣电池类产品',
        '404' => '内置电池产品',
        '405' => '配套电池产品',
        '406' => '柱状纯电池产品',
        '407' => '纯电池产品',
        '408' => '手机',
        '409' => '内置大功率电池产品',
        '501' => '粉末类产品',
        '502' => '膏状类产品',
        '503' => '清洁胶产品',
        '601' => '液体类产品',
        '701' => '清抛类产品',
        '702' => '带油按摩腰带产品',
        '801' => '平衡车产品'
    );

    /**
     * PurchaseordersController constructor.
     */
    public function __construct()
    {
        $this->checkOrders = D('CheckOrders', 'Service');
        parent::__construct();
    }

    /**
     * 采购单首页
     */
    public function index()
    {
        $this->condition = $_GET;
        $this->display();
    }

    /**
     * 属性确认界面
     */
    public function attr_confirm_index()
    {
        if (!empty($_GET['recieveDetailsId'])) {
            $data           = $this->checkOrders->getReceiveDetailById($_GET['recieveDetailsId']);
            $data['logic_v']   = $data['logic_v'] ? $data['logic_v'] : $data['logic_attr'];
            $data['logisShow'] = $this->chooseLogistics($data['logic_v']);
            $this->ajaxReturn(json_encode($data));
        }

        $this->display();
    }

    /**
     * 物流属性确认
     */
    public function check_attr()
    {
        $this->condition = $_GET;

        $checkOrders = D('CheckOrders', 'Service');
        $checkOrders->updateAttr($this->condition);
    }

    /**
     * SKU质检标准确认
     */
    public function check_standard_index()
    {
        $this->condition = array_filter($_GET);

        if (!empty($this->condition['recieveDetailsId']) || !empty($this->condition['batchCode'])) {

            $data = $this->checkOrders->getCheckStandardDetail($this->condition);

            if (null == $data) {
                $this->ajaxReturn(false);
            } else {
                $this->ajaxReturn(json_encode($data));
            }
        }

        $this->display();
    }

    /**
     * 质检标准确认
     */
    public function checkSkuStandard()
    {
        $service         = $this->checkOrders;
        $this->condition = array_filter($_POST);

        $receiveDetailsIds = explode('-', $this->condition['recieveDetailId']);

        $result = $service->checkSkuStandard($receiveDetailsIds);

        if ($result) {
            $this->ajaxReturn('确认成功!');
        } else {
            $this->ajaxReturn('确认失败!请确认这些数据需要确认!');
        }
    }

    /**
     * 扫描质检主界面
     */
    public function scan_check_index()
    {
        $service         = $this->checkOrders;
        $this->condition = array_filter($_GET);

        if (!empty($this->condition['recieveDetailsId']) || !empty($this->condition['batchCode'])) {
            $data = $service->getScanCheckDetail($this->condition);
            if (null == $data) {
                $this->ajaxReturn(false);
            } else {
                $this->ajaxReturn(json_encode($data));
            }
        }

        $this->display();
    }

    /**
     * 扫描质检保存
     */
    public function scanCheckSave()
    {
        $service         = $this->checkOrders;
        $this->condition = array_filter($_POST);

        if (empty($this->condition['qualifiedNum']) && $this->condition['qualifiedNum']!=0) {
            $this->ajaxReturn('请正确输入数据!');
            return;
        }

        $result = $service->scanCheckSave($this->condition);

        if ($result) {
            $this->ajaxReturn('保存成功!');
        } else {
            $this->ajaxReturn('保存失败!请检查数据!');
        }
    }

    /**
     * 打印良品标签
     */
    public function print_qualified_tag()
    {
        layout(false);
        C('SHOW_PAGE_TRACE', false);

        $service         = $this->checkOrders;
        $this->condition = array_filter($_GET);
        $result          = $service->getCheckQualityDetails($this->condition);

        $result['quality_count'] = round($result['qualified_quantity'] / $result['check_quantity'], 3) * 100;
        $result['quality_count'] .= '%';

        $this->assign('data', $result);

        $this->display();
    }

    /**
     * 打印sku标签
     */
    public function print_sku_tag()
    {
        layout(false);
        C('SHOW_PAGE_TRACE', false);

        $service         = $this->checkOrders;
        $this->condition = array_filter($_GET);
        $result          = $service->getCheckQualityDetails($this->condition);

        $this->assign('data', $result);

        $this->display();
    }

    /**
     * 质检单报表
     */
    public function checked_orders_report()
    {
        $service         = $this->checkOrders;
        $this->condition = array_filter($_GET);

        if (!empty($this->condition['purchaseorder_id']) || !empty($this->condition['create_time_from'])
            || !empty($this->condition['create_time_to'])
            || !empty($this->condition['print_status'])
        ) {
            $data = $service->getCheckDetails($this->condition);
            foreach ($data as $k =>$v) {
                foreach ($v as $c =>$a) {
                    foreach($a as $key =>$item) {
                        $data[$k][$c][$key]['transfer_hopper_name'] =
                            PublicdataService::get_transfer_hopper_name($item['transfer_hopper_id']);
                    }

                }

            }

            $this->assign('data', $data);
            $this->assign('page', $service->page);
            $this->assign('count', $service->count);
        }

        $this->display();
    }

    /**
     * 打印相同采购单下的质检单
     */
    public function print_quality_report()
    {
        layout(false);
        C('SHOW_PAGE_TRACE', false);

        $service = $this->checkOrders;

        $this->condition = array_filter($_GET);

        $results = $service->print_quality_report($this->condition);
        foreach($results['result'] as $k =>$v) {
            $results['result'][$k]['transfer_hopper_name'] = PublicdataService::get_transfer_hopper_name($v['transfer_hopper_id']);
        }
        $this->assign('data', $results['result']);
        $this->assign('evidence', $results['evidence']);
        $this->assign('users', $results['users']);

        $this->display();
    }

    /**
     * 打印所有选中的质检单,形成质检报表
     */
    public function print_quality_reports()
    {
        layout(false);
        C('SHOW_PAGE_TRACE', false);
        $service         = $this->checkOrders;
        $this->condition = array_filter($_GET);

        $results = $service->print_quality_reports($this->condition);
        foreach($results['results'] as $k =>$v) {
            foreach($v['result'] as $c => $a){
                $results['results'][$k]['result'][$c]['transfer_hopper_name'] = PublicdataService::get_transfer_hopper_name($a['transfer_hopper_id']);
            }
        }
        $this->assign('results', $results['results']);
        $this->assign('users', $results['users']);

        $this->display();
    }

    /**
     * 质检明细报表
     */
    public function check_orders_details_report()
    {
        $service = $this->checkOrders;

        if (!empty($_GET)) {
            $this->condition = array_filter($_GET);

            if ('down' == $_GET['downcsv']) {
                unset($_GET['downcsv']);
                $this->condition = array_filter($_GET);
                $datas           = $service->getAllInfo($this->condition);
                $service->down_check_details_csv($datas);
                exit;
            }
            $data    = $service->select($this->condition);
            foreach($data['result'] as $k =>$v) {
                $sites = PublicInfoService::get_site_array();
                $data['result'][$k]['site_id']            = $sites[$v['site_id']];
                $data['result'][$k]['transfer_hopper_name'] = PublicdataService::get_transfer_hopper_name($v['transfer_hopper_id']);
                $data['result'][$k]['check_man'] = PublicdataService::get_user_name($v['check_man']);
                $data['result'][$k]['check_type'] = PublicdataService::getStandardFromSku($v['sku']);
            }
            $summary = $service->summary($this->condition);
            $this->assign('data', $data['result']);
            $this->assign('rebate', $data['rebate']);
            $this->assign('dominant', $data['enterprise_dominant']);
            $this->assign('suppliers', $data['suppliers']);
            $this->assign('summary', $summary);
            $this->assign('page', $service->page);
            $this->assign('count', $service->count);
        }

        $this->display();
    }

    /**
     * 选择物流属性 下拉框
     */
    public function chooseLogistics($logic_v)
    {
        $showLogistics = '';
        foreach ($this->logicNames as $key => $value) {
            $key2 = $key;
            if ('1' == $key) {
                $key2 = '001';
            }
            if ($logic_v == $key) {
                $showLogistics .= "<option value={$key} selected> {$key2}#{$value}</option>";
            } else {
                $showLogistics .= "<option value={$key} > {$key2}#{$value}</option>";
            }
        }

        return "<select name='skuLogistic' id='slt_logistic'>
                    <option value=''>--请选择属性--</option>".$showLogistics."</select>";
    }

    /**
     * 修改质检数量
     */
    public function reset_check_num(){
        $check_info = $this->checkOrders->get_stockin_num(trim(I('get.recieve_detail_id')));
        $bad_product_delivery = $this->checkOrders->get_bad_product_delivery($check_info[0]['id']);
        if($check_info[0]['stockin_num']>0){
            echo json_encode(array('flag'=>-1));exit;
        }elseif($bad_product_delivery>0){
            echo json_encode(array('flag'=>-3));exit;
        }else{
            $res = $this->checkOrders->reset_num(trim(I('get.recieve_detail_id')),$check_info);
            if($res){
                echo json_encode(array('flag'=>1));exit;
            }else{
                echo json_encode(array('flag'=>-2));exit;
            }
        }
    }
    public function checkSupplier($name) {
        $result = PublicdataService::get_supplier_ids($name);
        $this->ajaxReturn($result);
    }
}