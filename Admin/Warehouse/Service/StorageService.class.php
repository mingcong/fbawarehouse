<?php

namespace Warehouse\Service;

class StorageService {
    //数据库
    public $_db = 'fbawarehouse';

    /**
     * 构造方法
     *
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '', $param = array()) {
        $this->storage = M('wms_storage', ' ', $this->_db);
    }


    /**
     * 描述：判断储位是否有效接口
     * 参数：$_array 储位数组
     * 返回值：array 数组
     */
    public function validate_storage($_array = array()) {
        if (!$_array) {
            return array('error' => 1, 'msg' => 'array is unvalid');
        }

        $result = array();
        foreach ($_array as $key => $value) {
            $count = $this->storage->where(array('storage_position' => $value))->count();
            if ($count > 0) {
                $result[$value] = 1;
            } else {
                $result[$value] = 0;
            }
        }

        return $result;
    }
}