<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-3-13
 * Time: 下午3:40
 */
namespace Warehouse\Service;
use Inbound\Service\PublicInfoService;
use Warehouse\Model\AccountSellerModel;

class AccountSellerService {
    public $AccountSeller       = null;
    //查询记录总数
    public $count = 0;
    //页面展示包含页面样式
    public $page = 0;
    //数据
    public $data = array();

    public $nowtime;

    public function __construct($table = '',$param = array()) {
        $this->AccountSeller            = D('AccountSeller','Model');
        $this->nowtime                  = date('Y-m-d H:i:s',time());
    }
    /**
     * @param array $_array
     * @param bool  $flag
     * @return mixed
     * 首页数据查询与显示
     */
    public function select($_array = array(),$flag = TRUE) {
        //echo 111;exit;
        $_array = empty($_array)?$_GET:$_array;
        $accountseller  = $this->get_this_model($_array,$flag);

        if($flag){
            $AccountSeller_count       = clone $accountseller;
            $this->count               = $AccountSeller_count->count();//数量
            $Page                      = new \Org\Util\Page($this->count,20);// 实例化分页类 传入总记录数和每页显示的记录数
            $this->page                = $Page->show();// 分页显示输出

            $accountseller->order("account_id")
                ->limit($Page->firstRow.','.$Page->listRows);
        }
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $result     = $accountseller->select();
        //var_dump ($result);exit;
        $result     = $this->account_sellers_detail_data($result);
        //$returndata = $this->seller_index_data($result,$flag);
        return $result;
    }
    public function seller_index_data($result,$flag) {
        $return_result = array();
        foreach($result as $key => $val){
            $return_result[$key]['id']                                 = $val['id'];
            $return_result[$key]['account_id']                         = $val['account_id'];
            $return_result[$key]['seller_ids']                         = $val['seller_ids'];
        }
        return $return_result;
    }
    /**
     * @param $data
     * @return mixed
     * 格式化
     */
    public function account_sellers_detail_data($data,$table = 'fba_account_sellers') {
        $sale_arr = PublicInfoService::salesmanGet();
        //var_dump ($sale_arr);exit;
        foreach($data as &$_data){
            //$_data['seller_id']             = PublicInfoService::get_user_name_by_id($_data['seller_id']);
            $_data['account_id']            = PublicInfoService::salesmanBindAccountsGet($table,$_data['account_id']);
            $sale_arr['seller']             = $sale_arr[$_data['seller']];
        }
        return $data;
    }
    
    /**
     * 获取model对象
     * 参数 查询条件
     * 返回 对象
     */
    public function get_this_model(&$_array) {
        $accountseller = M(AccountSellerModel::$table,'',$this->AccountSeller->_db);
        return $accountseller -> where($_array);
    }
}