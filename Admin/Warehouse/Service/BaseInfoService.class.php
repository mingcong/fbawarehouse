<?php
/**
 * Created by PhpStorm.
 * User: yanghaize
 * Date: 17-02-25
 * Time: 下午2:47
 */

namespace Warehouse\Service;

use Think\Model;

class BaseInfoService
{
    public static $model;

    protected static $_db= 'fbawarehouse';

    public static $enterprise_dominant = array();



    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function export_tax_rebate($_array=array())
    {
        $data=M('statusdics',' ',self::$_db)
            ->field('number,value')
            ->where(array('table' => 'wms_purchaseorders','colum_name'=>'export_tax_rebate'))
            ->select();
        foreach($data AS $k=>$v){
            $_array[$v['number']]=$v['value'];
        }
        return $_array?$_array:array();
    }

    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function enterprise_dominant($_array=array())
    {
        $data=M('statusdics',' ',self::$_db)
            ->field('number,value')
            ->where(array('table' => 'wms_purchaseorders','colum_name'=>'enterprise_dominant'))
            ->select();
        foreach($data AS $k=>$v){
            $_array[$v['number']]=$v['value'];
        }
        return $_array?$_array:array();
    }

    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function transfer_hopper_id($_array=array())
    {
        $data=M('statusdics',' ',self::$_db)
            ->field('number,value')
            ->where(array('table' => 'wms_purchaseorders','colum_name'=>'transfer_hopper_id'))
            ->select();
        foreach($data AS $k=>$v){
            $_array[$v['number']]=$v['value'];
        }
        return $_array?$_array:array();
    }

    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function transfer_type($_array=array())
    {
        $data=M('statusdics',' ',self::$_db)
            ->field('number,value')
            ->where(array('table' => 'wms_purchaseorders','colum_name'=>'transfer_type'))
            ->select();
        foreach($data AS $k=>$v){
            $_array[$v['number']]=$v['value'];
        }
        return $_array?$_array:array();
    }
    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function stock_in_type($_array=array())
    {
        $data=M('statusdics',' ',self::$_db)
            ->field('number,value')
            ->where(array('table' => 'wms_warehouseorders','colum_name'=>'type'))
            ->select();
        foreach($data AS $k=>$v){
            $_array[$v['number']]=$v['value'];
        }
        return $_array?$_array:array();
    }

    public static function stock_out_type()
    {
        $data = M('statusdics',' ',self::$_db)
            ->field('number,value')
            ->where(array('table' => 'wms_deliveryorders','colum_name'=>'type'))
            ->getField('number,value');

        return $data?$data:array();
    }
    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function supplier_name($id)
    {
        $data=M('skusystem_suppliers',' ',self::$_db)
            ->where(array('id'=>trim($id)))
            ->getfield('name');
        return $data?$data:'';
    }

    /**
     * @param $name
     * @return mixed-公司主体id
     * 根据公司名称获取公司id
     */
    public static function supplier_id($name)
    {
        $data=M('skusystem_suppliers',' ',self::$_db)
            ->where(array('name'=>array('LIKE',trim($name))))
            ->getfield('id');
        return $data?$data:'';
    }
    /** 通过用户id获取用户名称
     * 参数 数组,字符串
     * 返回 用户信息
     **/
    public static function get_user_name($id)
    {
        $data=M('admin')
            ->where(array('id'=>$id))
            ->getfield('remark');

        return $data?$data:'';
    }
    /** 通过用户id获取用户名称
     * 参数 数组,字符串
     * 返回 用户信息
     **/
    public static function get_user_id($name)
    {
        $data=M('admin')
            ->where(array('remark'=>$name))
            ->getfield('id');

        return $data?$data:'';
    }
    /** 通过用户id获取用户名称
     * 参数 数组,字符串
     * 返回 用户信息
     **/
    public static function get_sku_name($sku)
    {
        $data=M('skusystem_sku_cnname',' ',self::$_db)
            ->join('skusystem_cnname  on skusystem_sku_cnname.attr_id = skusystem_cnname.id')
            ->where(array('skusystem_sku_cnname.sku'=>$sku))
            ->getfield('skusystem_cnname.name');
        return $data?$data:'';
    }
    /** 通过用户id获取用户名称
     * 参数 数组,字符串
     * 返回 用户信息
     **/
    public static function get_sku_price($sku)
    {
        $data=M('skusystem_price',' ',self::$_db)
            ->where(array('sku'=>$sku))
            ->getfield('num');
        return $data?$data:'';
    }
    /** 通过用户id获取用户名称
     * 参数 数组,字符串
     * 返回 用户信息
     **/
    public static function Judge_is_sku_cname($_array)
    {
        $data=M('wms_storage_sku',' ',self::$_db)
            ->where($_array)
            ->getfield('id');
        return $data?$data:'';
    }
    /** 通过用户id获取用户名称
     * 参数 数组,字符串
     * 返回 用户信息
     **/
    public static function GetSkuStorage($_array)
    {
        $data=M('wms_storage_sku',' ',self::$_db)
            ->where($_array)
            ->getfield('id,storage_position');
        return $data?$data:array();
    }

}