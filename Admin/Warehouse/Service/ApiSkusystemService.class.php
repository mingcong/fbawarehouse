<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 17/2/23
 * Time: 16:29
 */

namespace Warehouse\Service;


use Home\Controller\CommonController;

class ApiSkusystemService extends CommonController
{
    /**
     * 批量更新sku的物流属性
     * @param array $attrs
     * @return mixed
     */
    public static function updateSkuArrAttr($attrs = array())
    {
        Vendor('phpRPC.phprpc_client');

        $server_url = C('SKUSYSTEM_URL');

        $client = new \PHPRPC_Client($server_url.'/newerpapi/updateSkuArrAttr');
        $results    = $client->getinfo($attrs);
        return $results;
    }

}