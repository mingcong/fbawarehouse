<?php

namespace Warehouse\Service;
use Inbound\Service\PublicInfoService;
class StockInService
{
    //数据库
    public $_db = 'fbawarehouse';

    public $Purchaseorder_details = null;

    public $stock_in = null;

    //数据
    public $data = array();

    public $Purchaseorder_details_data = array();

    public $nowtime;

    /**
     * PurchaseordersService constructor.
     *
     * @param string $table
     * @param array $param
     */
    public function __construct($table = '', $param = array())
    {
        $this->Purchaseorder_details = M('wms_purchaseorder_details', ' ', $this->_db);
        $this->stock_in = D('Warehouse/StockIn', 'Model');
    }

    /**
     * @param array $_array
     * @param bool $flag
     *
     * @return mixed
     * 首页数据查询与显示
     */
    public function purchaseorder_details($_array = array(), $flag = true)
    {
        $result = $this->Purchaseorder_details->field(
            'money/quantity as single_price,transportation_expense/quantity as transportation_expense'
        )
            ->where($_array)
            ->find();
        $result['single_price'] = $result['single_price']?$result['single_price']:0;
        $result['transportation_expense'] = $result['transportation_expense']?$result['transportation_expense']:0;
        return $result;
    }

    /**
     * @param array $_array
     * @param bool $flag
     *
     * @return mixed
     * 首页数据查询与显示
     */
    public function get_supplier_by_sku($_array = array(), $flag = true)
    {
        $result = M('supplier_skus', 'skusystem_', $this->_db)
            ->field('supplier_id')
            ->where($_array)
            ->select();
        foreach ($result AS &$_data) {
            $_data['supplier_id'] = BaseInfoService::supplier_name($_data['supplier_id']);
        }
        $data = array();
        $data['supplier_id'] = $result;
        $data['sku_name'] = BaseInfoService::get_sku_name($_array['sku']);
        $data['price'] = BaseInfoService::get_sku_price($_array['sku']);
        return $data;
    }

    /**
     * @param array $_array
     * @param bool $flag
     *
     * @return mixed
     * 首页数据查询与显示
     */
    public static function check_quality_details($_array = array(), $flag = TRUE)
    {
        if ($flag) {
            $stock_type = BaseInfoService::stock_in_type();
            $export_tax_rebate = BaseInfoService::export_tax_rebate();
            $enterprise_dominant = BaseInfoService::enterprise_dominant();
            $transfer_hopper_id = BaseInfoService::transfer_hopper_id();
            $transfer_type = BaseInfoService::transfer_type();
            $site_id= PublicInfoService::get_site_array();
            foreach ($_array AS &$_data) {
                $_data['supplier_id'] = BaseInfoService::supplier_name($_data['supplier_id']);
                $_data['check_man'] = BaseInfoService::get_user_name($_data['check_man']);
                $_data['site_id'] = $site_id[$_data['site_id']];
                $_data['print_status'] = $_data['print_status'] ? '已打印' : '未打印';
                $_data['store'] = $_data['store']=='11' ? '亚马逊中转仓' : '';
                $_data['type'] = $stock_type[$_data['type']] ? $stock_type[$_data['type']] : '';
                $_data['export_tax_rebate'] = $export_tax_rebate[$_data['export_tax_rebate']] ?
                    $export_tax_rebate[$_data['export_tax_rebate']] : '';
                $_data['enterprise_dominant'] = $enterprise_dominant[$_data['enterprise_dominant']] ?
                    $enterprise_dominant[$_data['enterprise_dominant']] : '';
                $_data['transfer_hopper_id'] = $transfer_hopper_id[$_data['transfer_hopper_id']] ?
                    $transfer_hopper_id[$_data['transfer_hopper_id']] : '';
                $_data['transfer_type'] = $transfer_type[$_data['transfer_type']] ?
                    $transfer_type[$_data['transfer_type']] : '';
            }
        }
        return $_array ? $_array : array();
    }

    /**
     * @param array $_array
     * @param bool $flag
     *
     * @return mixed
     * 首页数据查询与显示
     */
    public static function format_StockIn_details($_array = array(), $flag = true)
    {
        if ($flag) {
            $stock_type = BaseInfoService::stock_in_type();
            $export_tax_rebate = BaseInfoService::export_tax_rebate();
            $enterprise_dominant = BaseInfoService::enterprise_dominant();
            $transfer_hopper_id = BaseInfoService::transfer_hopper_id();
            $transfer_type = BaseInfoService::transfer_type();
            $site_id= PublicInfoService::get_site_array();
            foreach ($_array AS &$_data) {
                $_data['supplier_id'] = BaseInfoService::supplier_name($_data['supplier_id']);
                $_data['warehouse_man'] = BaseInfoService::get_user_name($_data['warehouse_man']);
                $_data['site_id'] = $site_id[$_data['site_id']];
                $_data['store'] = $_data['store']=='11' ? '亚马逊中转仓' : '';
                $_data['type'] = $stock_type[$_data['type']] ? $stock_type[$_data['type']] : '';
                $_data['export_tax_rebate'] = $export_tax_rebate[$_data['export_tax_rebate']] ?
                    $export_tax_rebate[$_data['export_tax_rebate']] : '';
                $_data['enterprise_dominant'] = $enterprise_dominant[$_data['enterprise_dominant']] ?
                    $enterprise_dominant[$_data['enterprise_dominant']] : '';
                $_data['transfer_hopper_id'] = $transfer_hopper_id[$_data['transfer_hopper_id']] ?
                    $transfer_hopper_id[$_data['transfer_hopper_id']] : '';
                $_data['transfer_type'] = $transfer_type[$_data['transfer_type']] ?
                    $transfer_type[$_data['transfer_type']] : '';
            }
        }
        return $_array ? $_array : array();

    }

    public function get_storage_inventories(&$storage_postion = array(), &$skus = array())
    {
        $model = M('warehouseorders', 'wms_', $this->_db);
        $where_map = array();
        if ($storage_postion and $skus) {
            $where_map = array(
                'storage_position' => array('IN', $storage_postion),
                'sku' => array('IN', $skus),
            );
        } elseif ($storage_postion) {
            $where_map = array(
                'storage_position' => array('IN', $storage_postion),
            );
        } else {
            $where_map = array(
                'sku' => array('IN', $skus),
            );
        }
    }

    public function get_cost_zero_value($month) {
        $result = $this->stock_in->get_cost_zero_value($month);

        $format_data = array();
        if (!empty($result)) {
            $format_data = $this->format_cost_zero_value($result);
        }
        return $format_data;
    }

    public function format_cost_zero_value($data) {
        $enterprise_dominant = BaseInfoService::enterprise_dominant();
        $stock_in_type = BaseInfoService::stock_in_type();

        foreach ($data as &$v) {
            $v['type'] = $stock_in_type[$v['type']];
            $v['enterprise_dominant'] = $enterprise_dominant[$v['enterprise_dominant']];
            $v['cost'] = 0;
        }

        return $data;
    }

    public function check_upload_data($sheetData){
        $flag = true;
        $str  = '';
        $input_str = array();
        foreach ($sheetData as  $key => $row) {
            if ($key == 1) continue;
            $empty_arr = array_values($row);
            if (empty($empty_arr)) continue;
            if($row['A']==''){
                $flag = false;
                $str .= '第'. $key .'行 入库明细ID 为空,导入失败\n';
            } else {
                preg_match('/(\D+)/',$row['A'],$input_str);
                if(!empty($input_str)) {
                    $str .= '第'. $key .'行 入库明细ID 输入不合法,导入失败\n';
                }
            }

            if($row['G']==''){
                $flag = false;
                $str .= '第'.$key.'行 成本 为空,导入失败\n';
            } else {
                preg_match('/(\D+)/',$row['G'],$input_str);
                if(!empty($input_str) || $row['G'] < 0) {
                    $str .= '第'. $key .'行 入库明细ID 输入不合法,导入失败\n';
                }
            }
        }

        return array(
            'status' => $flag,
            'message'=> $str
        );
    }

    public function update_cost($sheetData) {
        $message = $this->stock_in->save_cost($sheetData);
        return $message;
    }

    public function checkWarehouseOrder($condition) {
        $result = $this->stock_in->checkWarehouseOrder($condition);

        if(empty($result)) {
            $message['status'] = TRUE;
            $message['msg'] = '';
        } else {
            $message['status'] = false;
            $message['msg'] = '该主体SKU含有无采购单入库，请到采购单录入补全处录入采购单号';
        }

        return $message;
    }
}