<?php
/**
 * Created by PhpStorm.
 * User: gongxiaohua
 * Date: 17年2月22日
 * Time: 下午8:58
 */

namespace Warehouse\Service;

class PublicdataService
{
    protected static $_db = 'fbawarehouse';

    /**
     * 获取采购单明细状态数组
     */
    public static function get_purchaseorders_details_status($status)
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'table'  => 'wms_purchaseorder_details',
                    'number' => $status
                )
            )
            ->getField('value');
        return $arr == null ? '' : $arr;
    }




    /**
     * @param $id
     *
     * @return array|mixed
     * 获取仓库名
     */
    public static function get_transfer_hopper_name($id)
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'colum_name' => 'transfer_hopper_id',
                    'number'     => $id
                )
            )
            ->getField('value');
        return $arr == null ? '' : $arr;
    }

    /**
     * number=>colum_name
     *
     * @return array|mixed
     * 获取仓库名
     */
    public static function get_warehouse_names()
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'colum_name' => 'transfer_hopper_id',
                )
            )
            ->getField('number, value');
        return $arr == null ? '' : $arr;
    }

    /**
     * 出口退税
     *
     * @return string
     */
    public static function get_export_tax_rebate()
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'colum_name' => 'export_tax_rebate',
                )
            )
            ->getField('number,value');
        return $arr == null ? '' : $arr;
    }

    /**
     * @param $status
     *
     * @return array|mixed
     * 获取供应商名字
     */
    public static function get_supplier_name($id)
    {
        $arr = M('skusystem_suppliers', ' ', self::$_db)
            ->where(array('id' => $id))
            ->getField('name');
        return $arr == null ? '' : $arr;
    }

    /**
     * id=>name
     *
     * @return array|mixed
     */
    public static function get_suppliers()
    {
        $arr = M('skusystem_suppliers', ' ', self::$_db)
            ->getField('id, name');
        return $arr == null ? '' : $arr;
    }

    /**
     * @param $status
     *
     * @return array|mixed
     * 获取sku确认状态
     */
    public static function ckeck_sku_sign($sku)
    {
        $arr = M('skusystem_sku_qc_standard', ' ', self::$_db)
            ->where(array('sku' => $sku))
            ->getField('id');
        return $arr == null ? '' : $arr;
    }

    /**
     * 获取sku的规格
     */

    public static function get_sku_standard($sku)
    {
        $relervant_id = self::ckeck_sku_sign($sku);
        $arr          = M('skusystem_qc_standard', ' ', self::$_db)
            ->where(array('id' => $relervant_id))
            ->getField('standard');
        return $arr == null ? '' : $arr;
    }


    /**
     * @param $status
     *
     * @return array|mixed
     * 获取供应商id
     */
    public static function get_supplier_id($name)
    {
        $arr = M('skusystem_suppliers', ' ', self::$_db)
            ->where(array('name' => array('like', '%'.$name.'%')))
            ->getField('id');
        return $arr == null ? '' : $arr;
    }

    /**
     * @param $status
     *
     * @return array|mixed
     * 获取供应商ids
     */
    public static function get_supplier_ids($name)
    {
        $arr = M('skusystem_suppliers', ' ', self::$_db)
            ->where(array('name' => array('like', '%'.$name.'%')))
            ->field('id, name')
            ->select();
        return $arr == null ? '' : $arr;
    }
    /**
     * 根据用户名获取用户id
     */
    public static function get_user_id($name)
    {
        $admin_model = M('ea_admin', ' ', 'amazonadmin');
        return $admin_model->where(array('remark' => $name))
            ->getField('id');
    }

    /**
     * 根据用户id获取用户名
     */
    public static function get_user_name($id)
    {
        $admin_model = M('ea_admin', ' ', 'amazonadmin');
        return $admin_model->where(array('id' => $id))
            ->getField('remark');
    }

    /**
     * 用户id=>用户名
     */
    public static function get_user_names()
    {
        $admin_model = M('ea_admin', ' ', 'amazonadmin');
        return $admin_model->getField('id,remark');
    }

    /**
     * @param      $input
     * @param      $columnKey
     * @param null $indexKey
     *
     * @return array
     * 取出数组中指定的key
     */
    public static function i_array_column($input, $columnKey, $indexKey = null)
    {
        if (!function_exists('array_column')) {
            $columnKeyIsNumber = (is_numeric($columnKey)) ? true : false;
            $indexKeyIsNull    = (is_null($indexKey)) ? true : false;
            $indexKeyIsNumber  = (is_numeric($indexKey)) ? true : false;
            $result            = array();
            foreach ((array)$input as $key => $row) {
                if ($columnKeyIsNumber) {
                    $tmp = array_slice($row, $columnKey, 1);
                    $tmp = (is_array($tmp) && !empty($tmp)) ? current($tmp) : null;
                } else {
                    $tmp = isset($row[$columnKey]) ? $row[$columnKey] : null;
                }
                if (!$indexKeyIsNull) {
                    if ($indexKeyIsNumber) {
                        $key = array_slice($row, $indexKey, 1);
                        $key = (is_array($key) && !empty($key)) ? current($key) : null;
                        $key = is_null($key) ? 0 : $key;
                    } else {
                        $key = isset($row[$indexKey]) ? $row[$indexKey] : 0;
                    }
                }
                $result[$key] = $tmp;
            }
            return $result;
        } else {
            return array_column($input, $columnKey, $indexKey);
        }
    }

    /**
     * 获取主体
     * number=>主体名称
     *
     * @return string
     */
    public static function get_enterprise_dominants()
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'colum_name' => 'enterprise_dominant',
                )
            )
            ->getField('number,value');
        return $arr == null ? '' : $arr;
    }

    /**
     * @return mixed
     * 不良品入库单状态
     * khq 2017.3.2
     */
    public static function get_bad_products_status()
    {
        $bad_products_status = S('bad_products_status');
        if (!$bad_products_status or !is_array($bad_products_status)) {
            $status = M('statusdics', ' ', self::$_db)
                ->where(
                    array(
                        'table'      => 'wms_unqualified_deal_invoices',
                        'colum_name' => 'status',
                    )
                )
                ->getField('number,value');
            S('bad_products_status', $status, 86400);
        }
        return $bad_products_status;
    }

    /**
     * @return mixed
     * 获取不良品处理方式
     * khq 2017.3.5
     */
    public static function unqualified_deal_way()
    {
        $unqualified_deal_way = S('unqualified_deal_way');
        if (!$unqualified_deal_way or !is_array($unqualified_deal_way)) {
            $way = M('statusdics', ' ', self::$_db)
                ->where(
                    array(
                        'table'      => 'wms_unqualified_deal_invoices',
                        'colum_name' => 'fail_deal_way',
                    )
                )
                ->getField('number,value');
            S('unqualified_deal_way', $way, 86400);
        }
        return $unqualified_deal_way;
    }

    /**
     * 获取出库明细状态数组
     */
    public static function get_deliveryorders_type()
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'table'      => 'wms_deliveryorders',
                    'colum_name' => 'type',
                )
            )
            ->getField('number,value');
        return $arr == null ? '' : $arr;
    }

    /**
     * 通过出库类型获取出库状态
     */
    public static function get_deliveryorders_type_by_value($value)
    {
        $arr = M('statusdics', ' ', self::$_db)
            ->where(
                array(
                    'table'      => 'wms_deliveryorders',
                    'colum_name' => 'type',
                    'value'      => $value,
                )
            )
            ->getField('number');
        return $arr == null ? '' : $arr;
    }
    /**
     * 描述: 根据SKU获取质检标准
     * 作者: kelvin
     */
    public static function getStandardFromSku($sku) {
        if ($sku) {
            $data = M('skusystem_sku_qc_standard', ' ', self::$_db)
                ->alias('a')
                ->join('left join skusystem_qc_standard b on b.id = a.qc_standard_id')
                ->where("a.sku = '$sku'")
                ->getField('b.standard');
            return $data?$data:NULL;
        }

    }
}
