<?php
class Task_synctask extends Minion_Task
{
    /**
     * 定义时间格式
     * rpc同步数据，每分钟执行计划任务
     */

    public function _execute(array $params)
    {
        $url = "triggertablesync/rpcdata";
        Request::factory($url)->execute();
    }
	
}
