<?php
require_once(Kohana::find_file('vendor','phprpc/phprpc_server'));

/**
 * Created by PhpStorm.
 * User: cai
 * Date: 16-2-8
 * Time: 上午11:26
 */
class Controller_Triggerserver extends Controller
{
    /**
     * 暂时没有使用
     */
    public function Action_syn()
    {
        $server = new PHPRPC_Server();

        //新增、修改、删除数据
        function rpcdata($data)
        {
            if(isset($_GET['environment'])&&$_GET['environment']){
                $configenvironment = Kohana::$config->load('triggerenvironment');
                $environment_str   = $configenvironment->get('environment');
                if($environment_str==$_GET['environment']){
                    //环境正确
                }else{
                    return 'environment ['.$_GET['environment'].'->'.$environment_str.'] is wrong!';
                }
            }else{
                return 'environment is empty!';
            }
            //临时log记录
            //Kohana::$log->add(Log::INFO,'Rpc get data follow');
            //Kohana::$log->add(Log::INFO,var_export($data,TRUE));

            $temp_data   = array();
            $row1        = reset($data);
            $tablename   = $row1['table'];
            $warehouseid = $row1['warehouseid'];

            if ($warehouseid != 102) {
                return $warehouseid .' is wrong!';
            }

            // unset($row1);
            $lock_file = APPPATH.'cache/rpcdata_'.$tablename.'.lock';
            $fp        = fopen($lock_file,'w');
            if(flock($fp,LOCK_EX)){ // 文件锁
                Kohana::$log->add(Log::INFO, var_export($data, true));
                foreach($data as $key => $value){
                    $tablename   = $value['table'];

                    if($value['act']=='add'){
                        $sql = "SELECT id FROM ".$tablename." WHERE id= ".$value['data']['id']." LIMIT 1";
                        $query_data = DB::query(Database::SELECT,$sql)->execute('newerp')->current();
                        if(is_array($query_data)&&count($query_data)){
                            $res = DB::update($tablename)->set($value['data'])->where('id','=',$value['data']['id'])->execute('newerp');
                        }else{
                            $res = DB::insert($tablename,array_keys($value['data']))->values($value['data'])->execute('newerp');
                        }
                    }
                    if($value['act']=='update'){
                        $sql = "SELECT id FROM ".$tablename." WHERE id= ".$value['data']['id']." LIMIT 1";
                        $query_data = DB::query(Database::SELECT,$sql)->execute('newerp')->current();
                        if(!empty($query_data)) {
                            $res = DB::update($tablename)->set($value['data'])->where('id','=',$value['data']['id'])->execute('newerp');
                        } else {
                            $res = DB::insert($tablename,array_keys($value['data']))->values($value['data'])->execute('newerp');
                        }
                    }
                    if($value['act']=='delete'){
                        $res = DB::delete($tablename)->where('id','=',$value['data']['id'])->execute('newerp');
                    }
                    if($res !== false) {
                        $temp_data[] = $key;
                    }
                }
                // usleep(100000);     // 0.1秒
                flock($fp,LOCK_UN);
            }
            Kohana::$log->add(Log::INFO, var_export($temp_data, true));
            return $temp_data;
        }

        $server->add(array(
            'rpcdata'
        ));
        $server->start();
    }

    /**
     * 保存SKU基础资料数据
     */
    public function Action_synpurchaseorderpays()
    {
        $server = new PHPRPC_Server();

        //新增、修改、删除数据
        function rpcdata($data)
        {
            if(isset($_GET['environment'])&&$_GET['environment']){
                $configenvironment = Kohana::$config->load('triggerenvironment');
                $environment_str   = $configenvironment->get('environment');
                if($environment_str==$_GET['environment']){
                    //环境正确
                }else{
                    return 'environment ['.$_GET['environment'].'->'.$environment_str.'] is wrong!';
                }
            }else{
                return 'environment is empty!';
            }

            //临时log记录
            //Kohana::$log->add(Log::INFO,'Rpc get data follow');
            //Kohana::$log->add(Log::INFO,var_export($data,TRUE));

            $temp_data = array();

            $row1        = reset($data);
            $tablename   = $row1['table'];
            $warehouseid = $row1['warehouseid'];
            unset($row1);
            $lock_file = APPPATH.'cache/rpcdata_'.$tablename.'.lock';
            $fp        = fopen($lock_file,'w');
            if(flock($fp,LOCK_EX)){ // 文件锁
                foreach($data as $key => $value){
                    $tablename   = $value['table'];
                    $temp_data[] = $key;

                    if($value['act'] == 'add'){
                        /*目前中转预报转运系统仅接收采购单和采购明细的插入*/
                        if(in_array($tablename, array('wms_purchaseorders', 'wms_purchaseorder_details'))) {
                            $sql = "SELECT id
                                FROM ".$tablename."
                                WHERE id = ".$value['data']['id']."
                                LIMIT 1";
                            $query_data = DB::query(Database::SELECT,$sql)
                                ->execute('newerp')
                                ->current();
                            if(is_array($query_data)&&count($query_data)){
                                DB::update($tablename)
                                    ->set($value['data'])
                                    ->where('id','=',$value['data']['id'])
                                    ->execute('newerp');
                            }else{
                                DB::insert($tablename,array_keys($value['data']))
                                    ->values($value['data'])
                                    ->execute('newerp');
                            }
                        }
                    }
                    if($value['act'] == 'update'){
                        DB::update($tablename)
                            ->set($value['data'])
                            ->where('id','=', $value['data']['id'])
                            ->execute('newerp');

                    }
                    if($value['act'] == 'delete'){
                        DB::delete($tablename)
                            ->where('id', '=', $value['data']['id'])
                            ->execute('newerp');
                    }
                }
                // usleep(100000);     // 0.1秒
                flock($fp,LOCK_UN);
            }

            return $temp_data;
        }

        $server->add(array(
            'rpcdata'
        ));
        $server->start();
    }
}


