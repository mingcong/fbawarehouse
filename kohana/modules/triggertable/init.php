<?php
defined( 'SYSPATH' ) or die( 'No direct access allowed.' );
if(!Route::cache())
{


    Route::set('triggertable','triggertable(/<action>(/<id>))')
        ->defaults(array(
            'controller' => 'triggertable',
            'action' => 'rpcdata',
        ));


    Route::set('triggerserver','triggerserver(/<action>(/<id>))')
        ->defaults(array(
            'controller' => 'triggerserver',
            'action' => 'syn',
        ));

    Route::set('handclient','handclient(/<action>(/<id>))')
        ->defaults(array(
            'controller' => 'handclient',
            'action' => 'get_newerp_pu_info',
        ));

}



