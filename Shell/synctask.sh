#!/bin/bash
#

PNUM=`ps -ef|grep 'synctask.sh'|wc -l`

if [ $PNUM -gt 3 ];then
echo $PNUM-3 tasks running
exit
fi

php /var/www/kohana/index.php --uri=triggertablesync/rpcdata >> /var/www/logs/cron/`date +%F`.txt &

chpid="$!";
cur_dir=$(pwd)
cd $cur_dir
echo "$chpid" > ./sync_fbawarehouse_php.sid

while [ 1 ]
do
wait $chpid
sleep 0.1

php /var/www/kohana/index.php --uri=triggertablesync/rpcdata >> /var/www/logs/cron/`date +%F`.txt &

chpid="$!";
echo "$chpid" > ./sync_fbawarehouse_php.sid
done
